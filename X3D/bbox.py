from numpy import array

#======================================================================

class bbox:

    def __init__(self, *bboxes):
        self.maxX = 0
        self.minX = 0
        self.maxY = 0
        self.minY = 0
        self.wrap(*bboxes)
        
    def wrap(self, *bboxes):
        for abbox in bboxes:
            self.maxX = max(self.maxX, abbox.maxX)
            self.maxY = max(self.maxY, abbox.maxY)
            self.minX = min(self.minX, abbox.minX)
            self.minY = min(self.minY, abbox.minY)
            
    def max(self,abbox):
        return bbox(self,abbox)

    def width(self):
        return self.maxX-self.minX

    def height(self):
        return self.maxY-self.minY

    def a(self):
        return array([self.maxX,self.minY, 0.0])

    def b(self):
        return array([self.minX,self.maxY, 0.0])

    def upper(self):
        return array([self.maxX,self.maxY, 0.0])

    def lower(self):
        return array([self.minX,self.minY, 0.0])
    
    def lowerLeftTranslation(self):
        return -self.upper() + array([-1,-1,0])

    def lowerRightTranslation(self):
        return -self.b() + array([1,-1,0])

    def upperLeftTranslation(self):
        return -self.a() + array([-1,1,0])

    def upperRightTranslation(self):
        return -self.lower() + array([1,1,0])

    def diagonal(self):
        return self.upper()-self.lower()

    def center(self):
        return self.lower() + (self.diagonal()/2)

    def centerStr(self):
        return str(self.center())[1:-1]

    def close(self, a, b):
        return abs(a-b) < .001

    def snapToZero(self):

        if self.close(self.minX,0): self.minX = 0
        if self.close(self.maxX,0): self.maxX = 0
        if self.close(self.minY,0): self.minY = 0
        if self.close(self.maxY,0): self.maxY = 0

    def __str__(self):
        return "bbox[(%s,%s),(%s,%s)]" % (self.minX,self.minY,
                                          self.maxX,self.maxY)

