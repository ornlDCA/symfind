from NodeBased import *

#======================================================================

class SpaceGroups(NodeBased):

    def __init__(self, node):
        NodeBased.__init__(self, node)
        self.SpaceGroup = [] # Make it a list to start so we are sure it always has a value
        self.getAttributes()
        self.getElements()

    def spaceGroup(self):

        index = self.selected - 1
        sgs = getattr(self,"SpaceGroup",None)
        if sgs == None:
            print "SpaceGroups selected=%s has no SpaceGroup list" % self.selected
            return None
        if index >= len(sgs):
            print "SpaceGroups selected=%s has no cspding entry in the SpaceGroup list" % self.selected
            return None
        return self.SpaceGroup[index]



