from numpy import array, zeros, maximum, argmax, argmin
from bbox import *
from pprint import pprint 
import X3D

#======================================================================

class Processor:

    colors = ["Peach Puff",
              "Lavender",
              "Steel Blue",
              "Goldenrod",
              ]
    
    cubeOffset     = 1.5
    cubeSideLength = 1.0
    cubeSpacing    = cubeSideLength * 1.5

    def __init__(self, description):
        #pprint(description)
        self.__dict__.update(description)

    def phaseOffset(self):
        numberOfU = self.dimensions()[0]
        return float(numberOfU) * self.cubeSpacing + self.cubeOffset
    
    def Id(self):
        return self.World["id"]

    def coordinates(self):
        return array(self.Cube["coordinates"])

    def dimensions(self):
        return array(self.Cube["dimensions"])

    def U(self):
        return self.coordinates()[0]
    
    def V(self):
        return self.coordinates()[1]
    
    def Phase(self):
        return self.coordinates()[2]
    
    def configSlice(self):
        return self.coordinates()[3]

    def __str__(self):
        return "Processor[%s][%s][u=%s,v=%s,c=%s][p=%s]@%s" % (self.Id(),
                                                               self.coordinates(),
                                                               self.U(),
                                                               self.V(),
                                                               self.configSlice(),
                                                               self.Phase(),
                                                               self.boxCenterPos())
    
    def boxCenterPos(self):

        result = array([0.0,0.0,0.0])
        result[0] = float(self.U()) * self.cubeSpacing + self.cubeOffset + float(self.Phase()) * self.phaseOffset()
        result[1] = float(self.V()) * self.cubeSpacing + self.cubeOffset
        result[2] = float(self.configSlice()) * self.cubeSpacing + self.cubeOffset

        return result

    def toX3D(self, **keys):

        labelDirection = self.boxCenterPos()
        labelDirection[0] -= self.cubeSideLength + .04

        #print self

        return X3D.box(self.boxCenterPos(),
                       [self.cubeSideLength,self.cubeSideLength,self.cubeSideLength],
                       X3D.label(str(self.coordinates()),
                                 transform  = {"translation" : "%s %s %s" % tuple(labelDirection) },
                                 style      = "NORMAL",
                                 size       = .4,
                                 horizontal = "True"),
                       fill  =self.colors[self.V()])


        


