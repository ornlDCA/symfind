from NodeBased import *
from numpy import array, dot

#======================================================================

class SymmetryOperation(NodeBased):

    def __init__(self, node):
        NodeBased.__init__(self, node)
        self.getAttributes()
        self.matrix = getArray(node,(3,3))
        
    def transform(self, cartesianPosition, **keys):
        return dot(self.matrix,cartesianPosition)

    def toX3D(self, theBox, **keys):
        return self.SymmetryElement.toX3D(self, theBox,**keys)

    def toSVG(self, theBox, **keys):
        return self.SymmetryElement.toSVG(self, theBox,**keys)

