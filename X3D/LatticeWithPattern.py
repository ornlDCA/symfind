from NodeBased import *
from bbox      import *

import X3D
import SVG

#======================================================================

class LatticeWithPattern(NodeBased):
    
    def __init__(self, node):
        
        NodeBased.__init__(self,node)
        self.getAttributes()
        self.getElements()
        self.Pattern.lattice = self
        self.Pattern._symmetry  = self.Symmetry
        
    def box(self):
        return bbox(self.Lattice.box(),self.Pattern.box())

    def selectedSpaceGroup(self):
        
        for sg in self.SpaceGroups.SpaceGroup:
            if getattr(sg,"selected",None) == "true":
                return sg
        return None
    
    def toX3D(self, spaceGroup="selected", **keys):

        result = X3D.transform(self.Lattice.toX3D(),
                               self.Pattern.toX3D(),
                               self.Symmetry.toX3D(self.box()),
                               **keys)
        return result

    def reviewSpaceGroupsX3D(self, **keys):

        result = X3D.trans
        for sg in self.SpaceGroups.SpaceGroup:
            sg.spaceGroupReviewX3D()
        result = X3D.transform(self.Lattice.toX3D(),
                               self.Pattern.toX3D(),
                               self.Symmetry.toX3D(),
                               **keys)

        if keys.get("spaceGroup",None) == "selected":

            selected = self.selectedSpaceGroup()
        
            if selected != None:
                result.append(selected.toX3D(self.box()))
            
        return result

    

    def toSVG(self, **keys):
        
        return SVG.group(self.Lattice.toSVG(),
                         self.Pattern.toSVG(),
                         self.Symmetry.toSVG(self.box()),
                         **keys)

        


