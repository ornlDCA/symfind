
from NodeBased    import *

import X3D
import SVG

#======================================================================

class Symmetry(NodeBased):

    colors = ["Red","Blue","Green","Purple","Yellow",
              "Red","Blue","Green","Purple","Yellow",
              "Red","Blue","Green","Purple","Yellow",
              "Red","Blue","Green","Purple","Yellow"]

    def __init__(self, node):
        
        NodeBased.__init__(self,node)
        self.getAttributes()
        self.getElements()

        self._operations = self.operations()
        self._orbits     = self.orbits()

    def spaceGroup(self):
        return self.SpaceGroups.spaceGroup()

    def operations(self):
        return getattr(self.SymmetryGroup.AppliedSymmetryElements,
                       "AppliedSymmetryElement",
                       [])

    def orbits(self):
        result = getattr(self.SymmetryGroup.Orbits,
                         "orbit",
                         [])
        if type(result) == list:
            return result
        
        return [self.SymmetryGroup.Orbits.orbit]
    
    def orbitFor(self, posIndex):

        for orbit in self._orbits:
            positions = eval(orbit.positions)
            if posIndex in positions:
                return orbit
        print ("Exception ------------------------------------------------- in orbitFor(%s)", (posIndex))
        raise Exception(posIndex,self._orbits)

    def orbitColorFor(self, posIndex):
        try:
            colorIndex = self.orbitFor(posIndex).orbitNumber
        #print posIndex
        #print colorIndex
            return self.colors[colorIndex]
        except:
            return "Black"
    
#============================================================ 

    def toSVG(self, theBox,**keys):
        pass


    def toX3D(self,theBox,  **keys):

        result = X3D.transform(**keys)

        for op in self._operations:
            result.append(op.toX3D(theBox,**keys))

        sg = self.spaceGroup()

        if sg != None:
            result.append(self.spaceGroup().toX3D(theBox))
            
        return result


