from numpy import array, zeros, maximum, argmax, argmin
from bbox import *

import X3D
from Processor import Processor
from pprint import pprint

#======================================================================

class ConfigTeam:

    margin = .1

    def __init__(self, t):
        self.coordinates = t
        self.members = []

    def append(self,item):
        self.members.append(item)

    def __str__(self):
        result = "%s%s%s:\n" % (self.coordinates,self.middle(),self.boxSize())
        for m in self.members:
            result += str(m) + "\n"
        return result

    def firstMember(self):
        for m in self.members:
            if m.coordinates()[3] == 0:
                return m
        return None

    def lastMember(self):
        last = self.members[0]
        for m in self.members:
            if m.coordinates()[3] > last.coordinates()[3]:
                last = m
        return last

    def middle(self):
        l = self.lastMember()
        f = self.firstMember()
        result = l.boxCenterPos()
        result[2] = (l.boxCenterPos()[2] - f.boxCenterPos()[2])/2 + f.boxCenterPos()[2]
        return result

    def boxSize(self):

        return [Processor.cubeSideLength + self.margin,
                Processor.cubeSideLength + self.margin,
                Processor.cubeSpacing*len(self.members)]

    def toX3D(self):

        print self.middle()
        print self.boxSize()
        return X3D.transparentBox(self.middle(),
                                  self.boxSize())


class ProcessorHyperCube:
    
    cubeOffset     = 1.5
    cubeSideLength = 1
    cubeSpacing    = cubeSideLength * 1.5

    
    def __init__(self, descriptions):
        
        self.descriptions = descriptions
        self.processors = [Processor(description) for description in descriptions]
        self.findConfigTeams()

    def dimensions(self):
        return self.processors[0].Cube["dimensions"]

    def findConfigTeams(self):
        
        self.teams = {}
        for p in self.processors:
            t = tuple(p.Configuration["sliceCoordinates_"])
            theTeam = self.teams.get(t,None)
            if theTeam == None:
                theTeam = ConfigTeam(t)
                self.teams[t] = theTeam
            theTeam.append(p)

    def toX3D(self, **keys):

        result = X3D.transform(X3D.axes(length=10.0))

        for processor in self.processors:
            result.append(processor.toX3D())

        for k in self.teams.keys():
            team = self.teams[k]
            print "appending:"
            result.append(team.toX3D())
            
        return result
            

        


