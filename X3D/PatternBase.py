from NodeBased import *
from Occupant  import *
from numpy import array, zeros, maximum, argmax, argmin
from bbox import *

import X3D
import SVG


#======================================================================

class Pattern(NodeBased):

    def __init__(self, pattern):
        
        NodeBased.__init__(self,pattern)
        self.getAttributes()

        siteNodes = pattern.find('Sites')
        occNodes  = pattern.find('Occupants')

        self.numSites           = len(siteNodes)
        self.occupants          = {}
        for occ in occNodes:
            o = Occupant(occ)
            self.occupants[o.name] = o
        self.occupant           = {}
        self.cellPositions      = zeros((self.numSites,2), dtype=float)
        self.cartesianPositions = zeros((self.numSites,2), dtype=float)
        for node in siteNodes:
            self.processSite(node)

        
    def processSite(self, site):
        
        s            = attribute(site, "index")
        occupantName = attribute(site, "occupantName")

        cellPosition = getArray(site.find("CellPosition"))
        cartPosition = getArray(site.find('CartesianPosition'))
        for j in range(0,2):
            self.cellPositions[s,j] = cellPosition[j]
            self.cartesianPositions[s,j] = cartPosition[j]
            self.occupant[s] = self.occupants[occupantName]

    def __str__(self):
        return self.repr_int()
    
    def repr_int(self, offset=""):

        result =  "%sPattern[%s sites]: \n" % (offset,self.numSites)
        result += "%s--------------------------------------------------\n" % offset
        result += "%s  cell[0]   cell[1]   cart[0]   cart[1]  occupant \n" % offset
        result += "%s--------------------------------------------------\n" % offset
        for s in range(0,self.numSites):
            result += "%s %9f %9f %9f %9f %s \n" % (offset,
                                                    self.cellPositions[s,0],
                                                    self.cellPositions[s,1],
                                                    self.cartesianPositions[s,0],
                                                    self.cartesianPositions[s,1],
                                                    self.occupant[s])
        result += "%s-------------------------------------------------\n" % offset
        result += "%s %s \n" % (offset, self.bbox())
        result += "%s-------------------------------------------------\n" % offset
        return result


    def box(self):
        
        result = bbox()
        amax = argmax(self.cartesianPositions,0)
        amin = argmin(self.cartesianPositions,0)
        result.maxX = self.cartesianPositions[amax[0],0]
        result.maxY = self.cartesianPositions[amax[1],1]
        result.minX = self.cartesianPositions[amin[0],0]
        result.minY = self.cartesianPositions[amin[1],1]
        return result

    def circleRadius(self):
        
        box   = self.box()
        r     = box.width() / (self.numSites * 2)
        maxr  = box.width() / 5
        minr  = box.width() / 20
        
        if r > maxr:
            return maxr

        if r < minr:
            return minr
        
        return r

    def toX3D(self, **keys):
        
        result = X3D.transform(**keys)

        for s in range(0, self.numSites):
            
            result.append(X3D.sphere(X3D.extend(self.cartesianPositions[s,:]),
                                     radius=str(self.circleRadius()),
                                     fill=getattr(self.occupant[s],"color","yellow")))
        return result
        
    def toSVG(self, **keys):
        
        result = SVG.group()

        for s in range(0, self.numSites):
            result.append(SVG.circle(self.cartesianPositions[s,:],
                                     radius=str(self.circleRadius()),
                                     fill=getattr(self.occupant[s],"color","yellow")))

        return result
        


