from NodeBased import *
from LatticeWithPattern import *
from Crystal            import *

import X3D
import SVG

#======================================================================

class SuperCrystal(LatticeWithPattern):

    def __init__(self, node):

        LatticeWithPattern.__init__(self,node)
#        self.getAttributes()
#        self.getElements()
        

    def toX3D(self, **keys):

        crystal                = self.GivenCrystal.InputCrystal
        reciprocalCrystal      = self.ReciprocalCrystal
        reciprocalSuperCrystal = self.ReciprocalSuperCrystal

        result = X3D.transform(X3D.axes(length=10.0),
                               crystal               .toX3D( translation=crystal               .box().upperLeftTranslation() ),
                               LatticeWithPattern    .toX3D(self, translation=self                  .box().lowerLeftTranslation() ),
                               reciprocalSuperCrystal.toX3D( translation=reciprocalSuperCrystal.box().lowerRightTranslation()),
                               reciprocalCrystal     .toX3D( translation=reciprocalCrystal     .box().upperRightTranslation()),
                               **keys)
        return result


    def toSVG(self, **keys):

        crystal                = self.GivenCrystal.InputCrystal
        reciprocalCrystal      = self.ReciprocalCrystal
        superCrystal           = self
        reciprocalSuperCrystal = self.ReciprocalSuperCrystal

        result = SVG.group(#SVG.axes(length=10.0),
            crystal         .toSVG( transform="translate(%s, %s)" % (.1,.1+superCrystal.box().height())),
            LatticeWithPattern          .toSVG(self, transform="translate(%s, %s)" % (.1, .1)),
            reciprocalSuperCrystal.toSVG( transform="translate(%s, %s)" % (.1+superCrystal.box().width(), .1)),
            reciprocalCrystal     .toSVG( transform="translate(%s, %s)" % (.1+superCrystal.box().width(),.1+superCrystal.box().height())),
            **keys)

        return result

    
    def svgBox(self):

        crystal                = self.GivenCrystal.InputCrystal
        reciprocalCrystal      = self.ReciprocalCrystal
        reciprocalSuperCrystal = self.ReciprocalSuperCrystal

        b = bbox()
        b.minX = 0.0
        b.maxX = self.box().width() +  reciprocalCrystal.box().width() + .3
        b.minY = 0.0
        b.maxY = self.box().height() +  reciprocalCrystal.box().height() + .3

        return b
                    
