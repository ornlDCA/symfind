import os , platform
from numpy import array, zeros, maximum, argmax, argmin
import xml.etree.cElementTree as etree
#import lxml.etree as etree
from pprint import pprint
from bbox      import *

from NodeBased          import *
from CellParameters     import *
from Lattice            import *
from Pattern            import *
from LatticeWithPattern import *
from Occupant           import *
from SpaceGroups        import *
from SpaceGroup         import *
from Crystal            import *
from SuperCrystal       import *
from SymmetryOperation  import *
from SymmetryElement        import *
from AppliedSymmetryElement import *
from Symmetry               import *

import X3D
import SVG


NodeBased.theVars = vars()

#from Ft.Xml import Parse #, Element, Attribute

def generateX3D_SVG(inputDir):

    outputDir = inputDir

    #========================================================

    print "Started reading " + inputDir + "/SuperCrystal.xml"
    f = open(inputDir + "/SuperCrystal.xml","r")
    doc = etree.XML(f.read())
    f.close()
    sc = SuperCrystal(doc)

    print "Finished reading " + inputDir + "/SuperCrystal.xml"

    #========================================================
    
    print "Started reading " + inputDir + "/GivenCrystal.xml"

    f = open(inputDir + "/GivenCrystal.xml","r")
    doc = etree.XML(f.read())
    f.close()
    sc.GivenCrystal = Crystal(doc)

    print "Finished reading " + inputDir + "/GivenCrystal.xml"
    
    #========================================================
    
    print "Started reading " + inputDir + "/ReciprocalCrystal.xml"

    f = open(inputDir + "/ReciprocalCrystal.xml","r")
    doc = etree.XML(f.read())
    f.close()
    sc.ReciprocalCrystal = CrystalBase(doc)

    print "Finished reading " + inputDir + "/ReciprocalCrystal.xml"

    #========================================================
    
    print "Started reading " + inputDir + "/ReciprocalSuperCrystal.xml"

    f = open(inputDir + "/ReciprocalSuperCrystal.xml","r")
    doc = etree.XML(f.read())
    f.close()
    sc.ReciprocalSuperCrystal = LatticeWithPattern(doc)

    print "Finished reading " + inputDir + "/ReciprocalSuperCrystal.xml"
    
    print "#============================================================"

    pprint(sc)

    print "#============================================================"

    pprint(sc.GivenCrystal.InputCrystal)

    #============================================================

    w = X3D.Writer()
    w.add(sc.toX3D())
    fileName = "%s/%s.x3d" % (outputDir,"SuperCrystal")
    w.writeFile(fileName)
    
    print "Wrote %s " % fileName
    
    w = SVG.Writer()
    w.add(sc.toSVG())
    fileName = "%s/%s.svg" % (outputDir,"SuperCrystal")
    w.writeFile(sc.svgBox(), fileName)
    
    print "Wrote %s " % fileName


    #pprint(map(Pattern, doc.xpath(u'//Pattern')))

if __name__ == "__main__":

    import sys

    if len(sys.argv) >= 2:
        inputDirName   = sys.argv[1]
    else:
        print "Usage: python generateX3D.py PsimagGeometryDirectoryName"
        print "  the args were: %s " (sys.argv)
        sys.exit(-1)

    generateX3D_SVG(inputDirName)


