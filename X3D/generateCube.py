import os , platform
from numpy import array, zeros, maximum, argmax, argmin

from pprint import pprint

import X3D
from ProcessorHyperCube import ProcessorHyperCube

def generateCube(inputFileName,outputFileName):

    f = open(inputFileName,"r")
    data = eval(f.read())
    
    maxId  = 0

    for item in data:
        if item[2]["id"] > maxId:
            maxId = item[2]["id"]

    #print "maxId = %s" % maxId

    descriptions = [{} for d in range(0,maxId+1)]

    for item in data:
        descriptions[item[0]][item[2]["name"]] = item[2] 

    # pprint (descriptions)
    
    #============================================================

    w = X3D.Writer()

    w.add(ProcessorHyperCube(descriptions).toX3D())
    
    w.writeFile(outputFileName)
    
    print "Wrote %s " % outputFileName
    

if __name__ == "__main__":

    import sys

    if len(sys.argv) >= 3:
        inputFileName   = sys.argv[1]
        outputFileName  = sys.argv[2]
    else:
        print "Usage: python generateCube.py input.jsn cubeFile.x3d"
        print "  the args were: %s " % (sys.argv)
        sys.exit(-1)

    generateCube(inputFileName, outputFileName)


