## @package 
#
# \author Michael Summers
#

from NodeBased    import *

import X3D
import SVG

#======================================================================
##
#
#
class AppliedSymmetryElement(NodeBased):

    def __init__(self, node):
        
        skipElements = ["CartesianDirection","CartesianPosition"]
        NodeBased.__init__(self, node)
        
        self.getAttributes()
        self.getElements(skip=skipElements)

        self.SymmetryElement.cartesianPosition  = getArray(node.find('CartesianPosition'))
        self.SymmetryElement.cartesianDirection = getArray(node.find('CartesianDirection'))


#============================================================ 

    def toSVG(self, theBox,**keys):
        pass


    def toX3D(self, theBox, **keys):

        return X3D.transform(self.SymmetryElement.toX3D(theBox),
                             **keys)



