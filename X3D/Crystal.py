from NodeBased import *
from CrystalBase import *

#======================================================================

class Crystal(NodeBased):

    TypeDict = {"InputCrystal":CrystalBase,
                "ReducedCrystal":CrystalBase,
                "ConventionalCrystal":CrystalBase, }

    def __init__(self, node):
        NodeBased.__init__(self,node)
        self.getAttributes()
        self.getElements()
        
