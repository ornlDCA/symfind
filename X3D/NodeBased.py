from numpy import array

#======================================================================

def convertValue(val):
    
    try:
        return int(val)
    except:
        pass
    
    try:
        return float(val)
    except:
        return val

#======================================================================

def attribute(node, name):
    
    return convertValue(node.attrib[name])
    
#======================================================================

def getText(node):
    
    if len(node) == 0:
        arrayText = node.text
    else:
        arrayText = node[-1].tail
        
    return arrayText.strip()

def offsetText(offset, strng):
    
    lines = strng.split("\n")
    return ("\n%s   " % offset).join(lines)


#======================================================================

def getArray(node, shape=None):
    
    #print "node = %s " % node
    if len(node) == 0:
        arrayText = node.text
    else:
        arrayText = node[-1].tail

    ar = array(map(float,arrayText.split()))
    
    if shape == None:
        return ar
    
    ar.shape = shape
    return ar

#======================================================================

class NodeBased:

    theVars = None

    def __init__(self, node):

        print "NodeBased.__init__(%s)" % node.tag

        self._node = node
        self._text = []
        self.getText()

    def attribute(self, name):
        return attribute(self._node, name)

    def getAttrValue(self, attr):
        #print "attr = %s" % attr
        return convertValue(attr)

    def getTypeDict(self):
        ourClass  = self.__class__
        return getattr(ourClass,"TypeDict",None)

    def getArray(self, shape=None):

        return getArray(self._node, shape)

    def getElemValue(self, elem):

        name = elem.tag

        # perhaps name is an entry in a dictionary we keep
        td = self.getTypeDict()
        if td != None:
            tdcls = td.get(name,None)
            if tdcls != None:
                return tdcls(elem)

        # Perhaps name is the name of a known class
        gcls = self.theVars.get(name,None)
        if gcls != None:
            return gcls(elem)

        return StandardNode(elem)

    def getAttributes(self):
        for key, attrNode in self._node.attrib.iteritems():
            name = key
            setattr(self,name,self.getAttrValue(attrNode))
            
    def getElements(self, **keys):

        skip = keys.get("skip",[])
        
        for node in self._node:
            if node.tag in skip: continue
            #if node.__class__ != Element: continue
            attr = getattr(self,node.tag,None)
            if attr == None: 
                setattr(self,node.tag,self.getElemValue(node))
                continue
            if type(attr) == list:
                attr.append(self.getElemValue(node))
                continue
            setattr(self,node.tag,[attr, self.getElemValue(node)])
        
    def element(self, name, cls=None):
        path = unicode(name)
        if cls == None:
            cls = vars(name)
            print "cls = %s " % cls
        return cls(node.xpath(unicode(name))[0])

    def getText(self):
        self._text = []
        s = self._node.text
        if s != None:
            s = s.strip()
            if len(s) > 0:
                self._text.append(s)
        for node in self._node:
            s = node.tail.strip("\n\t ")
            if len(s) > 0:
                self._text.append(node.tail)
            
    def isAttrPair(self, keyValue):
        key, value = keyValue
        if key[0] == '_':                          return False
        if getattr(value,"repr_int",None) != None: return False
        if getattr(value,"shape",   None) != None: return False
        if type(value) == list:                    return False
        return True

    def isElementPair(self, keyValue):
        key, value = keyValue
        if key[0] == '_':                          return False
        if getattr(value,"repr_int",None) == None: return False
        return True
        
    def isArrayPair(self, keyValue):
        key, value = keyValue
        if key[0] == '_':                          return False
        if getattr(value,"shape",None) == None:    return False
        return True
        
    def isListPair(self, keyValue):
        key, value = keyValue
        if key[0] == '_':                          return False
        if type(value) == list:                    return True
        return False
        
            
    def repr_int(self, offset=""):

        headings = False
        
        result = "\n%s%s: %s Object \n" % (offset, self._node.tag, self.__class__.__name__)
        
        if len(self._text) > 0 and False:
            if headings: result += "%s---------------------------------------------- text \n" % offset
            result += "%s%s" % (offset,offsetText(offset, str(self._text)))
            result += "\n"
        
        # print attributes
        attributes = [keyValue for keyValue in  self.__dict__.iteritems() if self.isAttrPair(keyValue)]
        #print "attributes %s" % attributes
        if len(attributes)>0:
            if headings: result += "%s---------------------------------------------- attributes \n" % offset
            for attr in attributes:
                key, value = attr
                result += "%s   %s: %s\n" % (offset, key,value)
                
        # print array
        arrays = [keyValue for keyValue in  self.__dict__.iteritems() if self.isArrayPair(keyValue)]
        #print "arrays %s" % arrays
        if len(arrays) > 0 :
            if headings: result += "%s---------------------------------------------- arrays \n" % offset
            for ar in arrays:
                key, value = ar
                arrayStr = offsetText(offset, str(value))
                result += "%s   %s: \n%s   %s\n" % (offset, key, offset, arrayStr)

        # print lists
        lists = [keyValue for keyValue in  self.__dict__.iteritems() if self.isListPair(keyValue)]
        #print "lists %s" % lists
        if len(lists) > 0:
            if headings: result += "%s---------------------------------------------- lists \n" % offset
            for l in lists:
                key, value = l
                arrayStr = offsetText(offset, str(value))
                result += "%s   %s: \n%s   %s\n" % (offset, key, offset, arrayStr)

        # print elements
        elements = [keyValue for keyValue in  self.__dict__.iteritems() if self.isElementPair(keyValue)]
        #print "elements %s" % elements
        if len(elements) > 0:
            if headings: result += "%s---------------------------------------------- elements \n" % offset
            for key, value in elements:
                result += "\n" + value.repr_int(offset + "  ")

        if headings: result += "\n%s%s: END %s Object \n" % (offset, self._node.tag, self.__class__.__name__)

        #====================================================================== 
        
        return result

    def __repr__(self):
        return self.repr_int()
     
from StandardNode import *
