
from NodeBased    import *

import X3D
import Quaternion
import SVG

from math import degrees, radians

#======================================================================

class SymmetryElement(NodeBased):

    twoFoldLength   = 1/2.0
    threeFoldLength = 3/4.0
    fourFoldLength  = 1.0
    sixFoldLength   = 1.5
    
    twoFoldCapColor   ="Blue"
    threeFoldCapColor ="Green"
    fourFoldCapColor  ="Purple"
    sixFoldCapColor   ="Red"

    mirrorColor       ="Wheat"
    glideColor        ="Green"
    
    def __init__(self, node):
        
        skipElements = ["NetDirection","CellOffset"]
        NodeBased.__init__(self, node)
        
        self.getAttributes()
        self.getElements(skip=skipElements)

        NetDirectionNode = node.find('NetDirection')
        self.netDirection = getArray(NetDirectionNode)
            
        CellOffsetNode = node.find('CellPosition')
        self.cellOffset = getArray(CellOffsetNode)

#============================================================ 

    def toSVG(self, theBox,**keys):
        pass


    def toX3D(self, theBox,**keys):
        #print "toX3D(%s)" % (self.type)
        return vars(self.__class__)[self.type](self, theBox, **keys)


#============================================================ X3d related functions

    def identity(self, theBox, **keys):
        pass

    def twoFold(self, theBox, **keys):

        return X3D.arrow(X3D.extend(self.cartesianPosition),
                         array([0,0,self.twoFoldLength]),
                         capColor=self.twoFoldCapColor,
                         text=self.name,
                         horizontal="True")

    def translation(self, theBox, **keys):

        return X3D.arrow(X3D.extend(self.cartesianPosition, -.5),
                         X3D.extend(self.cartesianDirection*self.glideFraction),
                         radius=".01",
                         text="translation",
                         horizontal="True",
                         capColor="Yellow")

    def twoFoldN(self, theBox, **keys):

        return X3D.arrow(X3D.extend(self.cartesianPosition),
                         array([0,0,-self.twoFoldLength]),
                         capColor=self.twoFoldCapColor,
                         text=self.name,
                         horizontal="True")

    def threeFold(self,  theBox, **keys):

        return X3D.arrow(X3D.extend(self.cartesianPosition),
                         array([0,0,self.threeFoldLength]),
                         capColor=self.threeFoldCapColor,
                         text=self.name,
                         horizontal="True")

    def threeFoldN(self, theBox, **keys):

        return X3D.arrow(X3D.extend(self.cartesianPosition),
                         array([0,0,-self.threeFoldLength]),
                         capColor=self.threeFoldCapColor,
                         text=self.name,
                         horizontal="True")

    def fourFold(self,  theBox, **keys):

        return X3D.arrow(X3D.extend(self.cartesianPosition),
                         array([0,0,self.fourFoldLength]),
                         capColor=self.fourFoldCapColor,
                         text=self.name,
                         horizontal="True")

    def fourFoldN(self, theBox, **keys):

        return X3D.arrow(X3D.extend(self.cartesianPosition),
                         array([0,0,-self.fourFoldLength]),
                         capColor=self.fourFoldCapColor,
                         text=self.name,
                         horizontal="True")


    def sixFold(self,  theBox, **keys):

        return X3D.arrow(X3D.extend(self.cartesianPosition),
                         array([0,0,self.sixFoldLength]),
                         capColor=self.sixFoldCapColor,
                         text=self.name,
                         horizontal="True")

    def sixFoldN(self,  theBox, **keys):

        return X3D.arrow(X3D.extend(self.cartesianPosition),
                         array([0,0,-self.sixFoldLength]),
                         capColor=self.sixFoldCapColor,
                         text=self.name,
                         horizontal="True")

    def mirror(self,  theBox, **keys):

        p = self.cartesianDirection
        rotation    = "%s %s %s %s" % tuple(Quaternion.fromXYZR(0,0,1,radians(90)))
        translation = "%s %s %s"    % ( p[0]/2, p[1]/2, .3)

        return X3D.transform(X3D.plane(X3D.extend(self.cartesianPosition),
                                        X3D.extend(self.cartesianDirection),
                                        array([0,0,.5]),
                                        color=self.mirrorColor),
                             X3D.label(self.name,
                                       horizontal    = "True",
                                       transform     = { "translation" : translation,
                                                         "rotation"    : rotation }
                                       ))
                             
                              
    def glide(self,  theBox, **keys):

        glideArrowPosition = array([self.cartesianPosition[0],
                                    self.cartesianPosition[1],
                                    1.0])

        glidePlanePosition = X3D.extend(self.cartesianPosition)
        glidePlanePosition[2] += .5

        return X3D.transform(X3D.plane(glidePlanePosition,
                                       X3D.extend(self.cartesianDirection),
                                       array([0,0,.5]),
                                       color=self.glideColor,
                                       bottom=.5),
                             X3D.arrow(glideArrowPosition,
                                       X3D.extend(self.cartesianDirection)*self.glideFraction,
                                       text=self.name,
                                       capColor="Black"))

