from numpy import array, zeros, maximum, argmax, argmin, inner, cross
import xml.etree.cElementTree as etree
from bbox import *
from ColorTable import ColorTable
from math import pi, sqrt, acos, asin, sin, cos, radians

#======================================================================

def update(dLike, aDict):
    for key,value in aDict.iteritems():
        if getattr(value,"shape",None) != None:
            dLike[key] = str(value)[1:-1]
            continue
        dLike[key] = str(value)
        
#======================================================================

def extend(ar, z=0):
        return array([ar[0],ar[1],z])

#======================================================================

def transform(*args, **keys):

    trans = etree.Element("Transform")
    
    for el in args:
        if not el == None:
            trans.append(el)

    update(trans.attrib,keys)

    return trans

#======================================================================

def generic(name, *args, **keys):

    transform  = etree.Element("Transform")
    shape      = etree.Element("Shape")
    appearance = etree.Element("Appearance")
    material   = etree.Element("Material")
    item       = etree.Element(name)

    transform. append(shape)
    shape.     append(appearance)
    appearance.append(material)
    shape.     append(item)
    for arg in args:
        item.append(arg)

    update(transform.attrib,keys.get("transform",{}))
    update(material.attrib, keys.get("material", {}))
    update(item.attrib,     keys.get("item",     {}))

    return transform

#======================================================================

def label(text, **keys):
    
  textColor  = ColorTable.x3dStr(keys.get("color",   "Black"))

  fontNode = etree.Element("FontStyle")
  fontNode.attrib["size"]       = "%s" % keys.get("size",.1)
  fontNode.attrib["style"]      = keys.get("style","PLAIN")
  fontNode.attrib["horizontal"] = keys.get("horizontal","True")
  
  return generic("Text",
                 fontNode,
                 item      = { "string"      : text},
                 material  = { "diffuseColor": textColor},
                 **keys)

##     textTransformNode = etree.Element("Transform")
##     update(textTransformNode.attrib, keys)
    
##     textNode  = etree.Element("Text")
##     textNode.attrib["string"]     = text
##     textTransformNode.append(textNode)
    
##     matNode = etree.Element("Material")
##     matNode.attrib["diffuseColor"]  = textColor
##     textNode.append(matNode)
    
##     textNode.append(fontNode)

##     return textTransformNode

#======================================================================

def rotationTo(direction):

    length        = sqrt(inner(direction,direction))
    zero          = array([0,0,0])
    yAxis         = array([0,1,0])
    zAxis         = array([0,0,1])
    axis          = cross(direction,yAxis)
    if max(abs(axis)-zero) == 0:
        axis = zAxis

    cosTheta      = inner(direction,yAxis)/length
    theta         = acos(cosTheta)

    #print "cross(%s,%s) = %s  angle = %s" % (direction, yAxis, axis, theta)

    theRotation   = "%s %s %s %s" % (axis[0], axis[1], axis[2], -theta)

    return theRotation

#======================================================================

def cappedLine(length, **keys):

    defaultColor  = ColorTable.x3dStr(keys.get("color",   "Black"))
    capColor      = ColorTable.x3dStr(keys.get("capColor","Red"))
    defaultRadius = ".005"
    capRadius     = ".05"

    result = transform(generic("Cone",
                               transform = {"translation" : "0 %s 0" % (length+.05,)},
                               material  = {"diffuseColor": capColor}, 
                               item      = {"bottomRadius": capRadius,
                                            "height"      : ".1" }),
                       generic("Cylinder",
                               transform = {"translation" : "0 %s 0" % (length/2, ) },
                               material  = {"diffuseColor": keys.get("diffuseColor",defaultColor)},
                               item      = {"radius"      : keys.get("radius",      defaultRadius),
                                            "height"      : "%s" % (length,) }),
                       **keys)

    text = keys.get("text",None)
    if text!=None:
        result.append(label(text,
                            size = .05,
                            transform = {"translation" : "%s %s %s" % (.1, length, .1)}))
        
    return result

#======================================================================

def arrow(position, direction, **keys):

    length        = sqrt(inner(direction,direction))
    trns          = str(position)[1:-1]
    
    result = cappedLine(length,
                           translation = str(position)[1:-1],
                           rotation=rotationTo(direction),
                           **keys)

    update(result.attrib,keys)

    #etree.dump(result)

    return result

#======================================================================

def line(ar, ar0=array([0.0,0.0]), **keys):

        ar  = array([ar[0],ar[1],0])
        ar0 = array([ar0[0],ar0[1],0])
        return arrow(ar0, (ar-ar0), color="Green", **keys)


#======================================================================

def axisX(length=1.0, **keys ):
    return cappedLine(length, rotation="0 0 1 %s" % (-pi/2,), text="X axis", horizontal="true")

def axisZ(length=1.0, **keys):
    return cappedLine(length, rotation="1 0 0 %s" % (pi/2,), text="Z axis", horizontal="true")

def axisY(length=1.0, **keys):
    return cappedLine(length, text="Y axis", horizontal="true")

#======================================================================

def axes(length=1, **keys ):

    result = transform(sphere(array([0,0,0]), fill="blue"),
                       axisX(length),
                       axisY(length),
                       axisZ(length))
    update(result.attrib,keys)
    return result

#======================================================================

def sphere(ar, *extras, **keys):

    x3dColor = ColorTable.x3dStr(keys.get("fill",  "Grey"))
    radius = float(keys.get("radius", ".1"))
    
    parts = [generic("Sphere",
                     material={"diffuseColor":  x3dColor},
                     item    ={"radius"      : "%s" %(radius)})]

    parts.extend(extras)
    transformKeys = {"translation" : "%s %s %s" % (ar[0],ar[1], ar[2])}
    sphere = transform(*parts, **transformKeys)
    
    return sphere

#======================================================================

def box(ar, size, *extras, **keys):
    
    x3dColor = ColorTable.x3dStr(keys.get("fill",  "Grey"))
    
    parts = [generic("Box",
                     material={"diffuseColor":  x3dColor},
                     item    ={"size"        : "%s %s %s" % (size[0],size[1], size[2])})]
    
    parts.extend(extras)
    transformKeys = {"translation" : "%s %s %s" % (ar[0],ar[1], ar[2])}
    box = transform(*parts, **transformKeys)
    
    return box

#======================================================================

def transparentBox(ar, size, *extras, **keys):
    
    x3dColor = ColorTable.x3dStr(keys.get("fill",  "Grey"))
    
    parts = [generic("Box",
                     material={"diffuseColor":  x3dColor,
                               "transparency": "0.5"},
                     item    ={"size"        : "%s %s %s" % (size[0],size[1], size[2])})]
    
    parts.extend(extras)
    transformKeys = {"translation" : "%s %s %s" % (ar[0],ar[1], ar[2])}
    box = transform(*parts, **transformKeys)
    
    return box

#======================================================================

def planeCoordinates(position, direction1, direction2, **keys):
      
    coordNode = etree.Element("Coordinate")

    position0 = position
    position1 = position  + direction1
    position2 = position1 + direction2
    position3 = position  + direction2

    points =  " %s %s %s " % tuple(position0)
    points += " %s %s %s " % tuple(position1)
    points += " %s %s %s " % tuple(position2)
    points += " %s %s %s " % tuple(position3)
    
    coordNode.attrib["point"] = points
    
    return coordNode
    
#======================================================================

def plane(position, direction, direction2, *extras, **keys):

    color    = ColorTable.x3dStr(keys.get("color",  "Grey"))
    vertical = keys.get("vertical",  True)
    
    #print "plane color %s " % color
    
    keys["material"] = {"diffuseColor" : color,
                        "transparency" : ".8"}
    
    keys["item"]     = {"ccw"         : "TRUE",
                        "convex"      : "TRUE",
                        "creaseAngle" : "1.57",
                        "solid"       : "FALSE",
                        "coordIndex"  : "0 1 2 -1 2 3 0 -1"}

    parts = [generic("IndexedFaceSet",
                     planeCoordinates(position,direction,direction2,**keys),
                     **keys) ]
    parts.extend(extras)
    
    plane = transform(*parts)
    
    return plane

#======================================================================

def indexSet(position, direction, direction2, *extras, **keys):

    color    = ColorTable.x3dStr(keys.get("color",  "Grey"))
    vertical = keys.get("vertical",  True)
    
    #print "plane color %s " % color
    
    keys["material"] = {"diffuseColor" : color,
                        "transparency" : ".8"}
    
    keys["item"]     = {"ccw"         : "TRUE",
                        "convex"      : "TRUE",
                        "creaseAngle" : "1.57",
                        "solid"       : "FALSE",
                        "coordIndex"  : "0 1 2 -1 2 3 0 -1"}

    parts = [generic("IndexedFaceSet",
                     planeCoordinates(position,direction,direction2,**keys),
                     **keys) ]
    parts.extend(extras)
    
    plane = transform(*parts)
    
    return plane

#======================================================================

def regionCoordinate(verticies):
      
    coordNode = etree.Element("Coordinate")

    points = ""
    for v in verticies:
        points += " %s %s %s " % ( v[0], v[1], 0 )
    
    coordNode.attrib["point"] = points
    
    return coordNode
    
#======================================================================

def polygon(a, b, **keys) :

    otherCorner = a + b;
    
    result = generic("Polyline2D",material={"diffuseColor":"1 0 1"},
                     item={"lineSegments" : "0 0 %s %s %s %s %s %s 0 0 " %
                           (a[0], a[1], otherCorner[0], otherCorner[1], b[0], b[1]) })
    update(result.attrib,keys)
    return result

#======================================================================

def region(verticies, **keys) :

    color = ColorTable.x3dStr(keys.get("color",  "Red"))
    
    #print "plane color %s " % color

    keys["material"] = {"diffuseColor" : color,
                        "transparency" : ".2"}

    coordIndex = ""
    for i in range(0,len(verticies)):
        coordIndex += "%s " % i
    coordIndex += "-1"
    
    keys["item"]     = {"ccw"         : "TRUE",
                        "convex"      : "TRUE",
                        "creaseAngle" : "1.57",
                        "solid"       : "FALSE",
                        "coordIndex"  : "%s" % coordIndex}

    print "X3D.region %s" % keys
    
    result = generic("IndexedFaceSet",
                     regionCoordinate(verticies),
                     **keys)
    return result

#======================================================================

#======================================================================

class Writer:


    def __init__(self):

        self.x3dTag   = etree.Element("X3D")
        self.headTag  = etree.Element("head")
        backgroundTag  = etree.Element("Background")
        backgroundTag.attrib["skyColor"] = ".8 .8 .8"

        self.sceneTag = etree.Element("Scene")
        self.gTag     = etree.Element("Group")
        self.vptTag   = etree.Element("Viewpoint")
        
        self.x3dTag.append(self.headTag)
        self.x3dTag.append(self.sceneTag)
        self.sceneTag.append(backgroundTag)
        self.sceneTag.append(self.gTag)
        self.gTag.append(self.vptTag)

        
        self.x3dTag.attrib["profile"    ] = "Immersive"
        self.x3dTag.attrib["version"    ] = "3.0"
        self.x3dTag.attrib["xmlns:xsd"  ] = "http://www.w3.org/2001/XMLSchema-instance";
        self.x3dTag.attrib["xsd:noNamespaceSchemaLocation"] = "http://www.web3d.org/specifications/x3d-3.0.xsd";

        self.bbox = bbox()

    def add(self, *args):
        for arg in args:
            self.gTag.append(arg)
            
    def writeHeading(self,srm):
        version    = 1.0
        encoding    = "UTF-8"
        typeStr    = "X3D  PUBLIC \"ISO//Web3D//DTD X3D 3.0//EN\" \"http://www.web3d.org/specifications/x3d-3.0.dtd\" "
        srm.write("<?xml version=\"%s\" encoding=\"%s\"?>\n" % (version, encoding))
        srm.write("<!DOCTYPE %s>\n" % typeStr)

    def writeFile(self, filename):

        f = open(filename,"w")
        self.writeHeading(f)
        f.write(etree.tostring(self.x3dTag))
        f.close()
        
    #======================================================================

    def viewPoint(self):

        center      = self.bbox.center()
        position    = array([0, -1, 7])
        print center
        print position
        viewDir = position - center
        orientation = cross( viewDir, array([0, 0, -1]))
        self.vptTag.attrib["position"]    = str(position)[1:-1]
        self.vptTag.attrib["orientation"] = str(orientation)[1:-1] + " 0"
        self.vptTag.attrib["description"] = "Primary"
        self.vptTag.attrib["centerOfRotation"] = self.bbox.centerStr()



        

