import xml.etree.cElementTree as etree
from numpy import array, zeros, maximum, argmax, argmin
from bbox  import *

#======================================================================

def update(dLike, aDict):
    for key,value in aDict.iteritems():
        if getattr(value,"shape",None) != None:
            dLike[key] = str(value)[1:-1]
            continue
        dLike[key] = str(value)
        
#======================================================================

def clip(ar):
    return array([ar[0],ar[1]])

#======================================================================

def polygon(a, b, **keys) :

    otherCorner = a + b;

    poly = etree.Element("polygon")
    poly.attrib["points"]       = "0 0 %s %s %s %s %s %s" %  (a[0], a[1], otherCorner[0], otherCorner[1], b[0], b[1])
    poly.attrib["fill"]         = keys.get("color","beige")
    poly.attrib["stroke-width"] = "0px";
    return poly

#======================================================================

def line(ar, ar0=array([0.0,0.0]), **keys):

    line = etree.Element("line")
    line.attrib["x1"]     = str(ar0[0])
    line.attrib["y1"]     = str(ar0[1])
    line.attrib["x2"]     = str(ar[0])
    line.attrib["y2"]     = str(ar[1])
    line.attrib["stroke"] = keys.get("color","black")
    return line
        
#======================================================================

def circle(ar, **keys):

    circle = etree.Element("circle")
    circle.attrib["cx"]     = str(ar[0])
    circle.attrib["cy"]     = str(ar[1])
    circle.attrib["r"]      = keys.get("radius",  ".1")
    circle.attrib["stroke-width"] = keys.get("stroke-width",  ".01")
    circle.attrib["fill"]   = keys.get("fill",  "grey")
    circle.attrib["stroke"] = keys.get("stroke","black")

    return circle
        
#======================================================================

def group(*args, **keys):

    grp = etree.Element("g")
    for arg in args:
        if arg != None:
            grp.append(arg)
    update(grp.attrib,keys)
    return grp
        
#======================================================================

def axes(length=1, **keys ):

    result = group(circle(array([0,0]), fill="blue"),
                   line(array([length,0])),
                   line(array([0,length])))
    update(result.attrib,keys)
    return result

#======================================================================

class Writer:

    def __init__(self):

        self.svgTag = etree.Element("svg")
        self.gTag   = etree.Element("g")
        self.svgTag.append(self.gTag)
        
        self.svgTag.attrib["xmlns"      ] = "http://www.w3.org/2000/svg";
        self.svgTag.attrib["xmlns:xlink"] = "http://www.w3.org/1999/xlink";
        self.svgTag.attrib["version"    ] = "1.1";
        self.svgTag.attrib["baseProfile"] = "full";
        self.bbox = bbox()

    def viewBox(self):

        marginX    = self.bbox.width()/10.0
        marginX2x  = marginX * 2
        marginY    = self.bbox.height()/10.0
        marginY2x  = marginY * 2;

        return "%s %s %s %s" % (-marginX,
                                -marginY, 
                                self.bbox.width() + marginX2x,
                                self.bbox.height()+ marginY2x)

    def strokeWidth(self):
        return str(self.bbox.width()/100)

    def writeHeading(self,srm):
        version    = 1.0
        standAlone = "no"
        typeStr    = "svg  PUBLIC \"-//W3C//DTD SVG 1.1//EN\" \"http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd\" "
        srm.write("<?xml version=\"%s\" standalone=\"%s\"?>\n" % (version, standAlone))
        srm.write("<!DOCTYPE %s>\n" % typeStr)

    def scale(self, aBox):

        self.bbox = aBox

        self.bbox.snapToZero();
 
        self.svgTag.attrib["viewBox"]    = self.viewBox();
        self.gTag.attrib["stroke-width"] = self.strokeWidth();
     
        self.gTag.attrib["transform"] = "matrix( 1 0 0 -1 %s %s)" % (self.bbox.minX, self.bbox.maxY)

    def add(self, el):
        self.gTag.append(el)

    def writeFile(self, aBox, filename):

        self.scale(aBox);

        f = open(filename,"w")
        self.writeHeading(f)
        f.write(etree.tostring(self.svgTag))
        f.close()



