#!/usr/bin/python

import sys
from generate import *

if __name__ == "__main__":
    
    if len(sys.argv) == 2:
	inputDirName   = sys.argv[1]
    else:
        print "Usage: python X3D DirectoryHoldingGeometryXMLfiles"
        print "  the args were: %s " % (sys.argv)
        sys.exit(-1)
	
    generateX3D_SVG(inputDirName)

