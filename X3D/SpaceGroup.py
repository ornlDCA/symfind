from NodeBased    import *

import X3D
import SVG

#======================================================================

class SpaceGroup(NodeBased):

    def __init__(self, node):
        NodeBased.__init__(self, node)
        
        self.getAttributes()
        self.getElements()

        

    def toX3D(self, theBox, **keys):

#        print "X3ding %s translation : %s " % (self.name, theBox.upper())

        label = X3D.label(self.name,
                          horizontal    = "True",
                          size          =  .2,
                          transform     = {"translation" : theBox.upper(),
                                           "rotation"    : "1 0 0 1.15"})
        
        result = X3D.transform(label, **keys)

        aSymUnit = self.AsymmetricUnit
        verticies = eval(aSymUnit.verticies)

        if len(verticies) and False > 2:
            result.append(X3D.region(verticies))
        else:
            print "     %s has %s verticies in its asymmetric unit" % (self.name, len(verticies))
                    
        return result

    def toSVG(self, theBox, **keys):
        pass
