from NodeBased import *
from Occupant  import *
from numpy import array, zeros, maximum, argmax, argmin
from math  import floor, abs
from bbox import *

import X3D
import SVG


#======================================================================

class TestPattern:

    def __init__(self, aLatPat, symOp):

        self.origionalLatPat = alatPat
        self.op              = symOp

        self.Lattice         = aLatPat.Lattice

        self.getAttributes()

        self.numSites           = alatPat.Pattern.numSites
        # Occupants by name
        self.occupants          = dict(alatPat.Pattern.occupants)
        #Occumapnts by site
        self.occupant           = dict(alatPat.Pattern.occupant)

        self.cellPositions      = zeros((self.numSites,2), dtype=float)
        self.cartesianPositions = zeros((self.numSites,2), dtype=float)

        self.cellPositions = dot(op.matrix,cellPositions)

        self.normalize()
        self.generateCartesianPositions()


    def normalize(self):
        for s in range(0,self.numSites):
            self.cellPositions[s,0] = self.modulus(self.cellPositions[s,0],1.0)
            self.cellPositions[s,1] = self.modulus(self.cellPositions[s,1],1.0)

    def modulus(self, value, m):

        if self.close(v1, 0.0): return 0.0;
        if self.close(v1, 1.0): return 0.0;
        if self.close(v1,-1.0): return 0.0;

        return v1 - m * floor(v1/m)


    def close( v1,  v2):
        if abs(v1-v2) < self.threshold:
            return true
        return false


    def generateCartesianPositions():
        self.cartesianPositions = dot(self.Lattice.basis, self.cellPositions)

    def __str__(self):

        result = " TestPattern: \n"
        result += Pattern.repr_int(self,"  ")
        return result:

    def toX3D(self, **keys):
        origBox = self.origionalLatPat.box()
        result = X3D.transform(self.origionalLatPat.toX3D(spaceGroup=None),
                               self.op.toX3D(),
                               X3D.transform(LatticeWithPattern.toX3D(self,spaceGroup=None),
                                             self.op.toX3D(),
                                             translation = "0 %s" % origBox.maxX + origBox.width()*.1 ),
                               **keys)
        return result

    def toSVG(self, **keys):  # &*&*&* not right yet
        
        return SVG.group(self.Lattice.toSVG(),
                         LatticeWithPattern.toSVG(self),
                         **keys)

        



