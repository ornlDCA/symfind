from NodeBased import *
from bbox import *

import X3D
import SVG

#======================================================================
        
class Lattice(NodeBased):

    def __init__(self,node):
        NodeBased.__init__(self,node)
        self.getAttributes()
        self.basis = self.getArray((3,3))

##     def __str__(self):

##         result = "Lattice[%s %s %s]: %s \n" % (self.parameters.a,
##                                                self.parameters.b,
##                                                self.parameters.alpha,
##                                                self.bbox())
##         result += str(self.basis)
##         return result

    def a(self):
        return self.basis[0:2,0]

    def b(self):
        return self.basis[0:2,1]
    
    def box(self):
        result = bbox()

        a = self.a()
        b = self.b()
        ab = a+ b
        
        result.maxX = max(a[0], max(b[0], ab[0]))
        result.maxY = max(a[1], max(b[1], ab[1]))
        result.minX = min(a[0], min(b[0], ab[0]))
        result.minY = min(a[1], min(b[1], ab[1]))

        return result

    def toSVG(self, **keys):
        
        a = self.basis[0:2,0]
        b = self.basis[0:2,1]

        return SVG.group(SVG.polygon(a,b),
                         SVG.line(a),
                         SVG.line(b),
                         SVG.line(b+a, a),
                         SVG.line(b+a, b))

    def toX3D(self, **keys):

        a = self.basis[0:2,0]
        b = self.basis[0:2,1]
        
        result = X3D.transform(X3D.polygon(a,b),
                               X3D.line(a, text="a%s" % a, horizontal="True"),
                               X3D.line(b, text="b%s" % b, horizontal="True"),
                               **keys)
        return result


