//-*-C++-*-    

#ifndef Neighbors_HEADER_H
#define Neighbors_HEADER_H

//! \file Neighbors.h

#include <vector>

namespace SymFind {

  //======================================================================

  class Neighbors {

  public:

    typedef double Field;

    class Neighbor {
    public:

      Field  distance;
      size_t siteIndex;
      size_t directionIndex;

      Neighbor():
	distance(0),
	siteIndex(0),
	directionIndex(0)
      {}
	
      bool operator < (const Neighbor& other) const {
	return distance < other.distance;
      }

    };

    typedef Neighbor                  NeighborType;
    typedef std::vector<NeighborType> NeighborListType;

    //    private:

    std::vector<NeighborListType> neighborLists;

  public:

    //======================================================================

    Neighbors():
      neighborLists (0)
    {}

    //======================================================================

    static
    Field squaredLength(const Field* values, size_t numValues)  {
      Field result=0;
      for (size_t i=0; i < numValues; i++) 
	result += values[i] * values[i];
      return result;
    }
    
    //======================================================================
    // nearByCells:  a matix whose column are lattice cell coordinates
    //
    //       0, 0, 1, 1, 0,-1,-1,-1, 1
    //       0  1, 0, 1,-1, 0,-1, 1,-1
    //
    // this matrix's data is: {0,0,0,1,1,0,1,1,0,-1,-1,0,-1,-1}

    static
    int numCells() { return 9;}
    
    static
    int* coordinatesForCell(size_t c) {
      static int nearByCellCoordData[]
        = {0,0, 0,1, 1,0, 1,1, 0,-1, -1,0, -1,-1, -1,1, 1,-1};
      return &nearByCellCoordData[c*2];
    }
    
    //======================================================================
    
    template<typename MatrixLike>
    static
    void offsetToCell( Field*            offset,
		       size_t            c,
		       const MatrixLike& latticeVectors) {
      
      //      typedef typename MatrixLike::value_type T;

      int dimension = 2;

      for(size_t i=0; i< size_t(dimension); i++) {
	offset[i] = 
	  Field(
		latticeVectors(i,0) * coordinatesForCell(c)[0]
		+
		latticeVectors(i,1) * coordinatesForCell(c)[1]
		);
      }
    }
    
    //======================================================================

    template<typename MatrixLike1,
	     typename MatrixLike2>
    static
    void positionInCell(const Field*       site,
			size_t             c,
			const MatrixLike1& sites,
			const MatrixLike2& latticeVectors,
			Field*             siteCellPosition) {

      (void) sites;
      int dimension = 2;

      //      Field offset[dimension];
      std::vector<Field> offset(dimension);
      offsetToCell(&offset[0],c,latticeVectors);
      
      for(size_t i=0; i< size_t(dimension); i++) {
	siteCellPosition[i] = site[i] + offset[i];	
      }

//       std::cout << " offsetToCell("  << c << ") =  ["<< offset[0] << "," << offset[1] << "]\n";
//       std::cout << " position of ["<< site[0] << "," << site[1] << "] in cell " << c << " is ";
//       std::cout << "["<< siteCellPosition[0] << "," << siteCellPosition[1] << "]\n";
    }

    //======================================================================
    // Go through all of the nearby lattice cells compute
    // the distance from site1 to site2 in that cell.
    // return the least distance found.
    
    template<typename MatrixLike>
    static
    Field distanceBetween(size_t site1, 
			  size_t site2,
			  const MatrixLike& sites) {
      
      if(site1 == site2) return 0.0;

      //      int numberOfSites = sites.n_col();
      int dimension     = sites.n_row();

      Field result(0.0);

      for (int c=0;c<dimension;c++) {
        Field delta = sites(c,site1) - sites(c,site2);
        result += delta * delta;
      }
      return sqrt(result);
    }

    // //======================================================================
    // // Go through all of the nearby lattice cells compute
    // // the distance from site1 to site2 in that cell.
    // // return the least distance found.
    
    // template<typename MatrixLike1,
    //          typename MatrixLike2>
    // static
    // Field distanceBetween_(size_t site1, 
    //     		  size_t site2,
    //     		  const MatrixLike1& sites,
    //     		  const MatrixLike2& latticeVectors) {
      
    //   if(site1 == site2) return 0.0;

    //   int dimension = 2;
    //   Field result(10e9);
      
    //   for(size_t c=0; c< numCells(); c++) {
	
    //     Field site2positionC[dimension];
    //     positionInCell(&sites(0,site2),c,sites,latticeVectors,&site2positionC[0]);
	
    //     Field distance = 0;
    //     for (size_t i=0; i < dimension; i++) 
    //       distance += 
    //         (sites(i,site1) - site2positionC[i])
    //         *
    //         (sites(i,site1) - site2positionC[i]);
	
    //     if (distance < result)
    //       result = distance;
    //   }
    //   return sqrt(result);
    // }

    //======================================================================
    
    template<typename MatrixLike1,
	     typename MatrixLike2,
	     typename MatrixLike3,
	     typename MatrixLike4>
    static
    void loadFrom(const MatrixLike1& sites,
		  const MatrixLike2& latticeVectors,
		  MatrixLike3&       neighborDistances,
		  MatrixLike4&       nearestNeighbors) {

      (void) latticeVectors;

      size_t numberOfSites = sites.n_col();
      //      size_t dimension     = sites.n_row();
      
      neighborDistances.resize(numberOfSites,numberOfSites);
      
      for (size_t siteIndex = 0; siteIndex < numberOfSites; siteIndex++) 
	for (size_t neighborIndex = 0; neighborIndex < numberOfSites; neighborIndex++) 
	  
	  neighborDistances(neighborIndex,siteIndex)
	    
	    = distanceBetween(neighborIndex,siteIndex,sites);

      Field smallestDistance(10e10);
      for(size_t i=0; i< size_t(neighborDistances.size()); i++) {
	Field d = neighborDistances[i];
	if(d > 0 and d < smallestDistance)
	  smallestDistance = d;
      }

      //      std::cout << "smallestDistance = " << smallestDistance << "\n";

      Field small( 10e-3);

      int maxNumNearestNeighbors = 0;
      for (size_t siteIndex = 0; siteIndex < numberOfSites; siteIndex++) {
	int numNearestNeighbors = 0;
      	for (size_t neighborIndex = 0; neighborIndex < numberOfSites; neighborIndex++) {
	  if(siteIndex == neighborIndex) continue;
	  if ((neighborDistances(siteIndex,neighborIndex) - smallestDistance) < small)
	    numNearestNeighbors++;
	}
	if (numNearestNeighbors > maxNumNearestNeighbors)
	  maxNumNearestNeighbors = numNearestNeighbors;
      }

      //   std::cout << "maxNumNearestNeighbors = " << maxNumNearestNeighbors << "\n";

      nearestNeighbors.resize(maxNumNearestNeighbors, maxNumNearestNeighbors,numberOfSites, -1);
      for (size_t siteIndex = 0; siteIndex < numberOfSites; siteIndex++) {
	int numNearestNeighbors = 0;
      	for (size_t neighborIndex = 0; neighborIndex < numberOfSites; neighborIndex++) {
	  if(siteIndex == neighborIndex) continue;
	  if ((neighborDistances(siteIndex,neighborIndex) - smallestDistance) < small) {
	    nearestNeighbors(numNearestNeighbors,siteIndex) = neighborIndex;
	    numNearestNeighbors++;
	  }
	}
      }
    }

    //======================================================================

    const NeighborListType& neighborsOf(size_t siteIndex) const {
      return neighborLists[siteIndex];
    }
      
    NeighborListType& neighborsOf(size_t siteIndex) {
      return neighborLists[siteIndex];
    }
      
    const NeighborType& getNeighbor(size_t siteIndex,size_t neighborIndex) const {
      return neighborLists[siteIndex][neighborIndex];
    }

    NeighborListType& operator[] (size_t siteIndex) {
      return neighborLists[siteIndex];
    }

    NeighborType& operator() (size_t siteIndex, size_t neighborIndex) {
      return neighborsOf(siteIndex)[neighborIndex];
    }
      
    size_t numberOfNeighbors(size_t siteIndex) const {
      return neighborsOf(siteIndex).size();
    }
      
    // template<typename VectorLike>
    // void getNeighbors__(VectorLike& siteNeighbors,
    //     	      VectorLike& directions,
    //     	      size_t siteIndex) const {

    //   const NeighborListType& neighbors = neighborsOf(siteIndex);
	
    //   siteNeighbors.resize(neighbors.size());
    //   directions   .resize(neighbors.size());
	
    //   for (size_t n=0;n<neighbors.size();n++) {
    //     siteNeighbors[n]= neighbors[n].siteIndex;
    //     directions[n]   = neighbors[n].directionIndex;
    //   }
    // }

  };

} // namespace dca

#endif
