//-*-C++-*-

#ifndef SymFind_BravaisType_H
#define SymFind_BravaisType_H

/** \ingroup latticeConcepts */
/*@{*/

/** \file BravaisType.h
 *
 *  Contains the class definition for BravaisType objects and
 *  enumerations of the possible bravaisType objects by DIM
 *  specialization.
 */
 
namespace SymFind {

/** \ingroup latticeConcepts
 *
 * \brief Lattices can be classified into one of 14 BravaisTypes.
 *
 */

  template <Field, size_t DIM> class BravaisType {
    
  public:

    std::string name;
    
  };

}
#endif

/*@}*/
