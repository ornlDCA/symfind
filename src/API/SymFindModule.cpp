//-*-mode:C++;c-basic-offset:2;-*-
//
//--------------------------------------------------

#include "Utilities.h"  

#include "TinySciPy.h"

#include "SymFindC++.h"

#include "SymFind_ApiFunctions.h"

#include "MakePyFunction.h"
#include "RegisterPyFunctions.h"

//======================================================================

using Utilities::Open; // namespace Basics;

extern std::string symFind_OutputPath;

namespace TinySciPy {

  //----------------------------------------------------------------------

  //  MakeNoArgPyFunction (symFind_, dimension);
  MakeNoArgPyFunction (symFind_, latticeA)
  MakeNoArgPyFunction (symFind_, latticeB)
  MakeNoArgPyFunction (symFind_, latticeAngle)

  //-------------------------------------------------- space
    
  MakeTwoArgPyFunction(symFind_, spaceLatticeVectors,NUMBER,NUMBER)
  MakeTwoArgPyFunction(symFind_, spacePrimativeVectors,NUMBER,NUMBER)
  MakeNoArgPyFunction (symFind_, spaceZeroPointIndex)
  MakeNoArgPyFunction (symFind_, spaceNumberOfSites)
  MakeTwoArgPyFunction(symFind_, spaceSites,NUMBER,NUMBER)
  MakeOneArgPyFunction(symFind_, spaceSiteDegree,NUMBER)
  MakeTwoArgPyFunction(symFind_, spaceSiteDiffToSite,NUMBER,NUMBER)
  MakeTwoArgPyFunction(symFind_, spaceSiteSumToSite,NUMBER,NUMBER)
  MakeTwoArgPyFunction(symFind_, spaceSymmetryGroupAction,NUMBER,NUMBER)
  MakeTwoArgPyFunction(symFind_, spaceFullGroupAction,NUMBER,NUMBER)
  MakeNoArgPyFunction (symFind_, spaceNumberOfSymmetries)
  MakeTwoArgPyFunction(symFind_, spaceNearestNeighbor,NUMBER,NUMBER)
  MakeTwoArgPyFunction(symFind_, spaceNeighborDistances,NUMBER,NUMBER)

  //----------------------------------------------------------------------

  MakeOneArgPyFunction (symFind_, spaceSiteString,NUMBER)
  MakeOneArgPyFunction (symFind_, rIndexFromChars,STRING)

  //-------------------------------------------------- momentum
    
  MakeTwoArgPyFunction(symFind_, momentumLatticeVectors,NUMBER,NUMBER)

  MakeTwoArgPyFunction(symFind_, momentumPrimativeVectors,NUMBER,NUMBER)
  
  MakeNoArgPyFunction (symFind_, momentumZeroPointIndex)
  MakeNoArgPyFunction (symFind_, momentumNumberOfSites)
    
  MakeTwoArgPyFunction(symFind_, momentumSites,NUMBER,NUMBER)
  
  MakeOneArgPyFunction(symFind_, momentumSiteDegree,NUMBER)
  
  MakeTwoArgPyFunction(symFind_, momentumSiteDiffToSite,NUMBER,NUMBER)
    
  MakeTwoArgPyFunction(symFind_, momentumSiteSumToSite,NUMBER,NUMBER)
  
  MakeTwoArgPyFunction(symFind_, momentumSymmetryGroupAction,NUMBER,NUMBER)
  
  MakeTwoArgPyFunction(symFind_, momentumFullGroupAction,NUMBER,NUMBER)
  
  MakeNoArgPyFunction (symFind_, momentumNumberOfSymmetries)
  
  MakeTwoArgPyFunction(symFind_, momentumNearestNeighbor,NUMBER,NUMBER)

  MakeTwoArgPyFunction(symFind_, momentumNeighborDistances,NUMBER,NUMBER)

  //----------------------------------------------------------------------
  
  MakeNoArgPyFunction (symFind_, momentumNumberOfMeshSites)
  
  MakeTwoArgPyFunction(symFind_, momentumMeshSites,NUMBER,NUMBER)

  //----------------------------------------------------------------------

  MakeOneArgPyFunction (symFind_, momentumSiteString, NUMBER)
  MakeOneArgPyFunction (symFind_, kIndexFromChars,    STRING)

  Obj 
  MomentumPrimativeVectors(VM *tp) {
    (void) tp;
    int dim = symFind_dimension();
    std::vector< std::vector<T> > vectors(dim);
    for (int v=0; v< dim; v++ ) {
      vectors[v].resize(dim);
      for (int i=0; i< dim; i++ )
	vectors[v][i] = symFind_momentumPrimativeVectors(i,v);
    }
    return vectors;
  }

  //----------------------------------------------------------------------

  Obj SymFindPrint(TP) {

    if( not symFind_isOkToWrite() ) return None;

    AssertSignature(None,SymFindPrint,STRING);			

    std::string whereToPrint(tp->nextArg().string());
    if (whereToPrint == "std::cout")

      symFind_display(std::cout);

    else {

      std::ofstream printFile;

      //      SymFind_openFile("Geometry",printFile,whereToPrint);
      Open(printFile,
	   (symFind_OutputPath.size() >0)
	   ?  symFind_OutputPath + "/" + whereToPrint
	   : whereToPrint,
	   __FILE__,
	   std::ofstream::out);
      
      symFind_display(printFile);
    }
    return None;
  }

  //----------------------------------------------------------------------

  Obj SymFindPrintCrystals(TP) {

    if( not symFind_isOkToWrite() ) return None;

    AssertSignature(None,SymFindPrintCrystals,STRING);			
    
    std::string whereToPrint(tp->nextArg().string());

    if (whereToPrint == "std::cout")
      symFind_printCrystals(std::cout);
    else {
      std::ofstream printFile;

      //      SymFind_openFile("Geometry",printFile,whereToPrint);
      Open(printFile,
	 (symFind_OutputPath.size() >0)
	 ?  symFind_OutputPath + "/" + whereToPrint
	   : whereToPrint,
	 __FILE__,
	 std::ofstream::out);

      symFind_printCrystals(printFile);
    }
    return None;
  }

  //----------------------------------------------------------------------
  //
  // This call expects that the tinypy vm will have the initialization
  // input variables already set.
  //
  // ExtractAndSet extracts the values associated with input variable
  // names in the tinypy vm (i.e. tp) and sets C++ variables,
  // symFind_##TheName, to the extracted value.

  Obj SymFindInitialize(TP) {
    
    AssertSignature(None,SymFindInitialize,STRING,DICT);			
    
    symFind_OutputPath = tp->nextArg().string();
    
    // if ( symFind_isOkToWrite() ) 
    //   insurePathExists(symFind_OutputPath);
    
    Dict& specs = tp->nextArg();
    
    // These calls set up the C++ variables
    // (e.g. SymFind::C_API::dinension ) 
    // that are the inputs to symFind_init()
    
    //    int symFind_dimension(2);
    //    ExtractAndSet(symFind_,dimension);

    double symFind_latticeA     = specs["latticeA"];
    double symFind_latticeB     = specs["latticeB"];
    double symFind_latticeAngle = specs["latticeAngle"];

    std::vector<int> symFind_superLatticeA = specs["superLatticeA"];
    std::vector<int> symFind_superLatticeB = specs["superLatticeB"];
        
    // Call the "C" api to initialize the symFind code.
    symFind_init(symFind_latticeA, symFind_latticeB, symFind_latticeAngle,
		 symFind_superLatticeA[0], symFind_superLatticeA[1],
		 symFind_superLatticeB[0], symFind_superLatticeB[1]);
    
    return None;
  }

  //----------------------------------------------------------------------
  // SymFindLoadMesh has one arg an integer


  Obj SymFindLoadMesh(TP) {

    AssertSignature(None,SymFindLoadMesh,NUMBER,LIST);			

    typedef std::vector< std::vector<T> > VecVecType;

    int        numberOfMeshPointsPerOrigLatticePt = tp->nextArg();

    VecVecType vectors;
    vectors = tp->nextArg().list();

    // std::cout << "SymFindLoadMesh called, the vectors passed are:\n";
    // for(int i=0; i<vectors.size();i++)   {
    //   std::cout << "           ";
    //   for(int j=0; j<vectors.size();j++) 
    // 	std::cout << std::setw(15) << std::right << vectors[j][i] << ",";
    //   std::cout << "\n";
    // }
    // std::cout << "\n";
      
    symFind_loadMesh(numberOfMeshPointsPerOrigLatticePt,
		     vectors);
    return None;
  }
  
  //----------------------------------------------------------------------
  // 

  Obj SymFindDumpMesh(TP) {

    AssertSignature(None,SymFindDumpMesh,STRING);			

    if( not symFind_isOkToWrite() ) return None;

    std::string   whereToPrint(tp->nextArg().string());
    std::ofstream printFile;

    //    SymFind_openFile("Mesh",printFile,whereToPrint);
    Open(printFile,
	 (symFind_OutputPath.size() >0)
	 ?  symFind_OutputPath + "/" + whereToPrint
	 : whereToPrint,
	 __FILE__,
	 std::ofstream::out);
    
    symFind_dumpMesh(printFile);

    return None;
  }

  //======================================================================     
  //
  //  
  void Register_SymFind_PyModule(VM& vm) {

    // new a module dict for the Geometry module
    // This show up in tinypy via the Globa 'Geomety'
    //
    Obj SymFind_mod = Obj::make<Dict>();
        
    //------------------------------------------------------------

    //RegisterPyFunction(SymFind,dimension);
    RegisterPyFunction(SymFind,latticeA);
    RegisterPyFunction(SymFind,latticeB);
    RegisterPyFunction(SymFind,latticeAngle);
      
    //------------------------------------------------------------

    RegisterPyFunction2(SymFind,initialize,    SymFindInitialize);
    RegisterPyFunction2(SymFind,print,         SymFindPrint);
    RegisterPyFunction2(SymFind,printCrystals, SymFindPrintCrystals);

    RegisterPyFunction2(SymFind,loadMesh,SymFindLoadMesh);
    RegisterPyFunction2(SymFind,dumpMesh,SymFindDumpMesh);
        
    //------------------------------------------------------------

    RegisterPyFunction(SymFind,spaceNearestNeighbor);
    RegisterPyFunction(SymFind,spaceNeighborDistances);
    RegisterPyFunction(SymFind,spaceLatticeVectors);
    RegisterPyFunction(SymFind,spacePrimativeVectors);
    RegisterPyFunction(SymFind,spaceZeroPointIndex);
    RegisterPyFunction(SymFind,spaceNumberOfSites);
    RegisterPyFunction(SymFind,spaceSites);
    RegisterPyFunction(SymFind,spaceSiteDegree);
    RegisterPyFunction(SymFind,spaceSiteDiffToSite);
    RegisterPyFunction(SymFind,spaceSiteSumToSite);
    RegisterPyFunction(SymFind,spaceSymmetryGroupAction);
    RegisterPyFunction(SymFind,spaceFullGroupAction);
    RegisterPyFunction(SymFind,spaceNumberOfSymmetries);
        
    RegisterPyFunction(SymFind,spaceSiteString);
    RegisterPyFunction(SymFind,rIndexFromChars);

    //------------------------------------------------------------

    RegisterPyFunction(SymFind,momentumNearestNeighbor);
    RegisterPyFunction(SymFind,momentumNeighborDistances);
    
    RegisterPyFunction(SymFind,momentumLatticeVectors);
    RegisterPyFunction(SymFind,momentumPrimativeVectors);
    RegisterPyFunction(SymFind,MomentumPrimativeVectors);

    RegisterPyFunction(SymFind,momentumZeroPointIndex);
    RegisterPyFunction(SymFind,momentumNumberOfSites);
    RegisterPyFunction(SymFind,momentumSites);
    RegisterPyFunction(SymFind,momentumSiteDegree);
    RegisterPyFunction(SymFind,momentumSiteDiffToSite);
    RegisterPyFunction(SymFind,momentumSiteSumToSite);
    RegisterPyFunction(SymFind,momentumSymmetryGroupAction);
    RegisterPyFunction(SymFind,momentumFullGroupAction);
    RegisterPyFunction(SymFind,momentumNumberOfSymmetries);
        
    RegisterPyFunction(SymFind,momentumSiteString);
    RegisterPyFunction(SymFind,kIndexFromChars);

    //------------------------------------------------------------
        
    RegisterPyFunction(SymFind,momentumNumberOfMeshSites);
    RegisterPyFunction(SymFind,momentumMeshSites);
        
    //------------------------------------------------------------
    // bind special attributes to SymFind module
    //
    SymFind_mod.set("__doc__", 
		    "This module is always available.  It provides access to the\n"
		    " geometry functions profided by the SymFind package.");
 
    SymFind_mod.set( "__name__", "Geometry");
    
    SymFind_mod.set("__file__",__FILE__);
    
    // bind to tiny modules[]
    vm.modules.set("Geometry", SymFind_mod);
  }
    
  //======================================================================      
}

