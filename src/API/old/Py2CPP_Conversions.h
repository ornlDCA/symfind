//-*-mode:C++;c-basic-offset:2;-*-

//======================================================================
// From tinypy.c
//
//  typedef double tp_num;
//
//---------------------------------------------------------------------
//----------------------------------------------------------------------

template<typename T>
class ConvertTo {
  tp_vm *tp_;
public:
  ConvertTo(tp_vm *tp): tp_(tp) {}
  
  T 
  value(const tp_obj& obj) {
    return static_cast<T>(tp_type(tp_,TP_NUMBER,obj).number.val);
  }
  
  tp_obj
  value(const T& val) {
    return tp_number(val);
  }
};

//----------------------------------------------------------------------

template<>
class ConvertTo<std::string> {
  tp_vm *tp_;
public:
  ConvertTo(tp_vm *tp): tp_(tp) {}
  
  std::string 
  value(const tp_obj& obj) {
    return std::string(TP_CSTR(obj));
  }

  // Make a string in the tinypy vm (i.e. result) of the same size as v
  // copy v into result's data area,
  // tell the tinypy garbage collector to track result.
  // return the result, a python string object.
  //
  // Note that the resulting python object is a independent copy of the
  // input std::string.
  tp_obj 
  value(const std::string& v) {
    tp_obj result = tp_string_t(tp,static_cast<int>(v.size()));
    char *s       = result.string.info->s;
    memcpy(s,v.c_str(),v.size());
    return tp_track(tp,result);
  }

};

//----------------------------------------------------------------------

template<typename T> std::vector<T> tp_std_vector(tp_vm *tp,tp_obj obj);

template<typename T>
class ConvertTo< std::vector<T> > {
  tp_vm *tp_;
public:
  ConvertTo(tp_vm *tp): tp_(tp) {}
  
  std::vector<T> 
  value(tp_obj& obj) {
    return tp_std_vector<T>(tp,obj);
  }

  tp_obj
  value(const std::vector<T>& v) {
    tp_obj result = tp_list(tp_);
    for (int i=0; i< v.size(); i++) {
      tp_obj item = ConvertTo<T>(tp_).value(v[i]);
      _tp_list_append(tp_, result.list.val,item);
    }
    return result;
  }
};

//----------------------------------------------------------------------

template<typename T> std::map<std::string,T> tp_std_map(tp_vm *tp, tp_obj obj);

template<typename T>
class ConvertTo< std::map<std::string,T> > {
  tp_vm *tp_;
public:
  ConvertTo(tp_vm *tp): tp_(tp) {}
  
  std::map<std::string,T>
  value(tp_obj& obj) {
    return tp_std_map<T>(tp_, obj);
  }
};

//----------------------------------------------------------------------

template<typename T>
std::vector<T> 
tp_std_vector(tp_vm *tp, tp_obj obj) {
  tp_obj listObj = tp_type(tp,TP_LIST,obj);
  int l = tp_len(tp,listObj).number.val;
  std::vector<T> result(l);
  for (size_t i=0; i<result.size(); i++) {
    tp_obj item = tp_get(tp,listObj,tp_number(i));
    result[i] = ConvertTo<T>(tp).value(item);
  }
  return result;
}

//----------------------------------------------------------------------

template<typename T>
std::map<std::string,T> 
tp_std_map(tp_vm *tp, tp_obj obj) {
  tp_obj dictObj = tp_type(tp,TP_DICT,obj);
  int l = tp_len(tp,dictObj).number.val;
  std::map<std::string,T> result;
  for (size_t n=0; n<l; n++) {
    tp_obj key = dictObj.dict.val->items[n].key;
    tp_obj val = dictObj.dict.val->items[n].val;
    result[ConvertTo<std::string>(tp).value(key)] 
      = ConvertTo<T>(tp).value(val);
  }
  return result;
}

//----------------------------------------------------------------------

template<typename T>
T
extract(TP, tp_obj dict, const char* key) {
  tp_obj pyObj = tp_get(tp,dict,tp_string(key));	
  return ConvertTo<T>(tp).value(pyObj);
}

//----------------------------------------------------------------------
//
// Accessing the arguments to a python function call:
//
// #define TP tp_vm *tp
//
// The tinpy virtual machine (tp) contains tp->params which is python
// list object which contains the arguments.  
//
// #define TP_OBJ() (tp_get(tp,tp->params,tp_None))   
//
// This pops the parameter list returning the nest argument as a
// python object.
//
// tp_obj tp_type(TP,int t,tp_obj v)  
// This checks that the given object is of the given type (t).
// if it is it returns it otherwise it throws a python error.
//
// #define TP_TYPE(t) tp_type(tp,t,TP_OBJ())
//
// This retreives that next parameters and checks to see if it is of
// the given type.
//
// #define TP_NUM() (TP_TYPE(TP_NUMBER).number.val)
// Return the next parameter python object after checking that it is a
// number.
//
// #define TP_CSTR(v) ((tp_str(tp,(v))).string.val)
// Return the c_string associated with the  python string object
// which is associated with the given python object.
//
// #define TP_STR() (TP_CSTR(TP_TYPE(TP_STRING)))
// Pop the next argument from the stack as a c string.
//
// Looping over the arguments:
//   tp_obj obj
//   TP_LOOP(obj)
//     doSomethingWith(obj);
//   TP_END
//
