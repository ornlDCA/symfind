//-*-mode:C++;c-basic-offset:2;-*-

//======================================================================
// From tinypy.c
//
//  typedef double tp_num;
//
//  Note that in tinypy all numbers, includeing ints are stored as
//  doubles.
//
//  tp_obj tp_number(tp_num v);      
//
//  tp_obj tp_string(char const *v)
//
//---------------------------------------------------------------------
//
// Make a string in the tinypy vm (i.e. result) of the same size as v
// copy v into result's data area,
// tell the tinypy garbage collector to track result.
// return the result, a python string object.
//
// Note that the resulting python object is a independent copy of the
// input std::string.
//

tp_inline
tp_obj tp_std_string(TP, const std::string& v) {
    
  tp_obj result = tp_string_t(tp,static_cast<int>(v.size()));
  char *s       = result.string.info->s;
  memcpy(s,v.c_str(),v.size());
  return tp_track(tp,result);
}


