//-*-mode:C++;c-basic-offset:2;-*-

//======================================================================

#define MakeNoArgPyFunction(NameSpace, TheName)				\
  static								\
  Obj TheName (VM* tp) {						\
    (void) tp;								\
    return								\
      ( NameSpace##TheName() );						\
  }									\
    

#define MakeOneArgPyFunction(NameSpace, TheName,Arg1Type)		\
  static								\
  Obj TheName (VM* tp) {						\
    AssertSignature(None,TheName,Arg1Type);				\
    return								\
      ( NameSpace##TheName( tp->nextArg() ) );				\
  }									\
  

#define MakeTwoArgPyFunction(NameSpace, TheName,			\
			     Arg1Type, Arg2Type)			\
  static								\
  Obj TheName (VM* tp) {						\
    AssertSignature(None,TheName,					\
		    Arg1Type,Arg2Type);					\
    return								\
      ( NameSpace##TheName ( tp->nextArg(),tp->nextArg() ) );		\
  }									\
  
  
