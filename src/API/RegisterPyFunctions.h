//-*-mode:C++;c-basic-offset:2;-*-

//======================================================================
//
//  Register the function given by the function pointer assocaited
//  with TheFuncName as a tinypy function in the module TheModuleName.
//
#define RegisterPyFunction(TheModuleName, TheFuncName)			\
  TheModuleName##_mod .set(#TheFuncName,				\
			   Obj::make<Fnc>(TheFuncName,#TheFuncName));	\
  
//======================================================================
//
//  Register the function given by the function pointer assocaited
//  with TheCFuncName as a tinypy function (called ThePyFunctionName) in
//  the module TheModuleName.
//
#define RegisterPyFunction2(TheModuleName, TheFuncName, TheCFuncName)	\
  TheModuleName##_mod .set(#TheFuncName,				\
			   Obj::make<Fnc>(TheCFuncName,#TheCFuncName)); \

  
