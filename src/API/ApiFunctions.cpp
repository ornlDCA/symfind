//-*-C++-*-

/** \ingroup DCA */
/*@{*/

/*! \file ApiFunctions.cpp
 *  \brief 
 *
 */

#ifndef SymFind_API_Functions_CPP
#define SymFind_API_Functions_CPP

#define HostDevice

#include "Utilities.h"

#include "SymFindC++.h"

// #include "BasicFunctions.h"
// #include "LA_Functions.h"

//using namespace Basics;

#include "SymFind_ApiFunctions.h"

//======================================================================
//
// The global values hold the symmetry inputs and results.

enum {Dimension=2};

typedef double                                T;
typedef SymFind::SymFind_Crystal<T,Dimension> CrystalType2D;
typedef SymFind::Matrix<T>                    MeshMatrixType;

std::string       symFind_OutputPath;

int               symFind_dimension_    (2);
T                 symFind_latticeA_     (1);
T                 symFind_latticeB_     (1);
T                 symFind_latticeAngle_ (90.0);

std::vector<int>  symFind_superLatticeA_(2);
std::vector<int>  symFind_superLatticeB_(2);

CrystalType2D     symFind_rCluster2D_(::Space);
CrystalType2D     symFind_kCluster2D_(::Momentum);

int               symFind_numberOfMeshPointsPerOrigLatticeCell_(1);
MeshMatrixType    symFind_mesh_      (0,0,0,0);

//----------------------------------------------------------------------

namespace SymFind {

  void WignerSeitzCell_double_setMesh(int                   numPoints,
				      const std::vector< std::vector<double> >& latticeVectors, 
				      Matrix<double>&       sitesKmesh) ;
}
  
//======================================================================
//======================================================================

void symFind_loadMesh(int         numberOfMeshPointsPerOrigLatticeCell__,
		      const std::vector< std::vector<T> > latticeVectors) {
  
  // API specific assignment.
  symFind_numberOfMeshPointsPerOrigLatticeCell_ =
    numberOfMeshPointsPerOrigLatticeCell__;
  
  SymFind::WignerSeitzCell_double_setMesh(numberOfMeshPointsPerOrigLatticeCell__,
					  latticeVectors,
					  symFind_mesh_);
}

//======================================================================

void symFind_loadMesh(int numberOfMeshPointsPerOrigLatticeCell__) {
  
  int dim =  symFind_dimension_;
  std::vector< std::vector<T> > latticeVectors(dim);
  for (int v=0; v< dim; v++ ) {
    latticeVectors[v].resize(dim);
    for (int i=0; i< dim; i++ )
      latticeVectors[v][i] = symFind_momentumPrimativeVectors(i,v);
  }
  
  // API specific assignment.
  symFind_numberOfMeshPointsPerOrigLatticeCell_ =
    numberOfMeshPointsPerOrigLatticeCell__;
  
  SymFind::WignerSeitzCell_double_setMesh(numberOfMeshPointsPerOrigLatticeCell__,
					  latticeVectors,
					  symFind_mesh_);
}

//----------------------------------------------------------------------

extern "C" 

{

  void symFind_init(T latticeA_, 
		    T latticeB_, 
		    T latticeAngle_,
		    int superLatticeA_0, int superLatticeA_1,
		    int superLatticeB_0, int superLatticeB_1) {
    
    symFind_latticeA_     = latticeA_;
    symFind_latticeB_     = latticeB_;
    symFind_latticeAngle_ = latticeAngle_;
    
    symFind_superLatticeA_[0] = superLatticeA_0;
    symFind_superLatticeA_[1] = superLatticeA_1;
    symFind_superLatticeB_[0] = superLatticeB_0;
    symFind_superLatticeB_[1] = superLatticeB_1;
 

    SymFind::SymFind_Geometry<T,Dimension> geometry(latticeA_,latticeB_,latticeAngle_,
						    superLatticeA_0,superLatticeA_1,
						    superLatticeB_0,superLatticeB_1);

    geometry.fetch(symFind_rCluster2D_,symFind_kCluster2D_);
    

    if ( symFind_isOkToWrite() ) {
      
      std::ostringstream XML_Geometry_Path;

      if (symFind_OutputPath.size() > 0)
	XML_Geometry_Path << symFind_OutputPath << "/";
      XML_Geometry_Path << "XML_Geometry";

      //      insurePathExists(XML_Geometry_Path.str());

      SymFind::writeXML(geometry,
			XML_Geometry_Path.str().c_str());
    }
    
    SymFind_doneWithGeometry_2D_double();  // Needed?
  }

  //----------------------------------------------------------------------
  
  void symFind_dumpMesh(std::ostream& meshFile) {
    
    meshFile << "# WignerSeitz kpoint mesh:\n";
    meshFile << "# Try gnuplot:\n";
    meshFile << "#   splot 'file' u 2:3:4\n";
    meshFile << "#------------------------------\n";
  
    for(int i=0; i< symFind_mesh_.n_col(); i++) {
      meshFile << std::setw(5) << i
	       << std::setw(15) << symFind_mesh_(0,i)
	       << std::setw(15) << symFind_mesh_(1,i)
	       << std::setw(15) << "1\n";
    }
  }

  //----------------------------------------------------------------------

  void symFind_destroy() {    }

  //======================================================================
    
  int symFind_dimension() { return 2; }

  double symFind_latticeA()     { return symFind_latticeA_;     }
  double symFind_latticeB()     { return symFind_latticeB_;     }
  double symFind_latticeAngle() { return symFind_latticeAngle_; }
  
  //======================================================================
  
  
  double symFind_spaceLatticeVectors(int c, int s)           { 
    if(c == -1 && s == -1) return symFind_rCluster2D_.latticeVectors.capacity();
    if(c == -1 && s ==  0) return symFind_rCluster2D_.latticeVectors.n_row();
    if(c == -1 && s ==  1) return symFind_rCluster2D_.latticeVectors.l_dim();
    if(c ==  0 && s == -1) return symFind_rCluster2D_.latticeVectors.n_col();
    return symFind_rCluster2D_.latticeVectors(c,s);             
  }
    
    
  double symFind_spacePrimativeVectors(int c, int s)           { 
    if(c == -1 && s == -1) return symFind_rCluster2D_.primativeVectors.capacity();
    if(c == -1 && s ==  0) return symFind_rCluster2D_.primativeVectors.n_row();
    if(c == -1 && s ==  1) return symFind_rCluster2D_.primativeVectors.l_dim();
    if(c ==  0 && s == -1) return symFind_rCluster2D_.primativeVectors.n_col();
    return symFind_rCluster2D_.primativeVectors(c,s);             
  }
    
    
  int    symFind_spaceZeroPointIndex()                       { 
    return symFind_rCluster2D_.ccrystalPtr->zeroPointIndex;     
  }
    
  int    symFind_spaceNumberOfSites ()                       { 
    return symFind_rCluster2D_.sites.n_col();                   
  }
    
  double symFind_spaceSites         (int c, int s)           { 
    if(c == -1 && s == -1) return symFind_rCluster2D_.sites.capacity();
    if(c == -1 && s ==  0) return symFind_rCluster2D_.sites.n_row();
    if(c == -1 && s ==  1) return symFind_rCluster2D_.sites.l_dim();
    if(c ==  0 && s == -1) return symFind_rCluster2D_.sites.n_col();
    return symFind_rCluster2D_.sites(c,s);                      
  }
    
  int    symFind_spaceSiteDegree    (int site)               { 
    if(site == -2) return symFind_rCluster2D_.siteDegree.capacity();
    if(site == -1) return symFind_rCluster2D_.siteDegree.size();
    return symFind_rCluster2D_.siteDegree[site];                
  }
    
  int    symFind_spaceSiteDiffToSite(int site1, int site2)   { 
    if(site1 == -1 && site2 == -1) return symFind_rCluster2D_.siteDiffToSite.capacity();
    if(site1 == -1 && site2 ==  0) return symFind_rCluster2D_.siteDiffToSite.n_row();
    if(site1 == -1 && site2 ==  1) return symFind_rCluster2D_.siteDiffToSite.l_dim();
    if(site1 ==  0 && site2 == -1) return symFind_rCluster2D_.siteDiffToSite.n_col();
    return symFind_rCluster2D_.siteDiffToSite(site1,site2);     
  }
    
  int    symFind_spaceSiteSumToSite (int site1, int site2)   { 
    if(site1 == -1 && site2 == -1) return symFind_rCluster2D_.siteSumToSite.capacity();
    if(site1 == -1 && site2 ==  0) return symFind_rCluster2D_.siteSumToSite.n_row();
    if(site1 == -1 && site2 ==  1) return symFind_rCluster2D_.siteSumToSite.l_dim();
    if(site1 ==  0 && site2 == -1) return symFind_rCluster2D_.siteSumToSite.n_col();
    return symFind_rCluster2D_.siteSumToSite(site1,site2);     
  }
    
  int    symFind_spaceSymmetryGroupAction(int site, int sym) { 
    if(site == -1 && sym == -1) return symFind_rCluster2D_.symmetryGroupAction.capacity();
    if(site == -1 && sym ==  0) return symFind_rCluster2D_.symmetryGroupAction.n_row();
    if(site == -1 && sym ==  1) return symFind_rCluster2D_.symmetryGroupAction.l_dim();
    if(site ==  0 && sym == -1) return symFind_rCluster2D_.symmetryGroupAction.n_col();
    return symFind_rCluster2D_.symmetryGroupAction(site,sym);   
  }
    
  int    symFind_spaceFullGroupAction    (int site, int sym) { 
    if(site == -1 && sym == -1) return symFind_rCluster2D_.fullGroupAction.capacity();
    if(site == -1 && sym ==  0) return symFind_rCluster2D_.fullGroupAction.n_row();
    if(site == -1 && sym ==  1) return symFind_rCluster2D_.fullGroupAction.l_dim();
    if(site ==  0 && sym == -1) return symFind_rCluster2D_.fullGroupAction.n_col();
    return symFind_rCluster2D_.fullGroupAction    (site,sym);   
  }
  
  int    symFind_spaceNumberOfSymmetries ()                  { 
    return nRow(symFind_spaceSymmetryGroupAction);            
  }

    
  int symFind_spaceNearestNeighbor(int neighborNum, int site) { 
    if(neighborNum == -1 && site == -1) return symFind_rCluster2D_.nearestNeighbor.capacity();
    if(neighborNum == -1 && site ==  0) return symFind_rCluster2D_.nearestNeighbor.n_row();
    if(neighborNum == -1 && site ==  1) return symFind_rCluster2D_.nearestNeighbor.l_dim();
    if(neighborNum ==  0 && site == -1) return symFind_rCluster2D_.nearestNeighbor.n_col();
    return symFind_rCluster2D_.nearestNeighbor    (neighborNum,site);   
  }
    
  double symFind_spaceNeighborDistances(int neighborNum, int site) { 
    if(neighborNum == -1 && site == -1) return symFind_rCluster2D_.neighborDistances.capacity();
    if(neighborNum == -1 && site ==  0) return symFind_rCluster2D_.neighborDistances.n_row();
    if(neighborNum == -1 && site ==  1) return symFind_rCluster2D_.neighborDistances.l_dim();
    if(neighborNum ==  0 && site == -1) return symFind_rCluster2D_.neighborDistances.n_col();
    return symFind_rCluster2D_.neighborDistances    (neighborNum,site);   
  }
    
  //======================================================================
    
    
  double symFind_momentumLatticeVectors(int c, int s)           { 
    if(c == -1 && s == -1) return symFind_kCluster2D_.latticeVectors.capacity();
    if(c == -1 && s ==  0) return symFind_kCluster2D_.latticeVectors.n_row();
    if(c == -1 && s ==  1) return symFind_kCluster2D_.latticeVectors.l_dim();
    if(c ==  0 && s == -1) return symFind_kCluster2D_.latticeVectors.n_col();
    return symFind_kCluster2D_.latticeVectors(c,s);                
  }
    
  double symFind_momentumPrimativeVectors(int c, int s)           { 
    if(c == -1 && s == -1) return symFind_kCluster2D_.primativeVectors.capacity();
    if(c == -1 && s ==  0) return symFind_kCluster2D_.primativeVectors.n_row();
    if(c == -1 && s ==  1) return symFind_kCluster2D_.primativeVectors.l_dim();
    if(c ==  0 && s == -1) return symFind_kCluster2D_.primativeVectors.n_col();
    return symFind_kCluster2D_.primativeVectors(c,s);                
  }
    
  int    symFind_momentumZeroPointIndex()                       { 
    return symFind_kCluster2D_.ccrystalPtr->zeroPointIndex;        
  }
    
  int    symFind_momentumPiPointIndex()                         { 
    return symFind_kCluster2D_.ccrystalPtr->piPointIndex;          
  }
    
  int    symFind_momentumNumberOfSites ()                       { 
    return symFind_kCluster2D_.sites.n_col();                      
  }
    
  int    symFind_momentumNumberOfMeshSites ()                   { 
    return symFind_mesh_.n_col();                      
  }
    
  double symFind_momentumSites         (int c, int s)           { 
    if(c == -1 && s == -1) return symFind_kCluster2D_.sites.capacity();
    if(c == -1 && s ==  0) return symFind_kCluster2D_.sites.n_row();
    if(c == -1 && s ==  1) return symFind_kCluster2D_.sites.l_dim();
    if(c ==  0 && s == -1) return symFind_kCluster2D_.sites.n_col();
    return symFind_kCluster2D_.sites(c,s);                         
  }
    
  double symFind_momentumMeshSites(int c, int s)           { 
    if(c == -1 && s == -1) return symFind_mesh_.capacity();
    if(c == -1 && s ==  0) return symFind_mesh_.n_row();
    if(c == -1 && s ==  1) return symFind_mesh_.l_dim();
    if(c ==  0 && s == -1) return symFind_mesh_.n_col();
    return symFind_mesh_(c,s);                         
  }
    
  int    symFind_momentumSiteDegree    (int site)               { 
    if(site == -2) return symFind_kCluster2D_.siteDegree.capacity();
    if(site == -1) return symFind_kCluster2D_.siteDegree.size();
    return symFind_kCluster2D_.siteDegree[site];                   
  }
    
  int    symFind_momentumSiteDiffToSite(int site1, int site2)   { 
    if(site1 == -1 && site2 == -1) return symFind_kCluster2D_.siteDiffToSite.capacity();
    if(site1 == -1 && site2 ==  0) return symFind_kCluster2D_.siteDiffToSite.n_row();
    if(site1 == -1 && site2 ==  1) return symFind_kCluster2D_.siteDiffToSite.l_dim();
    if(site1 ==  0 && site2 == -1) return symFind_kCluster2D_.siteDiffToSite.n_col();
    return symFind_kCluster2D_.siteDiffToSite(site1,site2);        
  }
    
  int    symFind_momentumSiteSumToSite (int site1, int site2)   { 
    if(site1 == -1 && site2 == -1) return symFind_kCluster2D_.siteSumToSite.capacity();
    if(site1 == -1 && site2 ==  0) return symFind_kCluster2D_.siteSumToSite.n_row();
    if(site1 == -1 && site2 ==  1) return symFind_kCluster2D_.siteSumToSite.l_dim();
    if(site1 ==  0 && site2 == -1) return symFind_kCluster2D_.siteSumToSite.n_col();
    return symFind_kCluster2D_.siteSumToSite(site1,site2);         
  }
    
  int    symFind_momentumSymmetryGroupAction(int site, int sym) { 
    if(site == -1 && sym == -1) return symFind_kCluster2D_.symmetryGroupAction.capacity();
    if(site == -1 && sym ==  0) return symFind_kCluster2D_.symmetryGroupAction.n_row();
    if(site == -1 && sym ==  1) return symFind_kCluster2D_.symmetryGroupAction.l_dim();
    if(site ==  0 && sym == -1) return symFind_kCluster2D_.symmetryGroupAction.n_col();
    return symFind_kCluster2D_.symmetryGroupAction(site,sym);      
  }
    
  int    symFind_momentumFullGroupAction    (int site, int sym) { 
    if(site == -1 && sym == -1) return symFind_kCluster2D_.fullGroupAction.capacity();
    if(site == -1 && sym ==  0) return symFind_kCluster2D_.fullGroupAction.n_row();
    if(site == -1 && sym ==  1) return symFind_kCluster2D_.fullGroupAction.l_dim();
    if(site ==  0 && sym == -1) return symFind_kCluster2D_.fullGroupAction.n_col();
    return symFind_kCluster2D_.fullGroupAction    (site,sym);      
  }
    
  int    symFind_momentumNumberOfSymmetries ()                  { 
    return nRow(symFind_momentumSymmetryGroupAction);            
  }
    
  int symFind_momentumNearestNeighbor(int neighborNum, int site) { 
    if(neighborNum == -1 && site == -1) return symFind_kCluster2D_.nearestNeighbor.capacity();
    if(neighborNum == -1 && site ==  0) return symFind_kCluster2D_.nearestNeighbor.n_row();
    if(neighborNum == -1 && site ==  1) return symFind_kCluster2D_.nearestNeighbor.l_dim();
    if(neighborNum ==  0 && site == -1) return symFind_kCluster2D_.nearestNeighbor.n_col();
    return symFind_kCluster2D_.nearestNeighbor    (neighborNum,site);   
  }
    
  double symFind_momentumNeighborDistances(int neighborNum, int site) { 
    if(neighborNum == -1 && site == -1) return symFind_kCluster2D_.neighborDistances.capacity();
    if(neighborNum == -1 && site ==  0) return symFind_kCluster2D_.neighborDistances.n_row();
    if(neighborNum == -1 && site ==  1) return symFind_kCluster2D_.neighborDistances.l_dim();
    if(neighborNum ==  0 && site == -1) return symFind_kCluster2D_.neighborDistances.n_col();
    return symFind_kCluster2D_.neighborDistances    (neighborNum,site);   
  }
    
  //----------------------------------------------------------------------
    
  size_t
  symFind_kIndexFromChars(const char* instr) {
      
    for(int i=0; i< symFind_momentumNumberOfSites(); i++) {
      std::string cmp = symFind_momentumSiteString(i);
      if(strcmp(instr, cmp.c_str()) == 0) 
	return i;
    }
    std::ostringstream msg;
    msg << "symFind_kIndexFromChars("<<instr<<") failed!\n";
    if(symFind_isOkToWrite()) {
      msg << "possible values are: \n";
      for(int i=0; i< symFind_momentumNumberOfSites(); i++)
	msg << symFind_momentumSiteString(i) << "\n";
    }
    throw std::logic_error(msg.str());
  }
    
  //----------------------------------------------------------------------
    
  size_t
  symFind_rIndexFromChars(const char* instr) {
      
    for(int i=0; i< symFind_spaceNumberOfSites(); i++) {
      
      std::string cmp = symFind_spaceSiteString(i);
      if(strcmp(instr, cmp.c_str()) == 0) 
	return i;
    }
    std::ostringstream msg;
    msg << "symFind_rIndexFromChars("<<instr<<") failed!\n";
    if(symFind_isOkToWrite()) {
      msg << "possible values are: \n";
      for(int i=0; i<symFind_spaceNumberOfSites(); i++)
	msg << symFind_spaceSiteString(i) << "\n";
    }
    throw std::logic_error(msg.str());
  }

}

//======================================================================

std::string 
symFind_momentumSiteString(int s) {
  
  std::ostringstream resultBuff;
  resultBuff << "[";
  for (int c=0; c<symFind_dimension(); c++) {
    if (c!=0) resultBuff << ",";
    //    resultBuff << SymFind::tweekedValue(symFind_momentumSites(c,s));
    resultBuff << Utilities::tweekedValue(symFind_momentumSites(c,s));
  }
  resultBuff << "]";
  return resultBuff.str();
}

//----------------------------------------------------------------------
    
std::string 
symFind_spaceSiteString(int s) {
  
  std::ostringstream resultBuff;
  resultBuff << "[";
  for (int c=0; c<symFind_dimension(); c++) {
    if (c!=0) resultBuff << ",";
    //    resultBuff << SymFind::zeroTweekedValue(symFind_spaceSites(c,s));
    resultBuff << Utilities::zeroTweekedValue(symFind_spaceSites(c,s));
  }
  resultBuff << "]";
  return resultBuff.str();
}

//----------------------------------------------------------------------

void symFind_printCrystals(std::ostream& os) {
    
  os << "\n============================================ Space\n";

  print(os,symFind_rCluster2D_);

  os << "\n============================================ Momentum\n";

  print(os,symFind_kCluster2D_);

  os << "\n============================================\n";

}

//----------------------------------------------------------------------

using Utilities::writeMatStaticDataStatement;

void symFind_display(std::ostream& os) {
    
  os << "============================================ Space\n";
    os << "  spaceZeroPointIndex:    " << symFind_spaceZeroPointIndex() << "\n";
    os << "  spaceNumberOfSites:     " << symFind_spaceNumberOfSites() << "\n";
    os << "  spaceNumberOfSymmetries:" << symFind_spaceNumberOfSymmetries() << "\n\n";

    os << " spacePrimativeVectors["<< symFind_spacePrimativeVectors(-1,0) << ","<< symFind_spacePrimativeVectors(0,-1) << "]: \n";
    writeMatStaticDataStatement(symFind_spacePrimativeVectors,os);
    //    writeMatDoubleStaticDataStatement(symFind_spacePrimativeVectors,os);
    os << "\n\n";

    os << " spaceLatticeVectors["<< symFind_spaceLatticeVectors(-1,0) << ","<< symFind_spaceLatticeVectors(0,-1) << "]: \n";
    writeMatStaticDataStatement(symFind_spaceLatticeVectors,os);
    os << "\n\n";

    os << " spaceSites["<< symFind_spaceSites(-1,0) << ","<< symFind_spaceSites(0,-1) << "]: \n";
    writeMatStaticDataStatement(symFind_spaceSites,os);
    os << "\n\n";

    os << " spaceNearestNeighbor["<< symFind_spaceNearestNeighbor(-1,0) << ","<< symFind_spaceNearestNeighbor(0,-1) << "]: \n";
    writeMatStaticDataStatement(symFind_spaceNearestNeighbor,os);
    os << "\n\n";

    os << " spaceNeighborDistances["<< symFind_spaceNeighborDistances(-1,0) << ","<< symFind_spaceNeighborDistances(0,-1) << "]: \n";
    writeMatStaticDataStatement(symFind_spaceNeighborDistances,os);
    os << "\n\n";

    os << " spaceSiteDiffToSite["<< symFind_spaceSiteDiffToSite(-1,0) << ","<< symFind_spaceSiteDiffToSite(0,-1) << "]: \n";
    writeMatStaticDataStatement(symFind_spaceSiteDiffToSite,os);
    os << "\n\n";

    os << " spaceSiteSumToSite["<< symFind_spaceSiteSumToSite(-1,0) << ","<< symFind_spaceSiteSumToSite(0,-1) << "]: \n";
    writeMatStaticDataStatement(symFind_spaceSiteSumToSite,os);
    os << "\n\n";

    os << " spaceSymmetryGroupAction["<< symFind_spaceSymmetryGroupAction(-1,0) << ","<< symFind_spaceSymmetryGroupAction(0,-1) << "]: \n";
    writeMatStaticDataStatement(symFind_spaceSymmetryGroupAction,os);
    os << "\n\n";

    for (int s=0; s < symFind_spaceNumberOfSites(); s++) 
      os << " space site " << s << "   " << symFind_spaceSiteString(s) << "\n";
      
    os << "\n";

    //======================================================================

    os << "============================================ Momentum\n";
    os << "  momentumZeroPointIndex:    " << symFind_momentumZeroPointIndex() << "\n";
    os << "  momentumNumberOfSites:     " << symFind_momentumNumberOfSites() << "\n";
    os << "  momentumNumberOfSymmetries:" << symFind_momentumNumberOfSymmetries() << "\n\n";

    os << " momentumPrimativeVectors["
       << symFind_momentumPrimativeVectors(-1,0)
       << ","
       << symFind_momentumPrimativeVectors(0,-1) << "]: \n";
    writeMatStaticDataStatement(symFind_momentumPrimativeVectors,os);
    os << "\n\n";

    os << " momentumLatticeVectors["
       << symFind_momentumLatticeVectors(-1,0) << ","
       << symFind_momentumLatticeVectors(0,-1) << "]: \n";
    writeMatStaticDataStatement(symFind_momentumLatticeVectors,os);
    os << "\n\n";

    os << " momentumSites["
       << symFind_momentumSites(-1,0) << ","
       << symFind_momentumSites(0,-1) << "]: \n";
    writeMatStaticDataStatement(symFind_momentumSites,os);
    os << "\n\n";

    os << " momentumNearestNeighbor["<< symFind_momentumNearestNeighbor(-1,0) << ","<< symFind_momentumNearestNeighbor(0,-1) << "]: \n";
    writeMatStaticDataStatement(symFind_momentumNearestNeighbor,os);
    os << "\n\n";

    os << " momentumNeighborDistances["<< symFind_momentumNeighborDistances(-1,0) << ","<< symFind_momentumNeighborDistances(0,-1) << "]: \n";
    writeMatStaticDataStatement(symFind_momentumNeighborDistances,os);
    os << "\n\n";

    os << " momentumSiteDiffToSite["
       << symFind_momentumSiteDiffToSite(-1,0) 
       << ","<< symFind_momentumSiteDiffToSite(0,-1) << "]: \n";
    writeMatStaticDataStatement(symFind_momentumSiteDiffToSite,os);
    os << "\n\n";

    os << " momentumSiteSumToSite["
       << symFind_momentumSiteSumToSite(-1,0) << ","
       << symFind_momentumSiteSumToSite(0,-1) << "]: \n";
    writeMatStaticDataStatement(symFind_momentumSiteSumToSite,os);
    os << "\n\n";

    os << " momentumSymmetryGroupAction["
       << symFind_momentumSymmetryGroupAction(-1,0) << ","
       << symFind_momentumSymmetryGroupAction(0,-1) << "]: \n";
    writeMatStaticDataStatement(symFind_momentumSymmetryGroupAction,os);
    os << "\n\n";

    for (int s=0; s < symFind_momentumNumberOfSites(); s++) 
      os << " momentum site " << s << "   " << symFind_momentumSiteString(s) << "\n";
      
    os << "\n";
  }
  
  //----------------------------------------------------------------------



// //----------------------------------------------------------------------
// Move these to another file
// static
// void geometryToJSN(JSON::Writer& writer) {
    
//   writer["dimension"]       = dimension();
//   //    writer["groupActionType"] = groupActionType();
    
//   {
// 	JSON::Writer& cWriter(writer.getComponentWriter("LatticeParameters"));
// 	if (dimension() == 1) {
// 	  cWriter["a"]  = latticeA();
// 	}
// 	if (dimension() == 2) {
// 	  cWriter["a"]      = latticeA();
// 	  cWriter["b"]      = latticeB();
// 	  cWriter["alpha"]  = latticeAngle();
// 	}
//   }
//   {
// 	JSON::Writer& cWriter(writer.getComponentWriter("SuperLatticeBasis"));
// 	std::vector<int> lattice(dimension());
// 	if (dimension() == 1) {
// 	  copyIntVectorFunctionToVector(superLatticeA,lattice);
// 	  cWriter["A"]  = lattice;
// 	}
// 	if (dimension() == 2) {
// 	  copyIntVectorFunctionToVector(superLatticeA,lattice);
// 	  cWriter["A"]      = lattice;
// 	  copyIntVectorFunctionToVector(superLatticeB,lattice);
// 	  cWriter["B"]      = lattice;
// 	}
//   }
// }
  
// //----------------------------------------------------------------------

// static
// void writeGeometryRestartFile() {
      
//   if (not isOkToPlot()) return;
      
//   std::ofstream os(dca::OutputData::insureNestedPathExists("GeometryRestartFile").c_str());
      
//   JSON::Writer writer;
      
//   geometryToJSN(writer["geometry"]);
      
//   int  offset         = 1;
//   bool printLastBrace = true;
//   writer.print(os,offset,printLastBrace);
      
//   os.close();
// }

//======================================================================


/*@}*/
#endif
