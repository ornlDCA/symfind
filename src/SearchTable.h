//-*-C++-*-

#ifndef SymFind_SearchTable_H
#define SymFind_SearchTable_H
/** \file SearchTable.h
 *
 *  \brief Contains generic template class for holding search state transition tables.
 *
 */
 

namespace SymFind {

  //======================================================================
  /**
   *  \brief A generic template class for holding search state
   *         transition tables.
   *
   * \note This is the generic template it is not used directly only
   *       its specializations.
   */
  template<size_t DIM>
  class SearchTable {};

} /* namespace SymFind */

#endif /* SymFind_SearchTable */
