//-*-C++-*-

#ifndef  SymFind_ToXML_H
#define  SymFind_ToXML_H

/** \ingroup ToXML */
/*@{*/

/** \file ToXML.h
 *
 */



namespace SymFind {

  //void insureDirectoryExists(std::string dirName);

  using Utilities::Tag;
  using Utilities::XMLHeading;

  //====================================================================== 
  /** \ingroup XML
   *
   **/
  template<typename Field, size_t NROW, size_t NCOL>
  Tag toXML(const HermiteNormalForm<Field, NROW, NCOL>& hnf,
	    std::string name="HermiteNormalForm")
  {
    typedef HermiteNormalForm<Field,NROW,NCOL>       HermiteNormalFormType;
    typedef typename HermiteNormalFormType::BaseType MatType;
    typedef typename MatType::Traits                 MatTraitsType;

    Tag result(name);

    result["rank"] = hnf.rank();

    Tag transformTag("Transformation");
    std::ostringstream tbuff;
    MAT_PRINT<MatType,MatTraitsType>::JUST_NUMBERS( hnf.T, tbuff);
    transformTag.content << tbuff.str();
    
    result.add(transformTag);
    
    std::ostringstream buff;
    MAT_PRINT<MatType,MatTraitsType>::JUST_NUMBERS( hnf, buff);
    result.content << buff.str();
    
    return result;
  }
  
  //====================================================================== 

  /** \ingroup xml
   *
   * XML Output function for SeitzTranslations.
   */
  template<typename Field, size_t DIM> 
  Tag toXML(const SeitzTranslation<Field,DIM>& pos, 
	    std::string name="SeitzTranslation") {
      
    Tag result(name);
    for (size_t i=0; i<DIM; i++)
      result.content <<  " " << pos[i] ;
    return result;
  }
  //====================================================================== 

  /** \ingroup xml
   *
   * XML Output function for CartesianPositions.
   */
  template<typename Field, size_t DIM> 
  Tag toXML(const SeitzPosition<Field,DIM>& pos, 
	    std::string name="SeitzPosition") {
      
    Tag result(name);
    for (size_t i=0; i<DIM; i++)
      result.content <<  " " << pos[i] ;
    return result;
  }

  //======================================================================

  /** \ingroup xml
   *
   * XML Output function for SeitzMatrix
  */
  template<typename Field, size_t DIM>
  Tag toXML(const SeitzMatrix<Field,DIM>& m,
	    std::string name="SeitzMatrix") {
      
    typedef SeitzMatrix<Field,DIM>   MatType; 
    typedef typename MatType::Traits MatTraitsType;

    Tag result(name);
    std::ostringstream buff;
    MAT_PRINT<MatType,MatTraitsType>::JUST_NUMBERS( m, buff);
    result.content << buff.str();
    
    return result;
  }
  
  //====================================================================== 

  /** \ingroup xml
   *
   * XML Output function for CartesianPositions.
   */
  template<typename Field, size_t DIM> 
  Tag toXML(const CartesianPosition<Field,DIM>& pos,
	    std::string name = "CartesianPosition") {
      
    Tag result(name);
    for (size_t i=0; i<DIM; i++)
      result.content <<  " " << pos[i] ;
    return result;
  }

  //====================================================================== 

  /** \ingroup xml
   *
   * XML Output function for CartesianPositions.
   */
  template<typename Field, size_t DIM> 
  Tag toXML(const CartesianTranslation<Field,DIM>& pos,
	    std::string name="CartesianTranslation") {
      
    Tag result(name);
    for (size_t i=0; i<DIM; i++)
      result.content <<  " " << pos[i] ;
    return result;
  }

  //======================================================================
  
  /** \ingroup xml
   *
   * XML Output function for MetricTensor
   */
  template<typename Field, size_t DIM> 
  Tag toXML(const MetricTensor<Field,DIM>& metric,
	    std::string name = "MetricTensor") {
    
    Tag result(name);
    result["DIM"]    = 2;
     
    typedef  Mat<Field,2,2> MatType;

    std::ostringstream buff;
    MAT_PRINT<MatType,typename MatType::Traits>::JUST_NUMBERS( metric, buff);
    result.content << buff.str();
    
    return result;
  }

  //======================================================================
  //======================================================================

  /** \ingroup xml
   *
   * XML Output function for 1D CellParameters.
   */
  template<typename Field, typename Algorithms >
  Tag toXML(const CellParameters< Field, 1, Algorithms >& p,
	    std::string name="CellParameters") {
      
    Tag result(name);
    result["a"]     = p.a;

    return result;
  }

  //====================================================================== 
  
  /** \ingroup xml
   *
   * XML Output function for 2D CellParameters.
   */
  template<typename Field, typename Algorithms >
  Tag toXML(const CellParameters< Field, 2, Algorithms >& p,
	    std::string name="CellParameters") {
      
      Tag result(name);
      result["a"]     = p.a;
      result["b"]     = p.b;
      result["alpha"] = p.alpha;

      return result;
    }

  //====================================================================== 
  
  /** \ingroup xml
   *
   * XML Output function for 3D CellParameters.
   */
  template<typename Field, typename Algorithms >
  Tag toXML(const CellParameters< Field, 3 , Algorithms >& p,
	    std::string name="CellParameters") {
      
    Tag result(name);
    result["a"]     = p.a;
    result["b"]     = p.b;
    result["c"]     = p.c;
    result["alpha"] = p.alpha;
    result["beta"]  = p.beta;
    result["gamma"] = p.gamma;

    return result;
  }

  //====================================================================== 

  /** \ingroup xml
   *
   * XML Output function for CartesianPositions.
   */
  template<typename Field, size_t DIM> 
  Tag toXML(const CellTranslation<Field,DIM>& t, std::string name="CellTranslation") {
      
    Tag result(name);
    for (size_t i=0; i<DIM; i++)
      result.content <<  " " << t[i] ;
    return result;
  }

  //====================================================================== 

  /** \ingroup xml
   *
   * XML Output function for CartesianPositions.
   */
  template<typename Field, size_t DIM, typename Algorithms> 
  Tag toXML(const CellPosition<Field,DIM,Algorithms>& pos, 
	    std::string name="CellPosition") {
      
    Tag result(name);
    for (size_t i=0; i<DIM; i++)
      result.content <<  " " << pos[i] ;
    return result;
  }

  //====================================================================== 

  /** \ingroup xml
   *
   * XML Output function for CartesianPositions.
   */
  template<size_t DIM> 
  Tag toXML(const LatticeCoordinates<DIM>& lTrans,
	    std::string name="LatticeCoordinates") {
      
    Tag result(name);
    for (size_t i=0; i<DIM; i++)
      result.content <<  " " << lTrans[i] ;
    return result;
  }

  //====================================================================== 
  /** \ingroup XML
   *
   * SpaceGroup  XML Symmetry Element Output
   **/
  template<typename Field, size_t DIM, typename Algorithms> 
  Tag toXML(const SymmetryElement<Field,DIM,Algorithms>& el,
	    std::string name="SymmetryElement") {

    Tag result(name);
    result["type"]             = el.type;
    result["name"]             = el.name;
    result["unNormalizedName"] = el.unNormalizedName;
    result["DIM"]              = DIM;
    result["glideFraction"]    = el.glideFraction;
    result["id"]               = el.id;
    result["trace"]            = el.trace;

    if(el.type != "glide" && 
       el.type != "mirror" && 
       el.type != "translation" &&
       el.type != "identity") {
      result["rotationAngle"]  = el.rotationAngle;
      result["sense"]          = el.sense;
    }

    result.add(toXML(el.netDirection,    "NetDirection"));
    result.add(toXML(el.cellPosition,    "CellPosition"));
    result.add(toXML(el.translationPart, "TranslationPart"));

    return result;
  }

  //====================================================================== 
  /** \ingroup XML
   *
   * SpaceGroup  XML output
   **/
  template<typename Field, size_t DIM, typename LatticeType, typename Algorithms> 
  Tag toXML(const SymmetryOperation<Field,DIM,Algorithms>& symOp,
	    const LatticeType& lat,
	    std::string        name = "SymmetryOperation",
	    std::string        type = "Cartesian" ) {

    //    typedef SymmetryOperation<double,2,Algorithms>  SymmetryOperationType;
    //    typedef SeitzPosition<double,2>                 SeitzPositionType;
    //    typedef CartesianPosition<double,2>             CartesianPositionType;
    //    typedef SeitzTranslation<double,2>              SeitzTranslationType;
    //    typedef HermiteNormalForm<Field, DIM+1, DIM+1>  HermiteNormalFormType;
    typedef SeitzMatrix< Field, DIM >               MatType;
    typedef typename MatType::Traits                MatTraitsType;

    (void) lat;
    Tag result(name);

    result["name"]             = symOp.name;
    result["DIM"]              = DIM;
    result["type"]             = type;
    //result.add(toXML(symmetryElement(symOp, lat)));

    //     Tag hermitNormalFormTag("HermitNormalForm");
    //     Mat<Field,DIM+1,DIM+1> mat;
    //     Copy(mat,b);
    //     result.add(toXML(HermiteNormalFormType(mat)));

    std::ostringstream buff;
    MAT_PRINT<MatType,MatTraitsType>::JUST_NUMBERS( symOp, buff);
    result.content << buff.str();

    return result;
  }

  //======================================================================

  /** \ingroup XML
   * Crystall Base XML output stream operator 
   **/
  template<typename Field, size_t DIM, typename Occupant, typename LatticeType, typename Algorithms>
  Tag toXML(const TestPattern<Field,DIM,Occupant,LatticeType,Algorithms>& latticeWithPattern,
	    std::string name="TestPattern") {
    
    const LatticeType&                            lattice = latticeWithPattern;
    const Pattern<Field,DIM,Occupant,Algorithms>& pattern = latticeWithPattern.pattern;

    Tag tag = toXML( lattice, name );
    
    Tag baseTag("IsA");
    baseTag["type"] = "LatticeWithPattern";
    tag.add(baseTag);
    
    tag.add(toXML(pattern,"Pattern",false));

    return tag;
  }

  //======================================================================

  /** \ingroup xml
   *
   * XML Output function for LatticeTransformation
   */
  template<typename Field, size_t DIM>
  Tag toXML(const LatticeTransformation<Field,DIM>& latTrans,
	    std::string name="LatticeTransformation") {
      
    typedef typename LatticeTransformation<Field,DIM>::MatrixType MatType; 
    typedef typename MatType::Traits                              MatTraitsType;

    Tag result(name);
    result.add(toXML(latTrans.cellOrigToNew,"cellOrigToNew"));
    result.add(toXML(latTrans.cellNewToOrig,"cellNewToOrig"));
    
    std::ostringstream buff;
    MAT_PRINT<MatType,MatTraitsType>::JUST_NUMBERS( latTrans, buff);
    result.content << buff.str();
    
    return result;
  }
  
  //====================================================================== 
  /** \ingroup XML
   *
   * SpaceGroup  XML output
   **/
  template<typename Field, size_t DIM, typename Occupant, typename LatticeType, typename Algorithms> 
  Tag toXML(const AppliedSymmetryElement<Field,DIM,Occupant,LatticeType,Algorithms>& symEl,
	    std::string name="AppliedSymmetryElement") {

    Tag result(name);

    result["latticeDistance"]  = symEl.latticeDistance;
    result["patternDistance"]  = symEl.patternDistance;
    result["distance"]         = symEl.distance;
    result["id"]               = symEl.id;

    result.add(toXML(symEl.element));
    result.add(toXML(symEl.cartesianTranslation,"CartesianDirection"));
    result.add(toXML(symEl.cartesianPosition   ,"CartesianPosition"));
    
    const LatticeType& lat = symEl.testPattern;
    result.add(toXML(symEl.operation,lat));
    //result.add(toXML(symEl.testPattern,"TestPattern"));

    return result;

  }

  //======================================================================
  
  /** \ingroup XML
   *
   * \brief the Pattern XML outputer
   *
   */
  template<typename Field, typename Occupant, size_t DIM, typename Algorithms>
  Tag addSiteTableXML(const Pattern<Field,DIM,Occupant,Algorithms>& pat,
		      std::string name="SiteTable") {
      
    Tag siteTable(name);
    siteTable["type"] = "Add";
    siteTable["DIM"] = pat.NUMPOS;
    Matrix<int> table(0,0,0,0);
    pat.buildPlusIndex(table);
    std::ostringstream buff;
    table.print(buff);
    siteTable.content << std::endl << buff.str();
    return siteTable;
  }
    
  //======================================================================
  /** \ingroup XML
   *
   * \brief the Pattern XML outputer
   *
   */
  template<typename Field, typename Occupant, size_t DIM, typename Algorithms>
  Tag diffSiteTableXML(const Pattern<Field,DIM,Occupant,Algorithms>& pat,
		       std::string name="SiteTable") {

    Tag siteTable(name);
    siteTable["type"] = "Diff";
    siteTable["DIM"] = pat.NUMPOS;
    Matrix<int> table(0,0,0,0);
    pat.buildDiffIndex(table);
    std::ostringstream buff;
    table.print(buff);
    siteTable.content << std::endl << buff.str();
    return siteTable;
  }
    
  //============================================================

  /** \ingroup XML
   *
   * \brief the Pattern XML outputer
   *
   */
  template<typename Field, typename Occupant, size_t DIM, typename Algorithms>
  Tag toXML(const Pattern<Field,DIM,Occupant,Algorithms>& pat,
	    std::string name="Pattern",bool doTables=true) {
    
    Tag result(name);

    //============================================================ Occupants

    Tag occsTag("Occupants");
    for(size_t occ=0; occ<pat.occupants.size(); occ++) {
      Tag occTag("Occupant");
      occTag["patternIndex"] = occ;
      occTag["name"]         = pat.occupants[occ].name;
      occTag["color"]        = pat.occupants[occ].color;
      occTag["startIndex"]   = pat.occupantStart[occ];
      occTag["numPositions"] = pat.occupantNumPositions[occ];
      occsTag.add(occTag);
    }

    result.add(occsTag);

    //============================================================ Occupants

    Tag sitesT("Sites");
    sitesT["numSites"] = pat.NUMPOS;
    for(size_t pos=0; pos<pat.NUMPOS; pos++) {

      Tag siteTag("Site");
      siteTag["index"]=pos;
      siteTag["occupantIndex"]=pat.occupant[pos];
      siteTag["occupantName"] =pat.getOccupant(pos).name;
      siteTag.add(toXML(pat.cellPositions[pos]));
      siteTag.add(toXML(pat.cartesianPositions[pos]));
      sitesT.add(siteTag);
    }
    result.add(sitesT);

    //============================================================ Site Map Tables

    if (doTables) {
      result.add(diffSiteTableXML(pat));
      result.add(addSiteTableXML(pat));
    }
	  
    //============================================================ For Easy Reading

    result.content <<  pat;

    return result;
  }

  //====================================================================== 
  
  /** \ingroup xml
   *
   * XML Output function for Lattice
   */
  template<typename Field, size_t DIM,typename Algorithms>
  Tag toXML(const Lattice<Field,DIM,Algorithms>& lat,
	    std::string name="Lattice") {
      
    typedef typename Lattice<Field,DIM,Algorithms>::MatType MatType; 
    typedef typename MatType::Traits                        MatTraitsType;

    Tag result(name);
    result["det"]    = lat.det;
    result["order"]  = lat.order;
    result["type"]   = lat.parameters.typeString();
    result.add(toXML(lat.parameters));
    result.add(toXML(lat.metric));
    result.add(toXML(lat.transform));
    
    std::ostringstream buff;
    MAT_PRINT<MatType,MatTraitsType>::JUST_NUMBERS( lat, buff);
    result.content << buff.str();
    
    return result;
  }
  
  //======================================================================

  template<typename Field>
  Tag toXML(const Orbits<Field>& orbits,
	    std::string name="Orbits") {
    
    Tag result(name);

    size_t numOrbits = orbits.size();
    result["number"] = numOrbits;
    
    for (size_t i=0; i < numOrbits; i++) {
      Tag orbitTag("orbit");
      orbitTag["orbitNumber"]  = i;
      orbitTag["positions"] = orbits.orbitString(i);
      result.add(orbitTag);
    }
      
    result.content << "pos     ->  orbit  [orbitPositions]" << std::endl;
    result.content << "------------------------------------------" << std::endl;
    for (size_t posIndex=0; posIndex < orbits.numPatternPos; posIndex++) {
	
      if (orbits.posIndexToOrbit.count(posIndex) == 0)
	result.content << posIndex << "->  -1 [] !" << std::endl;
      else {
	std::map<size_t,size_t>& indexToOrbit = const_cast<std::map<size_t,size_t>&>(orbits.posIndexToOrbit);
	const size_t orbitIndex = indexToOrbit[posIndex];
	result.content << posIndex << "->  " << orbitIndex << " " << orbits.orbitString(orbitIndex) << std::endl;
      }
    }
    return result;
  }
  //======================================================================
    
  template<typename Field, size_t DIM, typename Occupant,
 	   template<typename, size_t, typename> class LatticeTemplate,
	   typename Algorithms>
  Tag toXML(const GroupAction<Field,DIM,Occupant,LatticeTemplate,Algorithms>& groupAction,
	    std::string name="GroupAction",
	    const std::vector<int> selectedOPs=std::vector<int>(0) ) 
  {
    typedef GroupAction<Field,DIM,Occupant,LatticeTemplate,Algorithms> ThisType;
    typedef typename ThisType::BaseType                                BaseType;

    int numSelected = selectedOPs.size();
    Tag result(name);
    result["rows"] = (numSelected == 0)?groupAction.n_row():numSelected;
    result["cols"] = groupAction.n_col();
      
    std::ostringstream buff;
    groupAction.print(buff,selectedOPs);
    result.content << std::endl << std::endl << buff.str();

    BaseType uniqueAction(0,0);

    groupAction.uniqueActions(uniqueAction);
    Tag uniqueActionsTag("UniqueActions");
    uniqueActionsTag["rows"] = uniqueAction.n_row();
    uniqueActionsTag["cols"] = uniqueAction.n_col();
      
    std::ostringstream buff2;
    groupAction.print(uniqueAction,buff,selectedOPs);
    uniqueActionsTag.content << std::endl << std::endl << buff2.str();

    result.add(uniqueActionsTag);
    
    return result;
  }

  //============================================================ 
  /** \ingroup symmetryConcepts 
   *
   * \brief 
   */
  template<typename Field, size_t DIM, typename Occupant,
	   template<typename, size_t, typename> class LatticeTemplate,
	   typename Algorithms>
  Tag makeAppliedSymmetryElementsTag(const SymmetryGroup<Field,DIM,Occupant,LatticeTemplate,Algorithms>& symGrp, 
				     const std::string& name="AppliedSymmetryElements")  {
    Tag result(name);
    for (size_t i=0; i < symGrp.appliedElements.size(); i++) 
      result.add(toXML(*symGrp.appliedElements[i]));
    return result;
  }
  
  //======================================================================

  template<typename Field, size_t DIM, typename Occupant,
 	   template<typename, size_t, typename> class LatticeTemplate,
	   typename Algorithms>
  Tag toXML(const GroupMultiplicationTable<Field,DIM,Occupant,LatticeTemplate,Algorithms>& multiplicationTable,
	    std::string name="MultiplicationTable") {
    
    Tag result(name);
    
    result["rows"] = multiplicationTable.appliedElements.size();
    result["cols"] = multiplicationTable.appliedElements.size();
    std::ostringstream buff2;
    multiplicationTable.print(buff2);
    result.content << std::endl << std::endl <<buff2.str();
    return result;
  }

  //============================================================ 
  /** \ingroup xml
   *
   * SymmetryGroup XML Tag factory
   *
   **/
  template<typename Field, size_t DIM, typename Occupant,
 	   template<typename, size_t, typename> class LatticeTemplate,
	   typename Algorithms>
  Tag toXML(const SymmetryGroup<Field,DIM,Occupant,LatticeTemplate,Algorithms>& symmetryGroup,
	    std::string name="SymmetryGroup") {
    
    Tag result(name);

    result.add(toXML(symmetryGroup.multiplicationTable));
    result.add(toXML(symmetryGroup.groupAction));
    result.add(toXML(symmetryGroup.orbits));

    //std::vector<int> unique = symmetryGroup.uniquePermutationOps();
    //result.add(symmetryGroup.makeGroupActionTag("UniqueGroupAction",unique));

    result.add(makeAppliedSymmetryElementsTag(symmetryGroup));
    //result.add(symmetryGroup.makePointGroupTag());

    return result;
  }
  
  //======================================================================
  
  /** \ingroup xml
   * Crystall Base xml constructor
   **/
  template<typename Field, size_t DIM, typename Occupant,
	   template<typename, size_t, typename> class LatticeTemplate,
	   typename Algorithms>
  Tag opsToXML(const Symmetry<Field,DIM,Occupant,LatticeTemplate,Algorithms>& symmetry) {
    
    Tag opsTag("SymmetryOperations");

    opsTag["number"] = symmetry.appliedSymmetryElements.size();
    
    std::ostringstream buff;
    buff << "[";
    for (size_t i=0; i< symmetry.appliedSymmetryElements.size(); i++) {
      if (i!=0) buff << ",";
      buff << symmetry.appliedSymmetryElements[i].element.id;
    }
    buff << "]";
    opsTag["operationIds"] = buff.str();

    opsTag.content << std::endl << std::endl;

    for (size_t i=0; i< symmetry.appliedSymmetryElements.size(); i++) {
      opsTag.content << symmetry.appliedSymmetryElements[i].id 
		     << "  '" 
		     << symmetry.appliedSymmetryElements[i].element.name << "'" << std::endl;
    }
    return opsTag;
  }
  //======================================================================
  
  /** \ingroup xml
   * Crystall Base xml constructor
   **/
  template<typename Field, size_t DIM, typename Occupant,
	   template<typename, size_t, typename> class LatticeTemplate,
	   typename Algorithms>
  Tag toXML(const Symmetry<Field,DIM,Occupant,LatticeTemplate,Algorithms>& symmetry,
	    std::string name="Symmetry") {
    
    //    typedef LatticeTemplate<Field,DIM,Algorithms>                         LatticeType;
    typedef SpaceGroup<Field,DIM,Occupant,Algorithms>                     SpaceGroupType;
   
    //    const LatticeType& lattice = symmetry.latpat;
    
    Tag tag(name);

    tag.add(toXML(symmetry.symmetryGroup));
    tag.add(toXML(symmetry.pointGroup,"PointGroup"));
    
//     tag.add(opsToXML(symmetry));

//     Tag allRatingsTag("AllOperations");

//     for (size_t i=0; i< symmetry.allAppliedSymmetryElements.size(); i++) {

//       allRatingsTag.add(toXML(symmetry.allAppliedSymmetryElements[i]));
//     }
    
//     tag.add(allRatingsTag);

    Tag spaceGroupsTag("SpaceGroups");
    
    spaceGroupsTag["selected"] = symmetry.selectedSpaceGroupIndex;

    if (symmetry.spaceGroupsSet) {
      for (size_t i=1; i<symmetry.spaceGroups.size(); i++) {
	const SpaceGroupType& spaceGroup = symmetry.spaceGroups[i];
	Tag sgtag = toXML(spaceGroup);
	sgtag.add(asymmetricUnitToXML(spaceGroup, symmetry.latpat));
	spaceGroupsTag.add(sgtag);
      }
    }

    tag.add(spaceGroupsTag);

    return tag;
  }

  //====================================================================== 
  
  /** \ingroup XML
   * Crystall Base XML output stream operator 
   **/
  template<typename Field, size_t DIM, typename Occupant, typename LatticeType, typename Algorithms>
  Tag toXML(const LatticeWithPattern<Field,DIM,Occupant,LatticeType,Algorithms>& latticeWithPattern,
	    std::string name="LatticeWithPattern",bool doTables=true) {
    
    const LatticeType&                            lattice = latticeWithPattern;
    const Pattern<Field,DIM,Occupant,Algorithms>& pattern = latticeWithPattern.pattern;

    Tag tag = toXML( lattice, name );
    
    Tag baseTag("IsA");
    baseTag["type"] = "LatticeWithPattern";
    tag.add(baseTag);
    
    tag.add(toXML(pattern,"Pattern",doTables));

    return tag;
  }
    
  //======================================================================

  /** \ingroup xml
   * Crystall Base xml constructor
   **/
  template<typename Field, size_t DIM, typename Occupant,
	   template<typename, size_t, typename> class LatticeTemplate,
	   typename Algorithms>
  Tag toXML(const CrystalBase<Field,DIM,Occupant,LatticeTemplate,Algorithms>& crystalBase,
	    std::string name="CrystalBase") {
    
    typedef LatticeTemplate<Field,DIM,Algorithms>                            LatticeType;
    typedef LatticeWithPattern<Field, DIM, Occupant,LatticeType, Algorithms> BaseType;
    typedef Symmetry<Field,DIM,Occupant,LatticeTemplate,Algorithms>          SymmetryType;

    const LatticeType&  lattice            = crystalBase;
    const BaseType&     latticeWithPattern = crystalBase;
    const SymmetryType& symmtery           = crystalBase.symmetry;

    Tag tag(name);
    tag.add(toXML( lattice ));
    tag.add(toXML( latticeWithPattern.pattern ));
    tag.add(toXML( symmtery ));
    //   tag.add(movementTableXML(crystalBase));

    return tag;
  }

  //======================================================================

  /** \ingroup XML
   * Cell XML output stream operator 
   **/
  template<typename Field, size_t DIM, typename Occupant,typename Algorithms>
  Tag toXML(const Crystal<Field,DIM,Occupant,Algorithms>& crystal,
	    std::string name="Crystal") {

    typedef CrystalBase<Field, DIM,Occupant,Lattice,Algorithms> BaseType;

    Tag tag(name);

//     Tag baseTag("IsA");
//     baseTag.content << "Crystal";
//     tag.add(baseTag);

    tag.add(toXML( static_cast<BaseType>(crystal), "InputCrystal"));
    tag.add(toXML( crystal.reducedCrystal,         "ReducedCrystal"));
    //tag.add(toXML(crystal.conventionalCrystal, "ConventionalCrystal"));
    return tag;
  }

  //======================================================================

  /** \ingroup XML
   *
   * SpaceGroup  XML output
   **/
  template<typename Field, size_t DIM_, typename Occupant, typename LatticeType, typename Algorithms> 
  Tag asymmetricUnitToXML(const SpaceGroup<Field,DIM_,Occupant,Algorithms>& spaceGroup,
			  const LatticeWithPattern<Field,DIM_,Occupant,LatticeType, Algorithms>& latpat) {

    const LatticeType& lat = latpat;
    std::vector< CartesianPosition<Field,DIM_> >  verticies = spaceGroup.cartesianAsymmetricUnitVerticies(lat);
    
    Tag result("AsymmetricUnit");

    std::ostringstream buff;
    buff << "[";
    for (size_t i=0; i< verticies.size(); i++) {
      if (i!=0) buff << ",";
      buff << "(";
      for (size_t j=0; j<DIM_; j++) {
	if (j!=0) buff << ",";
	buff << verticies[i][j];
      }
      buff << ")";
    }
    buff << "]";
    result["verticies"] = buff.str();
    return result;
  }

  //======================================================================

  /** \ingroup XML
   *
   * SpaceGroup  XML output
   **/
  template<typename Field, size_t DIM_, typename Occupant, typename Algorithms> 
  Tag toXML(const SpaceGroup<Field,DIM_,Occupant,Algorithms>& spaceGroup,
	    std::string name="SpaceGroup") {
    
    Tag result(name);
    result["name"]     = spaceGroup.name;
    result["DIM"]      = DIM_;
    result["number"]   = spaceGroup.number;
    result["numOps"]   = spaceGroup.numOperations();
    result["centered"] = spaceGroup.centered;
    result["rating"]   = spaceGroup.rating;
    result["selected"] = spaceGroup.selected;

    std::ostringstream buff;
    buff << "[";
    for (size_t i=0; i< spaceGroup.operationIds.size(); i++) {
      if (i!=0) buff << ",";
      buff << spaceGroup.operationIds[i];
    }
    buff << "]";
    result["operationIds"] = buff.str();
    return result;
  }

  //======================================================================

  /** \ingroup XML
   *
   * SpaceGroup  XML output
   **/
  template<typename Field, size_t DIM_, 
	   typename Occupant, 
	   template<typename, size_t, typename> class LatticeTemplate, 
	   typename Algorithms>
  Tag toXML(const SpaceGroup<Field,DIM_,Occupant,Algorithms>& spaceGroup,
	    const Symmetry<Field,DIM_,Occupant,LatticeTemplate,Algorithms>& symmetry,
	    std::string name="SpaceGroup") {
    
    Tag result = toXML(spaceGroup,name);
    
    if (spaceGroup.loaded) {
      
      typedef LatticeTemplate<Field,DIM_,Algorithms>                             LatticeType;
      typedef AppliedSymmetryElement<Field,DIM_,Occupant,LatticeType,Algorithms> AppliedSymmetryElementType;
      
      for (size_t i=0; i< symmetry.allAppliedSymmetryElements.size(); i++) {
	if (spaceGroup.opFlags != 1) continue;
	
	const AppliedSymmetryElementType& op = symmetry.allAppliedSymmetryElements[i];
	result.add(toXML(*op));
      }
    }
    return result;
  }

//   //======================================================================
    

//   template<typename Field, size_t DIM, typename Occupant, typename Algorithms, 
// 	   template<typename, size_t, typename, typename> class BuilderHelperTemplate, // = SuperCrystalBuilder,
// 	   typename ObjectType>
//   void writeXML(SuperCrystal<Field,DIM,Occupant,Algorithms,BuilderHelperTemplate>& superCrustal,
// 		const std::string outputDir, 
// 		const std::string name, 
// 		const ObjectType& obj)  {
    
//     std::ofstream xmlFile;
//     xmlFile.open((outputDir+"/"+name+".xml").c_str());
//     xmlFile << XMLHeading(XMLHeading::XHTML) << std::endl;
//     xmlFile << toXML(obj)  << std::endl;
//     xmlFile.close();
//   }

  //
 // {
    
 //    int result = mkdir(dirName.c_str(),S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
 //    if(result == 0)     return;
 //    if(errno == EEXIST) return;
 //    std::ostringstream msg;
 //    msg << "SuperCrystal could not find or create directory " << dirName << " errno = " << errno << "\n";
 //    throw std::range_error(msg.str());
 //  }
  

//   template<typename Field, size_t DIM, typename Occupant, typename Algorithms, 
// 	   template<typename, size_t, typename, typename> class BuilderHelperTemplate , //= SuperCrystalBuilder,
// 	   typename ObjectType>
//   void dumpXML(SuperCrystal<Field,DIM,Occupant,Algorithms,BuilderHelperTemplate>& superCrystal,
// 	       std::string directoryName)  {

//     typedef SuperCrystal<Field,DIM,Occupant,Algorithms,BuilderHelperTemplate> ThisType;
//     typedef typename ThisType::BaseType                                       BaseType;

//     insureDirectoryExists(directoryName);

//     dumpXML<ThisType>   (directoryName, "SuperCrystal",          superCrystal);
//     dumpXML<CrystalType>(directoryName, "GivenCrystal",          superCrystal.crystal);
//     dumpXML<BaseType>   (directoryName, "ReciprocalCrystal",     superCrystal.reciprocalCrystal);
//     dumpXML<BaseType>   (directoryName, "ReciprocalSuperCrystal",superCrystal.reciprocalSuperCrystal);
//   }

//   /** \ingroup ostream
//    *
//    * Output stream operator for SuperCrystal
//    */
//   template<typename Field, size_t DIM, typename Occupant, typename Algorithms, 
// 	   template<typename, size_t, typename, typename> class BuilderHelperTemplate>
//   Tag toXML(const SuperCrystal<Field,DIM,Occupant,Algorithms,BuilderHelperTemplate>& superCrystal,
// 	    std::string name="SuperCrystal") {

//     typedef CrystalBase<Field,DIM,Occupant,Lattice,Algorithms>  BaseType;

//     Tag tag = toXML( (BaseType)superCrystal, "SuperCrystal" );

//     Tag subLatticeCoordsTag("SubLatticeCoords");
//     for(size_t i=0; i< superCrystal.subLatVecCoords.size(); i++) 
//       subLatticeCoordsTag.add(toXML(superCrystal.subLatVecCoords[i]));

//     tag.add(subLatticeCoordsTag);
//     //tag.add(toXML(superCrystal.crystal,               "GivenCrystal"));
//     //tag.add(toXML(superCrystal.reciprocalCrystal,     "ReciprocalCrystal"));
//     //tag.add(toXML(superCrystal.reciprocalSuperCrystal,"ReciprocalSuperCrystal"));

//     return tag;
//   }

//   //======================================================================
  
//   /**
//    *
//    */
//   template<typename Field>
//   Tag toXML(const Wedge<Field>& wedge)  {
    
//     Tag tag("Wedge");
      
//     tag["size"] = wedge.wedge2crystal.size();

//     std::ostringstream buff;
//     for (size_t i=0; i<wedge.wedge2crystal.size();i++)
//       buff << wedge[i] << " ";

//     tag["values"] = buff.str();
      
//     tag.add(dca::toXML(wedge.map2wedge_data, "map2wedge_data"));
    
//     if(wedge.crystal2wedge.size() >0)  {
	
//       Tag crystal2wedgeTag("crystal2wedge");
	
//       crystal2wedgeTag["size"] = wedge.crystal2wedge.size();

//       std::ostringstream buff;
//       for (size_t i=0; i<wedge.crystal2wedge.size();i++)
// 	buff << wedge.crystal2wedge[i] << " ";

//       crystal2wedgeTag["values"] = buff.str();
//       tag.add(crystal2wedgeTag);
//     }
      
//     return tag;
//   }
//   Tag toXML(const Crystal& crystal)  
//     {
      
//       Tag tag("DCA_Crystal");
      
//       tag["dim"]                = crystal.dimension;
//       if (crystal.type == Momentum)
// 	tag["type"]             = "Momentum";
//       else
// 	tag["type"]             = "Space";
//       tag["numberOfSites"]      = crystal.numberOfSites;
//       tag["numberOfSymmetries"] = crystal.numberOfSymmetries;
//       tag["piPoint"]            = crystal.piPointIndex;
//       tag["zeroPoint"]          = crystal.zeroPointIndex;
//       //	       tag["hasMesh"]            = crystal.hasMesh;

//       //	       tag["numMeshPointsPerOrigLatticeCell"] = crystal.numMeshPointsPerOrigLatticeCell;
      
//       tag.add(dca::toXML(crystal.sites,              "sites"));
//       tag.add(dca::toXML(crystal.siteDiffToSite,     "substract_table"));
//       tag.add(dca::toXML(crystal.siteSumToSite,      "add_table"));
//       tag.add(dca::toXML(crystal.symmetryGroupAction,"symmetryGroupAction"));
//       //	       tag.add(psimag::toXML(crystal.mesh,               "mesh"));

//       tag.add(toXML(crystal.wedge));

//       Tag siteDegreeTag("siteDegree");
//       {
// 	siteDegreeTag["size"] = crystal.siteDegree.size();
// 	std::ostringstream buff;
// 	for (size_t i=0; i<crystal.siteDegree.size();i++)
// 	  buff << crystal.siteDegree[i] << " ";
// 	siteDegreeTag["values"] = buff.str();
//       }
//       tag.add(siteDegreeTag);
      
//       return tag;
//     }

  //======================================================================
    

  template<typename ObjectType>
  void dumpXML(const std::string outputDir, 
	       const std::string name, 
	       const ObjectType& obj)  {
    (void) obj;
    std::ofstream xmlFile;
    xmlFile.open((outputDir+"/"+name+".xml").c_str());
    //    xmlFile << XMLHeading(XMLHeading::XHTML) << std::endl;
    //    xmlFile << toXML(obj)  << std::endl;
    xmlFile.close();
  }
  
  //======================================================================
  
  template<typename TheValueType>
  void dumpMeshXML(const std::string outputDir, const std::string name, const Matrix<TheValueType>& mesh)  {
    (void) mesh;    
    std::ofstream xmlFile;

    xmlFile.open((outputDir+name+".xml").c_str());
    //    xmlFile << XMLHeading(XMLHeading::XHTML) << std::endl;
    // xmlFile << toXML(mesh)  << std::endl;
    xmlFile.close();
    
  }
  
  //======================================================================
  
} /* namespace SymFind */

#endif //SymFind_ToXML
/*@}*/
