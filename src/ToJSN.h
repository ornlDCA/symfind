//-*-C++-*-

#ifndef  SymFind_ToJSN_H
#define  SymFind_ToJSN_H

/** \ingroup ToJSN */
/*@{*/

/** \file ToJSN.h
 *
 */
 
namespace SymFind {
  //======================================================================
  
  void toJSN(JSN& writer, const Neighbors::Neighbor& neighbor)  {
    writer["distance"]       = neighbor.distance;
    writer["siteIndex"]      = neighbor.siteIndex;
    writer["directionIndex"] = neighbor.directionIndex;
  }

  void toJSN(JSN& writer, const Neighbors& neighbors) {
    writer["neighborLists"] = neighbors.neighborLists;
  }
    
  //======================================================================

  void toJSN(JSN& writer, const Crystal& crystal)  {
	       
    writer["type"] = crystal.name();
    writer["dimension"] = crystal.dimension;
    
    writer["numberOfSites"]      = crystal.numberOfSites;
    writer["numberOfSymmetries"] = crystal.numberOfSymmetries;
    writer["piPoint"]            = crystal.piPointIndex;
    writer["zeroPoint"]          = crystal.zeroPointIndex;

    //	       writer["hasMesh"]            = crystal.hasMesh;
    //	       writer["numMeshPointsPerOrigLatticeCell"] = crystal.numMeshPointsPerOrigLatticeCell;
    //	       writer["mesh"]                = crystal.mesh;
	       	       
    writer["sites"]               = crystal.sites;
    writer["substract_table"]     = crystal.siteDiffToSite;
    writer["add_table"]           = crystal.siteSumToSite;
    writer["symmetryGroupAction"] = crystal.symmetryGroupAction;
    writer["latticeVectors"]      = crystal.latticeVectors;
	       
    writer["wedge"]               = crystal.wedge;
    writer["siteDegree"]          = crystal.siteDegree;
    writer["neighbors"]           = crystal.neighbors;
	       
  }
  //======================================================================
} /* namespace SymFind */

#endif //SymFind_ToJSN
/*@}*/
