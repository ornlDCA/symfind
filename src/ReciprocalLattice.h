//-*-C++-*-

#ifndef SymFind_ReciprocalLattice_H
#define SymFind_ReciprocalLattice_H

/** \ingroup latticeConcepts */
/*@{*/

/** \file Reciprocal.h Contains the template class definitions for
 *        producing Reciprocals of Lattices and structures.
 **/

namespace SymFind {
  
  template<typename Field, size_t DIM, typename Algorithms> class Lattice;

  //================================================== generic case
  template <typename Field, 
	    size_t   DIM, 
	    template<typename,size_t,typename> class LatticeTemplate,
	    typename Algorithms> 
  class ReciprocalLattice {};

  //========================================================================================== 1D case
  //========================================================================================== 

  template <typename Field, 
	    template<typename,size_t,typename> class LatticeTemplate, 
	    typename Algorithms> 
  class ReciprocalLattice<Field,1,LatticeTemplate,Algorithms>
  {
    //====================================================================== typedefs
    enum {DIM=1};
    typedef LatticeTemplate<Field,DIM,Algorithms>  LatticeType;
    typedef CartesianTranslation<Field,DIM>        CartesianTranslationType;
    typedef std::vector<CartesianTranslationType>  BasisVectorsType;
   //====================================================================== Member
    const LatticeType& lattice;
    //====================================================================== Constructor
    ReciprocalLattice(LatticeType& lat): lattice(lat) {}
    //====================================================================== Conversion Operator
//     template< template<typename, size_t, typename> class OtherLatticeTemplate >
//     operator OtherLatticeTemplate<Field,DIM,Algorithms>() {

//       typedef OtherLatticeTemplate<Field,DIM,Algorithms> OtherLatticeType;
     
//       Field vol = lattice.volume();
//       CartesianTranslationType b0 = lattice[0];
//       BasisVectorsType newBasis(1);
      
//       newBasis[0] = b0 / vol;
      
//       return OtherLatticeType(newBasis);
//     }
  };

  //========================================================================================== 2D case
  //========================================================================================== 

  template <typename Field, 
	    template<typename , size_t, typename > class LatticeTemplate, 
	    typename Algorithms> 
  class ReciprocalLattice<Field,2,LatticeTemplate,Algorithms>
  {
  public:
    //====================================================================== typedefs
    enum {DIM=2};
    typedef LatticeTemplate<Field,DIM,Algorithms>  LatticeType;
    typedef CartesianTranslation<Field,DIM+1>      CartesianTranslationType;
    typedef std::vector<CartesianTranslation<Field,DIM> >  BasisVectorsType;
    //====================================================================== Member
    const LatticeType& lattice;
    //====================================================================== Constructor
    explicit
    ReciprocalLattice(const LatticeType& lat):
      lattice(lat)
    {}
    //====================================================================== Converter

    LatticeType getLattice() {
      
      static Field twoPi =  Field(2) * convert<Field>(M_PI);
    
      Field vol = lattice.volume();
      
      CartesianTranslationType b0 = cartesianTranslation<Field>( lattice(0,0), lattice(1,0), Field(0) );
      CartesianTranslationType b1 = cartesianTranslation<Field>( lattice(0,1), lattice(1,1), Field(0) );
      CartesianTranslationType b2 = cartesianTranslation<Field>( Field(0),     Field(0),     Field(1) );
      
      std::vector< CartesianTranslation<Field,2> > basis;

      CartesianTranslation<Field,2> newB0 = projectOntoXY(((b1 % b2) / vol) * twoPi);
      CartesianTranslation<Field,2> newB1 = projectOntoXY(((b0 % b2) / vol) * twoPi);

      basis.push_back(newB0);
      basis.push_back(newB1);

      return LatticeType(basis);
    }

    //====================================================================== Conversion Operator

    operator LatticeType() { return getLattice(); }

  };

  //========================================================================================== 3D case
  //========================================================================================== 

  template <typename Field, 
	    template<typename, size_t, typename> class LatticeTemplate, 
	    typename Algorithms> 
  class ReciprocalLattice<Field,3,LatticeTemplate,Algorithms>
  {
  public:
    //====================================================================== typedefs
    enum {DIM=3};
    typedef LatticeTemplate<Field,DIM,Algorithms>  LatticeType;
    typedef CartesianTranslation<Field,DIM>        CartesianTranslationType;
    typedef std::vector<CartesianTranslationType>  BasisVectorsType;
    //====================================================================== Member
    const LatticeType& lattice;
    //====================================================================== Constructor
    ReciprocalLattice(LatticeType& lat): lattice(lat) {}
    //====================================================================== Conversion Operator
 //    template< template<typename, 
// 		       size_t, 
// 		       typename> class OtherLatticeTemplate >
//     operator OtherLatticeTemplate<Field,DIM,Algorithms>() {
      
//       typedef OtherLatticeTemplate<Field,DIM,Algorithms> OtherLatticeType;
//       Field vol = lattice.volume();

//       CartesianTranslationType b0 = lattice[0];
//       CartesianTranslationType b1 = lattice[1];
//       CartesianTranslationType b2 = lattice[2];
      
//       BasisVectorsType basis;

//       basis[0] = ((b1 % b2) / vol) * Field(2) * convert<Field>(M_PI);
//       basis[1] = ((b0 % b2) / vol) * Field(2) * convert<Field>(M_PI);
//       basis[2] = ((b0 % b1) / vol) * Field(2) * convert<Field>(M_PI);

//       return OtherLatticeType(basis);
//     }
  };
    
} // namespace SymFind
#endif //SymFind_Reciprocal_H
/*@}*/

