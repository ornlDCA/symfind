//-*-C++-*-       

#include "Utilities.h"
#include "SymFindC++.h"

namespace SymFind {
  typedef Matrix<int> IntMatrixType;
}

#include "Typefactory.h"

#include "WignerSeitzCell.h"

namespace SymFind {
  
  template<>
  int MeshUtil<1>::numPtsOnSide   (int& totalNumPoints) {
    return totalNumPoints;
  }

  template<>
  int MeshUtil<2>::numPtsOnSide   (int& totalNumPoints) {
    int maxRootSize(10000);
    for (int i=1;i<maxRootSize;i++) {
      if (i*i > totalNumPoints) {
        i = i-1;
        totalNumPoints = i*i;
        return i;
      }
    }
    totalNumPoints = maxRootSize * maxRootSize;
    return maxRootSize;
  }

  template<>
  int MeshUtil<3>::numPtsOnSide   (int& totalNumPoints) {

    static int maxRootSize(1000);
    for (int i=1;i<maxRootSize;i++) {
      if (i*i >totalNumPoints) {
        totalNumPoints = i*i;
        return i-1;
      }
    }
    totalNumPoints = maxRootSize * maxRootSize;
    return maxRootSize;
  }

  //======================================================================

  void WignerSeitzCell_double_setMesh(int                   numPoints,
				      const std::vector< std::vector<double> >& latticeVectors, 
				      Matrix<double>&       sitesKmesh) {
    
    int dim = latticeVectors.size();
    
    switch (dim) {
    case 2: {

      enum {DIM=2};

      typedef double                                        T;
      typedef Lattice<T,DIM,BasicCrystalAlgorithms>         LatticeType;
      typedef WignerSeitzCell<T,DIM,BasicCrystalAlgorithms> CellType;
      
      LatticeType lat(latticeVectors);
      CellType    cell(lat);
      cell.setMesh(numPoints,sitesKmesh); // => resize of sitesKmesh
      break;
    }
    default: {}
    }
  }

  //----------------------------------------------------------------------
  
} // namespace   

		   
