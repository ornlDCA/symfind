//-*-C++-*-       

#ifndef FETCHGEOMETRY_HEADER_H
#define FETCHGEOMETRY_HEADER_H

namespace SymFind {

  template<typename T>
  class CrystalAdapter {
  public:

    typedef typename Chooser<T>::CrystalType CrystalType;

    CrystalType& crystal;
    
    Matrix<double> sites;
    Matrix<double> latticeVectors;
    Matrix<int>    siteDiffToSite;
    Matrix<int>    siteSumToSite;
    Matrix<int>    symmetryGroupAction;
    Matrix<int>    fullGroupAction;
    
    CrystalAdapter(CrystalType& crystal_):
      crystal            (crystal_),
      latticeVectors     (crystal.latticeVectors),
      siteDiffToSite     (crystal.siteDiffToSite),
      siteSumToSite      (crystal.siteSumToSite),
      symmetryGroupAction(crystal.symmetryGroupAction),
      fullGroupAction   (crystal.fullGroupAction)
    {}
  };

  //====================================================================== fetchGeometry
  //
  template<typename SymmetryType>
  void fetchGroupAction(const SymmetryType&       symmetry,
			Matrix<int>&              target_data, 
			SymFind_GroupActionFilterType filterType) 
  {

    switch (filterType) {
      
      // Use the full group action table
    case (NoGroupActionFilter): {
      target_data = symmetry.symmetryGroup.groupAction;
      return;
    }
      // Use just the point group operations from the group action table
      //
    case (PointGroupOnly): {
      target_data = symmetry.pointGroup.groupAction;
      return;
    }
    case (UniqueGroupActionFilter): {
      symmetry.symmetryGroup.groupAction.uniqueActions(target_data);
      return;
    }
    case (FullGroupActionFilter): {
      throw std::logic_error("In fetchGroupAction, unknown filterType given");
    }
    }
  }

  //----------------------------------------------------------------------

  //fetchGeometry2d(4,2,0,4, 1,1,90,
  //  template<typename Field,
  //	   template<typename> class MatrixTemplate> //,
  //	   typename VectorLikeType>

  void fetchGeometry2D(double                     latticeA_length, 
		       double                     latticeB_length, 
		       double                     latticeAngle,
		       int                        superlatticeA_x,
		       int                        superlatticeA_y,
		       int                        superlatticeB_x,
		       int                        superlatticeB_y,
		       SymFind_GroupActionFilterType  filterType,
		       SymFind_Crystal_double&        rCluster_,
		       SymFind_Crystal_double&        kCluster_)
  {		
    enum{DIM=2};

    SymFind_Crystal<double> rCluster(rCluster_);
    SymFind_Crystal<double> kCluster(kCluster_);

    typedef SymFind::BasicCrystalAlgorithms     Algorithms;
  
    typedef SymFind::CellParameters      <double,DIM,Algorithms>                            CellParametersType;
    typedef SymFind::Lattice             <double,DIM,Algorithms>                                   LatticeType;
    typedef SymFind::Crystal             <double,DIM, Occupant, Algorithms>                        CrystalType;
    typedef SymFind::SuperCrystal        <double,DIM, Occupant, Algorithms>                   SuperCrystalType;
    typedef SymFind::SuperCrystalBuilder <double,DIM, Occupant, Algorithms>            SuperCrystalBuilderType;
    typedef SymFind::PatternData         <double,DIM, Occupant, Algorithms>                    PatternDataType;
    typedef SymFind::SymmetryElements    <double,DIM, Algorithms>                         SymmetryElementsType;
  
    //======================================== Build the crystal lattice
    LatticeType lat(CellParametersType(latticeA_length, latticeB_length, latticeAngle));

    //======================================== Build the crystal pattern
    PatternDataType patternData;
    patternData.insert( Occupant("atom1","yellow"), cellPosition<double,Algorithms>(0.0,0.0) );

    CrystalType* crystalPtr = new CrystalType(lat, patternData);
  
    //======================================== Build the K-point pattern

    PatternDataType* rpatternDataPtr = new PatternDataType;
    rpatternDataPtr->insert( Occupant("kpoint1","red"),  cellPosition<double,Algorithms>( 0.0, 0.0 ) );
    //rpatternData.insert( Occupant("kpoint2","pink"), cellPosition<double,Algorithms>(0.5,0.5) );

    //======================================== Build the SuperCrystal
    SuperCrystalBuilderType* builderPtr = new SuperCrystalBuilderType(superlatticeA_x, superlatticeA_y, superlatticeB_x, superlatticeB_y, *rpatternDataPtr);

    SuperCrystalType* superCrystPtr = new SuperCrystalType(*crystalPtr, *builderPtr);
  
    //======================================== Write four XML files describing the supercrystal in detail

//     if (parallelProcessingId == 0)
//       dumpXML(*superCrystPtr,xmlFileName);

    //======================================== Fetch the rCluster lattice 
    lat.getBasisVectors(rCluster.latticeVectors);
  
    //======================================== Fetch the real sites cartesian coordinates 
    superCrystPtr->pattern.setCartesianSites(rCluster.sites);
  
    //======================================== Fetch the kCluster lattice 
    superCrystPtr->reciprocalCrystal.getBasisVectors(kCluster.latticeVectors);
  
    //======================================== Fetch the k sites cartesian coordinates 
    superCrystPtr->reciprocalCrystal.pattern.setCartesianSites(kCluster.sites);
  
  
    //======================================== Fetch the real translation difference matricies
    superCrystPtr->pattern.buildDiffIndex(rCluster.siteDiffToSite);

    //======================================== Fetch the reciprocal translation difference matricies
    superCrystPtr->reciprocalCrystal.pattern.buildDiffIndex(kCluster.siteDiffToSite);

    //======================================== Fetch the reciprocal translation addition matricies
    superCrystPtr->reciprocalCrystal.pattern.buildPlusIndex(kCluster.siteSumToSite);
  
    //======================================== Fetch the real group action matricies
    fetchGroupAction(superCrystPtr->symmetry, rCluster.fullGroupAction,     NoGroupActionFilter);
    fetchGroupAction(superCrystPtr->symmetry, rCluster.symmetryGroupAction, filterType);

    //======================================== Fetch the reciprocal group action matricies
    fetchGroupAction(superCrystPtr->reciprocalCrystal.symmetry, kCluster.fullGroupAction, NoGroupActionFilter);
    fetchGroupAction(superCrystPtr->reciprocalCrystal.symmetry, kCluster.symmetryGroupAction, filterType);

    //     if (filterType == GroupActionFilter::PointGroupOnly) {
    //       //======================================== Fetch the real orbits
    //       rCluster.orbits = superCrystPtr->symmetry.pointGroup.orbits;
    
    //       //======================================== Fetch the reciprocal orbits
    //       kCluster.orbits = superCrystPtr->reciprocalCrystal.symmetry.pointGroup.orbits;
    //     }
    //     else {
    //       //======================================== Fetch the real orbits
    //       rCluster.orbits = superCrystPtr->symmetry.symmetryGroup.orbits;
    
    //       //======================================== Fetch the reciprocal orbits
    //       kCluster.orbits = superCrystPtr->reciprocalCrystal.symmetry.symmetryGroup.orbits;
    //     }

    SymmetryElementsType::kill(); // Free static memory

    delete rpatternDataPtr;
    delete builderPtr;
    delete superCrystPtr;
    delete crystalPtr;
  }

  //======================================================================
  //======================================================================

  template<typename T, 
	   size_t   DIM, 
	   typename Algorithms>
  void loadPatternData(int numPoints, 
		       SymFind::PatternData<T,DIM,Occupant,Algorithms>& patternData ) {
    
    size_t nSide( static_cast<size_t>(sqrt(static_cast<double>(numPoints))));
    
    double  nSideF(nSide);
    
    for(size_t i=0; i<nSide;i++) 
      for(size_t j=0; j<nSide;j++) 
	
	patternData.insert( Occupant("mesh","blue"),   cellPosition<double,Algorithms>(static_cast<double>(i)/nSideF,
										       static_cast<double>(j)/nSideF) );
  }
  
  // mesh fetch geometry
  template<typename T,
	   template<typename> class MatrixTemplate>
  void fetchMeshGeometry2D(double latticeA_length, 
			   double latticeB_length, 
			   double latticeAngle,
			   int superlatticeA_x,
			   int superlatticeA_y,
			   int superlatticeB_x,
			   int superlatticeB_y,
			   int numPoints,
			   MatrixTemplate<double>& sitesKmesh)
  {
    enum{DIM=2};
    typedef SymFind::BasicCrystalAlgorithms                                          Algorithms;
    typedef SymFind::LatticeCoordinates<DIM>                                         SubLatticeVecCoordType;
    typedef std::vector<SubLatticeVecCoordType>                                     SubLatticeVecCoordsType;
    typedef SymFind::CellParameters<double,2,Algorithms>                              CellParametersType;
    typedef SymFind::PatternData<double,DIM,Occupant,Algorithms>                      PatternDataType;
    typedef SymFind::Lattice<double,DIM,Algorithms>                                   LatticeType;
    typedef SymFind::LatticeWithPattern<double,DIM,Occupant,LatticeType,Algorithms>   LatticeWithPatternType;
    typedef SymFind::FloodTiler<double,DIM,Occupant,Lattice,Algorithms>               FloodTilerType;
    typedef SymFind::ReciprocalLattice<double,DIM,Lattice,Algorithms>                 ReciprocalLatticeType;

    typedef SymFind::SuperCrystalBuilder<double,DIM,Occupant,Algorithms>              SuperCrystalBuilderType;

    /** ====================================================================== Construct the given cell */

    PatternDataType patternData;
    loadPatternData(numPoints, patternData);

    LatticeType lat(CellParametersType(latticeA_length, latticeB_length, latticeAngle));

    LatticeWithPatternType crystalLatPat(lat, patternData);
  
    /** ====================================================================== Construct the k-point tile */
    PatternDataType rpatternData;
    loadPatternData(numPoints, rpatternData);

    /** ====================================================================== Construct a helper class */
    SuperCrystalBuilderType builder(superlatticeA_x,superlatticeA_y,superlatticeB_x,superlatticeB_y, rpatternData);
    SubLatticeVecCoordsType subLatVecCoords(builder.getSubLatticeVecCoords());

    /** ================================================== Construct the superCrystal's LatticeWithPattern  */
    LatticeWithPatternType superCrystalLatPat      (FloodTilerType(subLatVecCoords,crystalLatPat).getTiledLatticeWithPattern());

    /** ================================================== Construct the reciprocal lattice of the crystal that this is a SuperCrystal of. */
    //LatticeType            reciprocalCrystalLattice(ReciprocalLatticeType(crystalLatPat).getLattice());
  
    /**  ================================================== Construct the reciprocal lattice of this SuperCrystal's lattice. */
    LatticeType            reciprocalSuperLattice (ReciprocalLatticeType(superCrystalLatPat).getLattice()); 

    /**  ================================================== Construct the reciprocal  supe LatticeWithPattern. */
    LatticeWithPatternType reciprocalSuperLatPat  (reciprocalSuperLattice, builder.getReciprocalSuperCrystalPattern());

    /**  ================================================== Construct the reciprocal crystal's LatticeWithPattern. */
    //LatticeWithPatternType reciprocalCrystalLatPat(FloodTilerType(reciprocalCrystalLattice, reciprocalSuperLatPat)
    //						   .getTiledLatticeWithPattern());

    //reciprocalCrystalLatPat.pattern.loadCartesian(sitesKmesh);
    reciprocalSuperLatPat.pattern.loadCartesian(sitesKmesh);

    //  reciprocalSuperLatPat.pattern.loadCartesian(sitesRmesh);
  
  }
} // dca namespace
  
#endif
		   
