
#ifndef NDEBUG

#define ASSERT_IT(locals,assertion,body ) { locals if (!(assertion)) body };
#undef  ASSERT
#define ASSERT(assertion,except)          { if (!(assertion)) throw except; };
#endif /* ifndef NDEBUG */

#ifdef NDEBUG

// Does nothing if NDEBUG is defined
#define ASSERT_IT( locals,assertion,body )
#undef  ASSERT
#define ASSERT(assertion,except)
#endif // NDEBUG
