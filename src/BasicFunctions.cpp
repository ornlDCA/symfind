//-*-C++-*-

/** \file    CommonFunctions.c
 *  \ingroup interface
 *  \author  Michael Summers
 *
 *  \
 */

#include <sys/stat.h>
#include <cstdio>
#include <iostream>
#include <fstream>
#include <sstream>
#include <stdexcept>

#include "BasicFunctions.h"

//======================================================================

extern std::string symFind_OutputPath;

namespace Basics {

  void safeOpenFile(std::ofstream& file_,
		    const char*    fileName,
		    const char*    location,
		    std::ios_base::openmode mode) {
  
    file_.open(fileName,mode);
  
    if(not file_.good()) {
      std::ostringstream msg;
      msg << location
	  << " failed to open file: '" << fileName << "'\n";
      throw std::logic_error(msg.str());
    }
  }

  //======================================================================
  
  void safeOpenFile(std::ifstream& file_,
		    const char*    fileName,
		    const char*    location,
		    std::ios_base::openmode mode) {
    
    file_.open(fileName,mode);
    
    if(not file_.good()) {
      std::ostringstream msg;
      msg << location
	  << " failed to open file: '" << fileName << "'\n";
      throw std::logic_error(msg.str());
    }
  }

  //----------------------------------------------------------------------

  void SymFind_openFile(const char*    context,
			std::ofstream& printFile, 
			std::string    whereToPrint) {

    std::ostringstream fullWhereToPrint;
    if (symFind_OutputPath.size() >0)
      fullWhereToPrint << symFind_OutputPath << "/" << whereToPrint;
    else
      fullWhereToPrint << whereToPrint;
    
    //    std::cout << "Printing the " << context << " to: " << fullWhereToPrint.str() << "\n";
    
    safeOpenFile(printFile,
		 fullWhereToPrint.str().c_str(),
		 __FILE__,
		 std::ofstream::out);
  }

  //======================================================================

  void insureDirectoryExists(const char* dirName)  {
  
    int result = mkdir(dirName,S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
    if(result == 0)     return;
    if(errno == EEXIST) return;
    std::ostringstream msg;
    msg << "insureDirectoryExists: \n";
    msg << __FILE__ << " : " << __LINE__ << "\n";
    msg << "Could not find or create directory \"" << dirName << "\" errno = " << errno << "\n";
    throw std::range_error(msg.str());
  }

  //======================================================================

  void insureDirectoryExists(std::string dirName)  {
    insureDirectoryExists(dirName.c_str());
  }

  //======================================================================

  void insurePathExists(std::string path)  {

    const char* begin = path.c_str();
    const char* str   = begin;
    char        sep   = '/';
    
    do {
      while(*str != sep && *str) str++;
      std::string partialPath(begin, str);
      insureDirectoryExists(partialPath);
    } while (0 != *str++);
  }

  //======================================================================
}




