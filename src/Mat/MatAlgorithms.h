//-*-C++-*-

#ifndef SymFind_MatAlgorithms_H
#define SymFind_MatAlgorithms_H

/** \file MatAlgorithms.h
 *
 *  \ingroup FixedSizeMatrices
 *  \author  Michael Summers
 *
 *  \brief Contains template functions providing algorithms for
 *         standard operations on Matrices of any sort.
 */

#include "MatCopy.h"
#include "MatDet.h"
#include "MatDifference.h"
#include "MatEqual.h"
#include "MatIdentity.h"
#include "MatInverse.h"
#include "MatMagnitude.h"
#include "MatMax.h"
#include "MatPrint.h"
#include "MatSum.h"
#include "MatTrace.h"
#include "MatTraits.h"
#include "MatTranspose.h"
#include "MatMult.h"

#endif /* SymFind_MatAlgorithms_H */
