//-*-C++-*-

#ifndef  SymFind_SYMMETRY_ELEMENT_NAME
#define  SymFind_SYMMETRY_ELEMENT_NAME

/** \ingroup symmetryConcepts */
/*@{*/

/** \file SymmetryElementName.h
 *
 *  Contains a class for implementing the naming of symmetry elements and the parsing of names to elements.
 *
 *  \author  Mike Summers
 *
 */

#define DO_FRACTION(name,fraction)					\
									\
  static std::string name##Str ( fraction );				\
									\
  static Field name = convert<Field>( name##Str );			\
									\
  if ( Algorithms::close(value , name) ) {				\
    return name##Str;							\
  }									\
									\
  static std::string minus##name##Str( "-" fraction );			\
									\
  static Field minus##name = convert<Field>( minus##name##Str );	\
									\
  if (Algorithms::close( value , minus##name ) ) {			\
    return minus##name##Str;						\
  }									\
  

namespace SymFind {

  template<typename Field, size_t DIM, typename Algorithms>
  class SymmetryElement;
  

  template<typename Field, typename Algorithms>
  class Fraction {
  public:
    static std::string GET(Field value) {
  
      DO_FRACTION(zero,"0");
      DO_FRACTION(one,"1");
      DO_FRACTION(two,"2");
      DO_FRACTION(three,"3");

      DO_FRACTION(oneHalf,"1/2");
      DO_FRACTION(threeHalf,"3/2");
      DO_FRACTION(fiveHalf,"5/2");
      DO_FRACTION(sevenHalf,"7/2");

      DO_FRACTION(oneThird,"1/3");
      DO_FRACTION(twoThird,"2/3");
      DO_FRACTION(fourThird,"4/3");
      DO_FRACTION(fiveThird,"5/3");
      DO_FRACTION(sevenThird,"7/3");

      DO_FRACTION(oneQuarter,"1/4");
      DO_FRACTION(threeQuarter,"3/4");
      DO_FRACTION(fiveQuarter,"5/4");
      DO_FRACTION(sevenQuarter,"7/4");
      DO_FRACTION(nineQuarter,"9/4");
      DO_FRACTION(elevenQuarter,"11/4");

      DO_FRACTION(oneFifth,"1/5");
      DO_FRACTION(twoFifth,"2/5");
      DO_FRACTION(threeFifth,"3/5");
      DO_FRACTION(fourFifth,"4/5");
      DO_FRACTION(sixFifth,"6/5");
      DO_FRACTION(sevenFifth,"7/5");

      DO_FRACTION(oneSixth,"1/6");
      DO_FRACTION(fiveSixth,"5/6");
      DO_FRACTION(sevenSixth,"7/6");
      DO_FRACTION(elevenSixth,"11/6");

      DO_FRACTION(oneSeventh,"1/7");
      DO_FRACTION(twoSeventh,"2/7");
      DO_FRACTION(threeSeventh,"3/7");
      DO_FRACTION(fourSeventh,"4/7");
      DO_FRACTION(fiveSeventh,"4/7");
      DO_FRACTION(sixSeventh,"6/7");
      DO_FRACTION(eightSeventh,"8/7");

      DO_FRACTION(oneEighth,"1/8");
      DO_FRACTION(threeEighth,"3/8");
      DO_FRACTION(fiveEighth,"5/8");
      DO_FRACTION(sevenEighth,"7/8");
      DO_FRACTION(nineEighth,"9/8");
      DO_FRACTION(elevenEighth,"11/8");

      std::ostringstream buff;
      buff << std::setprecision(4) << value;
      // buff << value;
      return buff.str();
    }
  };

  /** \ingroup symmetryConcepts
   *
   * \brief A to-be-specialized class for implementing symmetry element names.
   *
   *
   */
  template<size_t DIM>
  class SymmetryElementName: 
    public std::string 
  {};
    
  /** \ingroup symmetryConcepts
   *
   * \brief A class for implementing 2D symmetry element names.
   *
   * The names uniquely identify a symmetry element class.
   */
  template<>
  class SymmetryElementName<2>: 
    public std::string 
  {
  public:
    typedef SymmetryElementName                     ThisType;

    //====================================================================== Constructors
    /**
     * \brief  Sets the name from a std::string
     */
    SymmetryElementName(const std::string& name):
      std::string(name)
    { }

    /**
     * \brief  Sets the name from a char*
     */
    SymmetryElementName(const char*& name):
      std::string(name)
    { }

    /**
     * \brief  The copy constructor 
     */
    SymmetryElementName(const ThisType& name):
      std::string(name)
    { }

    /**
     * \brief  Sets the name from a given Symmetry Element
     */
    template<typename Field, typename Algorithms>
    SymmetryElementName(const SymmetryElement<Field,2,Algorithms>& element):
      std::string("?")
    { 
      std::ostringstream buff;
      setDirectionPart(buff, element);
      buff << element.type;
      setPositionPart(buff, element);
      (*this) = buff.str();
    }

    //======================================================================
    //======================================================================

    /** 
     * \brief Check to make sure the glide ratio is right and return it.
     *
     */
    void setDirComponent(std::ostream& buff, 
			 const int&    netDirComponent,
			 bool          isFirst,
			 std::string   postfix) const {
      if (netDirComponent == 0) 
	return;
      if (!isFirst)
	buff << " ";
      switch (netDirComponent) {
      case -1: {
	buff << "-";
	break;
      }
      case 1: {
	break;
      }
      default:
	buff << netDirComponent;
      }
      buff << postfix;
    }

    //----------------------------------------------------------------------
    /**
     * \brief Add to the given ostream the direction part of the name
     *        corresponding to the given Symmetry Element.
     *
     */
    template<typename Field, typename Algorithms>
    void setDirectionPart(std::ostream& buff, 
			  const SymmetryElement<Field,2,Algorithms>& element) {
      
      static Field zero(0);

      // Don't print anything for a zero direction (e.g. rotations)
      if (element.netDirection[0] == 0 && 
	  element.netDirection[1] == 0)
	return;
      
      element.checkDirection();

      Field gRatio         = element.glideFraction;
      bool  zeroGlideRatio = Algorithms::close(gRatio,zero);

      if (!zeroGlideRatio)
	buff << Fraction<Field,Algorithms>::GET(gRatio) << " ";

      // The a direction
      setDirComponent(buff,element.netDirection[0],true,"a");

     // The b diirection
      setDirComponent(buff,element.netDirection[1],(element.netDirection[0] == 0),"b");

      if (!zeroGlideRatio)
	buff << " ";
      else
	buff << " ";
    }

    //----------------------------------------------------------------------
      
    template<typename Field, typename Algorithms>
    void setPositionPart(std::ostream& buff, 
			 const SymmetryElement<Field,2,Algorithms>& element) {
      
      Field zero(0);
      // Don't write anything for a zero position
      if (Algorithms::close(element.cellPosition[0], zero) &&
	  Algorithms::close(element.cellPosition[1], zero) )
	return;

      buff << "(";
      buff << Fraction<Field,Algorithms>::GET(element.cellPosition[0]);
      buff << ",";
      buff << Fraction<Field,Algorithms>::GET(element.cellPosition[1]);
      buff << ")";
    }

  };
  
} /* namespace SymFind */

#endif   //SymFind_SYMMETRY_ELEMENT_NAME

/*@}*/

