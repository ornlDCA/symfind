//-*-C++-*-       
#ifndef WignerSeitzCell_HEADER_H
#define WignerSeitzCell_HEADER_H

namespace SymFind {

  //----------------------------------------------------------------------
  
  template<typename T, size_t DIM, typename Algorithms>  
  class WignerSietzCellNeigbors {};

  template<typename T, typename Algorithms>
  class WignerSietzCellNeigbors<T,2,Algorithms> {
    typedef CartesianPosition<T,2>             CartesianPositionType;
    typedef std::vector<CartesianPositionType> RefPointsVectors;
    typedef  Lattice<T,2,Algorithms>           LatticeType;

    const LatticeType& lattice;
    RefPointsVectors   neighbors;

  public:

    WignerSietzCellNeigbors(const LatticeType& l): 
      lattice(l) 
    {
      neighbors.push_back(lattice.cartesianPosition( latticeCoordinate( 0,-1) ) );
      neighbors.push_back(lattice.cartesianPosition( latticeCoordinate( 0, 1) ) );
      neighbors.push_back(lattice.cartesianPosition( latticeCoordinate(-1,-1) ) );
      neighbors.push_back(lattice.cartesianPosition( latticeCoordinate(-1, 0) ) );
      neighbors.push_back(lattice.cartesianPosition( latticeCoordinate(-1, 1) ) );
      neighbors.push_back(lattice.cartesianPosition( latticeCoordinate( 1,-1) ) );
      neighbors.push_back(lattice.cartesianPosition( latticeCoordinate( 1, 0) ) );
      neighbors.push_back(lattice.cartesianPosition( latticeCoordinate( 1, 1) ) );
    }

    // template<typename MatrixLike1,
    //          typename MatrixLike2>
    // WignerSietzCellNeigbors(int                zeroPointIndex,
    //                         const MatrixLike2& nearestNeighbors,
    //                         const MatrixLike2& sites) {
    //   int numNeighbors = nRow(nearestNeighbors);
    //   for (int i=0; i< numNeighbors.size(); i++) {
    //     int s = nearestNeighbors(i,zeroPointIndex);
    //     if (s < 0) continue;
    //     neighbors.push_back(cartesianPosition( sites(0,s), sites(1,s)));
    //     neighbors.push_back(cartesianPosition(-sites(0,s),-sites(1,s)));
    //   }
    // }

    size_t size() const { return neighbors.size(); }
    
    const CartesianPositionType& operator[] (size_t i) const { return neighbors[i]; }
  };

  //----------------------------------------------------------------------
  template<int DIM>
  class MeshUtil {
  public:

    // // you get a square root that suqared will give you at least n pts.
    // static
    // int squareRootOrMore(int& n) {
    //   static int maxRootSize(1000);
    //   for (int i=0;i<maxRootSize;i++) {
    // 	if (i*i>=n) {
    // 	  n = i*i;
    // 	  return i;
    // 	}
    //   }
    //   n = maxRootSize * maxRootSize;
    //   return maxRootSize;
    // }

    // //======================================================================
    // // you get a square root that suqared will give you at least n pts.
    // static
    // int cubeRootOrMore(int& n) {
    //   static int maxRootSize(1000);
    //   for (int i=0;i<maxRootSize;i++) {
    // 	if (i*i*i>=n) {
    // 	  n = i*i*i;
    // 	  return i;
    // 	}
    //   }
    //   n = maxRootSize * maxRootSize * maxRootSize;
    //   return maxRootSize;
    // }

    static int numPtsOnSide   (int& totalNumPoints);
  };

  //----------------------------------------------------------------------
  template<typename T>
  void
  generateMeshPoints(int         numPoints, 
                     std::vector< CartesianPosition<T,2> >& points)    {
    
    static T SideLength = 2.0 * M_PI;
    
    int numberPtsOnSide     = MeshUtil<2>::numPtsOnSide(numPoints);
    int numPointsOnHalfSide = numberPtsOnSide / 2;
    if (numPointsOnHalfSide <=0 ) numPointsOnHalfSide = 1;
    T   increment           = SideLength / T(numPointsOnHalfSide + 1);

    // std::cout << "numPoints           :" << numPoints << "\n";
    // std::cout << "numberPtsOnSide     :" << numberPtsOnSide << "\n";
    // std::cout << "numPointsOnHalfSide :" << numPointsOnHalfSide << "\n";
    
    points.push_back(cartesianPosition<T>(0,0));
    
    for(size_t i=1; i <= size_t(numPointsOnHalfSide); i++) {
      points.push_back(cartesianPosition<T>( static_cast<T>(i) * increment,0.0) );
      points.push_back(cartesianPosition<T>(-static_cast<T>(i) * increment,0.0) );
    }
    
    for(size_t j=1; j <= size_t(numPointsOnHalfSide); j++) {
      points.push_back(cartesianPosition<T>(0.0, static_cast<T>(j) * increment) );
      points.push_back(cartesianPosition<T>(0.0,-static_cast<T>(j) * increment) );
    }
    
    for(size_t i=1; i <= size_t(numPointsOnHalfSide); i++) {
      for(size_t j=1; j <= size_t(numPointsOnHalfSide); j++) {
        
        points.push_back(cartesianPosition<T>( static_cast<T>(i) * increment,
                                               static_cast<T>(j) * increment));
        points.push_back(cartesianPosition<T>(-static_cast<T>(i) * increment,
                                               static_cast<T>(j) * increment));
        points.push_back(cartesianPosition<T>( static_cast<T>(i) * increment,
                                              -static_cast<T>(j) * increment));
        points.push_back(cartesianPosition<T>(-static_cast<T>(i) * increment,
                                              -static_cast<T>(j) * increment));
      }
    }
  }
  
  //----------------------------------------------------------------------

  template<typename T, size_t DIM, typename Algorithms>
  class WignerSeitzCell 
  {
  public:
    
    typedef Matrix<T>                   MatrixType;
    typedef CartesianTranslation<T,DIM> CartesianTranslationType;
    typedef CartesianTranslationType    CT;
    typedef CartesianPosition   <T,DIM> CartesianPositionType;
    typedef LatticeCoordinates  <DIM>   LatticeCoordinateTpe;

    typedef std::vector<CartesianTranslationType> BasisVectors;

    typedef Lattice<T,DIM,Algorithms>     LatticeType;

    typedef WignerSietzCellNeigbors<T,DIM,Algorithms> WignerSietzCellNeigborsType;

    int                         numPoints;
    //    int                         numPointsOnSide;

    const LatticeType&          lat;

    WignerSietzCellNeigborsType neighbors;


    //======================================================================
    
    WignerSeitzCell() = delete;

    WignerSeitzCell(const LatticeType& latt):
      numPoints      (0),
      //      numPointsOnSide(0),
      lat            (latt),
      neighbors      (lat)
    {
      // std::cout << "Producing WignerSeitzCell for the lattice: " << lat.parameters << " : \n";
      // std::cout << lat << "\n";
    }
    
    //======================================================================
    //  Returns true if the meshPoint pos is closest to the 0 k-vector 
    //  than to any other k-vector  and false otherwise. 
    //
    bool 
    isClosestToZero(const CartesianPositionType& pos)
    {
      static CartesianPositionType zero;

      T d0 = CT(pos - zero).length();

      for (int i=0; i< int(neighbors.size()); i++) {

	const CartesianPositionType& neighbor = neighbors[i];

	T K_dist  = CT(neighbor - pos).length();
	
	if ((d0-K_dist)>1e-8)  { // if distance to zero > distance another point,
	  return false;          // then return false given distance is not closest
	}
      }
      return true;	
    }
    
    //======================================================================
    
    static inline
    void setMeshPoint(MatrixType&           sitesKmesh,
		      int                            i,
		      const CartesianPositionType& pos) {
      
      if (DIM > 0) sitesKmesh(0,i) = pos[0];
      if (DIM > 1) sitesKmesh(1,i) = pos[1];
      if (DIM > 2) sitesKmesh(2,i) = pos[2];
    }
    
    //======================================================================
    //! Filters meshPoints into sitesKmesh such that only the mesh
    //  points closest to sitesK==0 will be considered

    void filterMeshPoints(const std::vector<CartesianPositionType>& meshPoints,
			  MatrixType&                               sitesKmesh)       // dim x numPoints
    {
      std::vector<CartesianPositionType> selectedPoints;
      
      for (size_t pt=0; pt<meshPoints.size();pt++) { // mesh point

	const CartesianPositionType& pos = meshPoints[pt];
        
	if (isClosestToZero(pos))      // if it is closest to zero,
	  selectedPoints.push_back(pos);    // then add this point
      }
      
      if (selectedPoints.size()<=0) {
	throw std::logic_error(__FILE__ 
			       "filterMeshPoints: "
			       "There are no points that survived the filtering process.\n");
      }
      
      // Note the probable reduction in the number of mesh points
      sitesKmesh.resize(DIM, selectedPoints.size());
      
      for (size_t i=0;i<selectedPoints.size();i++) 
	setMeshPoint(sitesKmesh, i, selectedPoints[i]);
    }
    
    //----------------------------------------------------------------------

    void setMesh(int numPts, MatrixType& meshPoints) {
      
      numPoints        = numPts;

      std::vector<CartesianPositionType> unfilteredPoints;
      generateMeshPoints(numPts,unfilteredPoints);
      filterMeshPoints(unfilteredPoints,meshPoints);
    }

    //----------------------------------------------------------------------
  };

  //----------------------------------------------------------------------
  
} // namespace   
#endif

  // template<typename T, typename Algorithms>
  // void
  // WignerSeitzCell<T,1, Algorithms>::
  // generateMeshPoints(int                                 numPoints, 
  //                    std::vector<CartesianPosition<T,1 > >& points)    {
    
  //   static T SideLength = 2.0 * M_PI;
    
  //   int numPointsOnHalfSide = numPoints/2;
  //   T   increment           = SideLength / T(numPointsOnHalfSide);
    
  //   points.push_back(cartesianPosition(0));
    
  //   for(size_t i=1; i <= numPointsOnHalfSide; i++) {
  //     points.push_back(cartesianPosition( static_cast<T>(i) * increment) );
  //     points.push_back(cartesianPosition(-static_cast<T>(i) * increment) );
  //   }
  // }
  
  // //======================================================================
  // //! Generate a [-nPI,nPI]x[-nPI,nPI]x[-nPI,nPI] [interval,square,cube] 
  // //  of mesh points
  // //  into a (dim x numPoints) matrix with row major offsets

  // void generateMeshPoints(MatrixType& meshPoints)
  // {
  //   meshPoints.resize(DIM,numPoints);

  //   T max_X = 0;
  //   for (size_t i=0; i<DIM;i++)
  //     if (std::abs(lat(0,i)) > max_X)
  //       max_X = std::abs(lat(0,i));

  //   T max_Y = 0;
  //   for (size_t i=0; i<DIM;i++)
  //     if (std::abs(lat(1,i)) > max_Y)
  //       max_Y = std::abs(lat(1,i));

  //   std::cout << "lat range: ["<<-max_X<<","<<max_X<< "] X ["<<-max_Y<<","<<max_Y<<"]\n";

  //   static T SideLength = 2.1 * std::max(max_X,max_Y);
  //   T   deltaPT         = SideLength / T(numPointsOnSide);
  //   T   shift           = -(SideLength/2.0) + deltaPT;

  //   //----------------------------------------------------------------------

  //   switch (DIM) {
  //   case 1: {
  //     for(size_t i=0; i< numPointsOnSide; i++) {
  //       T iFraction = T(i) / T(numPointsOnSide);
  //       meshPoints(0,i) = SideLength * iFraction + shift;
  //     }
  //     break;
  //   }
  //   case 2: {
  //     for(size_t i=0; i< numPointsOnSide; i++) {
  //       for(size_t j=0; j< numPointsOnSide; j++) {
	    
  //         int ptNum = i + numPointsOnSide*j;
	    
  //         T iFraction = static_cast<T>(i) / T(numPointsOnSide);
  //         meshPoints(0,ptNum) = SideLength * iFraction + shift;
	    
  //         T jFraction = static_cast<T>(j) / T(numPointsOnSide);
  //         meshPoints(1,ptNum) = SideLength * jFraction + shift;
  //       }
  //     }
  //     break;
  //   }
  //   case 3: {
	
  //     for(size_t i=0; i< numPointsOnSide; i++) {
  //       for(size_t j=0; j< numPointsOnSide; j++) {
  //         for(size_t k=0; k< numPointsOnSide; k++) {
	      
  //           int ptNum 
  //     	= i 
  //     	+ numPointsOnSide*j
  //     	+ numPointsOnSide*numPointsOnSide*k;
	      
  //           T iFraction = T(i) / T(numPointsOnSide);
  //           meshPoints(0,ptNum) = SideLength * iFraction + shift;
	      
  //           T jFraction = T(j) / T(numPointsOnSide);
  //           meshPoints(1,ptNum) = SideLength * jFraction + shift;
	      
  //           T kFraction = T(k) / T(numPointsOnSide);
  //           meshPoints(2,ptNum) = SideLength * kFraction + shift;
  //         }
  //       }
  //     }
  //     break;
  //   }
  //   }
  // }

  // //======================================================================
  // //  Returns true is thisMeshPoint is closest to the 0 k-vector 
  // //  than to any other k-vector  and false otherwise. 
  // //
  // static
  // bool 
  // isClosestToZero(const std::tuple<T,T,T>& thisMeshPoint, 
  //     	    const MatrixType&        sitesK)
  // {
  //   int dim       = sitesK.n_row();

  //   static T zero[3] = {0,0,0}; 

  //   T distToZero = distance(dim,thisMeshPoint,zero);
      
  //   for (size_t K_=0; K_ < sitesK.n_col(); K_++) { // Go through all of the K sites
	
  //     T distToK = distance(dim, thisMeshPoint, &sitesK(0,K_));
	
  //     // ~ same distance
  //     if ((distToZero-distToK) < 1e-8) return false;          

  //     if (distToZero > distToK) return false;          
  //   }
      
  //   return true;	
  // }

  // //======================================================================
  // //! Filters meshPoints into sitesKmesh such that only the mesh
  // //  points closest to sitesK==0 will be considered

  // void filterMeshPoints(const MatrixType& unfilteredMesh, 
  //     		  MatrixType&       meshPoints)       // dim x numPoints
  // {
  //   std::vector<int> selectedIndices(unfilteredMesh.n_col());
  //   selectedIndices.resize(0);
      
  //   for (size_t k_=0;k_<unfilteredMesh.n_col();k_++) { // mesh point
	
  //     const CartesianPositionType pos(&unfilteredMesh(0,k_));
	
  //     if ( isClosestToZero(pos) )  // if it is closest to zero,
  //       selectedIndices.push_back(k_);    // then add this point
  //   }
      
  //   if (selectedIndices.size()<=0) {
  //     throw
  //       std::logic_error("filterMeshPoints: "
  //                        "There are no points that survived the filtering process.\n");
  //   }
      
  //   // Note the probable reduction in the number of mesh points
  //   meshPoints.resize(DIM,selectedIndices.size());
      
  //   for (size_t i=0;i<selectedIndices.size();i++) {
  //     int pt = selectedIndices[i];
  //     for (size_t d=0;d<DIM;d++) 
  //       meshPoints(d,i) = unfilteredMesh(d,pt);
  //   }
  // }

		   
