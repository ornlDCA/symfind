//-*-C-*-    

#ifndef SymFind_CMatrix_CPP
#define SymFind_CMatrix_CPP

#include <errno.h>
#include <exception>
#include <stdexcept>

#include <stdlib.h>
#include "SymFind_CMatrix.h"

#define MAKE_MATRIX_Allocate(TypeName)					\
  									\
  void SymFind_mAllocate_##TypeName(SymFind_CMatrix_##TypeName* self,	\
				    int                   numEl) {	\
    self->ownData     = true;						\
    self->data        = 0;						\
    self->numElements = numEl;						\
    if (numEl > 0)							\
      self->data = (TypeName*) malloc(numEl*sizeof(TypeName));		\
  }									\
    									\
    void SymFind_mDropData_##TypeName(SymFind_CMatrix_##TypeName* self) { \
      if(self->ownData && self->data != 0) {				\
	free(self->data);						\
	self->lDim = 0;							\
	self->nRow = 0;							\
	self->nCol = 0;							\
	self->numElements = 0;						\
	self->data        = 0;						\
      }									\
    }									\
    									\
  
#define MAKE_MATRIX_SetTo(TypeName)					\
  									\
  void SymFind_mSetToVal_##TypeName(SymFind_CMatrix_##TypeName* self,	\
				    TypeName              initVal) {	\
    int numEl =  self->lDim*self->nCol;					\
    if (numEl == 0) return;						\
    for(int i=0; i< numEl; i++)						\
      (self->data)[i] = initVal;					\
  }									\
    									\
    void SymFind_mSetToOther_##TypeName(SymFind_CMatrix_##TypeName* self, \
					SymFind_CMatrix_##TypeName* other) { \
      int numEl      =  self->lDim*self->nCol;				\
      int otherNumEl =  other->lDim*other->nCol;			\
      for(int i=0; i< numEl; i++)					\
	if (i < otherNumEl)						\
	  (self->data)[i] =  (other->data)[i];				\
	else								\
	  (self->data)[i] = 0;						\
    }									\
    									\
    
#define MAKE_MATRIX_Initialize(TypeName)				\
  									\
  void SymFind_mInitialize_##TypeName(SymFind_CMatrix_##TypeName* self,	\
				      int ldim, int nrow, int ncol,	\
				      TypeName initVal) {		\
    self->lDim = ldim;							\
    self->nRow = nrow;							\
    self->nCol = ncol;							\
    									\
    SymFind_mAllocate_##TypeName(self,ldim*ncol);			\
    SymFind_mSetToVal_##TypeName(self,initVal);				\
  }									\
    									\
    SymFind_CMatrix_##TypeName*						\
    SymFind_make_matrix_##TypeName(int ld,int nr,int nc, TypeName i) {	\
      SymFind_CMatrix_##TypeName* result				\
	=								\
	(SymFind_CMatrix_##TypeName*)					\
	malloc(sizeof(SymFind_CMatrix_##TypeName));			\
      SymFind_mInitialize_##TypeName(result,ld,nr,nc,i);		\
      return result;							\
    }									\
    									\
    
#define MAKE_MATRIX_Assign(TypeName)   				        \
  									\
  void SymFind_mShallow_Assign_##TypeName(SymFind_CMatrix_##TypeName* self, \
					  SymFind_CMatrix_##TypeName* other) { \
    SymFind_mDropData_##TypeName(self);					\
    									\
    self->lDim        = other->lDim;					\
    self->nRow        = other->nRow;					\
    self->nCol        = other->nCol;					\
    self->numElements = other->numElements;				\
    self->data        = other->data;					\
    self->ownData     = false;						\
  }									\
    									\
    void SymFind_mDeep_Assign_##TypeName(SymFind_CMatrix_##TypeName* self, \
					 SymFind_CMatrix_##TypeName* other) { \
									\
      SymFind_mResize_##TypeName(self,other->lDim,other->nRow,other->nCol,0); \
      SymFind_mSetToOther_##TypeName(self,other);			\
    }									\
    									\
    void SymFind_mDestroy_##TypeName(SymFind_CMatrix_##TypeName* self) { \
      SymFind_mDropData_##TypeName(self);				\
      free(self);							\
    }									\
    									\
    
#define MAKE_MATRIX_Utils(TypeName)					\
  									\
  int SymFind_offset_##TypeName(SymFind_CMatrix_##TypeName* self, int r, int c) { \
    return r + self->lDim*c;						\
  } 									\
    									\
    TypeName* SymFind_mvGetPtr_##TypeName(SymFind_CMatrix_##TypeName* self, int i) { \
      if (i >= self->numElements) throw std::range_error("SymFind_mvGetPtr_" #TypeName);					\
      return &(self->data[i]);						\
    }									\
    									\
    TypeName SymFind_mvGet_##TypeName(SymFind_CMatrix_##TypeName* self, int i) { \
      if (i >= self->numElements) throw std::range_error("SymFind_mvGet_" #TypeName);					\
      return self->data[i];						\
    }									\
    									\
    TypeName* SymFind_mGetPtr_##TypeName(SymFind_CMatrix_##TypeName* self, int r, int c) { \
      int i = SymFind_offset_##TypeName(self,r,c);			\
	if (i >= self->numElements) throw std::range_error("SymFind_mGetPtr_" #TypeName); \
      return &(self->data[i]);						\
    }									\
    									\
    TypeName SymFind_mGet_##TypeName(SymFind_CMatrix_##TypeName* self, int r, int c) { \
      int i = SymFind_offset_##TypeName(self,r,c);			\
      if (i >= self->numElements) throw std::range_error("SymFind_mGet_" #TypeName);					\
      return self->data[i];						\
    }									\
									\
    void SymFind_mResize_##TypeName(SymFind_CMatrix_##TypeName* self,	\
				    int lDim, int nRow, int nCol, TypeName i) { \
									\
      if (not self->ownData) {						\
	throw std::logic_error("SymFind_mResize_" #TypeName " no own");    \
	return;								\
      }									\
      									\
      int newSize = lDim * nCol;					\
      if( newSize > self->numElements) {				\
	SymFind_mDropData_##TypeName(self);				\
	SymFind_mAllocate_##TypeName(self,newSize);			\
      }									\
      									\
      self->lDim = lDim;						\
      self->nRow = nRow;						\
      self->nCol = nCol;						\
      SymFind_mSetToVal_##TypeName(self,i);				\
    }									\
    									\
    
#define MAKE_MATRIX_CODE(TypeName)		\
  MAKE_MATRIX_Allocate(TypeName)		\
  MAKE_MATRIX_SetTo(TypeName)			\
  MAKE_MATRIX_Initialize(TypeName)		\
  MAKE_MATRIX_Assign(TypeName)			\
  MAKE_MATRIX_Utils(TypeName)			\

  MAKE_MATRIX_CODE(double)
  MAKE_MATRIX_CODE(float)
  MAKE_MATRIX_CODE(int)


#endif
 
