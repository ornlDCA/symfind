//-*-C++-*-    

#ifndef DCA_WEDGE_HEADER_H
#define DCA_WEDGE_HEADER_H

//! \file  Wedge.h
//! \brief Geometry/Symmetry

namespace SymFind {
  
  class Wedge_Association {
  public:
    size_t siteIndex;      
    size_t wedgeIndex;        
    size_t canonicalIndex;
    size_t symmetryIndex;
  };
  
  //======================================================================
  
  
  /* Essentially one crystal index / point from each orbit. */
  template<typename Field>
  class Wedge
  // vector of crystal indicies
  {
  public:

    typedef Field     FieldType;

    size_t                        identitySymmetryIndex;

    std::map<size_t,Wedge_Association>  crystal2Association;
    std::vector<size_t>                 wedge2crystal;
    std::vector<size_t>                 crystal2wedge;        /* Crystal indicies are mapped into the cspding wedge index. */
    IntMatrixType                       map2wedge_data;

    //======================================================================

    Wedge():
      crystal2Association(),
      map2wedge_data     (0,0,0,0)
    {}

    //======================================================================
    template<typename VectorLike, typename MatrixLike>
    void load(VectorLike& site2Wedge,
	      MatrixLike& wedge2Site) {

      site2Wedge.resize(crystal2wedge.size());
      for(size_t s=0; s< crystal2wedge.size(); s++)
	site2Wedge[s] = crystal2wedge[s];

      int maxWedgeSize(0);
      for(size_t w=0; w< wedge2crystal.size(); w++) {
	int count = 0;
	for(size_t s=0; s< size_t(site2Wedge.size()); s++)
	  if (site2Wedge[s] == int(w)) count++;
	if (count > maxWedgeSize) 
	  maxWedgeSize = count;
      }

      wedge2Site.resize(maxWedgeSize,maxWedgeSize,wedge2crystal.size(),-1);
      for(size_t w=0; w< wedge2crystal.size(); w++) {
	int count = 0;
	for(size_t s=0; s< size_t(site2Wedge.size()); s++)
	  if (site2Wedge[s] == int(w)) {
	    wedge2Site(count,w) = s;
	    count++;
	  }
      }
    }

    //! Returns the the wedge index of corresponding to siteIndex. 
    //! The given siteIndex must be in the wedge.
    inline
    size_t operator[](size_t wedgeIndex) const {
      return wedge2crystal[wedgeIndex];
    }

    //! Returns the the wedge index of corresponding to siteIndex. 
    //! The given siteIndex must be in the wedge.
    size_t indexFor(size_t siteIndex) const {
      return crystal2wedge[siteIndex];
    }
    //! Finds the transf. that puts siteIndex into the wedge,
    //! applies it to siteIndex2 and returns the index of the result.
    int map2wedge(size_t siteIndex,size_t siteIndex2) const { 
      return map2wedge_data(siteIndex,siteIndex2);  
    }
    
    bool wedge2CrystalContains(size_t siteIndex) const {
      for (size_t wedgeIndex=0; wedgeIndex < this->size(); wedgeIndex++) 
	if (siteIndex==wedge2crystal[wedgeIndex])
	  return true;
      return false;
    }
    
    // ---------------------------------------------------------------------- Initialization
    template<typename CrystalType>
    size_t getIdentitySymmetryIndex(const CrystalType&   crystal) {

      size_t numberOfSites      = crystal.sites.n_col();
      size_t numberOfSymmetries = crystal.symmetryGroupAction.n_row();

      for (size_t sym=0; sym < numberOfSymmetries; sym++) {
	for (size_t siteIndex=0; siteIndex < numberOfSites; siteIndex++) 
	  if (siteIndex != static_cast<size_t>(crystal.symmetryGroupAction(sym,siteIndex)))
	    break;
	return sym;
      }
      throw std::logic_error("Could not find a=n identity symmetry! in Wedge.getIdentitySymmetryIndex()");
    }
    
    // Inits the irreducible wedge
    // Later we will simply take one point from each orbit
    //
    template<typename CrystalType>
    void initialize(const CrystalType&   crystal) {
      
      int numberOfSites= crystal.sites.n_col();

      identitySymmetryIndex = getIdentitySymmetryIndex(crystal);
      
      crystal2wedge.resize(numberOfSites);

      for (size_t siteIndex=0; siteIndex< size_t(numberOfSites); siteIndex++) 
	makeAssociation(crystal, siteIndex);
      
      //! Finds the transf. that puts siteIndex into the wedge,
      //! applies it to siteIndex2 and sets map2wedge_data to the resulting index.
      
      map2wedge_data.resize(numberOfSites, numberOfSites);
      
      for (size_t siteIndex=0; siteIndex< size_t(numberOfSites); siteIndex++) {
	
	size_t sym = crystal2Association[siteIndex].symmetryIndex;
	for (size_t siteIndex2=0; siteIndex2< size_t(numberOfSites); siteIndex2++) 
	  map2wedge_data(siteIndex,siteIndex2) = crystal.symmetryGroupAction(sym, siteIndex2);
      }
    }
    
    //======================================================================

    template<typename CrystalType>
    void makeAssociation(const CrystalType&   crystal, size_t siteIndex) {
      
      // There is already an association in the map
      if (crystal2Association.count(siteIndex) > 0) return;
      
      int numberOfSymmetries = crystal.symmetryGroupAction.n_row();

      // We can find a symmetry that maps siteIndex into the wedge
      for (size_t sym=0; sym < size_t(numberOfSymmetries); sym++) {
	size_t transformedIndex = crystal.symmetryGroupAction(sym, siteIndex);
	try {
	  size_t wedgeIndex =  wedge2crystalContains(transformedIndex);
	  crystal2Association[siteIndex].wedgeIndex     = wedgeIndex;        // If the previous line did not throw.
	  crystal2Association[siteIndex].canonicalIndex = transformedIndex;
	  crystal2Association[siteIndex].siteIndex   = siteIndex;
	  crystal2Association[siteIndex].symmetryIndex  = sym;
	  crystal2wedge[siteIndex] = wedgeIndex;
	  return;
	}
	catch (std::range_error e)  {}
      }
      
      // Put siteIndex into the wedge
      size_t wedgeIndex = wedge2crystal.size();
      wedge2crystal.push_back(siteIndex);
      crystal2Association[siteIndex].wedgeIndex     = wedgeIndex;        // If the previous line did not throw.
      crystal2Association[siteIndex].canonicalIndex = siteIndex;
      crystal2Association[siteIndex].siteIndex      = siteIndex;
      crystal2Association[siteIndex].symmetryIndex  = identitySymmetryIndex;
      crystal2wedge[siteIndex]                      = wedgeIndex;
    }
  
    //======================================================================

    size_t wedge2crystalContains(size_t siteIndex) {

      for(size_t wedgeIndex=0; wedgeIndex < wedge2crystal.size(); wedgeIndex++) {

	if(siteIndex == wedge2crystal[wedgeIndex]) 
	  return wedgeIndex;
      }
      throw std::range_error("Wedge.findWedgeIndexFor");
    }
    
    //======================================================================
  };

  
  //======================================================================
  //
  template<typename JSN_WriterType, typename F>
  void toJSN(JSN_WriterType& writer,
	     const Wedge<F>& wedge)  
  {
    writer["name"]          = "Wedge";
    writer["wedge2crystal"] = wedge.wedge2crystal;
    //      writer["size"]          = wedge2crystal.size();
    writer["map2wedge_data"]= wedge.map2wedge_data;
    writer["crystal2wedge"] = wedge.crystal2wedge;
  }
    
  //======================================================================

} // namespace dca
#endif
