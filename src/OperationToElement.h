//-*-C++-*-

#ifndef  SymFind_OPERATION_TO_ELEMENT
#define  SymFind_OPERATION_TO_ELEMENT

/** \ingroup SymmetryConcepts */
/*@{*/

/** \file OperationtoElement.h
 *
 *  Contains a class for implementing symmetry operations.
 *
 *  \author Mike Summers
 *
 */
 

namespace SymFind {
  
  /**
   * \brief Return the a (canonical) point of this operation or throw
   *        an error.
   *
   *   The fixed point for:
   * 
   *    - the identity -> the origin
   *
   *    - a mirror     -> the a (or b) axis intercept
   *
   *    - a glide      -< the corresponding mirror 'fixed'point' 
   *
   * \note only 2D rotations have fixed points This should be else where
   */
  template<typename Field, size_t DIM, typename Algorithms>
  SeitzPosition<Field,DIM>
  fixedPoint(const SymmetryOperation<Field,DIM,Algorithms>& op) {

    typedef SeitzMatrix  <Field,DIM>                MatType;
    typedef SeitzPosition<Field,DIM>                SeitzPositionType;
    typedef SymmetryOperation<Field,DIM,Algorithms> SymmetryOperationType;

    static SeitzPositionType     zero;
    static SymmetryOperationType ident;
    SeitzPositionType            fixedPoint;

    MatType      symOp_ident         = op - ident;
    Field        detSymOp_ident      = Det(symOp_ident);

    if (Algorithms::close(detSymOp_ident,Field(0))) {
      throw std::range_error("SymmetryOperation Fixed Point");
      //later return a fixed point which is the cannonical offset
      // for the mirror or glide.
      //	return fixedPoint;
    }
    MatType      symOp_ident_inverse = symOp_ident.inverse();
    Multiply(symOp_ident_inverse, zero, fixedPoint);
    return fixedPoint;
  }

  //======================================================================
  
  /**
   * \brief Sets the translation part so that the operation is
   *        has the given point as a fixed point.
   *
   * \note This does not apply to glides.
   */
  template<typename Field, size_t DIM, typename Algorithms>
  void 
  setFixedPoint(SymmetryOperation<Field,DIM,Algorithms>& op,
		const SeitzPosition<Field,DIM>&          pos) {

    typedef SeitzMatrix  <Field,DIM>                MatType;
    typedef typename MatType::RotationType          RotationType;
    
    static RotationType identity;
    MakeIdentity(identity);
    
    RotationType i_r = identity - op.rotation;

    Multiply(i_r,pos,op.translation);
    
    //SeitzPositionType fPoint = fixedPoint();
    //      if (! (fPoint == pos) ) {
    // 	std::ostringstream buff;
    // 	buff << std::endl << "setFixedPoint(" << pos << "): " << std::endl;
    // 	buff << "      identity           = " << identity << std::endl;
    // 	buff << "      rotation           = " << this->rotation << std::endl;
    // 	buff << "      identity -rotation = " << i_r << std::endl;
    // 	buff << "      trans              = " << this->translation << std::endl;
    // 	buff << "      fPoint = " << fPoint << std::endl;
    // 	throw std::logic_error(buff.str());
    //       }
  }
   
  //======================================================================
 
    /**
     * \brief Return the glide vector for this operation.
     *
     * \note This assumes that the operation is either 
     *       a translation, a mirror or a glide.
     */
  template<typename Field, size_t DIM, typename Algorithms>
  SeitzTranslation<Field,DIM> 
  translationPart(const SymmetryOperation<Field,DIM,Algorithms>& op) {

    typedef SeitzMatrix  <Field,DIM>                 MatType;
    typedef SeitzTranslation<Field,DIM> SeitzTranslationType;

    static Field two(2);
    static Field zero(0);

    MatType sq;

    Multiply(op,op,sq);
    
    SeitzTranslationType result( sq.translation / two);

    for (size_t i=0; i< DIM; i++)
      if (Algorithms::close(result[i],zero)) result[i] = zero;
    
    return result;
  }
    
  //======================================================================

  /**
   * \brief Return the trace of this operation
   *
   */
  template<typename Field, size_t DIM, typename Algorithms>
  int
  sense(const SymmetryOperation<Field,DIM,Algorithms>& op,
	const SeitzPosition<Field,DIM>&        fixedPoint,
	const SeitzTranslation<Field,3>         givenAxis) {
    
    Field st = op.sinTheta(fixedPoint,givenAxis);
    int result = Algorithms::sign(st);

    return result;
  }

  //======================================================================
  /**
   * \brief Set values in the given symmetry element to reflect the given operation.
   *
   * \note This assumes that the operation type is either mirror or
   *       glide and that the translation part has already been set.
   */
  template<typename Field, typename LatticeType, typename Algorithms>
  void setRotationElements(SymmetryElement<Field,2,Algorithms>&         element,
			   const SymmetryOperation<Field,2,Algorithms>& cartesianOp,
			   const LatticeType& lat) {
    
    enum {DIM=2};

    typedef SymmetryOperation<Field,DIM,Algorithms> SymmetryOperationType;
    typedef CellPosition<Field,DIM,Algorithms>      CellPositionType;
    typedef CartesianPosition<Field,DIM>            CartesianPositionType;

    static Field                     zero(0);
    static Field                     minusTwo(-2);
    static Field                     minusOne(-1);
    static Field                     one(1);

    SymmetryOperationType            ident;
    CellPositionType                 zeroPos;
    static CellTranslation<Field,3>  zaxis     = cellTranslation<Field>(0,0,1);
    Field                            trace     = element.trace;
    //    SymmetryOperationType            cellOp;
    CartesianPositionType            cartesianPosition;

    // Use the lattice to translate to cell coordinates!
    //lat.cellOperation(cartesianOp, cellOp);

    // Set the cell position to the fixed-point
    Multiply((cartesianOp-ident).inverse(), zeroPos, cartesianPosition);
    element.cellPosition = lat.cellPosition(cartesianPosition);
    
    if (Algorithms::close(trace,minusTwo)) {
      element.type = "twoFold";
      //      element.rotationAngle = Field(180);
    }
    if (Algorithms::close(trace , minusOne)) {
      element.type = "threeFold";
      //      element.rotationAngle = Field(120);
    }
    if (Algorithms::close(trace , zero)) {
      element.type = "fourFold";
      //      element.rotationAngle = Field(90);
    }
    if (Algorithms::close(trace , one)) {
      element.type = "sixFold";
      //      element.rotationAngle = Field(60);
    }

    Field sinTheta_       = cartesianOp.sinTheta(cartesianPosition,zaxis);
    element.rotationAngle = degrees(asin(sinTheta_)); 
    element.sense         = Algorithms::sign(sinTheta_);

    if (element.sense == -1)
      element.type = element.type + "N";

  }

  //======================================================================
  /**
   * \brief Uses the given cell operation to set the element's
   *        position and netDirection.
   *
   * \note This assumes that the operation type is either mirror or
   *       glide.
   */
  template<typename Field, typename Algorithms>
  void setCanonicalPositionAndNetDirection(SymmetryElement<Field,2,Algorithms>&         element,
					   const SymmetryOperation<Field,2,Algorithms>& cellOp,
					   bool doNetDirection=true)
  {
    enum        {DIM=2};
    typedef CellPosition<Field,DIM,Algorithms>  CellPositionType;

    CellPositionType a = seitzPosition<Field,Field>(1,0);
    CellPositionType movedA;
    Multiply(cellOp,a,movedA);
    
    try {    // Make some data points so we can find the slope and intercepts

      CellPositionType origin;
      CellPositionType movedOrigin;
      Multiply(cellOp,origin,movedOrigin);
      
      element.cellPosition = mirrorLineIntercept(origin,movedOrigin,a,movedA);
      
      if (doNetDirection)
	element.netDirection = getNetDirectionFor(origin,movedOrigin,a,movedA);
    }
    catch (std::logic_error e) {  // try some other points
      
      CellPositionType b = seitzPosition<Field,Field>(0,1);
      CellPositionType movedB;
      Multiply(cellOp,b,movedB);
      
      element.cellPosition = mirrorLineIntercept(a,movedA,b,movedB);
      
      if (doNetDirection)
	element.netDirection = getNetDirectionFor(a,movedA,b,movedB);

      // If this catch fails again a logic_error will be thrown
    }
  }

  //======================================================================
  /**
   * \brief Return the x intercept of the mirror line assocaited with
   *        the cell operation that produced the given set of points.
   *
   * \note This assumes that the operation type producing the points
   *       is either a 2D mirror or a 2D glide.
   */
  template<typename Field, typename Algorithms>
  bool acceptableInterceptPt(const CellPosition<Field,2,Algorithms>&  p)
  {
    static Field zero(0);
    // On the x axis
    if (Algorithms::close(p[1],zero)) return true;
    // Not on the x axis but on the y axis
    if (Algorithms::close(p[0],zero)) return true;
    return false;
  }
  
  //======================================================================
  /**
   * \brief Return the x intercept of the mirror line assocaited with
   *        the cell operation that produced the given set of points.
   *
   * \note This assumes that the operation type producing the points
   *       is either a 2D mirror or a 2D glide.
   */
  template<typename Field, typename Algorithms>
  const CellPosition<Field,2,Algorithms> 
  mirrorLineIntercept(const CellPosition<Field,2,Algorithms>&  pt1,
		      const CellPosition<Field,2,Algorithms>&  movedPt1,
		      const CellPosition<Field,2,Algorithms>&  pt2,
		      const CellPosition<Field,2,Algorithms>&  movedPt2) 
  {
    enum {DIM=2};
    typedef CellPosition<Field,DIM,Algorithms> CellPositionType;
    
    // If pt1 did not move then it is on the mirrorLine 
    if (pt1.closeTo(movedPt1) && acceptableInterceptPt(pt1)) return pt1;
    
    // The points just swapped
    if (movedPt1.closeTo(pt2) && movedPt2.closeTo(pt1)) {
      const CellPositionType& biSect = bisector(pt1, pt2);
      if ( acceptableInterceptPt(biSect) )
	return biSect;
    }
    
    // get the intercept from the bisectors of the given points
    const CellPositionType p1(bisector(pt1, movedPt1 ));
    const CellPositionType p2(bisector(pt2, movedPt2 ));
    
    // If the bisectors are the same point
    if (p1.closeTo(p2)) {
      if (acceptableInterceptPt(p1))
	return p1;
      // The bisectors cannot be used to find the intercept
      throw std::logic_error("mirrorLineIntercept could not find a position");
    }
    
    return intercept<Field,Algorithms>(p1,p2);
  }

  //======================================================================
  /**
   * \brief Return the cell slope of the mirror line assocaited with
   *        the given cell operation.
   *
   * \note This assumes that the operation type is either mirror or
   *       glide.
   */
  template<typename Field, typename Algorithms>
  const LatticeCoordinates<2> getNetDirectionFor(const CellPosition<Field,2,Algorithms>&  pt1,
						 const CellPosition<Field,2,Algorithms>&  movedPt1,
						 const CellPosition<Field,2,Algorithms>&  pt2,
						 const CellPosition<Field,2,Algorithms>&  movedPt2) 
  {
    enum {DIM=2};
    typedef CellPosition<Field,DIM,Algorithms> CellPositionType;
    
    CellPositionType p1(bisector(pt1, movedPt1 ));
    CellPositionType p2(bisector(pt2, movedPt2 ));

    // If the bisectors are the same point
    if (p1.closeTo(p2)) 
      throw std::logic_error("mirrorLineXIntercept could not find a position");
    
    CellTranslation<Field,2> diff = p1 - p2;
    return latticeCoordinate(diff);
  }
  
  //======================================================================
  /**
   * \brief Set values in the given symmetry element to reflect the given operation.
   *
   * \note This assumes that the operation type is either mirror or
   *       glide and that the translation part has already been set.
   *
   * \note We are copying cartesianOp in so we can modify a temp of it.
   */
  template<typename Field, typename LatticeType, typename Algorithms>
  void setMirrorElements(SymmetryElement<Field,2,Algorithms>&         element,
			 const SymmetryOperation<Field,2,Algorithms>& cartesianOp,
			 const LatticeType&                           lat,
			 bool                                         verbose=false) {
    

    enum {DIM=2};

    typedef SymmetryOperation<Field,DIM,Algorithms>  SymmetryOperationType;
    typedef typename SymmetryOperationType::MatType  MatType;
    typedef typename MatType::TranslationType        TranslationType;

    typedef CellTranslation<Field,DIM>               CellTranslationType;

    static Field               two (2);
    static Field               zero(0);
    CellTranslation<Field,DIM> zeroDiff;
    SymmetryOperationType      cellOp;

    element.sense = 0;

    if(verbose) getTraceOut() << "   in setMirrorElements\n";

    // Use the lattice to translate to cell coordinates!
    lat.cellOperation(cartesianOp, cellOp);

    element.translationPart   = translationPart(cellOp);

    // Set the element type
    bool zeroTranslation = closeTo<Field,Field,DIM,0,Algorithms>(element.translationPart,zeroDiff);

    if(verbose) {
      getTraceOut() << "   zeroTranslation == " << zeroTranslation << "\n";
      getTraceOut() << "   element.trace   == " << element.trace   << "\n";
    }

    if(Algorithms::close(element.trace,two)) {
      // Set identity
      if (zeroTranslation) {
	element.type          = "identity";
	element.glideFraction = zero;
	return;
      }
      else {
	element.type          = "translation";
	element.netDirection  = latticeCoordinate(element.translationPart);
	element.setGlideFraction();
	return;
      }
    }

    if (zeroTranslation)  {
      element.type = "mirror";
      setCanonicalPositionAndNetDirection<Field,Algorithms>(element,cellOp);
      element.setGlideFraction();
      return;
    }
    else { 
      element.type = "glide";

      // Just use the mirror part of the glide operation to find the position
      COPY<TranslationType,
	CellTranslationType, 
	typename TranslationType::Traits, 
	typename CellTranslationType::Traits>::EXEC_MINUS(cellOp.translation, element.translationPart);
      
      setCanonicalPositionAndNetDirection<Field,Algorithms>(element,cellOp,false);

      element.netDirection  = latticeCoordinate(element.translationPart);
      element.setGlideFraction();
      return;
    }
  }

  //======================================================================
  /**
   * \brief Return an normalized Symmetry Element representing the
   *        given cartesian symmetry operation.
   *
   */
  template<typename Field, typename LatticeType, typename Algorithms>
  SymmetryElement<Field,2,Algorithms> 
  symmetryElement(const SymmetryOperation<Field,2,Algorithms>& cartesianOp,
		  const LatticeType&                           lat,
		  bool                                         verbose=false) {
    
    enum {DIM=2};
    typedef SymmetryOperation<Field,DIM,Algorithms> SymmetryOperationType;
    typedef SymmetryElement<Field,DIM,Algorithms>   SymmetryElementType;
    
    static Field                  zero(0);
    SymmetryOperationType         ident;
    SymmetryElementType           result;
    
    result.trace             = cartesianOp.trace();
    
    if (not Algorithms::close(Det(cartesianOp - ident), zero))  {  // has a fixed point
      if (verbose) 
	getTraceOut() << "not close(Det(cartesianOp - ident), zero)) => fixed point!\n";
      setRotationElements(result, cartesianOp, lat);
    }
    else { // no fixed point => ident mirror glide or translation
      if (verbose) 
	getTraceOut() << "close(Det(cartesianOp - ident), zero)) => no fixed point!\n";
      setMirrorElements(result, cartesianOp, lat, verbose);
    }
    result.unNormalizedName = SymmetryElementName<DIM>(result);

    if (verbose) {
      getTraceOut() << "---------------------------------------- Un_normalized element:\n";
      getTraceOut() << result;
    }
    
    normalize(result, verbose);

    result.name = SymmetryElementName<DIM>(result);
    return result;
  }

  //======================================================================

} /* namespace SymFind */

#endif   //SymFind_OPERATION_TO_ELEMENT

/*@}*/
