//-*-C-*-    

#ifndef SymFind_CCrystal_C
#define SymFind_CCrystal_C

#include <stdexcept>
#include <stdlib.h>
#include <math.h>
#include "SymFind_CCrystal.h"

extern "C" {

#define MakeSymFind_CCrystal_CODE(TypeName)				\
  									\
  void SymFind_CCrystal_Initialize_##TypeName(SymFind_CCrystal_##TypeName* self, \
					     int                         dim, \
					     SymFind_CCrystalTypes        t) { \
    									\
    self->initialized              = false;				\
    self->dimension                = dim;				\
    self->type                     = t;					\
    SymFind_mInitialize_##TypeName     (&self->primativeVectors,dim,dim,dim,0.0); \
    SymFind_mInitialize_##TypeName     (&self->latticeVectors,dim,dim,dim,0.0); \
    self->numberOfSites            = 0;					\
    SymFind_mInitialize_##TypeName     (&self->sites,0,0,0,0.0);	\
    self->piPointIndex             = 0;					\
    self->zeroPointIndex           = 0;					\
    SymFind_mInitialize_int            (&self->siteDiffToSite,     0,0,0,0); \
    SymFind_mInitialize_int            (&self->siteSumToSite,      0,0,0,0); \
    SymFind_mInitialize_int            (&self->symmetryGroupAction,0,0,0,0); \
    SymFind_mInitialize_int            (&self->fullGroupAction,    0,0,0,0); \
    self->numberOfSymmetries       = 0;					\
    SymFind_vInitialize_int            (&self->siteDegree,         0,0); \
    SymFind_mInitialize_##TypeName     (&self->neighborDistances,  0,0,0,0.0); \
    SymFind_mInitialize_int            (&self->nearestNeighbor,    0,0,0,0); \
    SymFind_vInitialize_int            (&self->site2Wedge,         0,0); \
    SymFind_mInitialize_int            (&self->wedge2Site,         0,0,0,0); \
  }									\
    									\
    void SymFind_CCrystal_DropData_##TypeName(SymFind_CCrystal_##TypeName* self) { \
									\
      SymFind_mDropData_##TypeName     (&self->primativeVectors);	\
      SymFind_mDropData_##TypeName     (&self->latticeVectors);		\
      SymFind_mDropData_##TypeName     (&self->sites);			\
      SymFind_mDropData_int            (&self->siteDiffToSite);		\
      SymFind_mDropData_int            (&self->siteSumToSite);		\
      SymFind_mDropData_int            (&self->symmetryGroupAction);	\
      SymFind_mDropData_int            (&self->fullGroupAction);	\
      SymFind_vDropData_int            (&self->siteDegree);		\
      SymFind_mDropData_##TypeName     (&self->neighborDistances);      \
      SymFind_mDropData_int            (&self->nearestNeighbor);        \
      SymFind_vDropData_int            (&self->site2Wedge);             \
      SymFind_mDropData_int            (&self->wedge2Site);             \
    }									\
    									\
    void SymFind_CCrystal_Deep_Assign_##TypeName(SymFind_CCrystal_##TypeName* self, \
					  	 SymFind_CCrystal_##TypeName* other) { \
      									\
      if(self == other) return;						\
      									\
      self->initialized         = other->initialized;			\
      self->dimension           = other->dimension;			\
      self->type                = other->type;				\
      									\
      SymFind_mDeep_Assign_##TypeName(&self->primativeVectors,		\
				      &other->primativeVectors);	\
      SymFind_mDeep_Assign_##TypeName(&self->latticeVectors,		\
				      &other->latticeVectors);		\
      SymFind_mDeep_Assign_##TypeName(&self->sites,			\
				      &other->sites);			\
      SymFind_mDeep_Assign_int(&self->siteDiffToSite,			\
			       &other->siteDiffToSite);			\
      SymFind_mDeep_Assign_int(&self->siteSumToSite,			\
			       &other->siteSumToSite);			\
      SymFind_mDeep_Assign_int(&self->symmetryGroupAction,		\
			       &other->symmetryGroupAction);		\
      SymFind_mDeep_Assign_int(&self->fullGroupAction,			\
			       &other->fullGroupAction);		\
      									\
      if (self->initialized) {						\
	self->numberOfSites      = other->numberOfSites;		\
	self->numberOfSymmetries = other->numberOfSymmetries;		\
	self->piPointIndex       = other->piPointIndex;			\
	self->zeroPointIndex     = other->zeroPointIndex;		\
	SymFind_vDeep_Assign_int(&self->siteDegree,			\
				 &other->siteDegree);			\
	SymFind_mDeep_Assign_int(&self->nearestNeighbor,        	\
				 &other->nearestNeighbor);		\
	SymFind_mDeep_Assign_##TypeName(&self->neighborDistances,	\
					&other->neighborDistances);     \
	SymFind_vDeep_Assign_int(&self->site2Wedge,      	        \
				 &other->site2Wedge);		        \
	SymFind_mDeep_Assign_int(&self->wedge2Site,      	        \
				 &other->wedge2Site);		        \
      }									\
    }									\
    									\
    int SymFind_equatingSymmetry_##TypeName(SymFind_CCrystal_##TypeName* self, \
					    int site, int site2) {	\
      									\
      for (int sym=0; sym<self->numberOfSymmetries; sym++) {		\
	int transformed = SymFind_mGet_int(&self->symmetryGroupAction,	\
					   sym,				\
					   site2);			\
	if (transformed == site)					\
	  return sym;							\
      }									\
      return -1;							\
    }									\
    									\
    void SymFind_initSiteDegrees_##TypeName(SymFind_CCrystal_##TypeName* self) { \
      									\
      SymFind_vResize_int(&self->siteDegree, self->numberOfSites,1);	\
      									\
      for (int site=0; site< self->numberOfSites; site++) {		\
	for (int site2=0; site2 < self->numberOfSites; site2++) {	\
	  if (SymFind_equatingSymmetry_##TypeName(self,site,site2) > 0)	\
	    (*SymFind_vGetPtr_int(&self->siteDegree,site))++;		\
	}								\
      }									\
    }									\
    									\
    int SymFind_indexFor_##TypeName(SymFind_CCrystal_##TypeName* self,	\
				    TypeName x, TypeName y) {		\
      									\
      if (self->dimension != 2) return -1;				\
      									\
      static TypeName eps(1e-4);					\
      									\
      for (int s=0; s < self->numberOfSites; s++) {			\
									\
	TypeName x_s = fabs(SymFind_mGet_##TypeName(&self->sites,0,s));	\
	TypeName y_s = fabs(SymFind_mGet_##TypeName(&self->sites,1,s));	\
									\
	if ( ( fabs(x_s  - x) < eps)					\
	     &&								\
	     ( fabs( y_s - y) < eps) )					\
	  return s;							\
      }									\
      return -1;							\
    }									\
    									\
    void SymFind_setPiPoint_##TypeName(SymFind_CCrystal_##TypeName* self) { \
      									\
      if (self->type == Space) {					\
	self->piPointIndex = -1;					\
	return;								\
      }									\
      									\
      if (self->dimension == 1) {					\
	self->piPointIndex = 0;						\
	return;								\
      }									\
      									\
      self->piPointIndex = SymFind_indexFor_##TypeName(self, M_PI, M_PI); \
    }									\
    									\
    void SymFind_setZeroPoint_##TypeName(SymFind_CCrystal_##TypeName* self) { \
      									\
      if (self->type == Space) {					\
	self->piPointIndex = -1;					\
	return;								\
      }									\
      									\
      if (self->dimension==1) {						\
	self->zeroPointIndex = int(self->numberOfSites/2.0);		\
	return;								\
      }									\
      									\
      self->zeroPointIndex = SymFind_indexFor_##TypeName(self, 0.0, 0.0); \
    }									\
    									\
    int getIdentitySymmetryIndex_##TypeName(SymFind_CCrystal_##TypeName* self) { \
      									\
      for (int sym=0; sym < self->numberOfSymmetries; sym++) {		\
	for (int s=0; s < self->numberOfSites; s++)			\
	  if (s != SymFind_mGet_int(&self->symmetryGroupAction,sym,s))	\
	    break;							\
	return sym;							\
      }									\
      return -1;							\
    }									\
    									\
    void SymFind_initAfterSymmetriesSetUp_##TypeName(SymFind_CCrystal_##TypeName* self) { \
      									\
      self->numberOfSites       = self->symmetryGroupAction.nCol;	\
      self->numberOfSymmetries  = self->symmetryGroupAction.nRow;	\
      SymFind_setPiPoint_##TypeName     (self);				\
      SymFind_setZeroPoint_##TypeName   (self);				\
      SymFind_initSiteDegrees_##TypeName(self);				\
      self->initialized         = true;					\
    }									\
    									\
    void SymFind_site_Index_2_vector_##TypeName(SymFind_CCrystal_##TypeName* self, \
						int siteIndex,		\
						SymFind_CVector_##TypeName* v)  { \
      									\
      for (int i=0; i<self->dimension; i++)				\
	*(SymFind_vGetPtr_##TypeName(v,i)) = SymFind_mGet_##TypeName(&self->sites, \
								     i,            \
								     siteIndex);   \
    }									\
    									\
    int SymFind_subtract_##TypeName(SymFind_CCrystal_##TypeName* self,	\
				    int                     posIndex1,	\
				    int                     posIndex2) { \
      return SymFind_mGet_int(&self->siteDiffToSite,			\
			      posIndex1,				\
			      posIndex2);				\
    }									\
    									\
    int SymFind_negate_##TypeName(SymFind_CCrystal_##TypeName* self,	\
				  int                     posIndex)  {	\
      return  SymFind_mGet_int(&self->siteDiffToSite,			\
			       posIndex,				\
			       self->zeroPointIndex);			\
    }									\
    									\
    int add_##TypeName(SymFind_CCrystal_##TypeName* self,		\
		       int posIndex1,					\
		       int posIndex2){					\
      return SymFind_mGet_int(&self->siteSumToSite,			\
			      posIndex1,				\
			      posIndex2);				\
    }									\
    									\
									\

  MakeSymFind_CCrystal_CODE(double)

  MakeSymFind_CCrystal_CODE(float)
  
}

#endif
 
