//-*-C++-*-

#ifndef  SymFind_SymmetryGroup
#define  SymFind_SymmetryGroup

/** \ingroup symmetryConcepts */
/*@{*/

/** \file Group.h
 *
 */  
namespace SymFind {

  template<typename Field, size_t DIM, typename Occupant,
	   template<typename, size_t, typename> class LatticeTemplate,
	   typename Algorithms>
  class Symmetry;

  template<typename Field, size_t DIM, 
	   typename Occupant, 
	   typename LatticeType,
	   typename Algorithms>
  class AppliedSymmetryElement;

  /** \ingroup symmetryConcepts 
   *
   * \brief The Group class
   *
   * \ note http://en.wikipedia.org/wiki/Group_action
   */
  template<typename Field, size_t DIM, typename Occupant,
	   template<typename, size_t, typename> class LatticeTemplate,
	   typename Algorithms>
  class SymmetryGroup
  {

    SymmetryGroup(const SymmetryGroup& other):
      numberOfOperations (other.numberOfOperations),
      numPatternPos      (other.numPatternPos),
      latpat             (other.latpat),
      multiplicationTable(other.multiplicationTable),
      group              (other.multiplicationTable.group),
      appliedElements    (other.multiplicationTable.appliedElements),
      groupAction        (appliedElements,latpat.pattern),
      orbits()
    {}
    

  public:

    typedef LatticeTemplate<Field,DIM,Algorithms>                             LatticeType;
    typedef LatticeWithPattern<Field,DIM,Occupant,LatticeType,Algorithms>     LatticeWithPatternType;
    typedef Pattern<Field,DIM,Occupant,Algorithms>                            PatternType;
    typedef CartesianPosition<Field,DIM>                                      CartesianPositionType;
    typedef CellPosition<Field,DIM, Algorithms>                               CellPositionType;

    typedef SymmetryGroup<Field,DIM,Occupant,LatticeTemplate,Algorithms>      ThisType;
    typedef Symmetry<Field,DIM,Occupant,LatticeTemplate,Algorithms>           SymmetryType;
    typedef AppliedSymmetryElement<Field,DIM,Occupant,LatticeType,Algorithms> AppliedSymmetryElementType;
    typedef std::vector<AppliedSymmetryElementType>                           AppliedSymmetryElementsType;
    typedef std::vector<AppliedSymmetryElementType*>                          AppliedSymmetryElementsPtrType;

    typedef std::pair<int,AppliedSymmetryElementType>                         AppliedSymmetryElementPairType;
    typedef std::map<std::string,AppliedSymmetryElementPairType>              GroupType;
    typedef SymmetryElement<Field,DIM,Algorithms>                             SymmetryElementType;
    typedef SymmetryOperation<Field,DIM,Algorithms>                           SymmetryOperationType;
    typedef TestPattern<Field,DIM,Occupant,LatticeType,Algorithms>            TestPatternType;
    typedef const std::pair<int,int>                                          IntPairType;
    typedef std::map<IntPairType,int>                                         MultiplicationTableMapType;
    typedef std::vector<int>                                                  PointPermutationType;

    typedef Orbits<Field>                                                     OrbitsType;
    typedef GroupAction<Field,DIM,Occupant,LatticeTemplate,Algorithms>        GroupActionType;

    typedef GroupMultiplicationTable<Field,DIM,Occupant,LatticeTemplate,Algorithms> GroupMultiplicationTableType;

    size_t                                   numberOfOperations;
    size_t                                   numPatternPos;

    const LatticeWithPatternType&            latpat;

    GroupMultiplicationTableType             multiplicationTable;
    const GroupType&                         group;           // kept in multiplication table
    const AppliedSymmetryElementsPtrType&    appliedElements; // kept in multiplication table, an index into group
    GroupActionType                          groupAction;
    OrbitsType                               orbits;
    /** \ingroup symmetryConcepts 
     *
     * \brief The SymmetryGroup class
     *
     * \note We cannot use latpat right away since it may be reduced
     *       after construction?  This also means that the
     *       initialization must be postponed.
     */
    SymmetryGroup(const LatticeWithPatternType&  lpat):
      numberOfOperations(0),
      numPatternPos(0),
      latpat(lpat),
      multiplicationTable(latpat),
      group(multiplicationTable.group),
      appliedElements(multiplicationTable.appliedElements),
      groupAction(appliedElements,latpat.pattern),
      orbits()
    {}
    
    /** \ingroup symmetryConcepts 
     *
     * \brief Initialize the SymmetryGroup with an itinial set of Applied Elements
     */
    void init(const AppliedSymmetryElementsType& initalAppliedElements,
	      std::string                        name)
    {
      multiplicationTable.init(initalAppliedElements, name);
      groupAction.init();
      orbits.init(appliedElements,groupAction);
    }

    /** \ingroup symmetryConcepts 
     *
     * \brief Initialize the SymmetryGroup with an itinial set of Applied Element Ptrs
     */
    void init(const AppliedSymmetryElementsPtrType& initalAppliedElements, std::string name )
    {
      multiplicationTable.init(initalAppliedElements, name);
      groupAction.init();
      orbits.init(appliedElements,groupAction);
    }

    //============================================================ 
    /** \ingroup symmetryConcepts 
     *
     * \brief 
     */
    void printGroup(std::ostream& os) {

      for (size_t i =0; i < appliedElements.size(); i++) {
	AppliedSymmetryElementType& appliedElement = *appliedElements[i];
	os << appliedElement.element.name << std::endl;
      }
    }
    
    //============================================================ 
    /** \ingroup symmetryConcepts 
     *
     * \brief 
     */
    AppliedSymmetryElementsPtrType pointGroupAppliedElements() const {
      
      AppliedSymmetryElementsPtrType result;
      CellPositionType               zeroPos;
      int numOps = appliedElements.size();
      
      for (int i =0; i < numOps; i++) {
	AppliedSymmetryElementType& appliedElement = *appliedElements[i];
	if (appliedElement.element.type == "translation") continue;
	if (appliedElement.element.type == "glide") continue;
	if (appliedElement.element.cellPosition.closeTo(zeroPos)) {
	  result.push_back(appliedElements[i]);
	}
      }
      return result;
    }
    
    //======================================================================
  };

} /** namespace spimag **/

#endif
/*@}*/
