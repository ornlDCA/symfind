//-*-C++-*-
#ifndef  SymFind_Cartesian_Translation
#define  SymFind_Cartesian_Translation

/** \ingroup extendedMatrices */
/*@{*/

/** \file CartesianTranslation.h
 *
 * Contains classes for implementing CartesianTranslation (a subclass of SeitzVector) objects.
 */
namespace SymFind {
  
  //============================================================ Generic
  /** \ingroup extendedMatrices
   *
   * \brief A class indicating that the translation is in the
   *        (implicit) cartesian system.
   */
  template<typename Field, size_t DIM> 
  class CartesianTranslation:
    public SeitzVector<Field, DIM, 0>
  {
  public:

    typedef          SeitzVector<Field,DIM,0> BaseType;
    typedef typename BaseType::ArrayType      ArrayType;
    typedef CartesianPosition<Field,DIM>      CartesianPositionType;

    // Constructors are never inherited :-(

    /** The Default Constructor produces the zero translation. */
    CartesianTranslation(): SeitzVector<Field, DIM, 0>() {}

    /** Construct a translation whose components are set to the given value. */
    template<typename IN_TYPE> CartesianTranslation(const IN_TYPE& val): SeitzVector<Field, DIM, 0>(val) {}

     /** Construct a translation whose components are set to the given value. */
    CartesianTranslation(const Vec<Field, DIM>& v):  SeitzVector<Field, DIM, 0>(v) {}

    /** Construct a translation whose components are set to the given value. */
    CartesianTranslation(const CartesianTranslation<Field, DIM>& v): SeitzVector<Field, DIM, 0>(v) {}

    /** Construct a translation whose components are set to the given value array. */
    CartesianTranslation(const ArrayType* vals):  SeitzVector<Field, DIM, 0>(vals) {}

    /** 
     * Return the length of the cartesian translation. 
     *
     * Convert data types as necessary.
     */
    Field length() const { return sqrt((*this) * (*this));  }

    CartesianPositionType plusOrigin() const {
      static CartesianPositionType origin = CartesianPositionType();
      return CartesianPositionType((*this) + origin);
    }
  };
  
  //======================================================================
  // 2 D
  //======================================================================
  
  template<typename Field, typename IN_TYPE>
  CartesianTranslation<Field,2> cartesianTranslation(IN_TYPE c0, IN_TYPE c1) {
    CartesianTranslation<Field,2> result;
    result[0] = convert<Field>(c0);
    result[1] = convert<Field>(c1);
    return result;
  }
  
  //======================================================================
  // 3 D
  //======================================================================
  
  template<typename Field, typename IN_TYPE>
  CartesianTranslation<Field,3> cartesianTranslation(IN_TYPE c0, IN_TYPE c1, IN_TYPE c2) {
    CartesianTranslation<Field,3> result;
    result[0] = convert<Field>(c0);
    result[1] = convert<Field>(c1);
    result[2] = convert<Field>(c2);
    return result;
  }

  //======================================================================

}  /* namspace SymFind */

#endif // SymFind_Cartesian_Translation
/*@}*/
