//-*-C++-*-
#ifndef  SymFind_CellTranslation
#define  SymFind_CellTranslation

/** \ingroup extendedMatrices */
/*@{*/

/** \file CellTranslation.h
 *
 * Contains a class for implementing CellTranslation (SeitzTranslation subclass) objects.
 */
namespace SymFind {

  template<typename Field, size_t DIM, typename Algorithms> class CellPosition;
  template<typename Field, size_t DIM>                      class CellTranslation;
  
  // template<typename T, typename Alg>
  // bool near( const CellTranslation<T,1>& lhs, const CellTranslation<T,1>& rhs );
  // template<typename T, typename Alg>
  // bool near( const CellTranslation<T,2>& lhs, const CellTranslation<T,2>& rhs );
  // template<typename T, typename Alg>
  // bool near( const CellTranslation<T,3>& lhs, const CellTranslation<T,3>& rhs );

  //======================================================================
  //
  /** \ingroup extendedMatrices
   *
   * \brief A marker class indicating that the Translation indicates a direction from within a Cell.
   *
   * These can be converted to  and from MillerIndices for a given Cell.
   */
  template<typename Field, 
	   size_t DIM> 
  class CellTranslation:
    public SeitzVector< Field, DIM, 0 > 
  {
  public:
    
    typedef          CellTranslation<Field,DIM>  ThisType;
    typedef          SeitzVector<Field,DIM,0>    BaseType;
    typedef typename BaseType::ArrayType         ArrayType;

    /** The Default Constructor produces the zero translation. */
    CellTranslation(): BaseType() {}

    /** Construct a translation whose components are set to the given value. */
    //template<typename IN_TYPE> CellTranslation(const IN_TYPE& val): SeitzVector<Field, DIM, 0>(val) {}
    
    /** Construct a translation whose components are set to the given value. */
    CellTranslation(const Vec<Field, DIM>& v):  BaseType(v) {}
    
    /** Construct a translation whose components are set to the given value. */
    CellTranslation(const BaseType& v):  BaseType(v) {}
    
    /** Construct a translation whose components are set to the given value. */
    CellTranslation(const ThisType& v): BaseType(v) {}
    
    /** Construct a translation whose components are set to the given value array. */
    CellTranslation(const ArrayType& vals):  BaseType(vals) {}
    
    /** Construct a translation whose components are set to the given value array. */
    template<typename Algorithms>
    explicit CellTranslation(const CellPosition<Field,DIM,Algorithms> p):  BaseType(p) {}

    ThisType& operator = (const ThisType& v)
    { 
      const BaseType& bv = v;
      BaseType& bt = (*this);
      bt           = bv;
      return *this; 
    }

    //----------------------------------------------------------------------
  };

  //======================================================================
  /**
   * \brief The vector times a scalor.
   */
  template<typename Field,size_t DIM>
  CellTranslation<Field,DIM>  operator * (const CellTranslation<Field,DIM>& lhs, Field scalar){
    CellTranslation<Field,DIM> result;
    for (size_t i=0; i< DIM;i++)
      result[i] = lhs[i] * scalar;
    return result;
  }

  template<typename Field, typename IN_TYPE> 
  CellTranslation<Field,2> cellTranslation(IN_TYPE t0, IN_TYPE t1) {
    CellTranslation<Field,2> result;
    result[0] = convert<Field>(t0);
    result[1] = convert<Field>(t1);
    return result;
  }

  template<typename Field, typename IN_TYPE> 
  CellTranslation<Field,3> cellTranslation(IN_TYPE t0, IN_TYPE t1, IN_TYPE t2) {
    CellTranslation<Field,3> result;
    result[0] = convert<Field>(t0);
    result[1] = convert<Field>(t1);
    result[2] = convert<Field>(t2);
    return result;
  }

  //====================================================================== near

  template<typename T1, typename T2>
  typename std::enable_if<(IsInCellCoordinates<1,T1>::value
			   and
			   IsInCellCoordinates<1,T2>::value),
			  bool>::type
  near ( const T1& lhs,
	 const T2& rhs ) {
    return ( close(lhs[0],rhs[0]));
  }
  
  //----------------------------------------------------------------------

  template<typename T1, typename T2>
  typename std::enable_if<(IsInCellCoordinates<2,T1>::value
			   and
			   IsInCellCoordinates<2,T2>::value),
			  bool>::type
  near ( const T1& lhs,
	 const T2& rhs ) {
    
    return ( close(lhs[0],rhs[0])
	     and
	     close(lhs[1],rhs[1]) );
  }
  
  //----------------------------------------------------------------------

  template<typename T1, typename T2>
  typename std::enable_if<(IsInCellCoordinates<3,T1>::value
			   and
			   IsInCellCoordinates<3,T2>::value),
			  bool>::type
  near ( const T1& lhs,
	 const T2& rhs ) {
    
    return ( close(lhs[0],rhs[0])
	     and
	     close(lhs[1],rhs[1])
	     and
	     close(lhs[2],rhs[2]) );
  }
  
  //====================================================================== exceeds

  template<typename T1, typename T2>
  typename std::enable_if<(IsInCellCoordinates<1,T1>::value
			   and
			   IsInCellCoordinates<1,T2>::value),
			  bool>::type
  exceeds ( const T1& lhs,
	    const T2& rhs ) {
    return ( double (std::abs(lhs[0])) > double (std::abs(rhs[0])) );
  }
  
  //----------------------------------------------------------------------

  template<typename T1, typename T2>
  typename std::enable_if<(IsInCellCoordinates<2,T1>::value
			   and
			   IsInCellCoordinates<2,T2>::value),
			  bool>::type
  exceeds ( const T1& lhs,
	    const T2& rhs ) {
    
    return ( double (std::abs(lhs[0])) > double (std::abs(rhs[0])) 
	     or
	     double (std::abs(lhs[1])) > double (std::abs(rhs[1])) );
  }
  
  //----------------------------------------------------------------------

  template<typename T1, typename T2>
  typename std::enable_if<(IsInCellCoordinates<3,T1>::value
			   and
			   IsInCellCoordinates<3,T2>::value),
			  bool>::type
  exceeds ( const T1& lhs,
	 const T2& rhs ) {
    
    return ( double (std::abs(lhs[0])) > double (std::abs(rhs[0])) 
	     or
	     double (std::abs(lhs[1])) > double (std::abs(rhs[1])) 
	     or
	     double (std::abs(lhs[2])) > double (std::abs(rhs[2]))  );
  }
  
}
  
#endif 
/*@}*/
