//-*-C++-*-

#ifndef  SymFind_SYMMETRY_ELEMENT_TO_OPERATION
#define  SymFind_SYMMETRY_ELEMENT_TO_OPERATION

/** \ingroup symmetryConcepts */
/*@{*/

/** \file Symmetryelement.h
 *
 *  Contains a class for implementing symmetry elements.
 *
 *  \author  Mike Summers
 *
 */
 
namespace SymFind {
  
  //======================================================================
  /** 
   * \brief Return the symmetry operation corresponding to this
   *        element in a given lattice.
   */

  template<typename Field, size_t DIM, typename Algorithms>
  SymmetryOperation<Field,DIM,Algorithms> 
  operationFor 
  (const SymmetryElement<Field,DIM,Algorithms>& element,
   const Lattice<Field,DIM,Algorithms>&         lattice)
  {
    typedef SymmetryOperation<Field,DIM,Algorithms> SymmetryOperationType;

    SymmetryOperationType result;
    bool                  resultSet(false);
    
    if (element.type == "identity") {
      resultSet = true;
      result =  IdentityElement<Field,DIM,Algorithms>::operationFor(element,lattice);
    }

    if (element.type == "translation") {
      result = IdentityElement<Field,DIM,Algorithms>::operationFor(element,lattice);
      result.setTranslation(lattice.cartesianTranslation(element.translationPart));
      result.name = element.name;
      resultSet = true;
    }
    if (element.type == "twoFold") {
      resultSet = true;
      result =  TwoFold<Field,DIM,Algorithms>::operationFor(element,lattice);
    }
    if (element.type == "threeFold") {
      resultSet = true;
      result =  ThreeFold<Field,DIM,Algorithms>::operationFor(element,lattice);
    }
    if (element.type == "threeFoldN") {
      resultSet = true;
      result =  ThreeFoldN<Field,DIM,Algorithms>::operationFor(element,lattice);
    }
    if (element.type == "fourFold") {
      resultSet = true;
      result =  FourFold<Field,DIM,Algorithms>::operationFor(element,lattice);
    }
    if (element.type == "fourFoldN") {
      resultSet = true;
      result =  FourFoldN<Field,DIM,Algorithms>::operationFor(element,lattice);
    }
    if (element.type == "sixFold") {
      resultSet = true;
      result =  SixFold<Field,DIM,Algorithms>::operationFor(element,lattice);
    }
    if (element.type == "sixFoldN") {
      resultSet = true;
      result =  SixFoldN<Field,DIM,Algorithms>::operationFor(element,lattice);
    }
    if (element.type == "glide") {
      resultSet = true;
      result =  Glide<Field,DIM,Algorithms>::operationFor(element,lattice);
    }
    if (element.type == "mirror") {
      resultSet = true;
      result =  Mirror<Field,DIM,Algorithms>::operationFor(element,lattice);
    }
    if ( resultSet ) {
      element.circleCheck(result, lattice);;
      return result;
    }
      
    std::ostringstream buff;
    buff << "In SymmetryElement.oparationFor, SymmetryElement type (=" << element.type << ") not found!";
    buff << "   " << (element) << std::endl;
    throw std::logic_error(buff.str());
  }

} /* namespace SymFind */

#endif   //SymFind_SYMMETRYELEMENT_TO_OPERATION

/*@}*/
