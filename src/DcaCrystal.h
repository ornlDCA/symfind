//-*-C++-*-    

#ifndef DCA_CRYSTAL_HEADER_H
#define DCA_CRYSTAL_HEADER_H
namespace SymFind {

/*   // Forwards */
/*   class Crystal; */
/*   Tag toXML(const Crystal& crystal); */

//  using namespace psimag;

  typedef enum {Momentum,Space} CrystalTypes;

  typedef double TheValueType;
  typedef dca::Matrix<TheValueType> TheMatrixType;
  typedef dca::Matrix<int>          IntMatrixType;
  class CrystalMembers {

    bool         initialized;
    size_t       dimension;
    CrystalTypes type;
    size_t       numberOfSites;        /*! Inited:     The number of sites in this crystal.                  */
    size_t       numberOfSymmetries;   /*  Inited:                   */
    int          piPointIndex;         /*! Inited:     If set, the index of (PI,PI,...) for this geometry, (usually only applies to k-space geometries.) */
    int          zeroPointIndex;       /*! Inited:     If set, the index of (0,0,  ...) for this geometry, (usually only applies to k-space geometries.) */
  };

  /*! Geometry/Symmetry Class
    
  Instances of this class can represent crystals of either one or two dimensional geometries.
    
  */
  class ICrystal {
	     
  public:
    //======================================================================
    
    static inline
    size_t byteSize(size_t numElements, 
		    size_t numSymmetries,
		    size_t dim) {
      return  
	sizeof(CrystalMembers) +
	dca::Matrix<TheValueType>::byteSize(dim,dim) + // latticeVectors
	dca::Matrix<TheValueType>::byteSize(dim,dim) +  // latticeVectors
	
	dca::Matrix<int>::byteSize(numElements,  numElements) +  // siteDiffToSite
	dca::Matrix<int>::byteSize(numElements,  numElements) +  // siteAddToSite
	dca::Matrix<int>::byteSize(numSymmetries,numElements); // symmetryGroupAction
    }
    
    //======================================================================

    //    typedef enum {Momentum,Space}                       CrystalType;

    typedef TheValueType                                FieldType;
    //    typedef std::vector<std::vector<int> >              OrbitsType;  // Don't want to use psimag::orbits because of dependencies.
    typedef GroupActionMatrixType                       IntMatrixType;
    typedef TheMatrixType                               FieldMatrixType;
    typedef ICrystal                                    ThisType;
    typedef Wedge<FieldType>                            WedgeType;

    bool                   initialized;

    size_t                 dimension;
    CrystalTypes           type;

    //	     std::vector<FieldType> phase;
    
    FieldMatrixType       latticeVectors  /*  From Input:               */;

    size_t                numberOfSites;        /*! Inited:     The number of sites in this crystal.                  */
    FieldMatrixType       sites;                /*! From Input: The positions of the sites. */
    int                   piPointIndex;         /*! Inited:     If set, the index of (PI,PI,...) for this geometry, (usually only applies to k-space geometries.) */
    int                   zeroPointIndex;       /*! Inited:     If set, the index of (0,0,  ...) for this geometry, (usually only applies to k-space geometries.) */
    
    // Precomputed tables from input
    //
    IntMatrixType         siteDiffToSite;       /*! From Input: Index table mapping site differences into the cspding site. */
    IntMatrixType         siteSumToSite;        /*! From Input: Index table mapping site sum into the cspding site. */
    IntMatrixType         symmetryGroupAction;  /*! From Input: Space Group Action, For each symmetry a row giving the cspding crystal permutation. */

    //The full symmetry group action needed?
    IntMatrixType         fullGroupAction;

    // Information regarding the crystals symmetries (note we may be using only the point group here
    //
    size_t                numberOfSymmetries;   /*  Inited:                   */
    std::vector<int>      siteDegree;           /*  Inited:     The number of points that map into the given point. */

    // Information regarding the crystal's irreducable wedge
    //
    WedgeType             wedge            /*  Inited:                   */;
    Neighbors             neighbors        /*  Inited:                   */;

    //======================================================================

    ICrystal(size_t                 dim, 
	    CrystalTypes           t): 
      initialized        (false),
      dimension          (dim),
      type               (t),
      latticeVectors     (dim,dim),
      numberOfSites      (0),
      sites              (0,0,0,FieldType(0)),
      piPointIndex       (0),
      zeroPointIndex     (0),
      siteDiffToSite     (0,0,0,int(0)),
      siteSumToSite      (0,0,0,int(0)),
      symmetryGroupAction(0,0,0,int(0)),
      fullGroupAction    (0,0,0,int(0)),
      numberOfSymmetries (0),
      siteDegree         (0),
      wedge              (),
      neighbors          ()
    {}
    
    //======================================================================

    ThisType& operator = (const ThisType& other) {

      //	       if(this == &other) return; // No need to copy
      initialized         = other.initialized;

      // Essential Input
      dimension           = other.dimension;
      type                = other.type;
      latticeVectors      = other.latticeVectors;
      sites               = other.sites;
      siteDiffToSite      = other.siteDiffToSite;
      siteSumToSite       = other.siteSumToSite;
      symmetryGroupAction = other.symmetryGroupAction;

      // Un-needed?
      fullGroupAction      = other.fullGroupAction;

      // This is done here so that there is no need to copy
      // full DcaCrystal to all cpus.

      if (initialized) {
	numberOfSites      = other.numberOfSites;
	numberOfSymmetries = other.numberOfSymmetries;
	piPointIndex       = other.piPointIndex;
	zeroPointIndex     = other.zeroPointIndex;
	wedge              = other.wedge;
	siteDegree         = other.siteDegree;
	neighbors          = other.neighbors;
      }
      else
	init();

      return *this;
    }

    //======================================================================

    // Call this after the symmetries, etc have been set up.
    void init() {
	       
      numberOfSites      = symmetryGroupAction.n_col();
      numberOfSymmetries = symmetryGroupAction.n_row();
//       setPiPoint();
//       setZeroPoint();
      wedge.init(*this);
      initSiteDegrees();
      neighbors.loadFrom(*this);
      initialized = true;
    }

    //======================================================================

    std::string name() const { 	   
      std::ostringstream nam;
      if(type == Momentum)
	nam << "Momentum";
      else
	nam << "Space";
      nam << "Crystal";
      return nam.str();
    }

    //----------------------------------------------------------------------

    template<typename ParallelProcessingType, typename GroupingType>
    int getBufferSize(const ParallelProcessingType& parallelProcessing,
		      GroupingType groupingType) const {
      
      int result(0);
      
      result += parallelProcessing.getBufferSize(dimension,       groupingType);
      result += parallelProcessing.getBufferSize(latticeVectors,  groupingType);
      result += parallelProcessing.getBufferSize(sites,           groupingType);
      result += parallelProcessing.getBufferSize(siteDiffToSite,  groupingType);
      result += parallelProcessing.getBufferSize(siteSumToSite,   groupingType);
      result += parallelProcessing.getBufferSize(symmetryGroupAction, groupingType);

      return result;
    }

    template<typename ParallelProcessingType, typename GroupingType>
    void pack(const ParallelProcessingType& parallelProcessing,
	      int*                    buffer,
	      int                     bufferSize,
	      int&                    position,
	      GroupingType            groupingType) {

      parallelProcessing.pack(buffer, bufferSize, position, dimension,       groupingType);
      parallelProcessing.pack(buffer, bufferSize, position, latticeVectors,  groupingType);
      parallelProcessing.pack(buffer, bufferSize, position, sites,           groupingType);
      parallelProcessing.pack(buffer, bufferSize, position, siteDiffToSite,  groupingType);
      parallelProcessing.pack(buffer, bufferSize, position, siteSumToSite,   groupingType);
      parallelProcessing.pack(buffer, bufferSize, position, symmetryGroupAction,  groupingType);
    }

    template<typename ParallelProcessingType, typename GroupingType>
    void unpack(const ParallelProcessingType& parallelProcessing,
		int*                    buffer,
		int                     bufferSize,
		int&                    position,
		GroupingType            groupingType) {
      
      parallelProcessing.unpack(buffer, bufferSize, position, dimension,       groupingType);
      parallelProcessing.unpack(buffer, bufferSize, position, latticeVectors,  groupingType);
      parallelProcessing.unpack(buffer, bufferSize, position, sites,           groupingType);
      parallelProcessing.unpack(buffer, bufferSize, position, siteDiffToSite,  groupingType);
      parallelProcessing.unpack(buffer, bufferSize, position, siteSumToSite,   groupingType);
      parallelProcessing.unpack(buffer, bufferSize, position, symmetryGroupAction,  groupingType);

    }

    //============================================================

    //! Dimension of this Crystal.
    int dim() const { 
      return dimension; 
    }

    //! Returns the volume (number of points) of this Geometry
    int volume() const {
      return numberOfSites;
    }
	
    //! Returns in v the coordinates of the crystal point
    // &*&*&* Later change transpose the the sites storage so that 
    // sites[i,numSites]
    // Then return a wrapped vector from site[i]

    template<typename VectorLikeType>
    void index2vector(int siteIndex, VectorLikeType& v) const {
      typedef typename VectorLikeType::value_type VectorField;
      for (size_t i=0; i<dimension; i++)
	v[i] = static_cast<VectorField>(sites(siteIndex,i));
    }
  
    // 	     //! Returns in v the coordinates of the crystal point
    // 	     template<typename MatrixField>
    // 	     void siteCopy(int siteIndex, MatrixTemplate<MatrixField> &m) const {
    // 	       for (size_t i=0; i<dimension; i++)
    // 		 m(siteIndex,i) = static_cast<MatrixField>(sites(siteIndex,i));
    // 	     }
  
    // 	     //! Returns in v the coordinates of the crystal point
    // 	     void sitesCopy(MatrixTemplate<Field> &m) const {
    // 	       for(size_t site=0; site < numberOfSites; site++)
    // 		 for (size_t i=0; i<dimension; i++)
    // 		   m(site,i) = sites(site,i);
    // 	     }
  
    // 	     //! Index of (PI,PI,...) for this (K-SPACE) Geometry
    // 	     int icKpi() const {
    // 	       return piPointIndex; 
    // 	     }
	
    //----------------------------------------------------------------------

    //! Given two crystal points (by index) substract them 
    // (posIndex2 - posIndex1) = siteDiffToSite(posIndex1,posIndex2)
    // posIndex2 = posIndex1 + siteDiffToSite(posIndex1,posIndex2)
    // and returns the corresponding index.
    // => 
    int subtract(size_t posIndex1, size_t posIndex2) const {
      return siteDiffToSite(posIndex1,posIndex2);
    }

    //! Given a crystal points (by index) substracts it from the zero point 
    //  and returns the corresponding index.
    int negate(size_t posIndex) const {
      return siteDiffToSite(posIndex,zeroPointIndex);
    }

    //! Given two geometry points (by index) adds them and returns the corresponding index.
    int add(size_t posIndex1,size_t posIndex2) const {
      return siteSumToSite(posIndex1,posIndex2);
    }

    //----------------------------------------------------------------------
  
    template<template<typename T, typename A> class VectorTemplate,
	     typename ScalarType, typename A>
    inline
    size_t index(const VectorTemplate<ScalarType, A>& thisvector) const {
			
      static ScalarType eps(1e-4);
			
      for (size_t siteIndex=0; siteIndex<numberOfSites; siteIndex++) {
	bool equal = true;
	for(size_t i=0; i<dimension;i++) 
	  if (fabs(sites(siteIndex,i) - thisvector[i]) > eps)
	    equal=false;
	if(equal)
	  return siteIndex;
      }
      std::ostringstream message;
      message << "Crystal.index(";
      for (size_t i=0; i<thisvector.size(); i++) 
	message << thisvector[i] << " ";
      message << ")  could not find an index for the given vector! " << std::endl;
      throw std::logic_error(message.str());
    } 
    
    //----------------------------------------------------------------------
		
    int parity(int i) const {

      std::vector<FieldType> v(dimension);

      FieldType sum(0);
      sites.getRow(i,v);
      for (size_t j=0;j<v.size();j++) 
	sum += v[j];
      if (int(sum) % 2==0) 
	return 1;
      return 0;
    }
		
    //======================================================================

    // 	     //! Number of symmetry operations for this Real Geometry.
    // 	     int nGroup() const {
    // 	       return numberOfSymmetries; 
    // 	     }

    //! Applies the specified transformation to the specified point i
    //! and return the index of the resulting point.
    size_t symmetryEquivalentPoint(size_t crystalIndex, size_t transformIndex) const { 
      return symmetryGroupAction(transformIndex,crystalIndex); 
    }

    // 	     //! Applies the specified transformation to the specified point i
    // 	     //! and return the index of the resulting point.
    // 	     size_t icequ(size_t crystalIndex,size_t transformIndex) const { 
    // 	       return symmetryGroupAction(transformIndex,crystalIndex); 
    // 	     }
  
    //----------------------------------------------------------------------

    // If there is a transformation that takes site2 into site
    // (Tsite2=site) return it otherwise throw an range error.
    inline
    size_t equatingSymmetry(size_t site, size_t site2) {
      
      for (size_t sym=0; sym<numberOfSymmetries; sym++) {
	size_t transformed = symmetryGroupAction(sym,site2);
	if (transformed == site) 
	  return sym; // yes, there is, site2 is equiv. to site
      }
      throw std::range_error("not symmetrically equivalent");
    }

    // 	     //-------------------------------------------------- Mesh
    
    // 	     void getMeshVector(int i, std::vector<Field> &v) const {
    // 	       for (size_t j=0;j<v.size();j++) 
    // 		 v[j] = mesh(i,j);
    // 	     }
    
    // 	     // Fine mesh interface:
    // 	     size_t sizeOfMesh() const {
    // 	       return mesh.n_row();
    // 	     }
    
    //======================================================================

	     

  private:
	
    //! Initializes siteDegree internal table. Wedge must be already init.
    inline void initSiteDegrees() {

      siteDegree.resize(numberOfSites);
      
      for (size_t site=0; site<numberOfSites; site++) {
	
	// how many points site2 are equivalent to site?
	for (size_t site2=0; site2 < numberOfSites; site2++) {
	  try {
	    equatingSymmetry(site,site2);    // will throw range_error if the symmetry does not exist
	    siteDegree[site]++;
	  }
	  catch (std::range_error&) { /* do nothing */ }
	}
      }
    }
    
//     //! Index of (PI,PI,....) for this k-space geometry
//     void setPiPoint()
//     {
//       if (type == Space) return;
//       if (dimension==1) {
// 	piPointIndex=0;
// 	return;
//       }
//       if(dimension==2) {
// 	try {
// 	  piPointIndex = index(piPoint<FieldType,2>());
// 	  return;
// 	}
// 	catch (std::logic_error& e) {
// 	  /* std::ostringstream message;
// 	     message << "Crystal 2D setPiPoint failed! " << std::endl;
// 	     message << "  " << e.what() << std::endl;
// 	     message << "  crystal is:" <<  this->toXML() << std::endl;
// 	     throw std::logic_error(message.str());*/
// 	  // Modified by G.A. 
// 	  // This is to accomodate lattices that have no piPointIndex
// 	  // It will be set to -1, client code must check result
// 	  piPointIndex= -1;
// 	  return;
// 	}
//       }
//       std::ostringstream message;
//       message << "Crystal.setPiPoint(): failed dimension = " << dimension << std::endl;
//       throw  std::logic_error(message.str());
//     }

//     //! Index of (0,0,...) for this K-SPACE Geometry
//     void setZeroPoint()
//     {

//       if (dimension==1) {	
// 	zeroPointIndex= int(numberOfSites/2.0);
// 	return;
//       }

//       try {
// 	zeroPointIndex= index(zeroPoint<FieldType,2>());
//       }
//       catch (std::logic_error& e) {
// 	std::ostringstream message;
// 	message << "Crystal 2D setZeroPoint failed! " << std::endl;
// 	message << "  " << e.what() << std::endl;
// 	//	message << "  crystal is:" <<  toXML(*this) << std::endl;
// 	throw std::logic_error(message.str());
//       }
//     }

  public:

  }; // end of class 

} // namespace dca
#endif
