//-*-C++-*-       
//
/** \file    BasicFunctions.h
 *  \ingroup SymFind
 *  \author  Michael S. Summers
 *
 *  
 */

#include "Utilities.h"

#include "SymFind_Vector.h"
#include "SymFind_Matrix.h"
#include "SymFind_CGeometry.h"

namespace SymFind {
  typedef Matrix<int>                   IntMatrixType; 
  typedef SymFind_GroupActionFilterType GroupActionFilterType;
}

#include "Typefactory.h"

#include "SymFind_Crystal.h"
#include "SymFind_Geometry.h"

extern "C" {

  //======================================================================

  void  SymFind_CGeometry_Init(SymFind_CGeometry* ptr) {
    ptr->ptr = 0;
  }
  
  //----------------------------------------------------------------------

  void SymFind_make_CGeometry2D_double(SymFind_CGeometry* geoPtr,
				       int    dim,
				       double latticeA_length, 
				       double latticeB_length, 
				       double latticeAngle,
				       int    superlatticeA_x,
				       int    superlatticeA_y,
				       int    superlatticeB_x,
				       int    superlatticeB_y) {
    
    switch (dim) 
      {
      case 1: {
	printf("One dimemsional geometries are not available yet!\n");
	exit(-911);
      }
      case 2: {
	geoPtr->ptr = new SymFind::Geometry<double,2> (latticeA_length,
						       latticeB_length, 
						       latticeAngle,
						       superlatticeA_x,
						       superlatticeA_y,
						       superlatticeB_x,
						       superlatticeB_y);
	return;
      }
      case 3: {
	printf("Three dimemsional geometries are not available yet!\n");
	exit(-911);
      }
      default:
	printf("Unknown dimension %d in SymFind_make_CGeometry2D_double\n",dim);
	exit(-911);
      }
  }

  //----------------------------------------------------------------------
  
  void SymFind_delete_CGeometry2D_double(SymFind_CGeometry* geoPtr) {
    
    if( geoPtr->ptr != 0)
      delete static_cast<SymFind::Geometry<double,2>*>(geoPtr->ptr);
  }
  
  //----------------------------------------------------------------------

  void SymFind_fetch_cgeometry_double(SymFind_CGeometry              geo,
				      SymFind_CCrystal_double*       rCluster_,
				      SymFind_CCrystal_double*       kCluster_,
				      SymFind_GroupActionFilterType  filterType) {
    
    SymFind::SymFind_Crystal<double,2> rCluster(*rCluster_);
    SymFind::SymFind_Crystal<double,2> kCluster(*kCluster_);

    // Cast the opaque pointer to the appropriate type
    //
    SymFind::Geometry<double,2>*  gptr =(SymFind::Geometry<double,2>*) geo.ptr;

    gptr->fetch(rCluster,kCluster,filterType);

    SymFind::Chooser<double>::initAfterSymmetriesSetUp(rCluster_);
    SymFind::Chooser<double>::initAfterSymmetriesSetUp(kCluster_);
  }

  // //----------------------------------------------------------------------
  
  // void SymFind_fetchWignerSeitzMesh_double(SymFind_CGeometry              geo,
  // 					   int                            numPoints,
  // 					   SymFind_CMatrix_double*        meshMatPtr) {
    
  //   // Cast the opaque pointer to the appropriate type
  //   //
  //   SymFind::Geometry<double,2>*  gptr =(SymFind::Geometry<double,2>*) geo.ptr;
  //   SymFind::Matrix<double> meshMat(*meshMatPtr);
    
  //   gptr->fetchWignerSeitzMesh(numPoints,meshMat);
  // }

  //======================================================================

  void SymFind_writeXML(SymFind_CGeometry geo, const char* directoryName) {
    
    typedef SymFind::Geometry<double,2>    GeometryType;
    typedef GeometryType::SuperCrystalType SuperCrystalType;
    typedef SuperCrystalType::BaseType     SuperCrystalBaseType;
    
    GeometryType&     geometry     = *((GeometryType*) geo.ptr);
    SuperCrystalType& superCrystal = *geometry.superCrystPtr;
    
    //    insureDirectoryExists(directoryName);
    
    SymFind::dumpXML   (directoryName, "SuperCrystal", 
			superCrystal);
    SymFind::dumpXML   (directoryName, "GivenCrystal",        
			superCrystal.crystal);
    SymFind::dumpXML<SuperCrystalBaseType>   (directoryName, "ReciprocalCrystal",    
					      superCrystal.reciprocalCrystal);
    SymFind::dumpXML<SuperCrystalBaseType>   (directoryName, "ReciprocalSuperCrystal",
					      superCrystal.reciprocalSuperCrystal);
  }

  // //======================================================================

  // void SymFind_C_Mesh_Geometry_Init(SymFind_C_Mesh_Geometry* ptr) {
  //   ptr->ptr = 0;
  // }
  
  // //----------------------------------------------------------------------

  // void SymFind_make_C_MeshGeometry2D_double(SymFind_C_Mesh_Geometry* meshPtr,
  // 					    double latticeA_length, 
  // 					    double latticeB_length, 
  // 					    double latticeAngle,
  // 					    int    superlatticeA_x,
  // 					    int    superlatticeA_y,
  // 					    int    superlatticeB_x,
  // 					    int    superlatticeB_y,
  // 					    int    numPts) {
    
  //   meshPtr->ptr = new SymFind::MeshGeometry<double,2> (latticeA_length,
  // 							latticeB_length, 
  // 							latticeAngle,
  // 							superlatticeA_x,
  // 							superlatticeA_y,
  // 							superlatticeB_x,
  // 							superlatticeB_y,
  // 							numPts);
  // }

  // //----------------------------------------------------------------------
  
  // void SymFind_delete_C_MeshGeometry2D_double(SymFind_C_Mesh_Geometry* meshPtr) {
    
  //   if( meshPtr->ptr != 0)
  //     delete static_cast<SymFind::MeshGeometry<double,2>*>(meshPtr->ptr);
  // }
  
  // //----------------------------------------------------------------------

  // void SymFind_fetch_mesh_double(SymFind_C_Mesh_Geometry  mesh,
  // 				 SymFind_CMatrix_double*  meshMatrixPtr) {

  //   SymFind_mInitialize_double(meshMatrixPtr,0,0,0,0.0);
  //   SymFind::Matrix<double> meshMat(*meshMatrixPtr);

  //   SymFind::MeshGeometry<double,2>* mptr =(SymFind::MeshGeometry<double,2>*) mesh.ptr;

  //   mptr->fetch(meshMat);
  // }

  //======================================================================

  void SymFind_print(SymFind_CCrystal_double* crystal_) {
    SymFind::SymFind_Crystal<double,2> aCrystal(*crystal_);
    SymFind::print(std::cout, aCrystal);  
  }

  void SymFind_mprintWithIndices(SymFind_CMatrix_double* mat) {
    SymFind::Matrix<double> Mat(*mat);
    SymFind::mprintWithIndices(Mat,std::cout);
  }
  

  void SymFind_doneWithGeometry_2D_double() {
    SymFind::Geometry<double,2>::doneWithGeometry();
  }

  //======================================================================
}


		   
