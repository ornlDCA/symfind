
#include "LA_Functions.h"

//======================================================================

void  writeCodeFor(intMatrixFunction f, const char* fName, std::ostream& os) {
  std::cout << capacity(f);
  MatrixFunctions<intMatrixFunction>::writeCodeFor(f, fName, os);
}

void writeMatIntStaticDataStatement(intMatrixFunction f, std::ostream& os) {
  MatrixFunctions<intMatrixFunction>::writeMatStaticDataStatement(f, os);
}

//======================================================================

void  writeCodeFor(doubleMatrixFunction f, const char* fName, std::ostream& os) {
  MatrixFunctions<doubleMatrixFunction>::writeCodeFor(f, fName, os);
}

void writeMatDoubleStaticDataStatement(doubleMatrixFunction f, std::ostream& os) {
  MatrixFunctions<doubleMatrixFunction>::writeMatStaticDataStatement(f, os);
}

// //======================================================================
  
  // template<typename MatrixLike>
  // void copyMatrixFunctionToMatrix(MFT f, MatrixLike& m) {
    
  //   int nr = nRow(f);
  //   int nc = nCol(f);
  //   for (int c=0; c<nc; nc++) {
  //     for (int r=0; r<nr; nr++) {
  // 	m(r,c) = f(r,c);
  //     }
  //   }
  // }

//======================================================================

void  writeCodeFor(intVectorFunction f, const char* fName, std::ostream& os) {
  VectorFunctions<intVectorFunction>::writeCodeFor(f, fName, os);
}

//======================================================================

void  writeCodeFor(doubleVectorFunction f, const char* fName, std::ostream& os) {
  VectorFunctions<doubleVectorFunction>::writeCodeFor(f, fName, os);
}

//======================================================================

void  writeCodeFor(charStarVectorFunction f, const char* fName, std::ostream& os) {
  VectorFunctions<charStarVectorFunction>::writeCodeFor(f, fName, os);
}


// //====================================================================== Usage

//   double usageVector_(int  i) {

//     // note the data below is laid out row major
//     // the actual matrix is the transpose of data
//     static const double data[] = {
//       0.0,1.0,2.0,3.0,4.0,5.0,6.0,7.0,8.0,9.0,
//       0.0,1.0,2.0,3.0,4.0,5.0,6.0,7.0,8.0,9.0,
//       0.0,1.0,2.0,3.0,4.0,5.0,6.0,7.0,8.0,9.0,
//       0.0,1.0,2.0,3.0,4.0,5.0,6.0,7.0,8.0,9.0,
//       0.0,1.0,2.0,3.0,4.0,5.0,6.0,7.0,8.0,9.0,
//       0.0,1.0,2.0,3.0,4.0,5.0,6.0,7.0,8.0,9.0,
//       0.0,1.0,2.0,3.0,4.0,5.0,6.0,7.0,8.0,9.0,
//       0.0,1.0,2.0,3.0,4.0,5.0,6.0,7.0,8.0,9.0,
//       0.0,1.0,2.0,3.0,4.0,5.0,6.0,7.0,8.0,9.0,
//       0.0,1.0,2.0,3.0,4.0,5.0,6.0,7.0,8.0,9.0
//     };

//     int vInfo = vectorInfo(i,10,10);
//     if(vInfo > 0) return vInfo;
//     return data[i];
//   }

//   //====================================================================== Usage

//   MakeVectorFunction(usageVector,
// 		     double,20,100,
// 		     0.0,1.0,2.0,3.0,4.0,5.0,6.0,7.0,8.0,9.0,
// 		     0.0,1.0,2.0,3.0,4.0,5.0,6.0,7.0,8.0,9.0,
// 		     0.0,1.0,2.0,3.0,4.0,5.0,6.0,7.0,8.0,9.0,
// 		     0.0,1.0,2.0,3.0,4.0,5.0,6.0,7.0,8.0,9.0,
// 		     0.0,1.0,2.0,3.0,4.0,5.0,6.0,7.0,8.0,9.0,
// 		     0.0,1.0,2.0,3.0,4.0,5.0,6.0,7.0,8.0,9.0,
// 		     0.0,1.0,2.0,3.0,4.0,5.0,6.0,7.0,8.0,9.0,
// 		     0.0,1.0,2.0,3.0,4.0,5.0,6.0,7.0,8.0,9.0,
// 		     0.0,1.0,2.0,3.0,4.0,5.0,6.0,7.0,8.0,9.0,
// 		     0.0,1.0,2.0,3.0,4.0,5.0,6.0,7.0,8.0,9.0 );
  
// //======================================================================
// Should be unnneded handled by generic vector operations

  // //======================================================================
  
  // template<typename T, typename VectorLike>
  // void copyDoubleVectorFunctionToVector(T f, VectorLike& v) {
    
  //   v.resize(len(f));
  //   for (int i=0; i<len(f); i++) 
  //     v[i] = f(i);
  // }
  
// void printList(const doubleVectorFunction& vector,
// 		 std::ostream& os,
// 		 char sep,
// 		 int width) {
//   os << "[ ";
    
//   int ln = size(vector);
    
//   for(int i=0; i< ln; i++) 
//     if (i==0)
// 	os  << std::setw(width) << vector(i);
//     else
// 	os  << sep << " "
// 	    << std::setw(width) << vector(i);
    
//   os << "]";
// }


//======================================================================

int capacity(doubleMatrixFunction mf) { return (int)mf(-1,-1); }
int nRow    (doubleMatrixFunction mf) { return (int)mf(-1, 0); }
int lDim    (doubleMatrixFunction mf) { return (int)mf(-1, 1); }
int nCol    (doubleMatrixFunction mf) { return (int)mf( 0,-1); }
int size    (doubleMatrixFunction mf) { return nRow(mf) * nCol(mf); }

//======================================================================

int size    (const doubleVectorFunction& vf) { return static_cast<int>(vf(-1)); };
int capacity(const doubleVectorFunction& vf) { return static_cast<int>(vf(-2)); };

//======================================================================

int capacity(intMatrixFunction mf) { return (int)mf(-1,-1); }
int nRow    (intMatrixFunction mf) { return (int)mf(-1, 0); }
int lDim    (intMatrixFunction mf) { return (int)mf(-1, 1); }
int nCol    (intMatrixFunction mf) { return (int)mf( 0,-1); }
int size    (intMatrixFunction mf) { return nRow(mf) * nCol(mf); }

//======================================================================

int size    (intVectorFunction    vf) { return vf(-1); };
int capacity(intVectorFunction    vf) { return vf(-2); };

//======================================================================

int size    (charStarVectorFunction vf) { return atoi(vf(-1)); };
int capacity(charStarVectorFunction vf) { return atoi(vf(-2)); };

//======================================================================
