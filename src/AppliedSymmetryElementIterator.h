//-*-C++-*-

#ifndef  SymFind_AppliedSymmetryElementIterator
#define  SymFind_AppliedSymmetryElementIterator

/** \ingroup symmetryConcepts */
/*@{*/

/** \file   AppliedSymmetryElementIterator.h
 *  \author Michael Summers
 */  

namespace SymFind {
  
  //====================================================================== 
  //
  /** \ingroup symmetryConcepts 
   *
   * \brief The 2D SpaceGroup class specialization.
   */
  template<typename Field, 
	   size_t DIM,
	   typename Occupant, 
	   template<typename, size_t, typename> class LatticeTemplate, 
	   typename Algorithms>
  class AppliedSymmetryElementIterator:
    public std::iterator< std::forward_iterator_tag, 
			  AppliedSymmetryElement<Field,DIM,Occupant,LatticeTemplate<Field,DIM,Algorithms>,Algorithms> >
  {
  public:
    typedef AppliedSymmetryElementIterator<Field,DIM,Occupant,LatticeTemplate,Algorithms> AppliedSymmetryElementIteratorType;

    typedef LatticeTemplate<Field,DIM,Algorithms>                             LatticeType;
    typedef SpaceGroup<Field,DIM,Occupant, Algorithms>                        SpaceGroupType;
    typedef AppliedSymmetryElement<Field,DIM,Occupant,LatticeType,Algorithms> AppliedSymmetryElementType;
    typedef std::vector< AppliedSymmetryElementType >                         AppliedSymmetryElementsType;
    typedef Symmetry<Field,DIM,Occupant,LatticeTemplate,Algorithms>           SymmetryType;

  protected:

    bool                               atEnd;
    size_t                             position;
    const SpaceGroupType&              spaceGroup;
    const AppliedSymmetryElementsType& allElements;
    size_t                             numElements;

    bool validPosition() { 
      if (position < numElements) 
	return (spaceGroup.opFlags[position] == 1); 
      if (position == numElements)
	return true;   //end position
      return false;
    }
    
  public:
    explicit AppliedSymmetryElementIterator(const SpaceGroupType& sg, const AppliedSymmetryElementsType& allEl):
      atEnd(false),
      position(0),
      spaceGroup(sg),
      allElements(allEl),
      numElements(allEl.size())
    {
      if (!validPosition())
	(*this)++;
    }

     explicit AppliedSymmetryElementIterator(const AppliedSymmetryElementIterator& other):
      atEnd(other.atEnd),
      position(other.position),
      spaceGroup(other.spaceGroup),
      allElements(other.allElements),
      numElements(allElements.size())
    {
      if (!validPosition())
	(*this)++;
    }

   static AppliedSymmetryElementIterator end(const SpaceGroupType& sg, const AppliedSymmetryElementsType& allEl) {
      AppliedSymmetryElementIterator result(sg,allEl);
      result.position = result.numElements;
      return result;
    }

    const AppliedSymmetryElementType& operator* () {
      return allElements[position];
    }

    AppliedSymmetryElementIteratorType& operator++ () {
      position++;
      if (position > numElements) 
	throw std::out_of_range("AppliedSymmetryElementIterator iterated to far!");
      while (!validPosition())
	position++;
      return *this;
    }

    AppliedSymmetryElementIteratorType operator++ (int dummy) {
      (void) dummy;
      AppliedSymmetryElementIteratorType save = *this;
      ++*this;
      return save;
    }

    bool operator== (const AppliedSymmetryElementIteratorType& other) { return (position == other.position); }
    bool operator!= (const AppliedSymmetryElementIteratorType& other) { return (position != other.position); }
  };

} /** namespace spimag **/

#endif // SymFind_SpaceGroup2D_Iterator

/*@}*/
