//-*-C++-*-       

#ifndef SymFind_GEOMETRY_2D_HEADER_H
#define SymFind_GEOMETRY_2D_HEADER_H

namespace SymFind {
  
  template<typename T, int DIM>  class Geometry     {};
  template<typename T, int DIM>  class MeshGeometry {};
  
  //====================================================================== fetchGeometry
  //
  template<typename SymmetryType>
  void fetchGroupAction(const SymmetryType&       symmetry,
			Matrix<int>&              target_data, 
			SymFind_GroupActionFilterType filterType) 
  {

    switch (filterType) {
      
      // Use the full group action table
    case (NoGroupActionFilter): {
      target_data = symmetry.symmetryGroup.groupAction;
      return;
    }
      // Use just the point group operations from the group action table
      //
    case (PointGroupOnly): {
      target_data = symmetry.pointGroup.groupAction;
      return;
    }
    case (UniqueGroupActionFilter): {
      symmetry.symmetryGroup.groupAction.uniqueActions(target_data);
      return;
    }
    case (FullGroupActionFilter): {
      throw std::logic_error("In fetchGroupAction, unknown filterType given");
    }
    }
  }

  //======================================================================

  template<typename T>
  class Geometry<T,2> {
  public:
    
    //----------------------------------------------------------------------

    enum {DIM=2};

    typedef BasicCrystalAlgorithms                            Algorithms;
    typedef CellParameters      <T,DIM,Algorithms>            CellParametersType;
    typedef Lattice             <T,DIM,Algorithms>            LatticeType;
    typedef Crystal             <T,DIM, Occupant, Algorithms> CrystalType;
    typedef SuperCrystal        <T,DIM, Occupant, Algorithms> SuperCrystalType;
    typedef SuperCrystalBuilder <T,DIM, Occupant, Algorithms> SuperCrystalBuilderType;
    typedef PatternData         <T,DIM, Occupant, Algorithms> PatternDataType;
    typedef SymmetryElements    <T,DIM, Algorithms>           SymmetryElementsType;
  
    //----------------------------------------------------------------------

    LatticeType              lat;
    PatternDataType          patternData;
    CrystalType*             crystalPtr;
    PatternDataType*         rpatternDataPtr;
    SuperCrystalBuilderType* builderPtr;
    SuperCrystalType*        superCrystPtr;

    //----------------------------------------------------------------------

    static 
    CrystalType* 
    loadPatternAndMakeCrystal(LatticeType&     lat,
			      PatternDataType& patternData,
			      std::string      name) {

      //======================================== Build the crystal pattern
      patternData.insert( Occupant("atom1","yellow"), 
			  cellPosition<T,Algorithms>(0.0,0.0) );

      return new CrystalType(lat, patternData,name);
    }

    //----------------------------------------------------------------------

    static 
    SuperCrystalBuilderType* 
    loadRPatternAndMakeSuperCrystalBuilder(PatternDataType& rpatternData,
					   int              superlatticeA_x,
					   int              superlatticeA_y,
					   int              superlatticeB_x,
					   int              superlatticeB_y) {
      
      //======================================== Build the recipricol crystal pattern
      rpatternData.insert( Occupant("kpoint1","red"), 
			   cellPosition<T,Algorithms>( 0.0, 0.0 ) );
      
      return new SuperCrystalBuilderType(superlatticeA_x, superlatticeA_y, 
					 superlatticeB_x, superlatticeB_y, 
					 rpatternData);
    }

    //======================================================================
    //  Geometry<T,2>( 1,1,90,4,2,0,4)
    //
    Geometry(T                          latticeA_length, 
	     T                          latticeB_length, 
	     T                          latticeAngle,
	     int                        superlatticeA_x,
	     int                        superlatticeA_y,
	     int                        superlatticeB_x,
	     int                        superlatticeB_y):
      lat              (CellParametersType(latticeA_length, 
					   latticeB_length, 
					   latticeAngle)),
      patternData      (),
      crystalPtr       (loadPatternAndMakeCrystal(lat,patternData,"Input")),
      rpatternDataPtr  (new PatternDataType),
      builderPtr       (loadRPatternAndMakeSuperCrystalBuilder(*rpatternDataPtr,
							       superlatticeA_x, superlatticeA_y, 
							       superlatticeB_x, superlatticeB_y)),
      superCrystPtr    (new SuperCrystalType(*crystalPtr, *builderPtr))
    {}
    
    //======================================================================

    static
    void doneWithGeometry() {
      SymmetryElementsType::kill(); // Free static memory
    }

    //======================================================================

    ~Geometry() {
      delete rpatternDataPtr;
      delete builderPtr;
      delete superCrystPtr;
      delete crystalPtr;
    }

    //======================================================================
    
    template<typename A_CrystalType>
    void fetch(A_CrystalType&          rCluster,
	       A_CrystalType&          kCluster,
	       GroupActionFilterType   filterType=PointGroupOnly) {
      
      //======================================== Fetch the rCluster lattice 
      lat.setBasisVectors(rCluster.primativeVectors);
      
      //======================================== Fetch the real sites cartesian coordinates 
      // The basis vectors for the superCrystal are the crystals latticeVectors
      //
      superCrystPtr->setBasisVectors(rCluster.latticeVectors);
      
      //======================================== Fetch the real sites cartesian coordinates 
      // rCluster.sites(c,site)
      superCrystPtr->pattern.setCartesianSites(rCluster.sites);
      
      //======================================== Fetch the kCluster lattice 
      //
      superCrystPtr->reciprocalCrystal.setBasisVectors(kCluster.latticeVectors);
      
      //======================================== Fetch the kCluster primative vectors 
      superCrystPtr->reciprocalSuperCrystal.setBasisVectors(kCluster.primativeVectors);
      
      //======================================== Fetch the k sites cartesian coordinates 
      // kCluster.sites(c,site)
      superCrystPtr->reciprocalCrystal.pattern.setCartesianSites(kCluster.sites);
      
      //======================================== Fetch the real translation difference matricies
      superCrystPtr->pattern.buildDiffIndex(rCluster.siteDiffToSite);
      
      Neighbors::loadFrom(rCluster.sites, rCluster.latticeVectors, 
			  rCluster.neighborDistances, rCluster.nearestNeighbor);

      //======================================== Fetch the reciprocal translation difference matricies
      superCrystPtr->reciprocalCrystal.pattern.buildDiffIndex(kCluster.siteDiffToSite);
      
      Neighbors::loadFrom(kCluster.sites,kCluster.latticeVectors,
			  kCluster.neighborDistances, kCluster.nearestNeighbor);

      //======================================== Fetch the reciprocal translation addition matricies
      superCrystPtr->reciprocalCrystal.pattern.buildPlusIndex(kCluster.siteSumToSite);
  
      //======================================== Fetch the real group action matricies
      fetchGroupAction(superCrystPtr->symmetry, rCluster.fullGroupAction,     NoGroupActionFilter);
      fetchGroupAction(superCrystPtr->symmetry, rCluster.symmetryGroupAction, filterType);
      
      //======================================== Fetch the reciprocal group action matricies
      fetchGroupAction(superCrystPtr->reciprocalCrystal.symmetry, kCluster.fullGroupAction, NoGroupActionFilter);
      fetchGroupAction(superCrystPtr->reciprocalCrystal.symmetry, kCluster.symmetryGroupAction, filterType);

      Wedge<T> rwedge;

      rwedge.initialize(rCluster);
      rwedge.load(rCluster.site2Wedge, rCluster.wedge2Site);

      Wedge<T> kwedge;

      kwedge.initialize(kCluster);
      kwedge.load(kCluster.site2Wedge, kCluster.wedge2Site);

    //     if (filterType == GroupActionFilter::PointGroupOnly) {
    //       //======================================== Fetch the real orbits
    //       rCluster.orbits = superCrystPtr->symmetry.pointGroup.orbits;
      
    //       //======================================== Fetch the reciprocal orbits
    //       kCluster.orbits = superCrystPtr->reciprocalCrystal.symmetry.pointGroup.orbits;
    //     }
    //     else {
    //       //======================================== Fetch the real orbits
    //       rCluster.orbits = superCrystPtr->symmetry.symmetryGroup.orbits;
    
    //       //======================================== Fetch the reciprocal orbits
    //       kCluster.orbits = superCrystPtr->reciprocalCrystal.symmetry.symmetryGroup.orbits;
    //     }

    }

    //======================================================================  

    CrystalType&
    reciprocalCrystal() {
      return    superCrystPtr-> reciprocalCrystal;
    }

    // //======================================================================  

    // // CrystalType&
    // // reciprocalSuperCrystal() {
    // //   return    superCrystPtr-> reciprocalSuperCrystal;
    // // }

    // void fetchWignerSeitzMesh(int numPoints, Matrix<T>& meshMat) {
      
    //   WignerSeitzCell<T,2,Algorithms>(superCrystPtr->reciprocalSuperCrystalLattice)
    // 	.setMesh(numPoints,meshMat);
    // }

    //======================================================================  
  };

  //======================================================================
  
  template<typename T>
  class MeshGeometry<T,2> {
  public:
    
    //----------------------------------------------------------------------
    
    enum {DIM=2};

    typedef BasicCrystalAlgorithms                                      Algorithms;
    typedef LatticeCoordinates<DIM>                                     SubLatticeVecCoordType;
    typedef std::vector<SubLatticeVecCoordType>                         SubLatticeVecCoordsType;
    typedef CellParameters<T,DIM,Algorithms>                            CellParametersType;
    typedef PatternData<T,DIM,Occupant,Algorithms>                      PatternDataType;
    typedef Lattice<T,DIM,Algorithms>                                   LatticeType;
    typedef LatticeWithPattern<T,DIM,Occupant,LatticeType,Algorithms>   LatticeWithPatternType;
    typedef FloodTiler<T,DIM,Occupant,Lattice,Algorithms>               FloodTilerType;
    typedef ReciprocalLattice<T,DIM,Lattice,Algorithms>                 ReciprocalLatticeType;
    typedef SuperCrystalBuilder<T,DIM,Occupant,Algorithms>              SuperCrystalBuilderType;
  
    //======================================================================
    
    static
    PatternDataType& 
    loadMeshPatternData(int numPoints, 
			PatternDataType& patternData ) {
      
      size_t nSide( static_cast<size_t>(sqrt(static_cast<double>(numPoints))));
      
      T  nSideF(nSide);
      
      for(size_t i=0; i<nSide;i++) 
	for(size_t j=0; j<nSide;j++) 
	  
	  patternData.insert( Occupant("mesh","blue"),  
			      cellPosition<T,Algorithms>(static_cast<T>(i)/nSideF,
							 static_cast<T>(j)/nSideF) );
      return patternData;
    }
    
    //----------------------------------------------------------------------

    LatticeType              lat;
    PatternDataType          patternData;
    LatticeWithPatternType   crystalLatPat;
    PatternDataType          rpatternData;
    SuperCrystalBuilderType  builder;
    SubLatticeVecCoordsType  subLatVecCoords;
    LatticeWithPatternType   superCrystalLatPat;
    LatticeType              reciprocalSuperLattice;
    LatticeWithPatternType   reciprocalSuperLatPat;

    //======================================================================

    MeshGeometry(T   latticeA_length, 
		 T   latticeB_length, 
		 T   latticeAngle,
		 int superlatticeA_x,
		 int superlatticeA_y,
		 int superlatticeB_x,
		 int superlatticeB_y,
		 int numPoints):
      lat                    (CellParametersType(latticeA_length, 
						 latticeB_length, 
						 latticeAngle)),
      patternData            (),
      crystalLatPat          (lat, loadMeshPatternData(numPoints,
						       patternData)),
      rpatternData           (),
      builder                (superlatticeA_x,superlatticeA_y,
			      superlatticeB_x,superlatticeB_y, 
			      loadMeshPatternData(numPoints,
						  rpatternData)),
      subLatVecCoords        (builder.getSubLatticeVecCoords()),
      superCrystalLatPat     (FloodTilerType(subLatVecCoords,crystalLatPat).getTiledLatticeWithPattern()),
      reciprocalSuperLattice (ReciprocalLatticeType(superCrystalLatPat).getLattice()),
      reciprocalSuperLatPat  (reciprocalSuperLattice, builder.getReciprocalSuperCrystalPattern())
    {}

    //======================================================================
    //
    // sitesKmesh(c,site)
    //
    template<typename MatrixLikeType>
    void fetch(MatrixLikeType& sitesKmesh) {
      // sitesKmesh(c,site)
      reciprocalSuperLatPat.pattern.loadCartesian(sitesKmesh);
    }
    //======================================================================
  };

  //======================================================================
} // SymFind namespace
  
#endif
		   
