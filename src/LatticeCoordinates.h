//-*-C++-*-
#ifndef  SymFind_LatticeCoordinates
#define  SymFind_LatticeCoordinates

/** \ingroup extendedMatrices */
/*@{*/

/** \file LatticeCoordinates.h
 *
 * Contains a class for implementing LatticeCoordinates (SeitzTranslation subclass) objects.
 */

namespace SymFind {

  template<typename Field, size_t DIM> class CellTranslation;
  template<typename Field, size_t DIM, typename Algorithms> class CellPosition;

  /** \ingroup extendedMatrices
   *
   * \brief A marker class indicating that the Translation indicates a direction from within a Cell.
   *
   * These can be converted to and from MillerIndices for a given Cell.
   */
  template<size_t DIM> 
  class LatticeCoordinates: 
    public SeitzVector< int, DIM, 0 >  // A SeitzTranaltion
  {
  public:

    enum{ IND=0 };
    typedef LatticeCoordinates<DIM>   ThisType;
    typedef SeitzVector<int,DIM,IND>  BaseType;

    /** The Default Constructor produces the zero lattice point. */
    LatticeCoordinates():        BaseType() {}

    /** Construct a translation whose components are set to the given value. */
    LatticeCoordinates(int val): BaseType(val) {}

    /** Copy Construct a translation. */
    LatticeCoordinates(const ThisType& v): BaseType(v) {}

    /** Copy Construct a translation. */
    LatticeCoordinates(const BaseType& v): BaseType(v) {}

    template<typename Field>
    CellTranslation<Field,DIM> cellTranslation() const {
      CellTranslation<Field,DIM> result;
      for (size_t i=0; i< DIM; i++)
	result[i] = (*this)[i];
      return result;
    }

    template<typename Field, typename Algorithms>
    CellPosition<Field,DIM,Algorithms> cellPosition() const {
      CellPosition<Field,DIM,Algorithms> result;
      for (size_t i=0; i< DIM; i++)
    	result[i] = (*this)[i];
      return result;
    }

    template<typename Algorithms>
    LatticeCoordinates<DIM> normalize() const {
      
      if(isNegative(*this)) {
	LatticeCoordinates<DIM> result(*this);
	result *= -1; //make it positive
	return result;
      }
      return *this;
    }    
    
    //       if(result[0] > 0)
    // 	return result;

    //       if (result[0] == 0)
    // 	if (result[1] >= 0)
    // 	  return result;

    ThisType& operator = (const ThisType& v)
    { 
      const BaseType& bv = v;
      BaseType& bt = (*this);
      bt = bv;
      return *this; 
    }
  };

  /**
   * The less operator.
   */
  template<size_t DIM>
  bool operator< (const LatticeCoordinates<DIM>& lhs, 
		  const LatticeCoordinates<DIM>& rhs) {
    
    for (size_t i = 0; i<DIM; i++) {
      int d1 = lhs[i] - rhs[i];
      if (d1 == 0) continue;
      return d1 < 0.0;
    }
    
    return false;
  }

  inline 
  LatticeCoordinates<2> latticeCoordinate(int t0, int t1) {
    LatticeCoordinates<2> result;
    result[0] = t0;
    result[1] = t1;
    return result;
  }
  
  //----------------------------------------------------------------------

  inline LatticeCoordinates<3> latticeCoordinate(int t0, int t1, int t2) {
    LatticeCoordinates<3> result;
    result[0] = t0;
    result[1] = t1;
    result[2] = t2;
    return result;
  }

  //======================================================================

} /* namspace SymFind */

#endif 
/*@}*/
