//-*-C++-*-

#ifndef  SymFind_NORMALIZE_SYMMETRY_ELEMENT
#define  SymFind_NORMALIZE_SYMMETRY_ELEMENT

/** \ingroup SymmetryConcepts */
/*@{*/

/** \file NormalizeSymmetryelement.h
 *
 *  Contains code for normalizing  symmetry elements.
 *
 *  \author  Mike Summers
 *
 */
 
namespace SymFind {

  
  //======================================================================
  /*
   * \brief If the translation is on the negative side of the b-c plane return true.
   *
   */
  template<typename Field, size_t DIM, int IND>
  bool isNegative(const SeitzVector<Field,DIM,IND>& v) {

    static Field zero(0);
    
    for(size_t i = 0; i<DIM; i++)
      if(!close(v[i],zero))
	return v[i] < zero;
    
    return false; // all zeros
  }

  //=====================================================================
  /*
   */
  template<typename Field, size_t DIM>
  void
  normalizeGlideDirection(CellTranslation<Field,DIM>& trans,
                          bool                        verbose= false) {
    
    if (verbose) 
      getTraceOut() << " normalizeGlideDirection[ " << trans << "] => ";

    if (isNegative(trans) ) 
      for (size_t i=0;i< DIM; i++) 
        trans[i] *= -1;

    if (verbose) 
      getTraceOut() << trans << "\n";
  }
  
 //=====================================================================
  /*
   * presuming thath normalizeToInternal was done before this call!
   *
   *   eg.   -1/3a       => (1 - 1/3)a
   *   eg.   -1/3a -1/3b => (1 - 1/3)a (1 - 1/3)b = 2/3a 2/3b
   */
  template<typename Field, size_t DIM>
  void
  normalizeTranslationDirection(CellTranslation<Field,DIM>& trans,
                                bool                        verbose= false) {
    
    if (verbose) 
      getTraceOut() << " normalizeDirection[ " << trans << "] => ";
    
    for (size_t i=0;i< DIM; i++) {
      
      if (close(trans[i], 0.0)) {
        trans[i] = 0.0;
        continue;
      }

      if (trans[i] < 0.0)
        trans[i] = 1.0 + trans[i];
    }
    
    if (verbose) 
      getTraceOut() << trans << "\n";
  }
  
  //=====================================================================
  /*
   *  If a netReduced translation may be further reduced to a
   *  translation internal to the lattice.
   *
   *  eg.  convert 1/2 (a - 2b)  => 1/2 a 
   *  eg.  convert 1/2 (2a + b)  => 1/2 b
   *  eg.  convert  a + b  =>  0
   *  eg.  convert  a      =>  0
   *  eg.  convert  b      =>  0
   *
   *    \note: latticeCoord * scaleFactor = trans
   *
   *        However,if the translation or any of its components
   *        becomes zero then return a new scalefactor of zero.
   */
  template<typename Field, size_t DIM>
  Field
  normalizeToInternal(CellTranslation<Field,DIM>& trans,
		      Field                       scaleFactor,
		      bool                        verbose= false) {
    
    static const CellTranslation<Field,DIM> zeroTrans;
    static CellTranslation<Field,DIM>       oldTrans;

    Field  newScaleFactor        = scaleFactor;
    //    bool   scaleFactorCloseToOne = close(scaleFactor,1.0);
    
    if (near(trans,zeroTrans)) return scaleFactor;

    if (verbose) 
      getTraceOut() << " normalizeToInternal: "
                    << "trans[ "<< trans << "] => ";
    
   // All net translations are the same as the zero translation.
    if (close(scaleFactor,1.0)) {
      trans          = zeroTrans;
      newScaleFactor = 0;
    }
    else {
      
      // Drop off any components that equate to the identify
      for (size_t i=0;i< DIM; i++) {
        Field remain = std::remainder(trans[i],1.0);
        if (close(remain,0.0)) remain = 0.0;
        trans[i] = remain;
      }
    }
    
    if (verbose) getTraceOut() << trans << "\n";
    
    return newScaleFactor;
  }
  
  //====================================================================== 
  //
  // This returns a LatticeCoordinate, scaleFactor tuple.

  // - The LatticeCoordinate is the shortest non-zero coordinate in
  //   the same direction as the given CellTranslation. 

  // - The scaleFactor is a value which satisfies:
  //
  //         latticeCoordinate * factor == trans.
  //
  // If the scale factor is an integer then the given translation is a 
  // lattice translation.
  //
  template<typename T>
  std::tuple< LatticeCoordinates<2>, T > 
  latticeCoordinateWithFactor(const CellTranslation<T,2>& trans,
			      bool                  stop=false) {
    
    static const T                    zero (0 );
    static const CellTranslation<T,2> zeroTrans;
    
    static T netDepth (20);  // Should be more than enough, if not
			     // something else is wrong.
    
    if ( near(trans,zeroTrans) )
      return std::make_tuple( latticeCoordinate(0,0), 1.0 );
    
    if (close(trans[0],zero)) {
      if (trans[1] > zero) {
        return std::make_tuple( latticeCoordinate(0,1), trans[1] );
      }
      else {
        return std::make_tuple( latticeCoordinate(0,-1), (trans[1] / T(-1)) );
      }
    }

    if (close(trans[1],zero)) {
      if (trans[0] > zero) {
        return std::make_tuple( latticeCoordinate(1,0), trans[0]);
      }
      else {
        return std::make_tuple( latticeCoordinate(-1,0), (trans[0]/ T(-1)) );
      }
    }
    
    // These are constructed in a way that will permit the search to
    // find shorter machest before longer ones.

    static std::vector<int> search_coords = {0};

    for (int i=1; i < netDepth; i++) {
      search_coords.push_back(i);
      search_coords.push_back(-i);
    }

    for (int n=1; n < int(search_coords.size()); n++)  {
      
      if (not sameSign(search_coords[n], trans[0]) ) continue;
      
      T nfactor = trans[0] /  T(search_coords[n]);
      
      for (int m=1; m < int(search_coords.size()); m++) { 
	
	if (not sameSign(search_coords[m], trans[1]) ) continue;
	
	T mfactor = trans[1] /  T(search_coords[m]);
	
	// std::cout << "\n[" << search_coords[n] << "," << search_coords[m] << "]" 
	//           << " ?=> {" << trans[0] << "," << trans[1] << "}" << "\n";
	// std::cout << "nfactor = " << nfactor << "\n";
	// std::cout << "mfactor = " << mfactor << "\n";
	
	if ( close( nfactor, mfactor ) ) {
	  
	  // std::cout << "\n results( {" << trans[0] << "," << trans[1] << "} )"
	  // 	    << " <= [" << search_coords[n] << "," << search_coords[m] << "]"
	  // 	    << " scaleFactor = " << nfactor << "\n";
	  
	  return
	    std::make_tuple(latticeCoordinate(search_coords[n],
					      search_coords[m]),
			    nfactor);
	}
      }
    }
    
    if (false) {  // This is for debugging purposes! 
                  // The stop prevents recursion!
                  // If debugging put a breakpoint @ again.
      if ( stop ) exit(-911);
      
      stop = true;    
      std::tuple< LatticeCoordinates<2>, T > 
	again = latticeCoordinateWithFactor(trans,stop);
    }

    std::ostringstream buff;
    buff << "\n@ '" << __FILE__ << "' : " << __LINE__ << "\n";
    buff << "latticeCoordinate(" << trans << ") failed!";
    throw std::logic_error(buff.str());
  }
  
  //----------------------------------------------------------------------
  
  template<typename T>
  LatticeCoordinates<2> 
  latticeCoordinate(const CellTranslation<T,2>& trans) {
    //    T scaleFactor;
    std::tuple< LatticeCoordinates<2>, T > result 
      = latticeCoordinateWithFactor(trans);
    //std::cout << trans << "  ==> " << result << " scaleFactor = " << scaleFactor << "\n";
    return std::get<0>(result);
  }
  
  //====================================================================== 
  /*
   *
   * \brief Returns a (latticeCoordinate, scaleFactor) tuple:

            - the coordinate is the maximal latticeCoordinate in the
            direction of the given CellTranslation which is no larger
            than the given CellTranslation.

	    - the scalefactor satisfies:

	      latticeCoordinate * scaleFactor == trans.

    This function is called after a call latticeCoordinateWithFactor
    which returns the (minResult, scaleFactor) which corresponds to
    the given CellTranslation trans. This result is expected as the
    first argument to this call. Also, this function should only be
    called if the incomming scaleFactor is greater than one.

  */
  template<typename Field>
  std::tuple< LatticeCoordinates<2>, Field > 
  maxContainedLatticeCoordinate_(const std::tuple< LatticeCoordinates<2>, Field >& minResult,
				 const CellTranslation<Field,2>&                   trans,
				 bool                                              verbose=false) {
    (void) verbose;    
    
    static int maxTries(20);

    const LatticeCoordinates<2>& minLatCoord = std::get<0>(minResult);
    const Field&                 scaleFactor = std::get<1>(minResult);
    
    int i(1);
    for (i = 1; i< maxTries; i++) {
      
      LatticeCoordinates<2> coord(minLatCoord * i);
      
      if( near(trans, coord) ) 
	return std::make_tuple(coord, ( scaleFactor / Field(i) ) );
      
      // if the result is to big:
      if ( exceeds( coord, trans ) ) {
	if (i >1) 
	  return std::make_tuple( (minLatCoord * (i-1) ),
				  scaleFactor / Field(i-1) );
	
	std::ostringstream msg;
	msg << "In maxContainedLatticeCoordinate_(...)\n"
	    << __FILE__ << " : " << __LINE__    << "\n"
	    << " trans       = " << trans       << "\n"
	    << " minResult   = (" << std::get<0>(minResult) << "," << std::get<1>(minResult) << ")\n"
	    << " i == 1 ! \n"
	    << "This should not happen if minResult was correct! \n";
	throw std::logic_error(msg.str());
      }
    }
    
    std::ostringstream msg;
    msg << "maxContainedLatticeCoordinate(" << trans << ") failed!"
	<< __FILE__ << " : " << __LINE__ << "\n"
	<< " trans       = " << trans       << "\n"
	<< " minResult   = (" << std::get<0>(minResult) << "," << std::get<1>(minResult) << ")\n"
	<< " i == " << i << " ! \n";
    
    throw std::logic_error(msg.str());
  }
  
  //======================================================================

  /* \brief If the translation is strictly longer than a corresponding
            net translation, subtract from the given translation the
            largest net translation that the given CellTranslation
            extends. Return the new scaleFactor which should now be:

	    scaleFactor <= 1.0

	    note:    latticeCoordinate * scaleFactor == trans.
  */
  template<typename Field, size_t DIM>
  Field 
  netReduce(CellTranslation<Field,DIM>& trans,
	    const char*                 title,
	    bool                        verbose=false) {
    
    typedef std::tuple< LatticeCoordinates<2>, Field >  TupleType;
    
    TupleType minLatCoordScaleFactorTuple
      = latticeCoordinateWithFactor(trans);
    
    LatticeCoordinates<2>& minLatCoord = std::get<0>(minLatCoordScaleFactorTuple);
    Field&                 scaleFactor = std::get<1>(minLatCoordScaleFactorTuple);
    
    if (verbose) 
      getTraceOut() << "----------------------------------------\n"
		    << "In " << title << " netReduce:\n"
		    << " trans       : " << trans       << "\n"
		    << " minLatCoord : " << minLatCoord << "\n"
		    << " scaleFactor : " << scaleFactor << "\n";
    
    
    if (close(scaleFactor,1.0)) 
      scaleFactor = 1.0;
	
    if ( scaleFactor <= 1.0) {
      if (verbose) 
	getTraceOut() << "  No need to reduce length in " << title<< " netReduce:\n"
		      << "    scaleFactor = " << scaleFactor << "\n"
		      << "    trans       = " << trans       << "\n";
      return scaleFactor;
    }
    
    // Otherwise: scaleFactor strictly > 0: trans is too long 
    
    TupleType maxLatCoordScaleFactorTuple 
      = maxContainedLatticeCoordinate_(minLatCoordScaleFactorTuple, 
				       trans, 
				       verbose);
    LatticeCoordinates<2>& maxLatCoord    = std::get<0>(maxLatCoordScaleFactorTuple);
    //    Field&                 maxScaleFactor = std::get<1>(maxLatCoordScaleFactorTuple);
    
    // trans -= maxLatCoord
    COPY<CellTranslation<Field,DIM>,
	 LatticeCoordinates<DIM>,
	 typename CellTranslation<Field,DIM>::Traits,
	 typename LatticeCoordinates<DIM>::Traits>::EXEC_MINUS(trans,maxLatCoord);
    
    Field newScaleFactor = std::remainder(scaleFactor,1.0);
    
    if (verbose) 
      getTraceOut() << "  After reduction in " << title<< " netReduce:\n"
		    << "    scaleFactor = " << scaleFactor << "\n"
		    << "    trans       = " << trans       << "\n";
    
    return newScaleFactor;
  }

  //======================================================================

  /* \brief Subtract the largest net translation that the given cell
   *        translation extends. Then normalize to a call-internal
   *        translation. Then normalize the direction.
   *        
   *        In normalizeToInternal, if the translation or any of its
   *        components becomes zero then the scalefactor is set to
   *        zero.
   */
  template<typename Field, size_t DIM>
  Field
  normalizeTranslation(CellTranslation<Field,DIM>& trans,
		       bool                        verbose=false) {
    
    static const CellTranslation<Field,DIM> zeroTrans;

    if (verbose)
      getTraceOut() << "==================================================\n"
		    << "In normalizeTranslation(trans= " << trans << ")\n";
	//		    <<  __FILE__ << " : " << __LINE__ << "\n";
    
    Field scaleFactor = netReduce(trans,"Translation", verbose);

    scaleFactor = normalizeToInternal(trans,scaleFactor,verbose);
    
    normalizeTranslationDirection(trans,verbose);

    return scaleFactor;
  }

  //======================================================================

  // Note: !!!!! SO far normalizeTranslation and normalizeGlide appear to be identical!

  /* \brief Subtract the largest net translation that the given cell
   *        translation extends.
   *  First we reduce the given cell translation to the smallest 
   *  translation that is a net point.
   *
   *  If the translation is on the negative side of the b-c call plane
   *  it is a negative translation. We convert it to the equivalent 
   *  one on the positive side of the B-C plane.
   *
   *  eg.  convert -a + 2b => a - 2b
   */
  template<typename Field, size_t DIM>
  Field normalizeGlide(CellTranslation<Field,DIM>& trans,
		       bool                        verbose= false) {
    
    static const CellTranslation<Field,DIM> zeroTrans;
    
    if (verbose)
      getTraceOut() << "==================================================\n"
		    << "In normalizeGlide(trans= " << trans << ")\n";
    //		    <<  __FILE__ << " : " << __LINE__ << "\n";
    
    Field scaleFactor = netReduce(trans,"Glide",verbose);
    
    // If scaleFactor == 1.0 then the trans is a net translation.
    // This is the same as a zero translation, which means that this 
    // is really a mirror operation.
    //
    if (scaleFactor == 1.0) { 
      trans = zeroTrans;
      if (verbose) 
	getTraceOut() << "  In Glide, after netReduce:\n"
		      << "     (scaleFactor == 1.0)"
		      << "      => Mirror operation"
		      << "      => trans = " << trans << "\n";
      return scaleFactor;
    }
    
    normalizeGlideDirection(trans,verbose);
    
    return scaleFactor;
  }
  
  //======================================================================
  /** 
   * \Brief Normalize the components of this symmetry element.
   *
   * \note This is a 2D function only!
   */
  template<typename Field, typename Algorithms> 
  void 
  normalize(SymmetryElement<Field,2,Algorithms>& element,
	    bool                                 verbose=false) {   

    enum {DIM=2};
    typedef CellTranslation<Field,DIM> CellTranslationType;
    
    static Field zero(0);
    //static Field one(1);
    CellTranslationType zeroTranslation;
    
    //Normalize the position
    element.cellPosition.normalize();
    
    //    if(element.type == "translation" ) verbose = true;
    //    if(element.type == "glide")        verbose = true;

    //Normalize the translation part 
    if (element.type == "translation") 
      normalizeTranslation(element.translationPart,verbose);
    
    if (element.type == "glide")
      normalizeGlide(element.translationPart,verbose);

    bool isZeroTranslation = near(element.translationPart, zeroTranslation);
    
    if (isZeroTranslation) {

      if (verbose)
        getTraceOut() << " Normalized translationPart is zero!\n";
		      // << " => a translation has become the identity or \n"
		      // << " => a glide has become a mirror\n";

      if (element.type == "translation") {
        if (verbose ) getTraceOut() << "   Changing a translation to the identity!\n";
	element.type    = "identity";
      }
      
      if (element.type == "glide") {
        if (verbose ) getTraceOut() << "   Changing a glide to a mirror!\n";
	element.type    = "mirror";
        if (verbose ) getTraceOut() << "     Setting the glideFraction from "
				    << element.glideFraction << " to zero!\n";
	element.glideFraction = zero;
      }
      
      if (element.type == "mirror") {
        if (verbose ) getTraceOut() << " Mirror: Resetting the netDirection"
				    << " from " << element.netDirection ;
	element.netDirection = element.netDirection.template normalize<Algorithms>();
        if (verbose ) getTraceOut() << " to " << element.netDirection;
      }
      
      if (element.type == "identity") {
        if (verbose ) getTraceOut() << " Identify: Resetting the netDirection"
				    << " from " << element.netDirection
				    << " to the zero translation\n";
	element.netDirection = latticeCoordinate(0,0);
      }
    }
    else {  // not isZeroTranslation

      // In 2D only glides and translations have translation parts
      if (element.type != "glide" && element.type != "translation" ) {

	std::ostringstream buff;
	buff << "SymmetryElement.normalize(" << element.unNormalizedName << ")" << std::endl;
	buff << " type is now " << element.type << " but the tranlationPart is:" << element.translationPart << std::endl;
	throw std::logic_error(buff.str());
      }

      // Set the netDirection for the glide from the transltionPart
      {
	if (verbose ) 
	  getTraceOut() << "Setting the netDirection using: \n"
			<< "  element.translationPart " << element.translationPart << "\n"
			<< "  from netDirection       " << element.netDirection << "\n" ;
	element.netDirection = latticeCoordinate(element.translationPart);
	if (verbose ) 
	  getTraceOut() << "  to   netDirection       " << element.netDirection << "\n" ;
      }

      // Set the glideFraction
      element.setGlideFraction(verbose);
    }

  }

} /* namespace SymFind */

#endif   //SymFind_NORMALIZE_SYMMETRY_OPERATION

/*@}*/
