//-*-C++-*-

#ifndef SymFind_Cell_H
#define SymFind_Cell_H

/** \ingroup latticeConcepts */
/*@{*/

/** \file  PrimitiveCell.h Contains the PrimitiveCell class definition.
 */

namespace SymFind {

  /** \ingroup latticeConcepts
   *
   * \brief The PrimitiveCell marker class.
   *
   * A primitive cell is a Cell whose basis vectors can be used to
   * generate the translation group of the lattice.
   *
   * \param Field: The scalar type used in the representation of the
   *               cell. Legal values include double, rational and
   *               sqrtExtendedRational.
   *
   * \param DIM Dimensionality of the lattice being represented. Legal values 
   *            are 1,2,3.
   */
  template <typename Field, size_t DIM>
  class PrimitiveCell: public Cell<Field,DIM> {
    
  public:
    
    
  }
}
#endif

/*@}*/
