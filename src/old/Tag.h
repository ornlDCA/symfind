//-*-C++-*-

#ifndef TAG_H
#define TAG_H

/** \ingroup ostream **/
/*@{*/

/** \file Tag.h
 *  Contains the class definition for XML Tag and related objects.
 */

namespace SymFind {

  //======================================================================

  class Tag: 
    public TagAttributes {
  public:

    class Elements: 
      public std::vector<Tag> {
    public:

      Elements():
	std::vector<Tag>()
      {}
      typedef std::vector<Tag> SuperType;
      
      template<typename TagCollectionType>
      void add(const TagCollectionType& tags) {
	this->insert(this->end(),tags.begin(),tags.end());
      }
      
      void add(const Tag& tag) {
	this->push_back(tag);
      }
    };
    
    
    std::string         name;
    Elements            elements;
    std::ostringstream  content;
    
    //------------------------------------------------------------ Default Constructor

    Tag(): name("span"), elements(), content("") {}

    //------------------------------------------------------------ One argument constructors

    Tag(const char* name_):                         name(name_) , elements(), content("") {}
    Tag(const std::string name_):                   name(name_) , elements(), content("") {}

    Tag(const Tag& tag): 
      TagAttributes(tag), 
      name(tag.name), 
      elements(tag.elements),
      content(tag.content.str()) //"")
    { 
      //      content.str(tag.content.str());  
    }

    //------------------------------------------------------------ Two argument constructors

    Tag(char* name_, const Elements&      elements_  ): name(name_), elements(elements_), content("")        {}
    Tag(char* name_, const TagAttributes& attributes ): TagAttributes(attributes), name(name_), elements(), content("")  {}
    Tag(char* name_, const Tag&        tag        ):    name(name_), elements(), content("") { elements.add(tag); }

    template<typename ContentType>
    Tag(char* name_, const ContentType& cont      ):    name(name_), elements(), content("") { content << " " << cont << " "; }

    //------------------------------------------------------------ Three argument constructors

    Tag(char* name_, const TagAttributes& attributes, const Elements& elements_): 
      TagAttributes(attributes), name(name_), elements(elements_), content("")  {}

    //------------------------------------------------------------

  /**
   * \brief Tag Assignment. 
   */
    Tag& operator= (const Tag& tag) {
      name                            = tag.name;
      elements                        = tag.elements;
      static_cast<TagAttributes>(*this)  = static_cast<TagAttributes>(tag);
      content.str(tag.content.str());
      return (*this);
    }

  /**
   * \brief Tag content Assignment. 
   */
    template<typename Type>
    Tag& operator << (const Type& something) {
      content << something;
      return *this;
    }

  /**
   * \brief Tag content Assignment. 
   */
    template<typename Type>
    Tag& operator << (const std::vector<Type>& something) {
      for(size_t i=0; i<something.size(); i++) 
	content << something[i] << " ";
      content << std::endl;
      return *this;
    }

  /**
   * \brief Add a collection of tags to this tags elements.
   */
    template< template<typename> class TagCollectionType >
    void add(const TagCollectionType<Tag>& tags) {
      elements.insert(elements.end(),tags.begin(),tags.end());
    }

    //======================================================================

    /**
     * \brief Add a tag to this tags elements.
     */
    void add(const Tag& tag) {
      elements.push_back(tag);
    }
    
    //======================================================================
    
    void toJSON(std::ostream& os, size_t offset=0) const {
      
      typedef TagAttributes::const_iterator AttributeItr;

      const TagAttributes& attrs(*this);
      size_t keyWidth = attrs.maxKeyWidth();
      
      std::string quotedTagNameKey("\"tag_name\"");

      os << std::setw(offset+1) << std::left << "{" 
	 << std::setw(keyWidth+2) << std::left << quotedTagNameKey << " : \"" 
	 << name << "\",\n";

      AttributeItr a;
      for (a=attrs.begin(); a!=attrs.end(); a++) {
	
	std::ostringstream quotedKey;
	quotedKey   << "\"" <<   a->first   << "\"";
	
	std::ostringstream quotedValue;
	quotedValue << "\"" <<   a->second << "\"";
	
	if (a != attrs.begin()) 
	  os << ",\n";

	os << std::setw(offset+1) << " "
	   << std::setw(keyWidth+2) << std::left << quotedKey.str() << " : "
	   << quotedValue.str();

      }
      if(content.str().length() > 0) {
	os  << ",\n";
	os << std::setw(offset+1) << " ";
	os << std::setw(keyWidth+2) << std::left << "\"content\"" << " : \n"
	   << "\""
	   << content.str() 
	   << "\"";
      }
      os << "}\n";
    }
  

    //======================================================================

    void writeInfoFile(std::ostringstream& path, size_t elementNumber=0) const {
      
      (void) elementNumber;
      const TagAttributes& attrs(*this);
      if (attrs.size() == 0 and content.str().length() == 0)
	return;
      
      std::ofstream of(path.str().c_str());
      toJSON(of);
      of.close();
    }

    //======================================================================
    void toFile(std::string dirPath, size_t elementNumber=0) const {
      
      (void) elementNumber;

      if (elements.size()==0) {
	std::ostringstream path;
	path <<	dirPath << "/" << name;  
// 	if (elementNumber != 0) 
// 	  path << "_" << elementNumber;
	path << ".jsn";
	writeInfoFile(path);
	return;
      }

      std::ostringstream path;
      path << dirPath << "/" << name;
      //      insureDirectoryExists(path.str());

      for (size_t i=0; i < elements.size(); i++)
	elements[i].toFile(path.str(),i);

//       if (elementNumber != 0) 
// 	path << "_" << elementNumber;
      path << "/information.jsn";
      writeInfoFile(path);
    }

    //======================================================================
    
  };

    
  //======================================================================

  namespace VectorLike {

    /** \ingroup ostream
     *
     * Output stream operator for SuperCrystal
     */
    template<typename VectorLikeType>
    Tag toXML(const VectorLikeType& vector,
	      std::string name="VectorLike") {
      Tag tag(name);
      tag["size"] = vector.size();
      tag.content << std::endl;
      print(vector,tag.content);
      return tag;
    }
    
  }
    
  //======================================================================

  namespace MatrixLike {

    /** \ingroup ostream
     *
     * Output stream operator for SuperCrystal
     */
    template<typename MatrixLikeType>
    Tag toXML(const MatrixLikeType& matrix,
	      std::string name="MatrixLike") {
      Tag tag(name);
      tag["rows"] = matrix.n_row();
      tag["cols"] = matrix.n_col();
      tag.content << std::endl;
      tag.content << std::endl;
      printWithIndices(matrix,tag.content);
      return tag;
    }
  }
    

  //======================================================================

  template<typename T>
  Tag toXML(std::string name,
	    const std::map<std::string,T> map) {

    Tag result(name);

    typedef std::map<std::string,T> InputMapType;
    typedef typename InputMapType::const_iterator MapItr;
    MapItr m;
    
    for (m=map.begin(); m!=map.end(); m++) {
      std::ostringstream strValue;
      strValue << m->second;
      result[m->first] = strValue;
    }
    return result;
  }

  //======================================================================

  inline 
  std::ostream& operator << (std::ostream& os, const Tag& t) {
    
    //    typedef TagAttributes::const_iterator AttributeItr;

    static const size_t contentBreakPoint = 100;
    std::string content = t.content.str();

    os << "<" 
       << t.name 
       << static_cast<TagAttributes>(t) ;

    if (t.elements.size() == 0 && content.size() == 0 ) {
      os << "/>" ;
      return os;
    }

    os << ">" ;

    if (t.elements.size() > 0 && content.size() > contentBreakPoint) 
      os << std::endl;

    for (size_t i=0; i < t.elements.size(); i++)
      os << std::endl << t.elements[i] ;

    if (t.elements.size() > 0)
      os << std::endl;

    os << " " << content << " ";
    
    if (content.size() >  contentBreakPoint) 
      os << std::endl;

    os << "</" << t.name << ">" ;
    return os;
  }

  //======================================================================

//   template<typename Field>
//   std::string toString(Field value) {
//     std::ostringstream buff;
//     buff << value ;
//     return buff.str();
//   }
  
  
  //======================================================================

}  

#endif

/*@}*/
