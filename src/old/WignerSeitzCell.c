//-*-C++-*-       
#ifndef WignerSeitzCell_HEADER_C
#define WignerSeitzCell_HEADER_C

#include <errno.h>
#include <exception>
#include <stdexcept>

#include <vector>

#include "SymFind_Vector.h"
#include "SymFind_Matrix.h"
#include "SymFind_CGeometry.h"

namespace SymFind {
  typedef Matrix<int>                   IntMatrixType; 
  typedef SymFind_GroupActionFilterType GroupActionFilterType;
}

#include "Typefactory.h"
#include "SymFindC++.h"

namespace SymFind {

  //======================================================================
  
  void fetchMeshWignerSeitz_double(int                  dim,
				   int                  numPoints,
				   const Matrix<double>& sitesK,
				   Matrix<double>&      sitesKmesh) {

    typedef double                 T;
    typedef BasicCrystalAlgorithms Alg;

    switch (dim) {
    case 1: {
      //      WignerSeitzMesh<T,1,Alg> (sitesK,numPoints).set(sitesKmesh);
      throw std::logic_error(__FILE__ "unimplemented");
    }
    case 2: {
      //      WignerSeitzCell<T,2,Alg> (sitesK,numPoints).setMesh(numPoints,sitesKmesh);
      throw std::logic_error(__FILE__ "unimplemented");
      return;
    }
    case 3: {
      //      WignerSeitzMesh<T,3,Alg> (sitesK,numPoints).set(sitesKmesh);
      throw std::logic_error(__FILE__ "unimplemented");
      return;
    }
    }
  }
   
} // namespace   
#endif
		   
