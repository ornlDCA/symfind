#define HostDevice 

#include <sys/resource.h>
#include <sys/stat.h>
#include <sys/time.h>
#ifdef HAVEPRCTL
#include <sys/prctl.h>
#endif
#include <sys/utsname.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <sys/mman.h>
#include <type_traits>

#include <fcntl.h>

#include <typeinfo>
//#include "typeinfo.h" //or <typeinfo>
#include <time.h>
#include <unistd.h>

#include <cstddef>

#include <string.h>
#include <string>
#include <complex>
#include <vector>
#include <set>
#include <queue>
#include <map>
#include <math.h>
#include <algorithm>

#include <cassert>

#include <iostream>
#include <sstream>
#include <fstream>
#include <iomanip>

#include <errno.h>
#include <exception>
#include <stdexcept>
#include <tuple>

#include "Assert_It.h"

std::ostream& getTraceOut();


#include "NullType.h"
#include "BasicClassification.h"
//#include "TypeManip.h"

#include "BasicCrystalAlgorithms.h"

#include "AbstractRat.h"
#include "FieldParser.h"
#include "FieldConvert.h"

#include "MatTraits.h"
#include "Vec.h"

#include "MatForEach.h"
#include "MatForEach2.h"
#include "MatForEach3.h"
#include "MatForEachDiagonal.h"

#include "MatReduce.h"
#include "MatReduce2.h"
#include "MatReduceDiagonal.h"
#include "MatCopy.h"
#include "MatDet.h"
#include "MatDifference.h"
#include "MatEqual.h"
#include "MatIdentity.h"
#include "MatInverse.h"
#include "MatMagnitude.h"
#include "MatMax.h"
#include "MatMult.h"
#include "MatPrint.h"
#include "MatSum.h"
#include "MatTrace.h"
#include "MatTranspose.h"
#include "MatIdentity.h"

#include "Mat.h"
#include "MatMagnitude.h"
#include "HermiteNormalForm.h"
#include "MatUtil.h"

#include "SeitzVector.h"
#include "SeitzTranslation.h"

#include "SeitzPosition.h"

#include "SeitzMatrixTraits.h"
#include "SeitzMatrix.h"

#include "CartesianPosition.h"
#include "CartesianTranslation.h"
#include "CartesianRotation.h"
#include "MillerDirection.h"

#include "MetricTensor.h"
#include "LatticeCoordinates.h"

#include "Classification.h"

#include "CellParameters.h"
#include "CellTranslation.h"
#include "CellPosition.h"
#include "CellDirection.h"
#include "CellRotation.h"

#include "SymmetryElementName.h"
#include "SymmetryElement.h"
#include "ElementToOperation.h"
#include "NormalizeSymmetryElement.h"

#include "SymmetryOperation.h"
#include "OperationToElement.h"

#include "TestPattern.h"
#include "AppliedSymmetryElement.h"

#include "Occupant.h"
#include "PatternData.h"

#include "LatticeTransformation.h"
#include "Pattern.h"
#include "InverseLatticeTransformation.h"
#include "Lattice.h"

#include "Orbits.h"

#include "GroupAction.h"
#include "GroupMultiplicationTable.h"
#include "SymmetryGroup.h"
#include "SpaceGroup.h"
#include "Symmetry.h"

#include "LatticeWithPattern.h"

#include "ReciprocalLattice.h"
#include "ReducedLattice.h"

#include "Centering.h"
#include "ForEachCentering.h"

#include "CrystalBase.h"
#include "Simple2DReducer.h"

#include "ReducedCrystal.h"
#include "ConventionalLattice.h"
#include "ConventionalCrystal.h"
#include "Crystal.h"

#include "SymmetryElements2D.h"
#include "IdentityElement2D.h"
#include "Mirror2D.h"
#include "Glide2D.h"
#include "TwoFold2D.h"
#include "ThreeFold2D.h"
#include "FourFold2D.h"
#include "SixFold2D.h"
#include "ThreeFoldN2D.h"
#include "FourFoldN2D.h"
#include "SixFoldN2D.h"

#include "AppliedSymmetryElementIterator.h"
#include "SpaceGroupData2D.h"
#include "SpaceGroup2D.h"

#include "Reciprocal.h"

#include "SpaceGroupConstructor.h"

/* namespace Symfind { */
/* typedef Simple2DReducer<double,Occupant,BasicCrystalAlgorithms>  Reducer;     */
/* typedef SpaceGroupConstructor<double,2>                          Classifier; */
/* } */

#include "FloodTiler.h"

#include "SuperCrystalBuilder.h"
#include "SuperCrystal.h"

#include "Neighbors.h"
#include "Wedge.h"
#include "WignerSeitzCell.h"
#include "Geometry2D.h"

/* #include "TagAttributeParser.h" */
/* #include "TagAttributes.h" */
/* #include "Tag.h" */
// #include "XMLHeading.h"

#include "ToXML.h"

#include "Neighbors.h"

//#include "Wedge.h"

