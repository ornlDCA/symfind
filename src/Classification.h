//-*-C++-*-

#ifndef SymFind_Classification_H
#define SymFind_Classification_H

/** \ingroup latticeConcepts */
/*@{*/

/** \file Classification.h  
 *
 */

namespace SymFind {

  //======================================================================
  
  template<typename M>
    class IsMatrixLike { public: enum {value=0};  };
    
  template< typename T>
    class IsMatrixLike< Matrix<T> > { public: enum {value=1};  };
    
  //======================================================================
  
  template<size_t DIM, typename T>
    class IsInCellCoordinates { public: enum {value=0};  };
    
  template<size_t DIM, typename Field, typename Algorithms>
    class IsInCellCoordinates< DIM, CellPosition<Field,DIM,Algorithms> > {
  public:
    enum {value=1};
  };

  template<size_t DIM, typename Field>
    class IsInCellCoordinates< DIM, CellTranslation<Field,DIM> > {
  public:
    enum {value=1};
  };

  template<size_t DIM>
    class IsInCellCoordinates< DIM, LatticeCoordinates<DIM> > {
  public:
    enum {value=1};
  };
  
  //======================================================================
  
  template<size_t DIM, typename T>
    class IsInCartesianCoordinates {public: enum {value=0};  };
  
  template<size_t DIM, typename Field>
    class IsInCartesianCoordinates< DIM, CartesianPosition<Field,DIM> > {
  public:
    enum {value=1};
  };
  
  template<size_t DIM, typename Field>
    class IsInCartesianCoordinates< DIM, CartesianTranslation<Field,DIM> > {
  public:
    enum {value=1};
  };

  //======================================================================
  
} /* namespace SymFind */

#endif

/*@}*/


