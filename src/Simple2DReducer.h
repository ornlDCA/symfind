//-*-C++-*-

#ifndef SymFind_Simple2DReducer_H
#define SymFind_Simple2DReducer_H

/** \ingroup latticeConcepts */
/*@{*/

/** \file  Simple2DReducer.h
 *  Contains a class providing a static method for the reduction of 2D lattices.
 */
namespace SymFind {

  template<typename, size_t,typename>          class ReducedLattice;

  template<typename Field, size_t DIM>  class SimpleReducer;

  /** \ingroup latticeConcepts
   *
   * \brief This class provides a static method for the reduction of 2D cells.
   *
   * \param Field: The scalar type used in the representation of the
   *               cell. Legal values include double, rational and
   *               sqrtExtendedRational.
   *
   */
  template<typename Field>
  class SimpleReducer<Field,2>
  {
  public:
    
    enum {DIM=2};
    
    template<typename ReducedCrystalType>
    static void reduce(ReducedCrystalType& reduced) {

      typedef typename ReducedCrystalType::Algorithms Algorithms;
      
      size_t count =0;
      
      while(true) {
	
	if (reduced.parameters.alpha > 90)
	  reduced.makeAcute(0,1);
	
	if (!close(reduced.parameters.a, reduced.parameters.b) &&
	    reduced.parameters.a > reduced.parameters.b)
	  reduced.swapBasisVectors(0,1);
	
	if (!reduced.reduceDiagonal(0,1)) {
	  
	  reduced.transform.finalize();   
	  
	  const ReducedLattice<Field,DIM,Algorithms> latticeCopyOfReduced(reduced);
	  ReducedLattice<Field,DIM,Algorithms>&      reducedLattice = reduced;
	  reduced.transform(latticeCopyOfReduced, reducedLattice); 
	  
	  return;
	}
	
	if (count++ > 100) throw std::range_error("SimpleReducer2D failed to converge!");
      }
    }
    
  };
  
} /* namespace SymFind */

#endif /* SymFind_Simple2DReducer_H */

/*@}*/
