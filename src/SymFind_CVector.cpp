//-*-C-*-    

#ifndef SymFind_CVector_C
#define SymFind_CVector_C

#include <errno.h>
#include <exception>
#include <stdexcept>

#include <stdlib.h>
#include "SymFind_CVector.h"
    
extern "C" {

#define MAKE_VECTOR_CODE(TypeName)					\
									\
    void SymFind_vAllocate_##TypeName(SymFind_CVector_##TypeName* self,	\
				      int numElements) {		\
      self->data     = 0;						\
      self->ownData  = true;						\
      self->capacity = numElements;					\
      if (numElements > 0)						\
	self->data = (TypeName*) malloc(numElements*sizeof(TypeName));	\
    }									\
    									\
    void SymFind_vDropData_##TypeName(SymFind_CVector_##TypeName* self) { \
      if(self->ownData && self->data != 0) {				\
	free(self->data);						\
	self->capacity = 0;						\
	self->size     = 0;						\
	self->data     = 0;						\
      }									\
    }									\
    									\
    void SymFind_vDestroy_##TypeName(SymFind_CVector_##TypeName* self) { \
      SymFind_vDropData_##TypeName(self);				\
      free(self);							\
    }									\
    									\
    void SymFind_vSetToVal_##TypeName(SymFind_CVector_##TypeName* self,	\
				      TypeName initVal) {		\
      for(int i=0; i< self->size; i++)					\
	(self->data)[i] = initVal;					\
    }									\
    									\
    void SymFind_vSetToOther_##TypeName(SymFind_CVector_##TypeName* self, \
					SymFind_CVector_##TypeName* other) { \
      int otherSize = other->size;					\
      for(int i=0; i< self->size; i++) {				\
	if (i< 	otherSize)						\
	  (self->data)[i] = (other->data)[i];				\
	else								\
	  (self->data)[i] = 0;						\
      }									\
    }									\
									\
    void SymFind_vInitialize_##TypeName(SymFind_CVector_##TypeName* self, \
					int                   size,	\
					TypeName              initVal) { \
      self->size = size;						\
      									\
      SymFind_vAllocate_##TypeName(self,size);				\
      SymFind_vSetToVal_##TypeName(self,initVal);			\
    }									\
  									\
    SymFind_CVector_##TypeName*						\
    SymFind_make_vector_##TypeName(int sz, TypeName i) {		\
      SymFind_CVector_##TypeName* result				\
	=								\
	(SymFind_CVector_##TypeName*)					\
	malloc(sizeof(SymFind_CVector_##TypeName));			\
      SymFind_vInitialize_##TypeName(result,sz,i);			\
      return result;							\
    }									\
    									\
    void SymFind_vResize_##TypeName(SymFind_CVector_##TypeName* self,	\
				    int sz,				\
				    TypeName i) {			\
      if (sz > self->capacity) {					\
	SymFind_vDropData_##TypeName(self);				\
	SymFind_vAllocate_##TypeName(self,sz);				\
      }									\
      self->size = sz;							\
      SymFind_vSetToVal_##TypeName(self,i);				\
    }									\
    									\
    TypeName* SymFind_vGetPtr_##TypeName(SymFind_CVector_##TypeName* self, int i) { \
      if (i >= self->capacity) throw std::range_error("SymFind_vGetPtr_" #TypeName);					\
      return &(self->data[i]);						\
    }									\
    									\
    TypeName SymFind_vGet_##TypeName(SymFind_CVector_##TypeName* self, int i) {	\
      if (i >= self->capacity) throw std::range_error("SymFind_vGet_" #TypeName);					\
      return self->data[i];						\
    }									\
									\
    void SymFind_vDeep_Assign_##TypeName(SymFind_CVector_##TypeName* self, \
					 SymFind_CVector_##TypeName* other) { \
      SymFind_vResize_##TypeName(self,other->size,0);			\
      SymFind_vSetToOther_##TypeName(self,other);			\
    }									\
									\
    void SymFind_vShallow_Assign_##TypeName(SymFind_CVector_##TypeName* self, \
					    SymFind_CVector_##TypeName* other) { \
      SymFind_vDropData_##TypeName(self);				\
      self->size    = other->size;					\
      self->data    = other->data;					\
      self->ownData = false;						\
    }									\
									\

    MAKE_VECTOR_CODE(double)
  
    MAKE_VECTOR_CODE(float)
    
    MAKE_VECTOR_CODE(int)

}

#endif
 
