# This is an input file for the SymFind code.
#
import Geometry

def main():

    # Set up a dictionary with the geometry parameters in it.
    
    specs = {}
    specs["dimension"]     = 2
    specs["latticeA"]      = 1
    specs["latticeB"]      = 1
    specs["latticeAngle"]  = 90

    numberOfMeshPointsPerOrigLatticeCell = 900

    print(["numberOfMeshPointsPerOrigLatticeCell",numberOfMeshPointsPerOrigLatticeCell])

    Nc = 4

    specs["t"]                     = 1
    
    if Nc == 1:
        specs["superLatticeA"] = [1,  0]
        specs["superLatticeB"] = [0,  1]
        specs["t"] = 4

    if Nc == 2:
        specs["superLatticeA"] = [2,  0]
        specs["superLatticeB"] = [0,  1]
        specs["t"] = 2

    if Nc == 3:
        specs["superLatticeA"] = [3,  0]
        specs["superLatticeB"] = [0,  1]
        specs["t"] = 2
        
    if Nc == 4:
        specs["superLatticeA"] = [2,  0]
        specs["superLatticeB"] = [0,  2]
        specs["t"] = 2

    if Nc == 6:
        specs["superLatticeA"] = [2,  2]
        specs["superLatticeB"] = [2, -1]

    if Nc == 8:
        specs["superLatticeA"] = [2,  2]
        specs["superLatticeB"] = [2, -2]

    if Nc == 16:
        specs["superLatticeA"] = [0,  2]
        specs["superLatticeB"] = [2,  0]

 
    Geometry.initialize("OutputData/InputEcho",specs)

#    print( Geometry.momentumSymmetryGroupAction(2,3) )

    Geometry.print("GeometryDump")
    Geometry.printCrystals("CrystalDump")
            
    MomentumLattice = Geometry.MomentumPrimativeVectors()

    Geometry.loadMesh(numberOfMeshPointsPerOrigLatticeCell,
                      MomentumLattice)

    Geometry.dumpMesh("MeshPLotFile")


main()

