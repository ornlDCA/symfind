//-*-C++-*-

/** \file    symFind.cpp
 *  \ingroup interface
 *  \author  Michael Summers
 *
 *  A main program providing an example use of the SymFind C++
 *  interface.
 *
 */

#include <cstdio>
#include <iostream>
#include <exception>
#include "SymFindC++.h"

namespace SymFind {
void safeOpenFile(std::ofstream& file_,
                  const char*    fileName,
                  const char*    location,
                  std::ios_base::openmode mode);
}
//======================================================================
//
int main(int argc,char *argv[]) 
{

  if (argc < 2) {
    printf("Usage: symFind xmlOurDir \n");
    for(int i=0; i<argc; i++)
      printf(" Arg[%d = %s \n", i , argv[1]);
    return -1;
  }

  typedef double                   T;

  SymFind::SymFind_Crystal<T,2> rCluster(Space);
  SymFind::SymFind_Crystal<T,2> kCluster(Momentum);
  SymFind::Matrix<T>            mesh    (0,0,0,0);
  {
    // We describe a cluster which is a supercell of a primitive lattice.
    //
    double a = 1.0;        //  length of primitive call basis vector 1
    double b = 1.0;        //  length of primitive call basis vector 2
    double angle = 60;     //  angle between a and b in degrees
    
    int latticeA1 = 2;  //  The integral multipliers that are used to produce the 
    int latticeA2 = 0;  //  super-cell lattice vectors from the primitive vectors
    int latticeB1 = 0;
    int latticeB2 = 2;
    
    // int latticeA1 = 2;  //  The integral multipliers that are used to produce the 
    // int latticeA2 = 2;  //  super-cell lattice vectors from the primitive vectors
    // int latticeB1 = 2;
    // int latticeB2 = -2;
      
    SymFind::SymFind_Geometry<T,2> geometry(a,b,angle,
					    latticeA1,
					    latticeA2,
					    latticeB1,
					    latticeB2);
    SymFind::writeXML(geometry,argv[1]);
    geometry.fetch(rCluster,kCluster);
    
    geometry.fetchWignerSeitzMesh(3600,mesh);
  }

  print(std::cout,rCluster);
  print(std::cout,kCluster);

  std::ofstream meshFile;
  SymFind::safeOpenFile(meshFile,"plotMe",
			__FILE__,std::ofstream::out);

  meshFile << "# WignerSeitz kpoint mesh:\n";
  meshFile << "# Try gnuplot:\n";
  meshFile << "#   splot 'file' u 2:3:4\n";
  meshFile << "#------------------------------\n";
  
  for(size_t i=0; i< mesh.n_col(); i++) {
    meshFile << std::setw(5) << i
              << std::setw(15) << mesh(0,i)
              << std::setw(15) << mesh(1,i)
              << std::setw(15) << "1\n";
  }
  
  SymFind_doneWithGeometry_2D_double();

  return 0;
}
  
