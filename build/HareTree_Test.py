
import Geometry

def main():
    
    FullOutDir = "OutputData" + "/DcaInput/N0"

    #======================================================================

    # Set up a dictionary with the geometry parameters in it.

    inputs = {}
    inputs["dimension"]     = 2
    inputs["latticeA"]      = 1
    inputs["latticeB"]      = 1
    inputs["latticeAngle"]  = 90

    numberOfMeshPointsPerOrigLatticeCell = 640000
    
    Nc = 4

    inputs["t"]                     = 1
    
    if Nc == 1:
        inputs["superLatticeA"] = [1,  0]
        inputs["superLatticeB"] = [0,  1]
        inputs["t"] = 4

    if Nc == 2:
        inputs["superLatticeA"] = [2,  0]
        inputs["superLatticeB"] = [0,  1]
        inputs["t"] = 2

    if Nc == 3:
        inputs["superLatticeA"] = [3,  0]
        inputs["superLatticeB"] = [0,  1]
        inputs["t"] = 2
        
    if Nc == 4:
        inputs["superLatticeA"] = [2,  0]
        inputs["superLatticeB"] = [0,  2]
        inputs["t"] = 2

    if Nc == 6:
        inputs["superLatticeA"] = [2,  2]
        inputs["superLatticeB"] = [2, -1]

    if Nc == 8:
        inputs["superLatticeA"] = [2,  2]
        inputs["superLatticeB"] = [2, -2]

    if Nc == 9:
        inputs["superLatticeA"] = [3,  0]
        inputs["superLatticeB"] = [0,  3]

    if Nc == 16:
        inputs["superLatticeA"] = [4,  0]
        inputs["superLatticeB"] = [0,  4]

    #------------------------------------------------------------
    
    Geometry.initialize(FullOutDir,inputs)

    MomentumLattice = Geometry.MomentumPrimativeVectors()

    Geometry.loadMesh(numberOfMeshPointsPerOrigLatticeCell,
                      MomentumLattice)

    Geometry.print("GeometryDump")

    Geometry.printCrystals("CrystalDump")
            
    Geometry.dumpMesh("MeshPLotFile")


main()

