//-*-C++-*-

//-*-C++-*-

/** \file    symFind.cpp
 *  \ingroup interface
 *  \author  Michael Summers
 *
 *  A main program providing an example use of the SymFind C++
 *  python interface.
 *
 */

//--------------------------------------------------

#include <iostream>
#include <string>
#include <complex>
#include <vector>
#include <map>
#include <iostream>
#include <fstream>
#include <sstream>

#include "Utility.h"
#include "TinySciPy.h"

//======================================================================

extern "C" {  bool symFind_isOkToWrite() { return true; } }

//======================================================================

namespace TinySciPy {
  void Register_SymFind_PyModule(TinySciPy::VM&);
  //   void Register_LinearAlgebra_PyModule(VM&);
}

//======================================================================
//
int main(int argc, const char *argv[]) 
{
  using namespace TinySciPy;
  
  if (argc < 2) {
    printf("Usage: pyLinearAlgebra input.py \n");
    for(int i=0; i<argc; i++)
      printf(" Arg[%d] = %s \n", i , argv[1]);
    return -1;
  }

  Obj code;

  { // Start up a VM for the compilation
    VM vm(argc,argv);
    int verbose = 0;
    code = vm.compileFile(argv[1],verbose);
  } 

  if (true) {

    VM vm(argc,argv); 

    math_init                      (vm);
    Register_SymFind_PyModule      (vm);
    //    Register_LinearAlgebra_PyModule(vm);
    
    Obj globals(Obj::make<Dict>());
    
    int verbose = 0;
    Obj result = vm.exec(code, globals,verbose);
  }
  
  return 0;
}
  
