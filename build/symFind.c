//-*-C-*-

/** \file    symFind.c
 *  \ingroup interface
 *  \author  Michael Summers
 *
 *  A main program providing an example use of the SymFind C
 *  interface.
 *
 */

#include <cstdio>
#include "SymFind.h"

//======================================================================
//
int main(int argc,char *argv[]) 
{
  if (argc < 2) {
    printf("Usage: symFind xmlOurDir \n");
    for(int i=0; i<argc; i++)
      printf(" Arg[%d = %s \n", i , argv[1]);
    return -1;
  }

  // Be sure to initialize everything!
  {
    SymFind_CGeometry geometry;
    SymFind_CGeometry_Init(&geometry);
    
    SymFind_make_CGeometry2D_double(&geometry,2,
				    1,1,60,
				    2,0,0,2);
    
    /* SymFind_make_CGeometry2D_double(&geometry,2, */
    /* 				    1,1,90, */
    /* 				    2,2,2,-2); */
    
    SymFind_writeXML(geometry, argv[1]);
    
    SymFind_CCrystal_double rCrystal;
    SymFind_CCrystal_Initialize_double(&rCrystal,2,Space);
    
    SymFind_CCrystal_double kCrystal;
    SymFind_CCrystal_Initialize_double(&kCrystal,2,Momentum);
    
    SymFind_fetch_cgeometry_double(geometry,
				   &rCrystal,
				   &kCrystal);
    
    SymFind_print(&rCrystal);

    SymFind_print(&kCrystal);

    SymFind_delete_CGeometry2D_double(&geometry);
    SymFind_CCrystal_DropData_double(&rCrystal);
    SymFind_CCrystal_DropData_double(&kCrystal);

  }

 
  SymFind_doneWithGeometry_2D_double();
  
  return 0;
}
  


