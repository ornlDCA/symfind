#!/usr/bin/python
# 
import argparse,os,sys
from subprocess import call

parser = argparse.ArgumentParser()

#parser.add_argument("source",      help="The file to compile.")
#parser.add_argument("target",      help="The name of the resulting executable")

parser.add_argument("source")
parser.add_argument("target")
parser.add_argument("--dryRun",   "-d",    help="Don't run the compiler => --verbose.",               action="store_true")
parser.add_argument("--gdb",      "-g",    help="Include symbols for the gdb-er.",                    action="store_true")
parser.add_argument("--valgrind", "-v",    help="If specified the executable will support valgrind.", action="store_true")
parser.add_argument("--indices",  "-i",    help="Turn on indice checking (very slow).",               action="store_true")
parser.add_argument("--verbose",  "-verb", help="Display the compilation command.",                   action="store_true")

args   = parser.parse_args()

#----------------------------------------------------------------------

TinySciPyDir =  "../tinyscipy"
UtilitiesDir =  "../utilities"

IncludeDirs = [ ".", 
                "../include",
                "../src",
                "../src/API",
                "../src/Mat",
                "%s/include" % (TinySciPyDir),
                "%s/include" % (UtilitiesDir)]

INCLUDEs =  " ".join([ "-I" + inc for inc in IncludeDirs])

#----------------------------------------------------------------------

Lib_Dirs     = " -L. -L%s/build -L%s/build " % (UtilitiesDir,TinySciPyDir)
LIBRARIES    = Lib_Dirs + " -llapack -lblas -lm -lSymFind -lTinySciPy -lUtilities "

#----------------------------------------------------------------------

CC           = "g++ -std=c++14 "

DEFINES      = "-DHAVE_RUSAGE_THREAD -DHAVEJPEG -DNOHAVENUMA "
 
FLAGs1       = "-fPIC -Wall -Wextra -Werror -Wreturn-type -Wno-long-long -pedantic -Wno-variadic-macros "
FLAGs2       = "-ffast-math -funroll-loops -fdiagnostics-show-option "

ASAN         =  "-fsanitize=address -fno-omit-frame-pointer -fno-optimize-sibling-calls"

#----------------------------------------------------------------------
# Automate the generation of Dependancies
#
TGT_NAME,TGT_SFX = os.path.splitext(args.target)

DEPFLAGS_PAT = "-MT %s -MMD -MP -MF .d/%s.Td " 
DEPFLAGS     = DEPFLAGS_PAT % (args.target,TGT_NAME)

print "DEPFLAGS", DEPFLAGS

POST_CMD_PAT = "mv -f .d/%s.Td .d/%s.d"
POSTCOMPILE  = POST_CMD_PAT % (TGT_NAME,TGT_NAME)

print "POSTCOMPILE", POSTCOMPILE

#----------------------------------------------------------------------

if(args.valgrind):
    DEFINES   = DEFINES + "-DHAVE_VALG "
#    LIBRARIES = LIBRARIES + "-L /usr/lib64/valgrind " 
#    LIBRARIES = LIBRARIES + "-lmpiwrap-amd64-linux " 

if(args.indices):
    DEFINES = DEFINES + "-D_GLIBCXX_DEBUG "
else:
    DEFINES = DEFINES + "-DNDEBUG "


if(args.gdb):
    CC = CC + "-g "
else:
    CC = CC + "-O2 "


if (args.dryRun):
    args.verbose = True

#------------------------------------------------------------
    
if TGT_SFX == ".o":
    SRC_TGT = "-c %s" % (args.source)
    LIBRARIES = ""
else:
    SRC_TGT = "%s -o %s" % (args.source,args.target)

CMD = "time %s %s %s %s %s %s" % (CC,
                                  FLAGs1 + FLAGs2 + DEPFLAGS ,
                                  DEFINES,
                                  INCLUDEs,
                                  SRC_TGT, 
                                  LIBRARIES)

PrintFmt = "time %s \n %s \n %s \n %s \n %s \n %s \n %s \n %s" 

CMD2 =  PrintFmt % (CC,
                    FLAGs1,
                    FLAGs2,
                    DEPFLAGS,
                    DEFINES,
                    INCLUDEs,
                    SRC_TGT,
                    LIBRARIES)

#======================================================================

print "-------------------------------  compiling: %s => %s" % (args.source, args.target)

if(args.verbose):
    print "---------------------------------------- CMD:"
    print CMD2
    print "---------------------------------------- "
    print "\n"

try:
    retcode = 0 
    if (not args.dryRun): 
        retcode  = call(CMD,        shell=True)
        retcode1 = call(POSTCOMPILE,shell=True)
    if retcode < 0:
        print >>sys.stderr, "Compilation was terminated by signal ", -retcode
    else:
        if (args.dryRun):
            print >>sys.stderr, "Compilation dry run! "
        else:
            print >>sys.stderr, "Compilation returned ", retcode
except OSError as e:
    print >>sys.stderr, "Compilation failed! :", e
    print CMD

print "-------------------------------  compiling: %s => %s done!" % (args.source, args.target)
