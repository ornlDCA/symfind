
#set args testXML_DGB

#
# C++ related beautifiers (optional)
#

set args input.py

set print pretty on
set print object on
set print array on
set print elements 1000
set print static-members on
set print vtbl on
set print demangle on
set demangle-style gnu-v3
set print sevenbit-strings off

#break SymmetryElement.h : 371
#break GroupMultiplicationTable.h : 88
#break GroupMultiplicationTable.h : 114
#break WignerSeitzMesh.h : 124
break SymFindModule.cpp : 190
#break WignerSeitzCell.c : 57

break main
