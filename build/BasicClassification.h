//-*-C++-*-

#ifndef SymFind_BasicClassification_H
#define SymFind_BasicClassification_H

/** \ingroup Classification */
/*@{*/

/** \file BasicClassification.h  
 *
 */

namespace SymFind {
  
  template<typename T>  
    struct CanBeScalar { 
      enum{value=(std::is_arithmetic<T>::value
		  or 
		  std::is_same<T,std::string>::value)}; 
    };
}


#endif
/*@}*/
