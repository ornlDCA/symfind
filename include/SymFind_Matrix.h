//-*-C++-*-    

#ifndef SymFind_Matrix_HEADER_H
#define SymFind_Matrix_HEADER_H

#include <iostream>
#include <sstream>
#include <fstream>
#include <iomanip>
#include <cmath>

#include "SymFind_CMatrix.h"
#include "SymFind_CVector.h"
#include "SymFind_CCrystal.h"
#include "SymFind_Chooser.h"

namespace SymFind {

  //======================================================================
  
  template<typename T>
  class Matrix {
  private:

    //----------------------------------------------------------------------
    
    Matrix():
      cmatrixPtr   (Choose::Make()),
      ownTheCMatrix(true)
    {}
    
    //----------------------------------------------------------------------

    //----------------------------------------------------------------------
    
    Matrix(const Matrix& other):
      cmatrixPtr   (Choose::Make()),
      ownTheCMatrix(true)
    {
      Choose::DeepAssign(cmatrixPtr,other.cmatrixPtr);
    }
    
    //----------------------------------------------------------------------

  public:
    
    typedef Chooser<T>                        Choose;
    typedef typename Chooser<T>::CMatrixType  CMatrixType;
    typedef T                                 value_type;
    typedef T                                 ValueType;
    
    CMatrixType*  cmatrixPtr;
    bool          ownTheCMatrix;
    
    //----------------------------------------------------------------------
    
    Matrix(CMatrixType& cMatrix_):
      cmatrixPtr   (&cMatrix_),
      ownTheCMatrix(false)
    {}
    
    //----------------------------------------------------------------------
    
    ~Matrix() {
      
      if(ownTheCMatrix and cmatrixPtr != 0) {
	Choose::Kill(cmatrixPtr);
	cmatrixPtr = 0;
      }
    }
    
    //----------------------------------------------------------------------
    
    Matrix(int lDim, int nRow, int nCol, T initVal):
      cmatrixPtr   (Choose::Make(lDim,nRow,nCol,initVal)),
      ownTheCMatrix(true)
    {}
    
    //----------------------------------------------------------------------
    
    Matrix(int nRow, int nCol):
      cmatrixPtr   (Choose::Make(nRow,nRow,nCol,T())),
      ownTheCMatrix(true)
    {}
    
    //----------------------------------------------------------------------

    Matrix& operator= (const Matrix& other) {
      Choose::DeepAssign(cmatrixPtr,other.cmatrixPtr);
      return *this;
    }
    
    //----------------------------------------------------------------------
    
    int capacity() const { return cmatrixPtr->lDim * cmatrixPtr->nCol; }
    
    //----------------------------------------------------------------------
    
    int size()     const { return cmatrixPtr->nRow * cmatrixPtr->nCol; }
    
    //----------------------------------------------------------------------

    void resize(int lDim, int nRow, int nCol) {
      Choose::Resize(cmatrixPtr,lDim,nRow,nCol,T());
    }
 
    //----------------------------------------------------------------------

    void resize(int lDim, int nRow, int nCol, T init) {
      Choose::Resize(cmatrixPtr,lDim,nRow,nCol,init);
    }

    //----------------------------------------------------------------------

    void resize(int nRow, int nCol) {
      Choose::Resize(cmatrixPtr,nRow,nRow,nCol,T());
    }

    //----------------------------------------------------------------------
    
    T& operator[] (int o) {
      return Choose::get(cmatrixPtr,o);
    }
    
    //----------------------------------------------------------------------
    
    const T& operator[] (int o) const {
      return Choose::get(cmatrixPtr,o);
    }
    
    //----------------------------------------------------------------------
    
    int l_dim() const { return cmatrixPtr->lDim; }
    int n_row() const { return cmatrixPtr->nRow; }
    int n_col() const { return cmatrixPtr->nCol; }
    
    //----------------------------------------------------------------------
    
    T& operator() (int r, int c) {
      return Choose::get(cmatrixPtr,r,c);
    }
    
    //----------------------------------------------------------------------
    
    const T& operator() (int r, int c) const {
      return Choose::get(cmatrixPtr,r,c);
    }
    
    //======================================================================

    void print(std::ostream& os) const {
      for (int o = 0; o < n_row(); o++) {
	for (int p = 0; p < n_col(); p++) 
	  os << (*this)(o,p) << " ";
	os << std::endl;
      }
    }

    //----------------------------------------------------------------------    
  }; // end of class 

  template<typename MatrixLikeType>
  void mprintWithIndices(const MatrixLikeType& matrix,
			 std::ostream& os,
			 int width=13) {
    
    typedef typename MatrixLikeType::ValueType ValueType;
    
    // Print Col Indices
    os << std::setw(width) << " " << " | "; // space for index column
    for(int j=0; j< (int)matrix.n_col(); j++) 
      os << " " << std::setw(width) << j;
    
    std::string line((width+1)*(matrix.n_col()+1),'_');
    os << "\n  " << line << "\n";
    
    for(int i=0; i<(int)matrix.n_row(); i++) {
      os << std::setw(width) << i << " | "; // space for index column
      for(int j=0; j<(int)matrix.n_col(); j++) 
	if (matrix(i,j) == static_cast<ValueType>(0.0))
	  os << " " << std::setw(width) << "_"; 
	else {
	  if (std::abs(matrix(i,j)) < 1.0E-6) 
	    os << " " << std::setw(width) << "."; // matrix(i,j);
	  else
	    os << " " << std::setw(width) << matrix(i,j);
	}
      os << "\n";
    }
  }
  
  //======================================================================
  template<typename T>
  void display(const Matrix<T>& mat, std::ostream& os) {
    
    os << "cmatrixPtr = " << (void*) mat.cmatrixPtr << "\n";
    if (mat.cmatrixPtr != 0) {
      os << "cmatrixPtr->lDim        = " << mat.cmatrixPtr->lDim << "\n";
      os << "cmatrixPtr->nRow        = " << mat.cmatrixPtr->nRow << "\n";
      os << "cmatrixPtr->nCol        = " << mat.cmatrixPtr->nCol << "\n";
      os << "cmatrixPtr->numElements = " << mat.cmatrixPtr->numElements << "\n\n";
      mprintWithIndices(mat, os);
      os << "\n\n";
    }
  }

  //======================================================================

} // namespace SymFind


#endif
