//-*-C-*-    

/** \file    SymFindC++.h
 *  \ingroup interface
 *  \author  Michael Summers
 *
 */

#ifndef SymFindCPP_HEADER_H
#define SymFindCPP_HEADER_H

#include <stdexcept>

#include "SymFind.h"

namespace SymFind { typedef SymFind_GroupActionFilterType GroupActionFilterType; }

#include "SymFind_Vector.h"
#include "SymFind_Matrix.h"
#include "SymFind_Crystal.h"
#include "SymFind_Geometry.h"
#include "SymFind_Geometry.h"

//#include "fetchMeshWignerSeitz.h"

#endif
 


