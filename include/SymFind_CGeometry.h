//-*-C-*-       

/** \file    SymFind_CGeometry.h
 *  \ingroup interface
 *  \author  Michael Summers
 *
 */

#ifndef SymFind_CGEOMETRY_H
#define SymFind_CGEOMETRY_H

extern "C" {

  typedef enum {NoGroupActionFilter,FullGroupActionFilter,
		PointGroupOnly,UniqueGroupActionFilter} SymFind_GroupActionFilterType; 

  //======================================================================

  typedef struct {

    void* ptr; // points to a C++ object

  } SymFind_CGeometry; 
  
  //----------------------------------------------------------------------

  void  SymFind_CGeometry_Init(SymFind_CGeometry* ptr) ;
  
  //----------------------------------------------------------------------

  void SymFind_make_CGeometry2D_double(SymFind_CGeometry* geoPtr,
				       int    dim,
				       double latticeA_length, 
				       double latticeB_length, 
				       double latticeAngle,
				       int    superlatticeA_x,
				       int    superlatticeA_y,
				       int    superlatticeB_x,
				       int    superlatticeB_y) ;
  
  void SymFind_delete_CGeometry2D_double(SymFind_CGeometry* geoPtr);

  //----------------------------------------------------------------------

  void SymFind_fetch_cgeometry_double
  (SymFind_CGeometry               geo,
   SymFind_CCrystal_double*        rCluster_,
   SymFind_CCrystal_double*        kCluster_,
   SymFind_GroupActionFilterType   filterType=PointGroupOnly) ;
  

  void SymFind_fetchWignerSeitzMesh_double(SymFind_CGeometry              geo,
					   int                            numPts,
					   SymFind_CMatrix_double*        meshMatPtr);

  /* //====================================================================== */

  /* typedef struct { */
  /*   void* ptr; // points to a C++ object */
  /* } SymFind_C_Mesh_Geometry;  */
  
  /* //---------------------------------------------------------------------- */

  /* void SymFind_C_Mesh_Geometry_Init(SymFind_C_Mesh_Geometry* ptr); */
  
  /* //---------------------------------------------------------------------- */

  /* void SymFind_make_C_MeshGeometry2D_double(SymFind_C_Mesh_Geometry* meshPtr, */
  /* 					    double latticeA_length,  */
  /* 					    double latticeB_length,  */
  /* 					    double latticeAngle, */
  /* 					    int    superlatticeA_x, */
  /* 					    int    superlatticeA_y, */
  /* 					    int    superlatticeB_x, */
  /* 					    int    superlatticeB_y, */
  /* 					    int    numPts); */
  
  /* //---------------------------------------------------------------------- */

  /* void SymFind_delete_C_MeshGeometry2D_double(SymFind_C_Mesh_Geometry* meshPtr); */

  /* //---------------------------------------------------------------------- */
  
  /* void SymFind_fetch_mesh_double(SymFind_C_Mesh_Geometry    mesh, */
  /* 				 SymFind_CMatrix_double*  meshMatrixPtr) ; */

  //======================================================================

  void SymFind_print            (SymFind_CCrystal_double* crystal_);
  
  void SymFind_mprintWithIndices(SymFind_CMatrix_double* mat);

  void SymFind_writeXML         (SymFind_CGeometry geo, const char* pathName);

  void SymFind_doneWithGeometry_2D_double();

}

#endif
		   
