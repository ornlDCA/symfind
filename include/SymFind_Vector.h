//-*-C++-*-    

#ifndef SymFind_Vector_HEADER_H
#define SymFind_Vector_HEADER_H

#include <iostream>
#include <sstream>
#include <fstream>
#include <iomanip>

#include "SymFind_CVector.h"
#include "SymFind_CMatrix.h"
#include "SymFind_CCrystal.h"
#include "SymFind_Chooser.h"

namespace SymFind {

  //======================================================================
  
  template<typename T>
  class Vector {
  private:

    //----------------------------------------------------------------------
    
    Vector():
      cvectorPtr   (0),
      ownTheCVector(true)
    {}
    
    //----------------------------------------------------------------------
    
  public:
    
    typedef Chooser<T>                       Choose;
    typedef typename Chooser<T>::CVectorType CVectorType;
    typedef T                                value_type;
    typedef T                                ValueType;
    
    CVectorType*  cvectorPtr;
    bool          ownTheCVector;
    
    //----------------------------------------------------------------------
    
    Vector(CVectorType& cVector_):
      cvectorPtr   (&cVector_),
      ownTheCVector(false)
    {}
    
    //----------------------------------------------------------------------
    
    Vector(int size, T initVal):
      cvectorPtr   (new CVectorType),
      ownTheCVector(true)
    {
      Choose::Initialize(cvectorPtr,size,initVal);
    }
    
    //----------------------------------------------------------------------
    
    ~Vector() {

      if(ownTheCVector and cvectorPtr != 0) {
	Choose::Kill(cvectorPtr);
	delete cvectorPtr;
	cvectorPtr = 0;
      }
    }
    
    //----------------------------------------------------------------------

    Vector& operator= (const Vector& other) {
      Choose::DeepAssign(&cvectorPtr,&other.cvectorPtr);
      return *this;
    }
    
    //----------------------------------------------------------------------
    
    int size()     const { return cvectorPtr->size; }
    
    //----------------------------------------------------------------------
    
    int capacity()     const { return cvectorPtr->capacity; }
    
    //----------------------------------------------------------------------

    void resize(int len) {
      Choose::Resize(cvectorPtr,len,0);
    }

    //----------------------------------------------------------------------
    
    T& operator[] (int o) {
      return Choose::get(cvectorPtr,o);
    }
    
    //----------------------------------------------------------------------
    
    const T& operator[] (int o) const {
      return Choose::get(cvectorPtr,o);
    }
    
    //----------------------------------------------------------------------    
  }; // end of class 


  template<typename VectorLikeType>
  void printList(const VectorLikeType& vector,
		 std::ostream& os,
		 char sep = ',',
		 int width=13) {
    os << "[ ";
    
    for(int i=0; i< static_cast<int>(vector.size()); i++) 
      if (i==0)
	os  << std::setw(width) << vector[i];
      else
	os  << sep << " "
	    << std::setw(width) << vector[i];
    
    os << "]";
  }


} // namespace SymFind


#endif
