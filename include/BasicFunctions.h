//-*-C++-*-
//
/** \file    BasicFunctions.h
 *  \ingroup Basics
 *  \author  Michael Summers
 *
 *  \
 */

#ifndef Basics_BasicFunctions_h
#define Basics_BasicFunctions_h

#include <iostream>
#include <fstream>
#include <string>

namespace Basics {

  // //======================================================================
  
  // void safeOpenFile(std::ofstream& file_,
  // 		    const char*    fileName,
  // 		    const char*    location,
  // 		    std::ios_base::openmode mode);
  
  // //======================================================================
  
  // void safeOpenFile(std::ifstream& file_,
  // 		    const char*    fileName,
  // 		    const char*    location,
  // 		    std::ios_base::openmode mode);
  
  // //======================================================================

  // void SymFind_openFile(const char*    context,
  //  			std::ofstream& printFile, 
  //  			std::string    whereToPrint);

  // //======================================================================
  
  // void insureDirectoryExists(const char* dirName);
  // void insureDirectoryExists(std::string dirName);
  // void insurePathExists     (std::string path);
  
  //======================================================================
}

//======================================================================

std::ostream& getTraceOut();


#endif
