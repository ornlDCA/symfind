//-*-C++-*-       

#ifndef SymFind_Crystal_HEADER_H
#define SymFind_Crystal_HEADER_H

namespace SymFind {

  //----------------------------------------------------------------------
  // The matricies in this object are surrogates.
  //
  
  template<typename T, int DIM>
  class SymFind_Crystal {

    SymFind_Crystal() 
    {}

  public:
    
    typedef Chooser<T>                ChooserType;
    typedef typename ChooserType::CCrystalType CCrystalType;
    
    CCrystalType*  ccrystalPtr;
    bool           ownCrystal;

    Matrix<T>        sites;
    Matrix<T>        primativeVectors;
    Matrix<T>        latticeVectors;        // each column is a vector
    Matrix<int>      siteDiffToSite;        // siteFor(c-r) is a row , col
    Matrix<int>      siteSumToSite;
    Matrix<int>      symmetryGroupAction;
    Matrix<int>      fullGroupAction;    
    Vector<int>      siteDegree;
    Matrix<T>        neighborDistances;    
    Matrix<int>      nearestNeighbor;   
    Vector<int>      site2Wedge;
    Matrix<int>      wedge2Site;

    //----------------------------------------------------------------------

    SymFind_Crystal(const SymFind_Crystal& other):
      ccrystalPtr          (other->ccrystalPtr),
      ownCrystal           (false),
      sites                (ccrystalPtr->sites),
      primativeVectors     (ccrystalPtr->primativeVectors),
      latticeVectors       (ccrystalPtr->latticeVectors),
      siteDiffToSite       (ccrystalPtr->siteDiffToSite),
      siteSumToSite        (ccrystalPtr->siteSumToSite),
      symmetryGroupAction  (ccrystalPtr->symmetryGroupAction),
      fullGroupAction      (ccrystalPtr->fullGroupAction),
      siteDegree           (ccrystalPtr->siteDegree),
      neighborDistances    (ccrystalPtr->neighborDistances),
      nearestNeighbor      (ccrystalPtr->nearestNeighbor),
      site2Wedge           (ccrystalPtr->site2Wedge),
      wedge2Site           (ccrystalPtr->wedge2Site)
    {}

    //----------------------------------------------------------------------

    SymFind_Crystal(CCrystalType& crystal_):
      ccrystalPtr          (&crystal_),
      ownCrystal           (false),
      sites               (ccrystalPtr->sites),
      primativeVectors    (ccrystalPtr->primativeVectors),
      latticeVectors      (ccrystalPtr->latticeVectors),
      siteDiffToSite      (ccrystalPtr->siteDiffToSite),
      siteSumToSite       (ccrystalPtr->siteSumToSite),
      symmetryGroupAction (ccrystalPtr->symmetryGroupAction),
      fullGroupAction     (ccrystalPtr->fullGroupAction),
      siteDegree          (ccrystalPtr->siteDegree),
      neighborDistances   (ccrystalPtr->neighborDistances),
      nearestNeighbor      (ccrystalPtr->nearestNeighbor),
      site2Wedge          (ccrystalPtr->site2Wedge),
      wedge2Site          (ccrystalPtr->wedge2Site)
    {}

    //----------------------------------------------------------------------
    
    SymFind_Crystal(SymFind_CCrystalTypes type):
      ccrystalPtr         (ChooserType::makeCCrystal(type,DIM)),
      ownCrystal          (true),
      sites               (ccrystalPtr->sites),
      primativeVectors    (ccrystalPtr->primativeVectors),
      latticeVectors      (ccrystalPtr->latticeVectors),
      siteDiffToSite      (ccrystalPtr->siteDiffToSite),
      siteSumToSite       (ccrystalPtr->siteSumToSite),
      symmetryGroupAction (ccrystalPtr->symmetryGroupAction),
      fullGroupAction     (ccrystalPtr->fullGroupAction),
      siteDegree          (ccrystalPtr->siteDegree),
      neighborDistances   (ccrystalPtr->neighborDistances),
      nearestNeighbor     (ccrystalPtr->nearestNeighbor),
      site2Wedge          (ccrystalPtr->site2Wedge),
      wedge2Site          (ccrystalPtr->wedge2Site)
    {}
    
    //----------------------------------------------------------------------
    
    ~SymFind_Crystal() {
      if( ownCrystal && ccrystalPtr !=0) {
	ChooserType::dropCCrystalData(ccrystalPtr);
	delete ccrystalPtr;
	ccrystalPtr = 0;
      }
    }

    //----------------------------------------------------------------------

    int dimension() const { return DIM; }

    // //----------------------------------------------------------------------

    // void fetchWignerSeitzMesh(int       numPoints,
    //     		      Matrix<T>& meshMat) {
    //   if (ccrystalPtr == 0) {
    //     std::ostringstream os;
    //     os << "======================================================================\n";
    //     os << __FILE__ << " : " << __LINE__ << "\n";
    //     os << " tried to fetchQWignerSeitzMesh on an uninitialized crystal!"                        << "\n";
    //     throw std::logic_error(os.str());
    //   }
    //   ChooserType::fetchWignerSeitzMesh(ccrystalPtr,numPoints,meshMat.cmatrixPtr);
    // }
    
    //----------------------------------------------------------------------
  };

  //======================================================================

  template<typename T, int DIM>
  void print(std::ostream& os, const SymFind_Crystal<T,DIM>& crystal) {

    if (crystal.ccrystalPtr == 0) {
      os << "======================================================================\n";
      os << " tried to print an uninitialized crystal!"                        << "\n";
      return;
    }
    
    os << "======================================================================\n";

    os << " dimension          = " << crystal.ccrystalPtr->dimension      << "\n";
    os << " initialized        = " << crystal.ccrystalPtr->initialized    << "\n";
    os << " type               = " << crystal.ccrystalPtr->type           << "\n";
    os << " numberOfSites      = " << crystal.ccrystalPtr->numberOfSites  << "\n";
    os << " piPointIndex       = " << crystal.ccrystalPtr->piPointIndex   << "\n";
    os << " zeroPointIndex     = " << crystal.ccrystalPtr->zeroPointIndex << "\n";
    os << " numberOfSymmetries = " << crystal.ccrystalPtr->numberOfSymmetries << "\n";

    os << "================================================== primativeVectors (row vectors):\n";
    mprintWithIndices(crystal.primativeVectors,os); os << "\n";
    
    os << "================================================== latticeVectors   (row vectors):\n";
    mprintWithIndices(crystal.latticeVectors,os); os << "\n";
    
    os << "================================================== sites   (column vectors):\n";
    mprintWithIndices(crystal.sites,os); os << "\n";

    os << "================================================== neighborDistances:\n";
    mprintWithIndices(crystal.neighborDistances,os); os << "\n\n";
    
    os << "================================================== nearestNeighbor:\n";
    mprintWithIndices(crystal.nearestNeighbor,os); os << "\n\n";
    
    os << "================================================== siteToWedge:\n";
    printList(crystal.site2Wedge,os); os << "\n\n";
    
    os << "================================================== wedgeToSite:\n";
    mprintWithIndices(crystal.wedge2Site,os); os << "\n\n";

    os << "================================================== siteDegree:\n";
    printList(crystal.siteDegree,os); os << "\n";

    os << "================================================== siteDiffToSite:\n";
    mprintWithIndices(crystal.siteDiffToSite,os); os << "\n";
    
    os << "================================================== siteSumToSite:\n";
    mprintWithIndices(crystal.siteSumToSite,os); os << "\n";
    
    os << "================================================== symmetryGroupAction:\n";
    mprintWithIndices(crystal.symmetryGroupAction,os); os << "\n";
    
    os << "================================================== fullGroupAction:\n";
    mprintWithIndices(crystal.fullGroupAction,os); os << "\n\n";
    
  }


} // SymFind namespace
  
#endif
		   
