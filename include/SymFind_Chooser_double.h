//-*-C++-*-    

/** \file    SymFind_Chooser_double.h
 *  \ingroup interface
 *  \author  Michael Summers
 *
 */

#ifndef SymFind_Chooser_double_HEADER_H
#define SymFind_Chooser_double_HEADER_H

namespace SymFind {
  
  template<>
  class Chooser<double> {
  public:

    typedef double             T;
    typedef SymFind_CMatrix_double  CMatrixType;
    typedef SymFind_CVector_double  CVectorType;
    typedef SymFind_CCrystal_double CCrystalType;
    
    //---------------------------------------------------------------------- Crystal
    //---------------------------------------------------------------------- 
    static
    void initAfterSymmetriesSetUp(CCrystalType* ptr) {
      SymFind_initAfterSymmetriesSetUp_double(ptr);
    }


    static
    SymFind_CCrystal_double*  makeCCrystal(SymFind_CCrystalTypes type, int DIM) {
      SymFind_CCrystal_double*  ccrystalPtr = new CCrystalType;
      SymFind_CCrystal_Initialize_double(ccrystalPtr,DIM,type);
      return ccrystalPtr;
    }
  
    static
    void  dropCCrystalData(SymFind_CCrystal_double*  ccrystalPtr) {
      SymFind_CCrystal_DropData_double(ccrystalPtr);
    }

    //---------------------------------------------------------------------- Matrix
    //---------------------------------------------------------------------- 
    static
    void Initialize(CMatrixType* ptr,
		    int lDim, int nRow, int nCol,
		    T   initVal) {
      SymFind_mInitialize_double(ptr,lDim,nRow,nCol,initVal);
    }
    //----------------------------------------------------------------------
    static
    void Resize(CMatrixType* ptr,
		int lDim, int nRow, int nCol, T i) {
      SymFind_mResize_double(ptr,lDim,nRow,nCol, i);
    }
    //----------------------------------------------------------------------
    static
    void Kill(CMatrixType* ptr) {
      SymFind_mDestroy_double(ptr);
    }
    //----------------------------------------------------------------------
    static
    CMatrixType*  Make() {
      return SymFind_make_matrix_double(0,0,0,0.0);
    }
    //----------------------------------------------------------------------
    static
    CMatrixType*  Make(int ld, int nr, int nc, T i) {
      return SymFind_make_matrix_double(ld,nr,nc,i);
    }
    //----------------------------------------------------------------------
    static
    void SetTo(CMatrixType* ptr, CMatrixType* otherPtr) {
      SymFind_mSetToOther_double(ptr,otherPtr);
    }
    //----------------------------------------------------------------------
    static
    void SetTo(CMatrixType* ptr, T val) {
      SymFind_mSetToVal_double(ptr,val);
    }
    //----------------------------------------------------------------------
    static
    void DeepAssign(CMatrixType* ptr, const CMatrixType* otherPtr) {
      SymFind_mDeep_Assign_double(ptr,const_cast<CMatrixType*>(otherPtr));
    }
    //----------------------------------------------------------------------
    static
    void ShallowAssign(CMatrixType* ptr, const CMatrixType* otherPtr) {
      SymFind_mDeep_Assign_double(ptr,const_cast<CMatrixType*>(otherPtr));
    }
    //----------------------------------------------------------------------
    static
    T& get(CMatrixType* ptr, int r, int c) {      
      return *SymFind_mGetPtr_double(ptr,r,c);
    }
    //----------------------------------------------------------------------
    static
    T& get(CMatrixType* ptr, int o) {
      return *SymFind_mvGetPtr_double(ptr,o);
    }

    //---------------------------------------------------------------------- Vector
    //---------------------------------------------------------------------- 
    static
    void Initialize(CVectorType* ptr, int size,T initVal) {
      SymFind_vInitialize_double(ptr,size,initVal);
    }
    //----------------------------------------------------------------------
    static
    void Resize(CVectorType* ptr, int size, T i) {
      SymFind_vResize_double(ptr,size, i);
    }
    //----------------------------------------------------------------------
    static
    void Kill(CVectorType* ptr) {
      SymFind_vDestroy_double(ptr);
    }
    //----------------------------------------------------------------------
    static
    void SetTo(CVectorType* ptr, CVectorType* otherPtr) {
      SymFind_vSetToOther_double(ptr,otherPtr);
    }
    //----------------------------------------------------------------------
    static
    void SetTo(CVectorType* ptr, T val) {
      SymFind_vSetToVal_double(ptr,val);
    }
    //----------------------------------------------------------------------
    static
    void DeepAssign(CVectorType* ptr, CVectorType* otherPtr) {
      SymFind_vDeep_Assign_double(ptr,otherPtr);
    }
    //----------------------------------------------------------------------
    static
    void ShallowAssign(CVectorType* ptr, CVectorType* otherPtr) {
      SymFind_vShallow_Assign_double(ptr,otherPtr);
    }
    //----------------------------------------------------------------------
    static
    T& get(CVectorType* ptr, int o) {
      return *SymFind_vGetPtr_double(ptr,o);
    }
    //----------------------------------------------------------------------
  };

} // namespace SymFind


#endif
