//-*-C++-*-    

/** \file    SymFind_Chooser_float.h
 *  \ingroup interface
 *  \author  Michael Summers
 *
 */

#ifndef SymFind_Chooser_float_HEADER_H
#define SymFind_Chooser_float_HEADER_H


namespace SymFind {
  
  template<>
  class Chooser<float> {
  public:

    typedef float                  T;
    typedef SymFind_CMatrix_float  CMatrixType;
    typedef SymFind_CVector_float  CVectorType;
    typedef SymFind_CCrystal_float CrystalType;
    
    //---------------------------------------------------------------------- Crystal
    //---------------------------------------------------------------------- 
    static
    void initAfterSymmetriesSetUp(CrystalType* ptr) {
      SymFind_initAfterSymmetriesSetUp_float(ptr);
    }

    static
    void  dropCCrystalData(SymFind_CCrystal_float*  ccrystalPtr) {
      SymFind_CCrystal_DropData_float(ccrystalPtr);
    }
  
    //---------------------------------------------------------------------- Matrix
    //---------------------------------------------------------------------- 
    static
    void Initialize(CMatrixType* ptr,
		    int lDim, int nRow, int nCol,
		    T   initVal) {
      SymFind_mInitialize_float(ptr,lDim,nRow,nCol,initVal);
    }
    //----------------------------------------------------------------------
    static
    void Resize(CMatrixType* ptr,
		int lDim, int nRow, int nCol, T i) {
      SymFind_mResize_float(ptr,lDim,nRow,nCol,i);
    }
    //----------------------------------------------------------------------
    static
    void Kill(CMatrixType* ptr) {
      SymFind_mDestroy_float(ptr);
    }
    //----------------------------------------------------------------------
    static
    CMatrixType*  Make() {
      return SymFind_make_matrix_float(0,0,0,0.0);
    }
    //----------------------------------------------------------------------
    static
    CMatrixType*  Make(int ld, int nr, int nc, T i) {
      return SymFind_make_matrix_float(ld,nr,nc,i);
    }
    //----------------------------------------------------------------------
    static
    void SetTo(CMatrixType* ptr, CMatrixType* otherPtr) {
      SymFind_mSetToOther_float(ptr,otherPtr);
    }
    //----------------------------------------------------------------------
    static
    void SetTo(CMatrixType* ptr, T val) {
      SymFind_mSetToVal_float(ptr,val);
    }
    //----------------------------------------------------------------------
    static
    void DeepAssign(CMatrixType* ptr, const CMatrixType* otherPtr) {
      SymFind_mDeep_Assign_float(ptr,const_cast<CMatrixType*>(otherPtr));
    }
    //----------------------------------------------------------------------
    static
    void ShallowAssign(CMatrixType* ptr, const CMatrixType* otherPtr) {
      SymFind_mDeep_Assign_float(ptr,const_cast<CMatrixType*>(otherPtr));
    }
    //----------------------------------------------------------------------
    static
    T& get(CMatrixType* ptr, int r, int c) {      
      return *SymFind_mGetPtr_float(ptr,r,c);
    }
    //----------------------------------------------------------------------
    static
    T& get(CMatrixType* ptr, int o) {
      return *SymFind_mvGetPtr_float(ptr,o);
    }

    //---------------------------------------------------------------------- Vector
    //---------------------------------------------------------------------- 
    static
    void Initialize(CVectorType* ptr, int size,T initVal) {
      SymFind_vInitialize_float(ptr,size,initVal);
    }
    //----------------------------------------------------------------------
    static
    void Resize(CVectorType* ptr, int size, T i) {
      SymFind_vResize_float(ptr,size, i);
    }
    //----------------------------------------------------------------------
    static
    void Kill(CVectorType* ptr) {
      SymFind_vDestroy_float(ptr);
    }
    //----------------------------------------------------------------------
    static
    void SetTo(CVectorType* ptr, CVectorType* otherPtr) {
      SymFind_vSetToOther_float(ptr,otherPtr);
    }
    //----------------------------------------------------------------------
    static
    void SetTo(CVectorType* ptr, T val) {
      SymFind_vSetToVal_float(ptr,val);
    }
    //----------------------------------------------------------------------
    static
    void DeepAssign(CVectorType* ptr, CVectorType* otherPtr) {
      SymFind_vDeep_Assign_float(ptr,otherPtr);
    }
    //----------------------------------------------------------------------
    static
    void ShallowAssign(CVectorType* ptr, CVectorType* otherPtr) {
      SymFind_vShallow_Assign_float(ptr,otherPtr);
    }
    //----------------------------------------------------------------------
    static
    T& get(CVectorType* ptr, int o) {
      return *SymFind_vGetPtr_float(ptr,o);
    }
    //----------------------------------------------------------------------
  };

} // namespace SymFind


#endif
