//-*-C++-*-    

#ifndef SymFind_Chooser_int_HEADER_H
#define SymFind_Chooser_int_HEADER_H


namespace SymFind {
  
  template<>
  class Chooser<int> {
  public:

    typedef int             T;
    typedef SymFind_CMatrix_int CMatrixType;
    typedef SymFind_CVector_int CVectorType;
    
    //---------------------------------------------------------------------- Matrix
    //---------------------------------------------------------------------- 
    static
    void Initialize(CMatrixType* ptr,
		    int lDim, int nRow, int nCol,
		    T   initVal) {
      SymFind_mInitialize_int(ptr,lDim,nRow,nCol,initVal);
    }
    //----------------------------------------------------------------------
    static
    CMatrixType*  Make() {
      return SymFind_make_matrix_int(0,0,0,0);
    }
    //----------------------------------------------------------------------
    static
    CMatrixType*  Make(int ld, int nr, int nc, T i) {
      return SymFind_make_matrix_int(ld,nr,nc,i);
    }
    //----------------------------------------------------------------------
    static
    void Resize(CMatrixType* ptr,
		int lDim, int nRow, int nCol, int i) {
      SymFind_mResize_int(ptr,lDim,nRow,nCol,i);
    }
    //----------------------------------------------------------------------
    static
    void Kill(CMatrixType* ptr) {
      SymFind_mDestroy_int(ptr);
    }
    //----------------------------------------------------------------------
    static
    void SetTo(CMatrixType* ptr, CMatrixType* otherPtr) {
      SymFind_mSetToOther_int(ptr,otherPtr);
    }
    //----------------------------------------------------------------------
    static
    void SetTo(CMatrixType* ptr, T val) {
      SymFind_mSetToVal_int(ptr,val);
    }
    //----------------------------------------------------------------------
    static
    void DeepAssign(CMatrixType* ptr, CMatrixType* otherPtr) {
      SymFind_mDeep_Assign_int(ptr,otherPtr);
    }
    //----------------------------------------------------------------------
    static
    void ShallowAssign(CMatrixType* ptr, CMatrixType* otherPtr) {
      SymFind_mDeep_Assign_int(ptr,otherPtr);
    }
    //----------------------------------------------------------------------
    static
    T& get(CMatrixType* ptr, int r, int c) {      
      return *SymFind_mGetPtr_int(ptr,r,c);
    }
    //----------------------------------------------------------------------
    static
    T& get(CMatrixType* ptr, int o) {
      return *SymFind_mvGetPtr_int(ptr,o);
    }

    //---------------------------------------------------------------------- Vector
    //---------------------------------------------------------------------- 
    static
    void Initialize(CVectorType* ptr, int size,T initVal) {
      SymFind_vInitialize_int(ptr,size,initVal);
    }
    //----------------------------------------------------------------------
    static
    void Resize(CVectorType* ptr, int size, T i) {
      SymFind_vResize_int(ptr,size,i);
    }
    //----------------------------------------------------------------------
    static
    void Kill(CVectorType* ptr) {
      SymFind_vDestroy_int(ptr);
    }
    //----------------------------------------------------------------------
    static
    void SetTo(CVectorType* ptr, CVectorType* otherPtr) {
      SymFind_vSetToOther_int(ptr,otherPtr);
    }
    //----------------------------------------------------------------------
    static
    void SetTo(CVectorType* ptr, T val) {
      SymFind_vSetToVal_int(ptr,val);
    }
    //----------------------------------------------------------------------
    static
    void DeepAssign(CVectorType* ptr, CVectorType* otherPtr) {
      SymFind_vDeep_Assign_int(ptr,otherPtr);
    }
    //----------------------------------------------------------------------
    static
    void ShallowAssign(CVectorType* ptr, CVectorType* otherPtr) {
      SymFind_vShallow_Assign_int(ptr,otherPtr);
    }
    //----------------------------------------------------------------------
    static
    T& get(CVectorType* ptr, int o) {
      return *SymFind_vGetPtr_int(ptr,o);
    }
    //----------------------------------------------------------------------
  };

} // namespace SymFind


#endif
