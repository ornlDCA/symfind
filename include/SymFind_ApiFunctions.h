//-*-C++-*-

/** \ingroup DCA */
/*@{*/

/*! \file API_Functions.h  
 *  \brief 
 *
 */

#ifndef SymFind_API_Functions_H
#define SymFind_API_Functions_H

#include <iostream>
#include <ostream>
#include <sstream>
#include <exception>
#include <vector>
#include <map>
#include <string.h>
#include <limits>
#include <typeinfo>
#include <exception>
#include <stdexcept>

namespace TinySciPy {
  class VM;
  void Register_SymFind_PyModule(VM&);
}

typedef    double T;

void        symFind_display(std::ostream& os);

void        symFind_printCrystals(std::ostream& os);

std::string symFind_momentumSiteString(int s);

std::string symFind_spaceSiteString   (int s);

void        symFind_loadMesh
              (int         numberOfMeshPointsPerOrigLatticeCell__,
	       const std::vector< std::vector<T> > latticeVectors);

void        symFind_loadMesh(int numberOfMeshPointsPerOrigLatticeCell__);

extern "C" {

  //----------------------------------------------------------------------

  bool   symFind_isOkToWrite();

  //----------------------------------------------------------------------
  
  void   symFind_init(T latticeA_, T latticeB_, T latticeAngle_,
		      int superlatticeA_0, int superlatticeA_1,
		      int superlatticeB_0, int superlatticeB_1);

  T      symFind_latticeA();
  T      symFind_latticeB();
  T      symFind_latticeAngle();

  void   symFind_dumpMesh(std::ostream& os) ;
  
  //----------------------------------------------------------------------
  
  void   symFind_destroy();
  
  //======================================================================
  
  int    symFind_dimension();
  
  //======================================================================
  
  T      symFind_spacePrimativeVectors(int c, int s)           ;
  
  T      symFind_spaceLatticeVectors(int c, int s)           ;
  
  int    symFind_spaceZeroPointIndex()                       ;
  
  int    symFind_spaceNumberOfSites ()                       ;
    
  T      symFind_spaceSites         (int c, int s)           ;
  
  int    symFind_spaceSiteDegree    (int site)               ;
  
  int    symFind_spaceSiteDiffToSite(int site1, int site2)   ;
  
  int    symFind_spaceSiteSumToSite (int site1, int site2)   ;
  
  int    symFind_spaceSymmetryGroupAction(int site, int sym) ;
    
  int    symFind_spaceFullGroupAction    (int site, int sym) ;
    
  int    symFind_spaceNumberOfSymmetries ()                  ;

  int    symFind_spaceNearestNeighbor(int neighborNum, int site) ;
    
  T      symFind_spaceNeighborDistances(int neighborNum, int site) ;
    
  //======================================================================
    
    
  T      symFind_momentumPrimativeVectors(int c, int s)           ;

  T      symFind_momentumLatticeVectors(int c, int s)           ;
    
  int    symFind_momentumZeroPointIndex()                       ;
    
  int    symFind_momentumPiPointIndex()                         ;
    
  int    symFind_momentumNumberOfSites ()                       ;
    
  int    symFind_momentumNumberOfMeshSites ()                   ;
    
  T      symFind_momentumSites         (int c, int s)           ;
    
  T      symFind_momentumMeshSites(int c, int s)           ;
    
  int    symFind_momentumSiteDegree    (int site)               ;
    
  int    symFind_momentumSiteDiffToSite(int site1, int site2)   ;
    
  int    symFind_momentumSiteSumToSite (int site1, int site2)   ;
    
  int    symFind_momentumSymmetryGroupAction(int site, int sym) ;
    
  int    symFind_momentumFullGroupAction    (int site, int sym) ;
    
  int    symFind_momentumNumberOfSymmetries ()                  ;
    
  int    symFind_momentumNearestNeighbor  (int neighborNum, int site) ;

  T      symFind_momentumNeighborDistances(int neighborNum, int site) ;
  
  //----------------------------------------------------------------------
    
  size_t symFind_kIndexFromChars(const char* instr) ;

  //----------------------------------------------------------------------
    
  size_t  symFind_rIndexFromChars(const char* instr) ;

  //======================================================================

}

/*@}*/
#endif
