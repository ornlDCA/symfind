//-*-C-*-    

#ifndef SymFind_CMatrix_H
#define SymFind_CMatrix_H

extern "C" {

#define MAKE_MATRIX(TypeName)						\
  									\
  typedef struct  {							\
    TypeName* data;							\
    bool      ownData;							\
    int       lDim;							\
    int       nRow;							\
    int       nCol;							\
    int       numElements;						\
  } SymFind_CMatrix_##TypeName;						\
    									\
    void SymFind_mAllocate_##TypeName(SymFind_CMatrix_##TypeName* self,	\
				      int                   numEl) ;	\
    void SymFind_mDropData_##TypeName(SymFind_CMatrix_##TypeName* self); \
									\
    void SymFind_mSetToVal_##TypeName(SymFind_CMatrix_##TypeName* self,	\
				      TypeName              initVal) ;	\
    void SymFind_mSetToOther_##TypeName(SymFind_CMatrix_##TypeName* self, \
					SymFind_CMatrix_##TypeName* other) ; \
    void SymFind_mInitialize_##TypeName(SymFind_CMatrix_##TypeName* self, \
					int ldim, int nrow, int ncol,	\
					TypeName initVal) ;		\
    SymFind_CMatrix_##TypeName*						\
    SymFind_make_matrix_##TypeName(int ld,int nr,int nc, TypeName i) ;	\
    void SymFind_mShallow_Assign_##TypeName(SymFind_CMatrix_##TypeName* self, \
					    SymFind_CMatrix_##TypeName* other) ; \
    void SymFind_mDeep_Assign_##TypeName(SymFind_CMatrix_##TypeName* self, \
					 SymFind_CMatrix_##TypeName* other) ; \
    void SymFind_mDestroy_##TypeName(SymFind_CMatrix_##TypeName* self) ; \
    int SymFind_offset_##TypeName(SymFind_CMatrix_##TypeName* self, int r, int c) ; \
    TypeName* SymFind_mvGetPtr_##TypeName(SymFind_CMatrix_##TypeName* self, int i) ; \
    TypeName SymFind_mvGet_##TypeName(SymFind_CMatrix_##TypeName* self, int i) ; \
    TypeName* SymFind_mGetPtr_##TypeName(SymFind_CMatrix_##TypeName* self, int r, int c) ; \
    TypeName SymFind_mGet_##TypeName(SymFind_CMatrix_##TypeName* self, int r, int c) ; \
    void SymFind_mResize_##TypeName(SymFind_CMatrix_##TypeName* self,	\
				    int lDim, int nRow, int nCol, TypeName i) ;	\
									\
    
  
  MAKE_MATRIX(double)
    
  MAKE_MATRIX(float)

  MAKE_MATRIX(int)
}

#endif
 
