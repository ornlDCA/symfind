//-*-C-*-

/** \file    symFind.c
 *  \ingroup interface
 *  \author  Michael Summers
 *
 *  Note the use of a mcaro like a template
 *
 */

#ifndef SymFind_CCrystal_H
#define SymFind_CCrystal_H

#include "SymFind_CMatrix.h"
#include "SymFind_CVector.h"

extern "C" {

  typedef enum {Momentum,Space}                         SymFind_CCrystalTypes;
    
#define MakeSymFind_CCrystal(TypeName) 					\
    									\
    typedef  struct  {							\
      bool                        initialized;				\
      int                         dimension;				\
      SymFind_CCrystalTypes       type;					\
      SymFind_CMatrix_##TypeName  primativeVectors;			\
      SymFind_CMatrix_##TypeName  latticeVectors;			\
      int                         numberOfSites;			\
      SymFind_CMatrix_##TypeName  sites;				\
      int                         piPointIndex;				\
      int                         zeroPointIndex;			\
      SymFind_CMatrix_int         siteDiffToSite;			\
      SymFind_CMatrix_int         siteSumToSite;			\
      SymFind_CMatrix_int         symmetryGroupAction;			\
      SymFind_CMatrix_int         fullGroupAction;			\
      int                         numberOfSymmetries;			\
      SymFind_CVector_int         siteDegree;				\
      SymFind_CMatrix_##TypeName  neighborDistances;  			\
      SymFind_CMatrix_int         nearestNeighbor;  			\
      SymFind_CVector_int         site2Wedge;			        \
      SymFind_CMatrix_int         wedge2Site;			        \
    } SymFind_CCrystal_##TypeName ;					\
    									\
    void SymFind_CCrystal_Initialize_##TypeName(SymFind_CCrystal_##TypeName* self, \
					       int                   dim, \
					       SymFind_CCrystalTypes      t) ; \
									\
    void SymFind_CCrystal_DropData_##TypeName(SymFind_CCrystal_##TypeName* self); \
									\
    void SymFind_CCrystal_Deep_Assign_##TypeName(SymFind_CCrystal_##TypeName* self, \
						SymFind_CCrystal_##TypeName* other) ; \
									\
    int SymFind_equatingSymmetry_##TypeName(SymFind_CCrystal_##TypeName* self, \
					    int site, int site2) ;	\
									\
    void SymFind_initSiteDegrees_##TypeName(SymFind_CCrystal_##TypeName* self) ; \
									\
    int SymFind_indexFor_##TypeName(SymFind_CCrystal_##TypeName* self,	\
				    TypeName x, TypeName y) ;		\
									\
    void SymFind_setPiPoint_##TypeName(SymFind_CCrystal_##TypeName* self) ; \
									\
    void SymFind_setZeroPoint_##TypeName(SymFind_CCrystal_##TypeName* self) ; \
									\
    int getIdentitySymmetryIndex_##TypeName(SymFind_CCrystal_##TypeName* self) ; \
									\
    void SymFind_initAfterSymmetriesSetUp_##TypeName(SymFind_CCrystal_##TypeName* self) ; \
									\
    void SymFind_site_Index_2_vector_##TypeName(SymFind_CCrystal_##TypeName* self, \
						int siteIndex,		\
						SymFind_CVector_##TypeName* v)  ; \
									\
    int SymFind_subtract_##TypeName(SymFind_CCrystal_##TypeName* self,	\
				    int                     posIndex1,	\
				    int                     posIndex2) ; \
									\
    int SymFind_negate_##TypeName(SymFind_CCrystal_##TypeName* self,	\
				  int                     posIndex)  ;	\
									\
    int add_##TypeName(SymFind_CCrystal_##TypeName* self,		\
		       int posIndex1,					\
		       int posIndex2);					\
									\

   
  MakeSymFind_CCrystal(double)

  MakeSymFind_CCrystal(float)

}

#endif
 
