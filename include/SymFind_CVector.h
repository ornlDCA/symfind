//-*-C-*-    

#ifndef SymFind_CVector_HEADER_H
#define SymFind_CVector_HEADER_H

extern "C" {
    
#define MAKE_VECTOR(TypeName)						\
  									\
  typedef struct SymFind_CVector_##TypeName {				\
    TypeName* data;							\
    bool      ownData;							\
    int       size;							\
    int       capacity;							\
  } SymFind_CVector_##TypeName;						\
    									\
    void SymFind_vAllocate_##TypeName(SymFind_CVector_##TypeName* self,	\
				      int numElements);			\
    void SymFind_vDropData_##TypeName(SymFind_CVector_##TypeName* self); \
    void SymFind_vDestroy_##TypeName(SymFind_CVector_##TypeName* self); \
    void SymFind_vSetToVal_##TypeName(SymFind_CVector_##TypeName* self,	\
				      TypeName initVal);		\
    void SymFind_vSetToOther_##TypeName(SymFind_CVector_##TypeName* self, \
					SymFind_CVector_##TypeName* other); \
    void SymFind_vInitialize_##TypeName(SymFind_CVector_##TypeName* self, \
					int                   size,	\
					TypeName              initVal); \
    void SymFind_vResize_##TypeName(SymFind_CVector_##TypeName* self,	\
				    int sz, TypeName i);				\
    TypeName* SymFind_vGetPtr_##TypeName(SymFind_CVector_##TypeName* self, \
					 int i);			\
    TypeName SymFind_vGet_##TypeName(SymFind_CVector_##TypeName* self,	\
				     int i);				\
    void SymFind_vDeep_Assign_##TypeName(SymFind_CVector_##TypeName* self, \
					 SymFind_CVector_##TypeName* other); \
    void SymFind_vShallow_Assign_##TypeName(SymFind_CVector_##TypeName* self, \
					    SymFind_CVector_##TypeName* other); \
    									\
    
  MAKE_VECTOR(double)
    
  MAKE_VECTOR(float)

  MAKE_VECTOR(int)

}

#endif
 
