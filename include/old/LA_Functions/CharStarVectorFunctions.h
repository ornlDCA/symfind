//-*-C++-*-

/** \ingroup DCA */
/*@{*/

/*! \file CharStarVectorFunctions.h  
 *  \brief 
 *
 */

#ifndef DCA_CharStarVectorFunctions_H
#define DCA_CharStarVectorFunctions_H

#include "LinearAlgebraTemplatePrototypes.h"

// This is a vector of c strings

//================================================= Construction Macro

#define MakeCharStarVectorFunction(FunctionName,ReturnType,Len,Capacity, ...) \
  									\
  const char* FunctionName(int i) {					\
    									\
    static const char* data[] = {					\
      __VA_ARGS__							\
    };									\
    									\
    if(i == -2) return #Capacity ;					\
    if(i == -1) return #Len     ;					\
    if(i >= Len ) i = Len - 1;						\
    return data[i];							\
  }									\
  
//====================================================================== Usage
//
//   MakeCharStarVectorFunction(charStarUsageVector,
// 			     char *,3,3,
// 			     "fooo",
// 			     "from",
// 			     "toad");
  
//----------------------------------------------------------------------

typedef const char* (*charStarVectorFunction)   (int i);

//======================================================================
// charStarVectorFunction(-1) = the length of the vector
// charStarVectorFunction(-2) = the length of the underlying storage vector

int size    (charStarVectorFunction vf);
int capacity(charStarVectorFunction vf);
  
// The following enable intVectorFunction to be VectorLike
template<> struct HasValueType<charStarVectorFunction> { static const bool value = 1; }; 
template<> struct ValueType   <charStarVectorFunction> { typedef const char* type;            };

template<> struct VectorLikeAccess<charStarVectorFunction> {
  //  static int&       elementAt(      intVectorFunction& v, int i) { return v(i); }
  static const char* elementAt(const charStarVectorFunction& v, int i) { return v(i); }
};

//======================================================================

template<typename VT>
typename std::enable_if<IsVectorLike<VT>::value>::type
operator <<= (VT& v, charStarVectorFunction f) {

  ASSERT_IT(/* no locals */,
	    size(v)<= size(f),
	    throwRangeError("v <<= charStarVectorFunction f  [size(v) > size(f)]!"); );
  
  for (int i=0; i<size(f); i++) 
    v[i] = f(i);
}

// //======================================================================
  
// namespace dca {
  
//   //======================================================================
  
//   void writeValues(charStarVectorFunction f, std::ostream& os) {
//     os << "[";
//     for (int i=0; i< size(f); i++) os << f(i) << ", ";
//     os << "]";
//   }
  
//   //======================================================================
  
//   std::string valuesToString(charStarVectorFunction f) {
  
//     std::ostringstream os;
//     writeValues(f,os);
//     return os.str();
//   }
  
//   //======================================================================
  
//   template<typename VectorLike>
//   void copyCharStarVectorFunctionToVector(charStarVectorFunction f, VectorLike& v) {
    
//     v.resize(size(f));
//     for (int i=0; i<size(f); i++) 
//       v[i] = f(i);
//   }
  
//   //======================================================================
  
//   char** makeCharStarData_(int cap) {
//     if(cap <= 0) exit(EXIT_FAILURE);	
//     int numBytes = sizeof(char *) * cap;
//     char** data = (char**)malloc(numBytes);					
//     memset(data,0,numBytes);
//     return data;						
//   }
  
//   char** maybe_resize_charStarData_(char** oldData, int cap, int* oldCap) {
    
//     if(cap     <=0)   exit(EXIT_FAILURE);
//     if(*oldCap <=0)   exit(EXIT_FAILURE);
    
//     if(cap <= *oldCap) return oldData;
    
//     // Going to resize
//     int oldNumBytes = sizeof(char*) * *oldCap;	
    
//     char** newData = makeCharStarData_(cap);
//     *oldCap = cap;
    
//     memcpy(newData,oldData,oldNumBytes);	
    
//     if(oldData != NULL)							
//       free(oldData);							
    
//     return newData;
//   }
  
//   //======================================================================

//   void writeCharStarDataArray(charStarVectorFunction f,
// 			      std::ostream&          os) {

//     int count  = 0;
//     int length = size(f);
//     int cap    = capacity(f);
    
//     if (length >= 20)
//       os << "\n               ";
//     for(int i=0; i<length; i++) {
//       os << "\"" << f(i) << "\"" << delimiterOrNot(i,cap,", ");
//       if ((count != 0) && ((count++)%10 == 0) )
// 	os << "\n               ";
//     }
//     if (length >= 20)
//       os << "\n";
//   }

//   //======================================================================
  
//   void writeCharStarStaticDataStatement(charStarVectorFunction f,
// 					std::ostream& os) {
    
// //    int length = size(f);
//  //   int cap    = capacity(f);
    
//  //   int count = 0;
//     os << "            static const char*  data[]  = {";
//     writeCharStarDataArray(f,os);
//     os << "};\n";
//   }

//   //====================================================================== Usage

//   const char* charStarUsageVector_(int  i) {

//     // note the data below is laid out row major
//     // the actual matrix is the transpose of data
//     static const char* data[] = {
//       "fooo",
//       "from",
//       "toad"
//     };

//     if(i == -2) return "3";
//     if(i == -1) return "3";	

//     return data[i];
//   }

//   //======================================================================
  
//   void writeCodeFor_(charStarVectorFunction f, const char* fName, std::ofstream& file) {
    
//     file << "        char* " << fName << "(int i) {\n";
//     file << "           int len_ = " << size(f)      << ";\n"; 
//     file << "           int cap  = " << capacity(f) << ";\n"; 

//     writeCharStarStaticDataStatement(f,file);

//     file << "           if(i == -2) return \"" << capacity(f) << "\"; \n";
//     file << "           if(i == -1) return \"" << size(f)      << "\"; \n";
//     file << "           if(i >= len_) i = len_-1;\n";
//     file << "           return data[i];\n";
//     file << "        }\n\n";
//   }
  
//   //======================================================================
  
//   void writeCodeFor(charStarVectorFunction f, const char* fName, std::ostream& os) {
    
//     os << "    MakeCharStarVectorFunction(" << fName << ",char*," << size(f) << "," << capacity(f) << ",";
//     writeCharStarDataArray(f,os);
//     os << ");\n";
//   }
  
//   //======================================================================
// } // end namespace DCA

/*@}*/
#endif

