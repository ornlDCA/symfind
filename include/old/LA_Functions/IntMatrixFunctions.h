//-*-C++-*-

/** \ingroup DCA */
/*@{*/

/*! \file IntMatrixFunctions.h  
 *  \brief 
 *
 */

#ifndef Header_IntMatrixFunctions_H
#define Header_IntMatrixFunctions_H

//======================================================================

typedef       int (*intMatrixFunction)(int r, int c);

template<> class IsMatrixLike< intMatrixFunction > {public: enum {value = 1}; };
//template<> class IsVectorLike< intMatrixFunction > {public: enum {value = 1}; };

//======================================================================

int capacity(intMatrixFunction mf);
int nRow    (intMatrixFunction mf);
int lDim    (intMatrixFunction mf);
int nCol    (intMatrixFunction mf);
int size    (intMatrixFunction mf);

//======================================================================

// // The following enable intMatrixFunction to be MatrixLike
template<> class HasValueType< intMatrixFunction > {public: static const bool value = 1; }; 
template<> class ValueType   < intMatrixFunction > {public: typedef int type;         };

//======================================================================

// template<> class VectorLikeAccess<intMatrixFunction> {
// public:
//   //  static int&       elementAt(      intVectorFunction& v, int i) { return v(i); }
//   static const int& elementAt(const intMatrixFunction& m, int i) { 
//     int c = i / lDim(m);
//     int r = i % lDim(m);
//     return m(r,c); 
//   }
// };

//======================================================================

/*@}*/
#endif

