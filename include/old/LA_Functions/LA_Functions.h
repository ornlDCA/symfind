//-*-C++-*-

/** \ingroup LinearAlgebra */
/*@{*/

/*! \file LA_Functions.h  
 *  \brief 
 *
 */

#ifndef HEADER_LA_Functions_H
#define HEADER_LA_Functions_H

#define HostDevice

#include <type_traits>
#include <iostream>
#include <ostream>
#include <sstream>
#include <exception>
#include <vector>
#include <string.h>
#include <limits>
#include <typeinfo>
#include <exception>
#include <stdexcept>

#include "Assert_It.h"
#include "ThrowFunctions.h"
#include "LinearAlgebraTemplatePrototypes.h"

#include "CharStarVectorFunctions.h"
#include "DoubleVectorFunctions.h"
#include "DoubleMatrixFunctions.h"
#include "IntVectorFunctions.h"
#include "IntMatrixFunctions.h"

#include "Tweeked.h"
#include "SF_VectorFunctions.h"
#include "MatrixFunctions.h"

//====================================================================== 

/*@}*/
#endif
