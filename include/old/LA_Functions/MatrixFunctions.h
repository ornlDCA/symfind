//-*-C++-*-

/** \ingroup DCA */
/*@{*/

/*! \file MatrixFunctions.h  
 *  \brief 
 *
 */

#ifndef HEADER_MatrixFunctions_H
#define HEADER_MatrixFunctions_H

//======================================================================

#define MakeMatrixFunction(FunctionName,ReturnType,LD,NR,NC,CAP, ...)	\
  									\
  ReturnType FunctionName(int r, int c) {				\
									\
    if(r == -1 && c ==  0) return NR;					\
    if(r == -1 && c ==  1) return LD;					\
    if(r ==  0 && c == -1) return NC;					\
    if(r <   0 || c <   0) return CAP;					\
    									\
    static const ReturnType data[] = {					\
      __VA_ARGS__							\
    };									\
									\
    int offset = r+c*LD;						\
    if (offset >= CAP) offset = CAP -1;					\
    return data[offset];						\
  }									\

//======================================================================

template<typename MFT>  
class MatrixFunctions: 
  public VectorMatrixFunctions {
public:

  typedef typename ValueType<MFT>::type T;

  //----------------------------------------------------------------------
  
  static
  T columnMajorAccess(int  r, int  c, int ld,	
		      const T* dataPtr) {
    return dataPtr[r+c*ld];
  }
  
  //----------------------------------------------------------------------
  
  static
  int matrixInfo(int  r, int  c,			
		 int nr, int ld, int nc, int cap) {
    if(r == -1 && c ==  0) return nr;
    if(r == -1 && c ==  1) return ld;	
    if(r ==  0 && c == -1) return nc;	
    if(r <   0 || c <   0) return cap;
    return -1;
  }

  //======================================================================
  
  static
  T transpose(MFT df, int  r, int  c) {
    if(r == -1 && c ==  0) return (T) df( 0,-1); // rows => cols
    if(r == -1 && c ==  1) return (T) df( r, c);   // ldim unchanged
    if(r ==  0 && c == -1) return (T) df(-1, 0); // cols => rows
    if(r <= -1 || c <= -1) return (T) df( r, c);
    return df(c,r);
  }
  
  //======================================================================
  static
  void writeDataArray(MFT f,
			    std::ostream& os) {

    int nrow = nRow(f);
    int ldim = lDim(f);
    int ncol = nCol(f);
    int cap  = capacity(f);
    
    int precision_ = std::numeric_limits<T>::digits10+1;
    os.precision(precision_);
    
    for(int c=0; c<ncol; c++) {
      os << "                      ";
      for(int r=0; r<ldim; r++) {
	int offset = r + c * ldim;
	if (r < nrow)
	  os << tweekedValue_(f(r,c)) << delimiterOrNot(offset,cap,", ");
	else
	  os << (T) 0 << delimiterOrNot(offset,cap,", ");
      }
      os << "\n";
    }
  }

  //======================================================================
  static
  void writeMatStaticDataStatement(MFT f,
					 std::ostream& os) {
    os << "           // Note the data below is visually laid out row major. \n";
    os << "           // the actual col major matrix is the transpose of this visual appearence. \n";
    os << "           static const T data[]  = {\n";
    writeDataArray(f,os);
    os << "           };\n";
  }
  
  //====================================================================== Usage
  static
  T usageMatrix(int  r, int  c) {

    // note the data below is laid out row major
    // the actual matrix is the transpose of data
    static const T data[] = {
      0.0,1.0,2.0,3.0,4.0,5.0,6.0,7.0,8.0,9.0,
      0.0,1.0,2.0,3.0,4.0,5.0,6.0,7.0,8.0,9.0,
      0.0,1.0,2.0,3.0,4.0,5.0,6.0,7.0,8.0,9.0,
      0.0,1.0,2.0,3.0,4.0,5.0,6.0,7.0,8.0,9.0,
      0.0,1.0,2.0,3.0,4.0,5.0,6.0,7.0,8.0,9.0,
      0.0,1.0,2.0,3.0,4.0,5.0,6.0,7.0,8.0,9.0,
      0.0,1.0,2.0,3.0,4.0,5.0,6.0,7.0,8.0,9.0,
      0.0,1.0,2.0,3.0,4.0,5.0,6.0,7.0,8.0,9.0,
      0.0,1.0,2.0,3.0,4.0,5.0,6.0,7.0,8.0,9.0,
      0.0,1.0,2.0,3.0,4.0,5.0,6.0,7.0,8.0,9.0
    };

    int matInfo = matrixInfo(r,c,10,10,10,100);
    if(matInfo > 0) return(T) matInfo;
    return columnMajorAccess(r,c,10,data);
  }

  //======================================================================
  static
  void writeCodeFor_(MFT f, const char* fName, std::ostream& file) {
    
    file << "        "<< typeid(T).name() << " " << fName << "(int r, int c) {\n";
    file << "           int nrows = " << nRow(f) << ";\n"; 
    file << "           int ldim  = " << lDim(f) << ";\n"; 
    file << "           int ncols = " << nCol(f) << ";\n"; 
    file << "           int cap   = " << capacity(f) << ";\n"; 

    writeMatStaticDataStatement(f,file);

    file << "           int matInfo = matrixInfo(r,c,nrows,ldim,ncols,cap);\n";
    file << "           if(matInfo > 0) return(T) matInfo;\n";
    file << "           return columnMajorAccessInt(r,c,ldim,data);\n";
    file << "        }\n\n";
  }
  
  //======================================================================

  static
  void writeCodeFor(MFT f, const char* fName, std::ostream& os) {
    
    os << "    MakeMatrixFunction(" << fName << ","<< typeid(T).name() << "," 
       << lDim(f)     << "," 
       << nRow(f)     << "," 
       << nCol(f)     << "," 
       << capacity(f) << ",\n";
    writeDataArray(f,os);
    os << ");\n";

  }
  
  //======================================================================
};

void  writeCodeFor(intMatrixFunction f, const char* fName, std::ostream& os);
void  writeMatIntStaticDataStatement(intMatrixFunction f, std::ostream& os);
void  writeCodeFor(doubleMatrixFunction f, const char* fName, std::ostream& os);
void  writeMatDoubleStaticDataStatement(doubleMatrixFunction f, std::ostream& os);

/*@}*/
#endif

