//-*-C++-*-

/** \ingroup DCA */
/*@{*/

/*! \file DoubleVectorFunctions.h  
 *  \brief 
 *
 */

#ifndef DCA_DoubleVectorFunctions_H
#define DCA_DoubleVectorFunctions_H

//======================================================================

typedef double (*doubleVectorFunction)   (int i);

template<> class IsVectorLike< doubleVectorFunction > {public: enum {value = 1}; };

//======================================================================
// doubleVectorFunction(-1) = the length of the vector
// doubleVectorFunction(-2) = the length of the underlying storage vector

int size    (const doubleVectorFunction& vf);
int capacity(const doubleVectorFunction& vf);

//======================================================================
// The following enable intVectorFunction to be VectorLike

template<> class HasValueType<doubleVectorFunction> {public: static const bool value = 1; }; 
template<> class ValueType   <doubleVectorFunction> {public: typedef double type;         };

// template<> class VectorLikeAccess<doubleVectorFunction> {
// public:
//   //  static double&       elementAt(      doubleVectorFunction& v, int i) { return v(i); }
//   static const double& elementAt(const doubleVectorFunction& v, int i) { return v(i); }
// };

//======================================================================

/*@}*/
#endif

