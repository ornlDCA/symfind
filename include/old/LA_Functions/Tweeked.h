//-*-C++-*-

/** \ingroup DCA */
/*@{*/

/*! \file Tweeked.h
 *  \brief 
 *
 */

#ifndef SymFind_Tweeked_H
#define SymFind_Tweeked_H

#include <cmath>

//======================================================================
//======================================================================

//#define M_PI  3.14159265358979323846

namespace SymFind {
  
  //----------------------------------------------------------------------
    
  template<typename T>
  bool tweekedPi(std::ostringstream& result, 
		 int denominator, 
		 const T& value) {
      
    const double small( 1E-4);
      
    for (int n=0; n <= (4 * denominator); n++) {
      
      if (fabs(double(n)/double(denominator)*M_PI - value) < small) {
	if(n%denominator == 0)
	  result << (n/denominator) << "pi"; 
	else
	  result << n << "/" <<  denominator << "pi";
	return true;
      }
      
      if (fabs(double(n)/double(denominator)*M_PI + value) < small) {
	if(n%denominator == 0)
	  result << "-" << (n/denominator) << "pi"; 
	else
	  result << "-" << n << "/" <<  denominator << "pi";
	return true;
      }
    }
      
    return false;
  }
    
  //----------------------------------------------------------------------

  template<typename T>
  bool tweekedPi(std::ostringstream& result, const T& value) {
      
    const double small( 1E-4);
    
    if (fabs(M_PI-value) < small)  {
      result << "pi";
      return true;
    }
    if (fabs(M_PI+value) < small) {
      result << "-pi";
      return true;
    }
      
    for (int i=2; i < 16; i++) {
      if (tweekedPi (result,i,value)) 
	return true;
    }
    return false;
  }
    
  //----------------------------------------------------------------------
    
  template<typename T>
  std::string
  tweekedValue(const T& value)  {
      
    const double small( 1E-4);

    if (std::fabs(value) < small) 	return "0";
      
    std::ostringstream result;
      
    if (tweekedPi(result,value))
      return result.str();
      
    result << value;
    return result.str();
  }

  //----------------------------------------------------------------------
    
  template<typename T>
  std::string 
  zeroTweekedValue(const T& value)  {
      
    const double small( 1E-4);
    if (std::fabs(value) < small) 	return "0";
      
    std::ostringstream result;
    result << value;
    return result.str();
  }

  //----------------------------------------------------------------------
   
}
 


/*@}*/
#endif
