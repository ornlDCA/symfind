//-*-C++-*-

/** \ingroup LinearAlgebra */
/*@{*/

/*! \file IntVectorFunctions.h  
 *  \brief 
 *
 */

#ifndef HEADER_IntVectorFunctions_H
#define HEADER_IntVectorFunctions_H

//======================================================================

typedef       int    (*intVectorFunction)   (int i);

template<> class IsVectorLike< intVectorFunction > {public: enum {value = 1}; };

//======================================================================
// intVectorFunction(-1) = the length of the vector
// intVectorFunction(-2) = the length of the underlying storage vector

int size    (intVectorFunction    vf);
int capacity(intVectorFunction    vf);

//======================================================================
// The following enable intVectorFunction to be VectorLike

template<> class HasValueType<intVectorFunction> {public: static const bool value = 1; }; 
template<> class ValueType   <intVectorFunction> {public: typedef int type;            };

// template<> class VectorLikeAccess<intVectorFunction> {
// public:
//   //  static int&       elementAt(      intVectorFunction& v, int i) { return v(i); }
//   static const int& elementAt(const intVectorFunction& v, int i) { return v(i); }
// };

//======================================================================

/*@}*/
#endif

