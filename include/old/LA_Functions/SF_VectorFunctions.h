//-*-C++-*-

/** \ingroup DCA */
/*@{*/

/*! \file VectorFunctions.h  
 *  \brief 
 *
 */

#ifndef Header_Vectorfunctions_H
#define Header_Vectorfunctions_H

//======================================================================

#define MakeVectorFunction(FunctionName,ReturnType,Len,Capacity, ...)	\
  									\
  ReturnType FunctionName(int i) {					\
									\
    static const ReturnType data[] = {					\
      __VA_ARGS__							\
    };									\
									\
    if(i == -2) return Capacity ;					\
    if(i == -1) return Len     ;					\
    if(i >= Len ) i = Len - 1;						\
    return data[i];							\
  }									\


//======================================================================

class VectorMatrixFunctions  {
public:
  
  //======================================================================
  static
  std::string 
  tweekedValue_(const double& value, bool  doTweek=true) {
    return SymFind::tweekedValue(value); //,doTweek);
  }

  //======================================================================
  static
  std::string 
  tweekedValue_(const char* value, bool  doTweek=true) {
    return value;
  }

  //======================================================================
  
  static
  const 
  char* delimiterOrNot(int offset, int cap, const char* delim) {
    if (offset < cap -1) return delim;
    return "";
  }
    
};

//======================================================================
//======================================================================

template<typename VFT>  
class VectorFunctions:
  public VectorMatrixFunctions {
public:

  typedef typename ValueType<VFT>::type T;

  //======================================================================
  
  static
  void 
  writeValues(VFT f, std::ostream& os) {
    os << "[";
    for (int i=0; i< size(f); i++) 
      os << f(i) << ", ";
    os << "]";
  }
  
  //======================================================================
  
  static
  std::string 
  valuesToString(VFT f) {
    
    std::ostringstream os;
    writeValues(f,os);
    return os.str();
  }
  
  //======================================================================
  //======================================================================
  // doubleVectorFunction(-1) = the length of the vector
  // doubleVectorFunction(-2) = the length of the underlying storage vector
  static
  int vectorInfo(int  i, int  len_, int cap) {
    if(i == -2) return cap;
    if(i == -1) return len_;	
    return -1;
  }
  
  //======================================================================
  
  static
  T* 
  makeData_(int cap) {
    if(cap <= 0) exit(EXIT_FAILURE);	
    int numBytes = sizeof(T) * cap;
    T* data = (T*)malloc(numBytes);					
    memset(data,0,numBytes);
    return data;						
  }
  
  //======================================================================
  
  static
  T* 
  maybe_resize_Data_(T* oldData, int cap, int* oldCap) {
    
    if(cap     <=0)   exit(EXIT_FAILURE);
    if(*oldCap <=0)   exit(EXIT_FAILURE);
    
    if(cap <= *oldCap) return oldData;
    
    // Going to resize
    int oldNumBytes = sizeof(T) * *oldCap;	
      
    T* newData = makeData_(cap);
    *oldCap = cap;
    
    memcpy(newData,oldData,oldNumBytes);	
    
    if(oldData != NULL)							
      free(oldData);							
    
    return newData;
  }
    
  //======================================================================
  
  static
  void 
  writeDataArray(VFT f,
		 std::ostream& os) {
    int count  = 0;
    int length = size(f);
    int cap    = capacity(f);
    
    if (length >= 20)
      os << "\n               ";
    for(int i=0; i<length; i++) {
      os << tweekedValue_( f(i) )     << delimiterOrNot(i,cap,", ");
      if ((count != 0) && ((count++)%10 == 0) )
	os << "\n               ";
    }
    if (length >= 20)
      os << "\n";
  }
  
  //======================================================================
    
  static
  void writeVecStaticDataStatement(VFT f,
				   std::ostream& os) {
    
    int precision_ = std::numeric_limits<T>::digits10+1;
    os.precision(precision_);
    
    //    int count = 0;
    os << "            static const double data[]  = {";
    writeDataArray(f,os);
    os << "};\n";
  }
  
  //======================================================================
  
  static
  void writeCodeFor_(VFT f, 
		     const char* fName, std::ostream& file) {
      
    file << "        " << typeid(T).name() << " " << fName << "(int i) {\n";
    file << "           int len_ = " << size(f) << ";\n"; 
    file << "           int cap  = " << capacity(f) << ";\n"; 
      
    writeVecStaticDataStatement(f,file);
      
    file << "           int vInfo = vectorInfo(i,len_,cap);\n";
    file << "           if(vInfo > 0) return vInfo;\n";
    file << "           if(i >= len_) i = len_-1;\n";
    file << "           return data[i];\n";
    file << "        }\n\n";
  }
    
  //======================================================================
  
  static
  void 
  writeCodeFor(VFT f, const char* fName, std::ostream& os) {
      
    os << "    MakeVectorFunction" 
       << "(" << fName << ","<< typeid(T).name() 
       <<"," << size(f) << "," << capacity(f) << ",";
    writeDataArray(f,os);
    os << ");\n";
  }
    
  //======================================================================
};

void  writeCodeFor(intVectorFunction f, const char* fName, std::ostream& os);
void  writeCodeFor(doubleVectorFunction f, const char* fName, std::ostream& os);
void  writeCodeFor(charStarVectorFunction f, const char* fName, std::ostream& os);

/*@}*/
#endif

