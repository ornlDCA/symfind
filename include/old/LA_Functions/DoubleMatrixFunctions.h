//-*-C++-*-

/** \ingroup DCA */
/*@{*/

/*! \file DoubleMatrixFunctions.h  
 *  \brief 
 *
 */

#ifndef Header_DoubleMatrixFunctions_H
#define Header_DoubleMatrixFunctions_H

//======================================================================

typedef double (*doubleMatrixFunction)(int r, int c);

//======================================================================

int capacity(doubleMatrixFunction mf);
int nRow    (doubleMatrixFunction mf);
int lDim    (doubleMatrixFunction mf);
int nCol    (doubleMatrixFunction mf);
int size    (doubleMatrixFunction mf);

//======================================================================

//template<> class IsVectorLike< doubleMatrixFunction > {public: enum {value = 1}; };
template<> class IsMatrixLike< doubleMatrixFunction > {public: enum {value = 1}; };

// // The following enable doubleMatrixFunction to be IsMatrixLike
template<> class HasValueType< doubleMatrixFunction > {public: static const bool value = 1; }; 
template<> class ValueType   < doubleMatrixFunction > {public: typedef double type;         };

//======================================================================

// template<> class VectorLikeAccess<doubleMatrixFunction> {
// public:
//   //  static double&       elementAt(      doubleVectorFunction& v, int i) { return v(i); }
//   static const double& elementAt(const doubleMatrixFunction& m, int i) { 
//     int c = i / lDim(m);
//     int r = i % lDim(m);
//     return m(r,c); 
//   }
// };

//======================================================================

/*@}*/
#endif

