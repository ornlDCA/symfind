//-*-C++-*-   
/** \ingroup LinearAlgebra */
/*@{*/

/*! \file LinearAlgebraTemplatePrototypes.h  
 *
 */

#ifndef LinearAlgebraTemplatePrototypes_H
#define LinearAlgebraTemplatePrototypes_H

//======================================================================
// This is the start of a tagging approach whereby we mark the classes
// that we consider to be VectorLike or MatrixLike and if their data
// are laid out in a BLAS way.

template<typename T>  class IsVectorLike { public: enum {value = 0}; };
template<typename T>  class IsMatrixLike { public: enum {value = 0}; };

template<typename MT> class IsDiagonalMatrix { public: enum {value = 0}; };

template<typename VT> struct            VectorLikeAccess;
template<typename MT, typename V> class AreCompatableBlasMatrixAndType;

template<typename T> class HasValueType {public: static const bool value = 0; }; 
template<typename T> class ValueType    {public: typedef   void type;         };

//----------------------------------------------------------------------

typedef enum { NOT_BLAS, 
	       BLAS_VT,  //dense linear vector data
	       BLAS_BD,  //bidiagonal
	       BLAS_DI,  //diagonal
	       BLAS_GB,  //general band
	       BLAS_GE,  //general (i.e., unsymmetric, in some cases rectangular)
	       BLAS_GG,  //general matrices, generalized problem (i.e., a pair of general matrices)
	       BLAS_GT,  //general tridiagonal
	       BLAS_HB,  //(complex) Hermitian band
	       BLAS_HE,  //(complex) Hermitian
	       BLAS_HG,  //upper Hessenberg matrix, generalized problem (i.e a Hessenberg and a
	                 //triangular matrix)
	       BLAS_HP,  //(complex) Hermitian, packed storage
	       BLAS_HS,  //upper Hessenberg
	       BLAS_OP,  //(real) orthogonal, packed storage
	       BLAS_OR,  //(real) orthogonal
	       BLAS_PB,  //symmetric or Hermitian positive definite band
	       BLAS_PO,  //symmetric or Hermitian positive definite
	       BLAS_PP,  //symmetric or Hermitian positive definite, packed storage
	       BLAS_PT,  //symmetric or Hermitian positive definite tridiagonal
	       BLAS_SB,  //(real) symmetric band
	       BLAS_SP,  //symmetric, packed storage
	       BLAS_ST,  //(real) symmetric tridiagonal
	       BLAS_SY,  //symmetric
	       BLAS_TB,  //triangular band
	       BLAS_TG,  //triangular matrices, generalized problem (i.e., a pair of triangular matrices)
	       BLAS_TP,  //triangular, packed storage
	       BLAS_TR,  //triangular (or in some cases quasi-triangular)
	       BLAS_TZ,  //trapezoidal
	       BLAS_UN,  //(complex) unitary
	       BLAS_UP   //(complex) unitary, packed storage
}   BLAS_Type;

template<typename BT> class BlasType     { public: enum {value = NOT_BLAS}; };

//====================================================================== 

/*@}*/
#endif
