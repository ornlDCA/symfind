//-*-C++-*-  
/** \ingroup DCA */
/*@{*/

/*! \file ThrowFunctions.h  
 *
 */

#ifndef _ThrowFunctions_H
#define _ThrowFunctions_H

//======================================================================

#ifdef CPU_CODE    

HostDevice
void assembleErrorString(std::ostringstream& msg) {}

//======================================================================

template<typename T, typename... Params>
HostDevice
void assembleErrorString(std::ostringstream& msg, const T& v, Params... parameters) {
  msg << v;
  assembleErrorString(msg,parameters...);
}

#endif

//======================================================================

template<typename T,typename... Params>
HostDevice
void throwRangeError( T& v, Params... parameters) {
#ifdef CPU_CODE    
  std::ostringstream msg;
  assembleErrorString(msg,v,parameters...);
  throw std::range_error(msg.str());
#endif
}

//======================================================================

template<typename T, typename... Params>
HostDevice
void throwLogicError( T& v, Params... parameters) {
#ifdef CPU_CODE    
  std::ostringstream msg;
  assembleErrorString(msg,v,parameters...);
  throw std::logic_error(msg.str());
#endif
}

//======================================================================

/*@}*/
#endif
