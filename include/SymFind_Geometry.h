//-*-C++-*-       

#ifndef SymFind_Geometry_HEADER_H
#define SymFind_Geometry_HEADER_H

namespace SymFind {
  
  template<typename T, int DIM>
  class SymFind_Geometry 
  {};

  //======================================================================

  template<>
  class SymFind_Geometry<double,2> {

    template<typename T, int DIM>
    friend  
    void writeXML(const SymFind_Geometry<T,DIM>& geo, const char* pathName);
    
  private:

    double latticeA_length; 
    double latticeB_length; 
    double latticeAngle;
    int    superlatticeA_x;
    int    superlatticeA_y;
    int    superlatticeB_x;
    int    superlatticeB_y;

    SymFind_CGeometry       geometry;

    //    SymFind_C_Mesh_Geometry meshGeometry;

  public:

    typedef double T;

    //----------------------------------------------------------------------
    
    SymFind_Geometry(T      latticeA_length_, 
		     T      latticeB_length_, 
		     T      latticeAngle_,
		     int    superlatticeA_x_,
		     int    superlatticeA_y_,
		     int    superlatticeB_x_,
		     int    superlatticeB_y_):
      latticeA_length(latticeA_length_), 
      latticeB_length(latticeB_length_), 
      latticeAngle   (latticeAngle_   ),
      superlatticeA_x(superlatticeA_x_),
      superlatticeA_y(superlatticeA_y_),
      superlatticeB_x(superlatticeB_x_),
      superlatticeB_y(superlatticeB_y_)
    {
      
      SymFind_make_CGeometry2D_double(&geometry,
				      2,
				      latticeA_length, 
				      latticeB_length, 
				      latticeAngle,
				      superlatticeA_x,
				      superlatticeA_y,
				      superlatticeB_x,
				      superlatticeB_y);
    }

    //----------------------------------------------------------------------
    
    ~SymFind_Geometry() {
      
      SymFind_delete_CGeometry2D_double(&geometry);
      // if(meshGeometry.ptr != 0)
      // 	SymFind_delete_C_MeshGeometry2D_double(&meshGeometry);
    }

    //----------------------------------------------------------------------

    void fetch(SymFind_Crystal<T,2>& rCrystal,
	       SymFind_Crystal<T,2>& kCrystal) {
      SymFind_fetch_cgeometry_double(geometry,
				     rCrystal.ccrystalPtr,
				     kCrystal.ccrystalPtr);
    }

    //----------------------------------------------------------------------

    void fetchWignerSeitzMesh( int numPoints,
			       Matrix<T>& mesh) {
      
      SymFind_fetchWignerSeitzMesh_double(geometry,
					  numPoints,
					  mesh.cmatrixPtr );
    }

    // //----------------------------------------------------------------------
    // //
    // // Lazy evaluation of meshGeometry

    // void fetchMesh(Matrix<T>& meshMat, int numPoints) {

    //   if(meshGeometry.ptr == 0)
    // 	SymFind_make_C_MeshGeometry2D_double(&meshGeometry,
    // 					     latticeA_length, 
    // 					     latticeB_length, 
    // 					     latticeAngle,
    // 					     superlatticeA_x,
    // 					     superlatticeA_y,
    // 					     superlatticeB_x,
    // 					     superlatticeB_y,
    // 					     numPoints);
      
    //   fetch_meshMat_double(meshGeometry,meshMat);
    // }
    
    //----------------------------------------------------------------------
  };

  //======================================================================

  template<typename T, int DIM>
  void writeXML(const SymFind_Geometry<T,DIM>& geo, const char* pathName) {
    SymFind_writeXML(geo.geometry,pathName);
  }

  //======================================================================
}
  
#endif
		   
