//-*-C++-*-    

#ifndef SymFind_Chooser_Generic_HEADER_H
#define SymFind_Chooser_Generic_HEADER_H


namespace SymFind {
  
  template<typename T>  class Chooser {};

} // namespace SymFind

#include "SymFind_Chooser_double.h"
#include "SymFind_Chooser_float.h"
#include "SymFind_Chooser_int.h"

#endif
